<?php

namespace GlobalcarBundle\DataFixtures\Testing;

use AppBundle\Entity\Geos2\APIUser;
use AppBundle\Entity\Globalcar\Historicovehiculos;
use AppBundle\Entity\Globalcar\Titulares;
use AppBundle\Entity\Globalcar\Vehiculos;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadGb4TestAltaSancionV1 implements FixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        // Vehiculo asociado al titular JaviviCarting

        // Vehiculo de prueba 2233PRU
        $veh = new Vehiculos();
        $veh->setMatricula('2233PRU');
        $veh->setColor('Negro');
        $veh->setFechamatriculacion(new \Datetime('1990-12-30'));
        $veh->setMarca('Seat');
        $veh->setModelo('Cordoba');
        $manager->persist($veh);

        $tit = $manager->getRepository('AppBundle:Titulares')->findOneBy(array(
            'nombre' => 'JaviviCarting'
        ));

        //Historico de vehiculo
        $hv = new Historicovehiculos();
        $hv->setIdtitular($tit);
        $hv->setIdvehiculo($veh);
        $hv->setOrigen('TEST');
        $hv->setTipotitular('r');
        $hv->setTemporal('n');
        $hv->setActivo('s');
        $manager->persist($hv);

        $manager->flush();
    }
}
