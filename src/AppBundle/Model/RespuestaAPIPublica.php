<?php

namespace AppBundle\Model;

use Symfony\Component\Serializer\Annotation\Groups;

class RespuestaAPIPublica
{
    private $errores = array();
    private $valores = array();
    private $incidencias = array();

    private $entorno;
    private $codigoRespuesta;

    /**
     * RespuestaAPIPublica constructor.
     */
    public function __construct($entorno)
    {
        $this->entorno = $entorno;
    }

    /**
     * @return mixed
     * @Groups({"camposRespuesta"})
     */
    public function getErrores()
    {
        return $this->errores;
    }

    /**
     * @param mixed $errores
     */
    public function setErrores($errores): void
    {
        $this->errores = $errores;
    }

    /**
     * @param $error
     */
    public function addError($error): void
    {
        /*if ($this->entorno == 'prod')
            $this->errores = array ('Peticion incorrecta');
        else*/

        if (is_array($error)) {
            $this->errores = array_merge($this->errores, $error);
        } else {
            $this->errores[] = $error;
        }
    }

    /**
     * @return mixed
     * @Groups({"camposRespuesta"})
     */
    public function getValor()
    {
        return $this->valores;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valores): void
    {
        $this->valores = $valores;
    }

    /**
     * @param $valor
     */
    public function addValor($valor): void
    {
        $this->valores[] = $valor;
    }

    /**
     * @return mixed
     * @Groups({"camposRespuesta"})
     */
    public function getIncidencias()
    {
        return $this->incidencias;
    }

    /**
     * @param mixed $incidencias
     */
    public function setIncidencias($incidencias): void
    {
        $this->incidencias = $incidencias;
    }

    /**
     * @param $incidencia
     */
    public function addIncidencia($incidencia): void
    {
        $this->incidencias[] = $incidencia;
    }

    /**
     * @return mixed
     */
    public function getCodigoRespuesta()
    {
        return $this->codigoRespuesta;
    }

    /**
     * @param mixed $codigoRespuesta
     */
    public function setCodigoRespuesta($codigoRespuesta): void
    {
        $this->codigoRespuesta = $codigoRespuesta;
    }
}
