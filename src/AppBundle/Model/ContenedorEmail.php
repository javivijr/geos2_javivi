<?php

namespace AppBundle\Model;


use AppBundle\Service\EnvioEmails;

class ContenedorEmail
{
//    const AMAZON_SES = 1;
//    const GMAIL_OAUTH2 = 3;
//    const MANDRILL = 4;

    protected $asunto = '';
    protected $bodyhtml = '';
    protected $bodytxt = '';
    protected $from = '';
    protected $to = array();
    protected $cc = array();
    protected $bcc = array();
    protected $metodoenvio = EnvioEmails::MANDRILL;
    protected $adjuntos = array();
    protected $adjuntosData = array();
    protected $fechallegada = null;
    protected $respuestato = '';
    protected $replyto = '';
    protected $customHeaders = array();

    /**********************************************************************
     * Añadimos la propiedad incrustados(array) para
     * almacenar las rutas de los archivos que queramos
     * incrustar más adelante en el método enviarEmail()
     * de la clase utilidadesClass.php
     * @fecha:23-03-16  @autor:Raul
     */
    protected $incrustados = array();

    public function getAsunto(){
        return $this->asunto;
    }

    public function setAsunto($asunto){
        $this->asunto = $asunto;
    }

    public function getBodyhtml(){
        return $this->bodyhtml;
    }

    public function setBodyhtml($bodyhtml){
        $this->bodyhtml = $bodyhtml;
    }

    public function getBodytxt(){
        return $this->bodytxt;
    }

    public function setBodytxt($bodytxt){
        $this->bodytxt = $bodytxt;
    }

    public function getFrom(){
        return $this->from;
    }

    public function setFrom($from){
        $this->from = $from;
    }

    public function getTo(){
        return $this->to;
    }

    public function addTo($mail,$nombre=''){
        if($nombre!='')
            $this->to[$mail] = $nombre;
        else
            $this->to[] = $mail;
    }

    public function getCc(){
        return $this->cc;
    }

    public function addCc($mail,$nombre=''){
        if($nombre!='')
            $this->cc[$mail] = $nombre;
        else
            $this->cc[] = $mail;
    }

    public function getBcc(){
        return $this->bcc;
    }

    public function addBcc($mail,$nombre=''){
        if($nombre!='')
            $this->bcc[$mail] = $nombre;
        else
            $this->bcc[] = $mail;
    }

    public function getMetodoenvio(){
        return $this->metodoenvio;
    }

    public function setMetodoenvio($metodoenvio){
        $this->metodoenvio = $metodoenvio;
    }

    public function getAdjuntos(){
        return $this->adjuntos;
    }

    public function addAdjuntosRuta($ruta,$nombre=''){
        if($nombre!='')
            $this->adjuntos[$nombre] = $ruta;
        else
            $this->adjuntos[] = $ruta;
    }

    public function getAdjuntosDatos() {
        return $this->adjuntosData;
    }

    public function addAdjuntosDatos($contenido, $nombre='') {
        if ($nombre=='')
            throw new \InvalidArgumentException('Nombre de fichero adjunto vacio.');
        $this->adjuntosData[$nombre] = $contenido;
    }

    public function getFechallegada(){
        return $this->fechallegada;
    }

    public function setFechallegada(\Datetime $fechallegada){
        $this->fechallegada = $fechallegada;
    }

    public function getRespuestaTo(){
        return $this->respuestato;
    }

    public function setRespuestaTo($respuestato){
        $this->respuestato = $respuestato;

    }

    public function getReplyTo(){
        return $this->replyto;
    }

    public function setReplyTo($replyto){
        $this->replyto = $replyto;

    }

    public function addCustomHeader($name, $value)
    {
        $this->customHeaders[$name] = $value;
    }

    public function getCustomHeader($name)
    {
        return (array_key_exists($name, $this->customHeaders)) ? $this->customHeaders[$name] : null;
    }

    public function getAllCustomHeaders()
    {
        return $this->customHeaders;
    }

    /**********************************************************************
     * Añadimos el setter/getter para la propiedad incrustados
     * @fecha:23-03-16  @autor:Raul
     */
    public function getIncrustados(){
        return $this->incrustados;
    }

    public function addIncrustados($ruta,$nombre=''){
        if($nombre!='')
            $this->incrustados[$nombre] = $ruta;
        else
            $this->incrustados[] = $ruta;
    }
}