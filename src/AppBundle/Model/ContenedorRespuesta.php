<?php

namespace AppBundle\Model;


class ContenedorRespuesta
{
    public $estado=true;
    public $errores = array();
    public $ok = array();
    public $mensajes = array();
    public $datos;
    public $scripts = array();
    public $values=array();
    public $avisos=array();

    const MENSAJE_GENERICO = 'standar';
    const MENSAJE_CONFIRMACION = 'ok';
    const MENSAJE_ERROR = 'error';
    const MENSAJE_ALERTA = 'alerta';

    const ALINEACION_IZQUIERDA = 'izquierda';
    const ALINEACION_DERECHA = 'derecha';
    const ALINEACION_CENTRO = 'centro';
    const ALINEACION_JUSTIFICADO = 'justificado';

    /**
     * getEstado.
     *
     * Vemos el estado de la respuesta
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @return bool  Devuelve true si la peticion se ejecuto correctamente o false si ocurrio algun error.
     */
    public function getEstado(){
        return $this->estado;
    }

    /**
     * setEstado.
     *
     * Marca el estado de la respuesta
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param bool $ok  Parametro que indica si la peticion fue correcta o no
     */
    public function setEstado($ok){
        $this->estado = ($ok===true);
    }

    /**
     * addErrores.
     *
     * Añade un error de validacion de un campo a la respuesta
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param string $campo  Indica el nombre del campo con error
     * @param string $texto  Indica el texto del error a mostrar
     */
    public function addErrores($campo, $texto){
        $this->setEstado(false);
        if(key_exists($campo, $this->errores))
            $this->errores[$campo] .= ", ".$texto;
        else
            $this->errores[$campo] = $texto;
        $this->setEstado(false);
    }

    /**
     * addOk.
     *
     * Pone en verde (quitando errores) un campo a la respuesta
     *
     * @author  Pablo (fusilando código de Alejandro, of course, que milagros no hago...)
     *
     * @since 1.0
     *
     * @param string $campo  Indica el nombre del campo que queremos poner OK
     */
    public function addOk($campo){
        if (!isset($this->errores[$campo])) {
            $this->ok[$campo] = true;
        }
    }

    /**
     * addMensaje.
     *
     * Añade un mensaje a la respuesta del tipo deseado
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param string $texto  Indica el texto del mensaje
     * @param string $tipo   Indica el tipo del mensaje segun las constantes de la clase
     */
    public function addMensaje($texto, $tipo, $titulo='AVISO',$funcion=''){
        $this->mensajes[] = array('texto'=>$texto,'tipo'=>$tipo,'titulo'=>$titulo,'funcion'=>$funcion);
    }

    /**
     * setDatos.
     *
     * Marca los datos de la respuesta
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param array $datos  Indica los datos que queremos enviar en la respuesta
     */
    public function setDatos($datos){
        $this->datos = $datos;
    }

    /**
     * addScript.
     *
     * Marca los script javascript a ejecutar con la respuesta
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param string $script  Indica el codigo javascript a ejecutar con la respuesta
     */
    public function addScript($script){
        $this->scripts[] = $script;
    }

    /**
     * addValue.
     *
     * Cambia el valor de un campo de un formulario
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param string $script  Indica el codigo javascript a ejecutar con la respuesta
     * @param string $script  Indica el codigo javascript a ejecutar con la respuesta
     * @param string $script  Indica el codigo javascript a ejecutar con la respuesta
     */
    public function addValue($formulario,$campo,$valor){
        $this->values[] = array('formulario'=>$formulario,'campo'=>$campo,'valor'=>$valor);
    }

    /**
     * addAviso.
     *
     * Añade un aviso lateral a la respuesta del tipo deseado
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param string $texto         Indica el texto del mensaje
     * @param string $tipo          Indica el tipo del mensaje segun las constantes de la clase
     * @param string $titulo        Indica el titulo del aviso
     * @param string $retraso       Tiempo, en milisegundos, que permanecerá el aviso en pantalla
     * @param string $alineacion    Determina como se alineará el texto dentro del aviso
     */
    public function addAviso($texto, $tipo, $titulo='AVISO', $retraso='', $alineacion=''){
        $this->avisos[] = array('texto'=>$texto,'tipo'=>$tipo,'titulo'=>$titulo, 'retraso'=>$retraso, 'alineacion'=>$alineacion);
    }
}