<?php

namespace AppBundle\Model;

class FilesystemModel
{
    private $id;
    private $type;
    private $hasRutaPrefix;
    private $rutaPrefix;

    public function __construct(array $fsArray)
    {
        // Estos dos valores son obligatorios
        if (!array_key_exists('id', $fsArray) || !array_key_exists('type', $fsArray)) {
            throw new \InvalidArgumentException('Argumento invalido al instanciar FilesystemModel');
        }
        $this->setId($fsArray['id']);
        $this->setType($fsArray['type']);

        $this->setHasRutaPrefix(isset($fsArray['rutaPrefix']) ? $fsArray['rutaPrefix'] : false);
        if ($this->hasRutaPrefix) {
            $this->defineRuta();
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getHasRutaPrefix()
    {
        return $this->hasRutaPrefix;
    }

    /**
     * @param mixed $hasRutaPrefix
     */
    public function setHasRutaPrefix($hasRutaPrefix): void
    {
        $this->hasRutaPrefix = $hasRutaPrefix;
    }

    /**
     * @return mixed
     */
    public function getRutaPrefix()
    {
        return $this->rutaPrefix;
    }

    /**
     * @param mixed $rutaPrefix
     */
//    public function setRutaPrefix($rutaPrefix): void
//    {
//
//        $this->rutaPrefix = $rutaPrefix;
//    }


    private function defineRuta(): void
    {
        if ($this->id === 'amazonS3FS') {
            $this->rutaPrefix = date('Y/m/d/');
        }

        /*if ($this->aplicacion == self::GB4 || $this->aplicacion == self::TKL || $this->aplicacion == self::MVR || $this->aplicacion=='copiasficherosintercambio' || $this->aplicacion=='logemails') {

        }*/
    }
}
