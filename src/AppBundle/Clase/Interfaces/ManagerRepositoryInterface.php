<?php

namespace AppBundle\Clase\Interfaces;


use Symfony\Bridge\Doctrine\RegistryInterface;

interface ManagerRepositoryInterface
{
    public function __construct(RegistryInterface $doctrine);
    public function findOneBy(array $criteria);
    public function findBy(array $criteria);
    public function findAll();
}