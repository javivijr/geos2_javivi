<?php
/**
 * Created by PhpStorm.
 * User: javivi
 * Date: 16/04/18
 * Time: 16:37
 */

namespace AppBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends HttpException
{
    private $errores;

    public function __construct(ConstraintViolationListInterface $constraintViolationList)
    {
        $message = [];

        /**
         * @var ConstraintViolationInterface $violation
         */
        foreach ($constraintViolationList as $violation) {
            $message[$violation->getPropertyPath()] = $violation->getMessage();
            $this->errores[$violation->getPropertyPath()] = $violation->getMessage();
        }

        parent::__construct(400, json_encode($message));
    }


    /**
     * @return array
     */
    public function getErrores()
    {
        return $this->errores;
    }
}
