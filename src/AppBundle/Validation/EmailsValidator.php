<?php

namespace AppBundle\Validation;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Symfony\Component\Validator\ConstraintValidator;

use Symfony\Component\Validator\Exception\RuntimeException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class EmailsValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Emails) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Emails');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $aEmails = explode(',',$value);

        $emailConstraint = new Email();
        $emailConstraint->message = $constraint->message;
        $emailValidator = new EmailValidator();
        $emailValidator->context = $this->context;

        foreach ($aEmails as $email) {
            $emailValidator->validate($email,$emailConstraint);
        }

        return;

    }
}