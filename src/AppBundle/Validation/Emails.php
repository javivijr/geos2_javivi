<?php

namespace AppBundle\Validation;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Emails extends Constraint
{
    public $message = 'Email invalido.';
}