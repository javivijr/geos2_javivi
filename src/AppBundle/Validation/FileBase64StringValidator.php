<?php

namespace AppBundle\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\RuntimeException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class FileBase64StringValidator extends ConstraintValidator
{
    private $maxsize;

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof FileBase64String) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\FileBase64String');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        if ($constraint->maxsize === null) {
            throw new RuntimeException('Tamaño maximo de fichero no especificado');
        } else {
            $this->maxsize = $constraint->maxsize;
        }

        $value = (string) $value;

        if (strlen($value) > $this->maxsize) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->addViolation();

            return;
        }
    }
}
