<?php

namespace AppBundle\Validation;

use Symfony\Component\Validator\Constraint;

/**
 * Class FileBase64String
 * @package AppBundle\Validation
 * @Annotation
 */
class FileBase64String extends Constraint
{
    public $message = 'Fichero no valido';
    public $maxsize;
}
