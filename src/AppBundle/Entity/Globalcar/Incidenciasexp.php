<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidenciasexp
 *
 * @ORM\Table(name="incidenciasexp", indexes={@ORM\Index(name="idexpedientes_IE_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idescriptores_IE_FK_idx", columns={"iddescriptores"})})
 * @ORM\Entity
 */
class Incidenciasexp
{
    /**
     * @var int
     *
     * @ORM\Column(name="idincidenciasExp", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idincidenciasexp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=0, nullable=false)
     */
    private $estado;

    /**
     * @var \Descriptores
     *
     * @ORM\ManyToOne(targetEntity="Descriptores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddescriptores", referencedColumnName="iddescriptores")
     * })
     */
    private $iddescriptores;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idincidenciasexp.
     *
     * @return int
     */
    public function getIdincidenciasexp()
    {
        return $this->idincidenciasexp;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Incidenciasexp
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Incidenciasexp
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado.
     *
     * @param string $estado
     *
     * @return Incidenciasexp
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado.
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set iddescriptores.
     *
     * @param \AppBundle\Entity\Globalcar\Descriptores|null $iddescriptores
     *
     * @return Incidenciasexp
     */
    public function setIddescriptores(\AppBundle\Entity\Globalcar\Descriptores $iddescriptores = null)
    {
        $this->iddescriptores = $iddescriptores;

        return $this;
    }

    /**
     * Get iddescriptores.
     *
     * @return \AppBundle\Entity\Globalcar\Descriptores|null
     */
    public function getIddescriptores()
    {
        return $this->iddescriptores;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Incidenciasexp
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
