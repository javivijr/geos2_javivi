<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bajasemail
 *
 * @ORM\Table(name="bajasemail")
 * @ORM\Entity
 */
class Bajasemail
{
    /**
     * @var int
     *
     * @ORM\Column(name="idbajasemail", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idbajasemail;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo", type="string", length=400, nullable=false)
     */
    private $motivo;



    /**
     * Get idbajasemail.
     *
     * @return int
     */
    public function getIdbajasemail()
    {
        return $this->idbajasemail;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Bajasemail
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set motivo.
     *
     * @param string $motivo
     *
     * @return Bajasemail
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo.
     *
     * @return string
     */
    public function getMotivo()
    {
        return $this->motivo;
    }
}
