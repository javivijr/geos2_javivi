<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pagos
 *
 * @ORM\Table(name="pagos", indexes={@ORM\Index(name="idexpedientes_PG_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idorganismospago_FKKKK", columns={"idorganismospago"})})
 * @ORM\Entity
 */
class Pagos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpagos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpagos;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaProntoPago", type="datetime", nullable=true)
     */
    private $fechaprontopago;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaPago", type="datetime", nullable=true)
     */
    private $fechapago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referenciaPago", type="string", length=50, nullable=true)
     */
    private $referenciapago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="importePagar", type="decimal", precision=6, scale=2, nullable=true)
     */
    private $importepagar;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaPagoOrganismo", type="datetime", nullable=true)
     */
    private $fechapagoorganismo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="descuento", type="integer", nullable=true)
     */
    private $descuento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="factura", type="string", length=25, nullable=true)
     */
    private $factura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="facturado", type="decimal", precision=6, scale=2, nullable=true)
     */
    private $facturado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="asumido", type="decimal", precision=6, scale=2, nullable=true)
     */
    private $asumido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motivo", type="string", length=200, nullable=true)
     */
    private $motivo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referenciaCliente", type="string", length=200, nullable=true)
     */
    private $referenciacliente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=5000, nullable=true)
     */
    private $observaciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="medioPago", type="string", length=1, nullable=true)
     */
    private $mediopago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="metodoPago", type="string", length=500, nullable=true)
     */
    private $metodopago;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idbancos", type="integer", nullable=true)
     */
    private $idbancos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pagador", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $pagador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="antiguonuevo", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $antiguonuevo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emisora", type="string", length=20, nullable=true)
     */
    private $emisora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referencia", type="string", length=20, nullable=true)
     */
    private $referencia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificacion", type="string", length=20, nullable=true)
     */
    private $identificacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="control", type="string", length=20, nullable=true)
     */
    private $control;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campoAdicional", type="string", length=20, nullable=true)
     */
    private $campoadicional;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Organismospago
     *
     * @ORM\ManyToOne(targetEntity="Organismospago")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idorganismospago", referencedColumnName="idorganismospago")
     * })
     */
    private $idorganismospago;



    /**
     * Get idpagos.
     *
     * @return int
     */
    public function getIdpagos()
    {
        return $this->idpagos;
    }

    /**
     * Set fechaprontopago.
     *
     * @param \DateTime|null $fechaprontopago
     *
     * @return Pagos
     */
    public function setFechaprontopago($fechaprontopago = null)
    {
        $this->fechaprontopago = $fechaprontopago;

        return $this;
    }

    /**
     * Get fechaprontopago.
     *
     * @return \DateTime|null
     */
    public function getFechaprontopago()
    {
        return $this->fechaprontopago;
    }

    /**
     * Set fechapago.
     *
     * @param \DateTime|null $fechapago
     *
     * @return Pagos
     */
    public function setFechapago($fechapago = null)
    {
        $this->fechapago = $fechapago;

        return $this;
    }

    /**
     * Get fechapago.
     *
     * @return \DateTime|null
     */
    public function getFechapago()
    {
        return $this->fechapago;
    }

    /**
     * Set referenciapago.
     *
     * @param string|null $referenciapago
     *
     * @return Pagos
     */
    public function setReferenciapago($referenciapago = null)
    {
        $this->referenciapago = $referenciapago;

        return $this;
    }

    /**
     * Get referenciapago.
     *
     * @return string|null
     */
    public function getReferenciapago()
    {
        return $this->referenciapago;
    }

    /**
     * Set importepagar.
     *
     * @param string|null $importepagar
     *
     * @return Pagos
     */
    public function setImportepagar($importepagar = null)
    {
        $this->importepagar = $importepagar;

        return $this;
    }

    /**
     * Get importepagar.
     *
     * @return string|null
     */
    public function getImportepagar()
    {
        return $this->importepagar;
    }

    /**
     * Set fechapagoorganismo.
     *
     * @param \DateTime|null $fechapagoorganismo
     *
     * @return Pagos
     */
    public function setFechapagoorganismo($fechapagoorganismo = null)
    {
        $this->fechapagoorganismo = $fechapagoorganismo;

        return $this;
    }

    /**
     * Get fechapagoorganismo.
     *
     * @return \DateTime|null
     */
    public function getFechapagoorganismo()
    {
        return $this->fechapagoorganismo;
    }

    /**
     * Set descuento.
     *
     * @param int|null $descuento
     *
     * @return Pagos
     */
    public function setDescuento($descuento = null)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento.
     *
     * @return int|null
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set factura.
     *
     * @param string|null $factura
     *
     * @return Pagos
     */
    public function setFactura($factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura.
     *
     * @return string|null
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set facturado.
     *
     * @param string|null $facturado
     *
     * @return Pagos
     */
    public function setFacturado($facturado = null)
    {
        $this->facturado = $facturado;

        return $this;
    }

    /**
     * Get facturado.
     *
     * @return string|null
     */
    public function getFacturado()
    {
        return $this->facturado;
    }

    /**
     * Set asumido.
     *
     * @param string|null $asumido
     *
     * @return Pagos
     */
    public function setAsumido($asumido = null)
    {
        $this->asumido = $asumido;

        return $this;
    }

    /**
     * Get asumido.
     *
     * @return string|null
     */
    public function getAsumido()
    {
        return $this->asumido;
    }

    /**
     * Set motivo.
     *
     * @param string|null $motivo
     *
     * @return Pagos
     */
    public function setMotivo($motivo = null)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo.
     *
     * @return string|null
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Set referenciacliente.
     *
     * @param string|null $referenciacliente
     *
     * @return Pagos
     */
    public function setReferenciacliente($referenciacliente = null)
    {
        $this->referenciacliente = $referenciacliente;

        return $this;
    }

    /**
     * Get referenciacliente.
     *
     * @return string|null
     */
    public function getReferenciacliente()
    {
        return $this->referenciacliente;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Pagos
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set mediopago.
     *
     * @param string|null $mediopago
     *
     * @return Pagos
     */
    public function setMediopago($mediopago = null)
    {
        $this->mediopago = $mediopago;

        return $this;
    }

    /**
     * Get mediopago.
     *
     * @return string|null
     */
    public function getMediopago()
    {
        return $this->mediopago;
    }

    /**
     * Set metodopago.
     *
     * @param string|null $metodopago
     *
     * @return Pagos
     */
    public function setMetodopago($metodopago = null)
    {
        $this->metodopago = $metodopago;

        return $this;
    }

    /**
     * Get metodopago.
     *
     * @return string|null
     */
    public function getMetodopago()
    {
        return $this->metodopago;
    }

    /**
     * Set idbancos.
     *
     * @param int|null $idbancos
     *
     * @return Pagos
     */
    public function setIdbancos($idbancos = null)
    {
        $this->idbancos = $idbancos;

        return $this;
    }

    /**
     * Get idbancos.
     *
     * @return int|null
     */
    public function getIdbancos()
    {
        return $this->idbancos;
    }

    /**
     * Set pagador.
     *
     * @param string|null $pagador
     *
     * @return Pagos
     */
    public function setPagador($pagador = null)
    {
        $this->pagador = $pagador;

        return $this;
    }

    /**
     * Get pagador.
     *
     * @return string|null
     */
    public function getPagador()
    {
        return $this->pagador;
    }

    /**
     * Set antiguonuevo.
     *
     * @param string|null $antiguonuevo
     *
     * @return Pagos
     */
    public function setAntiguonuevo($antiguonuevo = null)
    {
        $this->antiguonuevo = $antiguonuevo;

        return $this;
    }

    /**
     * Get antiguonuevo.
     *
     * @return string|null
     */
    public function getAntiguonuevo()
    {
        return $this->antiguonuevo;
    }

    /**
     * Set emisora.
     *
     * @param string|null $emisora
     *
     * @return Pagos
     */
    public function setEmisora($emisora = null)
    {
        $this->emisora = $emisora;

        return $this;
    }

    /**
     * Get emisora.
     *
     * @return string|null
     */
    public function getEmisora()
    {
        return $this->emisora;
    }

    /**
     * Set referencia.
     *
     * @param string|null $referencia
     *
     * @return Pagos
     */
    public function setReferencia($referencia = null)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia.
     *
     * @return string|null
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set identificacion.
     *
     * @param string|null $identificacion
     *
     * @return Pagos
     */
    public function setIdentificacion($identificacion = null)
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    /**
     * Get identificacion.
     *
     * @return string|null
     */
    public function getIdentificacion()
    {
        return $this->identificacion;
    }

    /**
     * Set control.
     *
     * @param string|null $control
     *
     * @return Pagos
     */
    public function setControl($control = null)
    {
        $this->control = $control;

        return $this;
    }

    /**
     * Get control.
     *
     * @return string|null
     */
    public function getControl()
    {
        return $this->control;
    }

    /**
     * Set campoadicional.
     *
     * @param string|null $campoadicional
     *
     * @return Pagos
     */
    public function setCampoadicional($campoadicional = null)
    {
        $this->campoadicional = $campoadicional;

        return $this;
    }

    /**
     * Get campoadicional.
     *
     * @return string|null
     */
    public function getCampoadicional()
    {
        return $this->campoadicional;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Pagos
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idorganismospago.
     *
     * @param \AppBundle\Entity\Globalcar\Organismospago|null $idorganismospago
     *
     * @return Pagos
     */
    public function setIdorganismospago(\AppBundle\Entity\Globalcar\Organismospago $idorganismospago = null)
    {
        $this->idorganismospago = $idorganismospago;

        return $this;
    }

    /**
     * Get idorganismospago.
     *
     * @return \AppBundle\Entity\Globalcar\Organismospago|null
     */
    public function getIdorganismospago()
    {
        return $this->idorganismospago;
    }
}
