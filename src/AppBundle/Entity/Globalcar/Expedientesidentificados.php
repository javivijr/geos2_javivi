<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientesidentificados
 *
 * @ORM\Table(name="expedientesidentificados", uniqueConstraints={@ORM\UniqueConstraint(name="idunico_EI_I", columns={"idexpedientes", "idhistoricopersonas"})}, indexes={@ORM\Index(name="idhistoricopersonas_EI_FK_idx", columns={"idhistoricopersonas"}), @ORM\Index(name="expeidentfechaenvi", columns={"fechaenvio"}), @ORM\Index(name="IDX_4940576B59A462DE", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Expedientesidentificados
{
    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientesidentificados", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idexpedientesidentificados;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaenvio", type="datetime", nullable=true)
     */
    private $fechaenvio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createtime", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createtime = 'CURRENT_TIMESTAMP';

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Historicopersonas
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricopersonas", referencedColumnName="idhistoricoPersonas")
     * })
     */
    private $idhistoricopersonas;



    /**
     * Get idexpedientesidentificados.
     *
     * @return int
     */
    public function getIdexpedientesidentificados()
    {
        return $this->idexpedientesidentificados;
    }

    /**
     * Set fechaenvio.
     *
     * @param \DateTime|null $fechaenvio
     *
     * @return Expedientesidentificados
     */
    public function setFechaenvio($fechaenvio = null)
    {
        $this->fechaenvio = $fechaenvio;

        return $this;
    }

    /**
     * Get fechaenvio.
     *
     * @return \DateTime|null
     */
    public function getFechaenvio()
    {
        return $this->fechaenvio;
    }

    /**
     * Set createtime.
     *
     * @param \DateTime|null $createtime
     *
     * @return Expedientesidentificados
     */
    public function setCreatetime($createtime = null)
    {
        $this->createtime = $createtime;

        return $this;
    }

    /**
     * Get createtime.
     *
     * @return \DateTime|null
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Expedientesidentificados
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idhistoricopersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonas|null $idhistoricopersonas
     *
     * @return Expedientesidentificados
     */
    public function setIdhistoricopersonas(\AppBundle\Entity\Globalcar\Historicopersonas $idhistoricopersonas = null)
    {
        $this->idhistoricopersonas = $idhistoricopersonas;

        return $this;
    }

    /**
     * Get idhistoricopersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonas|null
     */
    public function getIdhistoricopersonas()
    {
        return $this->idhistoricopersonas;
    }
}
