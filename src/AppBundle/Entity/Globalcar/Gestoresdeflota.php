<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gestoresdeflota
 *
 * @ORM\Table(name="gestoresdeflota", indexes={@ORM\Index(name="idusuariosMtm_GF_FK_idx", columns={"idusuariosMtm"}), @ORM\Index(name="idusuarios_GF_FK_idx", columns={"idusuarios"}), @ORM\Index(name="idusuariosSuplente_GF_FK_idx", columns={"idusuariosSuplente"}), @ORM\Index(name="idusuariosLegal", columns={"idusuariosLegal"}), @ORM\Index(name="idusuariosSuplenteLegal", columns={"idusuariosSuplenteLegal"})})
 * @ORM\Entity
 */
class Gestoresdeflota
{
    /**
     * @var int
     *
     * @ORM\Column(name="idgestoresDeFlota", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgestoresdeflota;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rutaMailPresentacion", type="string", length=200, nullable=true)
     */
    private $rutamailpresentacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaAsignacion", type="datetime", nullable=true)
     */
    private $fechaasignacion;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosLegal", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarioslegal;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosSuplenteLegal", referencedColumnName="idusuarios")
     * })
     */
    private $idusuariossuplentelegal;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosMtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosSuplente", referencedColumnName="idusuarios")
     * })
     */
    private $idusuariossuplente;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarios", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarios;



    /**
     * Get idgestoresdeflota.
     *
     * @return int
     */
    public function getIdgestoresdeflota()
    {
        return $this->idgestoresdeflota;
    }

    /**
     * Set rutamailpresentacion.
     *
     * @param string|null $rutamailpresentacion
     *
     * @return Gestoresdeflota
     */
    public function setRutamailpresentacion($rutamailpresentacion = null)
    {
        $this->rutamailpresentacion = $rutamailpresentacion;

        return $this;
    }

    /**
     * Get rutamailpresentacion.
     *
     * @return string|null
     */
    public function getRutamailpresentacion()
    {
        return $this->rutamailpresentacion;
    }

    /**
     * Set fechaasignacion.
     *
     * @param \DateTime|null $fechaasignacion
     *
     * @return Gestoresdeflota
     */
    public function setFechaasignacion($fechaasignacion = null)
    {
        $this->fechaasignacion = $fechaasignacion;

        return $this;
    }

    /**
     * Get fechaasignacion.
     *
     * @return \DateTime|null
     */
    public function getFechaasignacion()
    {
        return $this->fechaasignacion;
    }

    /**
     * Set idusuarioslegal.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarioslegal
     *
     * @return Gestoresdeflota
     */
    public function setIdusuarioslegal(\AppBundle\Entity\Globalcar\Usuarios $idusuarioslegal = null)
    {
        $this->idusuarioslegal = $idusuarioslegal;

        return $this;
    }

    /**
     * Get idusuarioslegal.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarioslegal()
    {
        return $this->idusuarioslegal;
    }

    /**
     * Set idusuariossuplentelegal.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuariossuplentelegal
     *
     * @return Gestoresdeflota
     */
    public function setIdusuariossuplentelegal(\AppBundle\Entity\Globalcar\Usuarios $idusuariossuplentelegal = null)
    {
        $this->idusuariossuplentelegal = $idusuariossuplentelegal;

        return $this;
    }

    /**
     * Get idusuariossuplentelegal.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuariossuplentelegal()
    {
        return $this->idusuariossuplentelegal;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Gestoresdeflota
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }

    /**
     * Set idusuariossuplente.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuariossuplente
     *
     * @return Gestoresdeflota
     */
    public function setIdusuariossuplente(\AppBundle\Entity\Globalcar\Usuarios $idusuariossuplente = null)
    {
        $this->idusuariossuplente = $idusuariossuplente;

        return $this;
    }

    /**
     * Get idusuariossuplente.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuariossuplente()
    {
        return $this->idusuariossuplente;
    }

    /**
     * Set idusuarios.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarios
     *
     * @return Gestoresdeflota
     */
    public function setIdusuarios(\AppBundle\Entity\Globalcar\Usuarios $idusuarios = null)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }
}
