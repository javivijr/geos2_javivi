<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tmunicipios
 *
 * @ORM\Table(name="tmunicipios", indexes={@ORM\Index(name="idtProvincias_TM_FK_idx", columns={"idtProvincias"})})
 * @ORM\Entity
 */
class Tmunicipios
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtMunicipios", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtmunicipios;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=5, nullable=true, options={"fixed"=true})
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="municipio", type="string", length=150, nullable=true)
     */
    private $municipio;

    /**
     * @var \Tprovincias
     *
     * @ORM\ManyToOne(targetEntity="Tprovincias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtProvincias", referencedColumnName="idtProvincias")
     * })
     */
    private $idtprovincias;



    /**
     * Get idtmunicipios.
     *
     * @return int
     */
    public function getIdtmunicipios()
    {
        return $this->idtmunicipios;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Tmunicipios
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set municipio.
     *
     * @param string|null $municipio
     *
     * @return Tmunicipios
     */
    public function setMunicipio($municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio.
     *
     * @return string|null
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set idtprovincias.
     *
     * @param \AppBundle\Entity\Globalcar\Tprovincias|null $idtprovincias
     *
     * @return Tmunicipios
     */
    public function setIdtprovincias(\AppBundle\Entity\Globalcar\Tprovincias $idtprovincias = null)
    {
        $this->idtprovincias = $idtprovincias;

        return $this;
    }

    /**
     * Get idtprovincias.
     *
     * @return \AppBundle\Entity\Globalcar\Tprovincias|null
     */
    public function getIdtprovincias()
    {
        return $this->idtprovincias;
    }
}
