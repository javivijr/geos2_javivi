<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Plantillas
 *
 * @ORM\Table(name="plantillas")
 * @ORM\Entity
 */
class Plantillas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idplantillas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idplantillas;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=70, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=false, options={"default"="pre"})
     */
    private $tipo = 'pre';

    /**
     * @var string|null
     *
     * @ORM\Column(name="texto", type="text", length=65535, nullable=true)
     */
    private $texto;



    /**
     * Get idplantillas.
     *
     * @return int
     */
    public function getIdplantillas()
    {
        return $this->idplantillas;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Plantillas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Plantillas
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set texto.
     *
     * @param string|null $texto
     *
     * @return Plantillas
     */
    public function setTexto($texto = null)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto.
     *
     * @return string|null
     */
    public function getTexto()
    {
        return $this->texto;
    }
}
