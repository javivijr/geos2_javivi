<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organosllegadas
 *
 * @ORM\Table(name="organosllegadas", indexes={@ORM\Index(name="idllegadas_OLL_FK_idx", columns={"idllegadas"}), @ORM\Index(name="idorganismos_OLLFK_idx", columns={"idorganismos"})})
 * @ORM\Entity
 */
class Organosllegadas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idorganosLlegadas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idorganosllegadas;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @var \Llegadas
     *
     * @ORM\ManyToOne(targetEntity="Llegadas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idllegadas", referencedColumnName="idllegadas")
     * })
     */
    private $idllegadas;

    /**
     * @var \Organismos
     *
     * @ORM\ManyToOne(targetEntity="Organismos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idorganismos", referencedColumnName="idorganismos")
     * })
     */
    private $idorganismos;



    /**
     * Get idorganosllegadas.
     *
     * @return int
     */
    public function getIdorganosllegadas()
    {
        return $this->idorganosllegadas;
    }

    /**
     * Set cantidad.
     *
     * @param int|null $cantidad
     *
     * @return Organosllegadas
     */
    public function setCantidad($cantidad = null)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad.
     *
     * @return int|null
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set idllegadas.
     *
     * @param \AppBundle\Entity\Globalcar\Llegadas|null $idllegadas
     *
     * @return Organosllegadas
     */
    public function setIdllegadas(\AppBundle\Entity\Globalcar\Llegadas $idllegadas = null)
    {
        $this->idllegadas = $idllegadas;

        return $this;
    }

    /**
     * Get idllegadas.
     *
     * @return \AppBundle\Entity\Globalcar\Llegadas|null
     */
    public function getIdllegadas()
    {
        return $this->idllegadas;
    }

    /**
     * Set idorganismos.
     *
     * @param \AppBundle\Entity\Globalcar\Organismos|null $idorganismos
     *
     * @return Organosllegadas
     */
    public function setIdorganismos(\AppBundle\Entity\Globalcar\Organismos $idorganismos = null)
    {
        $this->idorganismos = $idorganismos;

        return $this;
    }

    /**
     * Get idorganismos.
     *
     * @return \AppBundle\Entity\Globalcar\Organismos|null
     */
    public function getIdorganismos()
    {
        return $this->idorganismos;
    }
}
