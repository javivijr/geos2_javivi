<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tmunicipspalma
 *
 * @ORM\Table(name="tmunicipspalma", indexes={@ORM\Index(name="idProvincia_tmunicipsPalma_FK_idx", columns={"idProvincia"}), @ORM\Index(name="idMunicipio_tmunicipsPalma_idx", columns={"idMunicipio"})})
 * @ORM\Entity
 */
class Tmunicipspalma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dispos", type="integer", nullable=true)
     */
    private $dispos;

    /**
     * @var int
     *
     * @ORM\Column(name="idMunicipio", type="integer", nullable=false)
     */
    private $idmunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreMunicipio", type="string", length=100, nullable=false)
     */
    private $nombremunicipio;

    /**
     * @var \Tprovincias
     *
     * @ORM\ManyToOne(targetEntity="Tprovincias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idProvincia", referencedColumnName="idtProvincias")
     * })
     */
    private $idprovincia;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dispos.
     *
     * @param int|null $dispos
     *
     * @return Tmunicipspalma
     */
    public function setDispos($dispos = null)
    {
        $this->dispos = $dispos;

        return $this;
    }

    /**
     * Get dispos.
     *
     * @return int|null
     */
    public function getDispos()
    {
        return $this->dispos;
    }

    /**
     * Set idmunicipio.
     *
     * @param int $idmunicipio
     *
     * @return Tmunicipspalma
     */
    public function setIdmunicipio($idmunicipio)
    {
        $this->idmunicipio = $idmunicipio;

        return $this;
    }

    /**
     * Get idmunicipio.
     *
     * @return int
     */
    public function getIdmunicipio()
    {
        return $this->idmunicipio;
    }

    /**
     * Set nombremunicipio.
     *
     * @param string $nombremunicipio
     *
     * @return Tmunicipspalma
     */
    public function setNombremunicipio($nombremunicipio)
    {
        $this->nombremunicipio = $nombremunicipio;

        return $this;
    }

    /**
     * Get nombremunicipio.
     *
     * @return string
     */
    public function getNombremunicipio()
    {
        return $this->nombremunicipio;
    }

    /**
     * Set idprovincia.
     *
     * @param \AppBundle\Entity\Globalcar\Tprovincias|null $idprovincia
     *
     * @return Tmunicipspalma
     */
    public function setIdprovincia(\AppBundle\Entity\Globalcar\Tprovincias $idprovincia = null)
    {
        $this->idprovincia = $idprovincia;

        return $this;
    }

    /**
     * Get idprovincia.
     *
     * @return \AppBundle\Entity\Globalcar\Tprovincias|null
     */
    public function getIdprovincia()
    {
        return $this->idprovincia;
    }
}
