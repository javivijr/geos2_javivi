<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colaimpresion
 *
 * @ORM\Table(name="colaimpresion")
 * @ORM\Entity
 */
class Colaimpresion
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcolaimpresion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcolaimpresion;

    /**
     * @var string
     *
     * @ORM\Column(name="expedientes", type="text", length=16777215, nullable=false)
     */
    private $expedientes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechainicio", type="datetime", nullable=false)
     */
    private $fechainicio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechafin", type="datetime", nullable=true)
     */
    private $fechafin;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=30, nullable=false)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="resultado", type="string", length=10000, nullable=true)
     */
    private $resultado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="param", type="string", length=200, nullable=true)
     */
    private $param;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoexpedientes", type="string", length=15, nullable=false)
     */
    private $tipoexpedientes;

    /**
     * @var string
     *
     * @ORM\Column(name="tipomulti", type="string", length=5, nullable=false)
     */
    private $tipomulti;

    /**
     * @var string
     *
     * @ORM\Column(name="salida", type="string", length=5, nullable=false)
     */
    private $salida;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor", type="string", length=50, nullable=false)
     */
    private $proveedor;



    /**
     * Get idcolaimpresion.
     *
     * @return int
     */
    public function getIdcolaimpresion()
    {
        return $this->idcolaimpresion;
    }

    /**
     * Set expedientes.
     *
     * @param string $expedientes
     *
     * @return Colaimpresion
     */
    public function setExpedientes($expedientes)
    {
        $this->expedientes = $expedientes;

        return $this;
    }

    /**
     * Get expedientes.
     *
     * @return string
     */
    public function getExpedientes()
    {
        return $this->expedientes;
    }

    /**
     * Set fechainicio.
     *
     * @param \DateTime $fechainicio
     *
     * @return Colaimpresion
     */
    public function setFechainicio($fechainicio)
    {
        $this->fechainicio = $fechainicio;

        return $this;
    }

    /**
     * Get fechainicio.
     *
     * @return \DateTime
     */
    public function getFechainicio()
    {
        return $this->fechainicio;
    }

    /**
     * Set fechafin.
     *
     * @param \DateTime|null $fechafin
     *
     * @return Colaimpresion
     */
    public function setFechafin($fechafin = null)
    {
        $this->fechafin = $fechafin;

        return $this;
    }

    /**
     * Get fechafin.
     *
     * @return \DateTime|null
     */
    public function getFechafin()
    {
        return $this->fechafin;
    }

    /**
     * Set estado.
     *
     * @param string $estado
     *
     * @return Colaimpresion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado.
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set resultado.
     *
     * @param string|null $resultado
     *
     * @return Colaimpresion
     */
    public function setResultado($resultado = null)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get resultado.
     *
     * @return string|null
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Set param.
     *
     * @param string|null $param
     *
     * @return Colaimpresion
     */
    public function setParam($param = null)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * Get param.
     *
     * @return string|null
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * Set tipoexpedientes.
     *
     * @param string $tipoexpedientes
     *
     * @return Colaimpresion
     */
    public function setTipoexpedientes($tipoexpedientes)
    {
        $this->tipoexpedientes = $tipoexpedientes;

        return $this;
    }

    /**
     * Get tipoexpedientes.
     *
     * @return string
     */
    public function getTipoexpedientes()
    {
        return $this->tipoexpedientes;
    }

    /**
     * Set tipomulti.
     *
     * @param string $tipomulti
     *
     * @return Colaimpresion
     */
    public function setTipomulti($tipomulti)
    {
        $this->tipomulti = $tipomulti;

        return $this;
    }

    /**
     * Get tipomulti.
     *
     * @return string
     */
    public function getTipomulti()
    {
        return $this->tipomulti;
    }

    /**
     * Set salida.
     *
     * @param string $salida
     *
     * @return Colaimpresion
     */
    public function setSalida($salida)
    {
        $this->salida = $salida;

        return $this;
    }

    /**
     * Get salida.
     *
     * @return string
     */
    public function getSalida()
    {
        return $this->salida;
    }

    /**
     * Set proveedor.
     *
     * @param string $proveedor
     *
     * @return Colaimpresion
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor.
     *
     * @return string
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }
}
