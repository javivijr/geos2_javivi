<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Identificaciones
 *
 * @ORM\Table(name="identificaciones", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_I_FK_idx", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Identificaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="ididentificaciones", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ididentificaciones;

    /**
     * @var int
     *
     * @ORM\Column(name="velocidadDenuncia", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $velocidaddenuncia;

    /**
     * @var int
     *
     * @ORM\Column(name="velocidadPermitida", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $velocidadpermitida;

    /**
     * @var int
     *
     * @ORM\Column(name="permisoSuspension", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $permisosuspension;

    /**
     * @var int
     *
     * @ORM\Column(name="puntosDenuncia", type="integer", nullable=false)
     */
    private $puntosdenuncia;

    /**
     * @var string
     *
     * @ORM\Column(name="quieropagarlapulsado", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $quieropagarlapulsado = 'n';

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get ididentificaciones.
     *
     * @return int
     */
    public function getIdidentificaciones()
    {
        return $this->ididentificaciones;
    }

    /**
     * Set velocidaddenuncia.
     *
     * @param int $velocidaddenuncia
     *
     * @return Identificaciones
     */
    public function setVelocidaddenuncia($velocidaddenuncia)
    {
        $this->velocidaddenuncia = $velocidaddenuncia;

        return $this;
    }

    /**
     * Get velocidaddenuncia.
     *
     * @return int
     */
    public function getVelocidaddenuncia()
    {
        return $this->velocidaddenuncia;
    }

    /**
     * Set velocidadpermitida.
     *
     * @param int $velocidadpermitida
     *
     * @return Identificaciones
     */
    public function setVelocidadpermitida($velocidadpermitida)
    {
        $this->velocidadpermitida = $velocidadpermitida;

        return $this;
    }

    /**
     * Get velocidadpermitida.
     *
     * @return int
     */
    public function getVelocidadpermitida()
    {
        return $this->velocidadpermitida;
    }

    /**
     * Set permisosuspension.
     *
     * @param int $permisosuspension
     *
     * @return Identificaciones
     */
    public function setPermisosuspension($permisosuspension)
    {
        $this->permisosuspension = $permisosuspension;

        return $this;
    }

    /**
     * Get permisosuspension.
     *
     * @return int
     */
    public function getPermisosuspension()
    {
        return $this->permisosuspension;
    }

    /**
     * Set puntosdenuncia.
     *
     * @param int $puntosdenuncia
     *
     * @return Identificaciones
     */
    public function setPuntosdenuncia($puntosdenuncia)
    {
        $this->puntosdenuncia = $puntosdenuncia;

        return $this;
    }

    /**
     * Get puntosdenuncia.
     *
     * @return int
     */
    public function getPuntosdenuncia()
    {
        return $this->puntosdenuncia;
    }

    /**
     * Set quieropagarlapulsado.
     *
     * @param string $quieropagarlapulsado
     *
     * @return Identificaciones
     */
    public function setQuieropagarlapulsado($quieropagarlapulsado)
    {
        $this->quieropagarlapulsado = $quieropagarlapulsado;

        return $this;
    }

    /**
     * Get quieropagarlapulsado.
     *
     * @return string
     */
    public function getQuieropagarlapulsado()
    {
        return $this->quieropagarlapulsado;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Identificaciones
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
