<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estadosexpediente
 *
 * @ORM\Table(name="estadosexpediente", uniqueConstraints={@ORM\UniqueConstraint(name="orden_UNIQUE", columns={"orden"})})
 * @ORM\Entity
 */
class Estadosexpediente
{
    /**
     * @var int
     *
     * @ORM\Column(name="idestadosExpediente", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idestadosexpediente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=30, nullable=true)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $orden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombrepago", type="string", length=30, nullable=true)
     */
    private $nombrepago;



    /**
     * Get idestadosexpediente.
     *
     * @return int
     */
    public function getIdestadosexpediente()
    {
        return $this->idestadosexpediente;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Estadosexpediente
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set orden.
     *
     * @param int $orden
     *
     * @return Estadosexpediente
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden.
     *
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set nombrepago.
     *
     * @param string|null $nombrepago
     *
     * @return Estadosexpediente
     */
    public function setNombrepago($nombrepago = null)
    {
        $this->nombrepago = $nombrepago;

        return $this;
    }

    /**
     * Get nombrepago.
     *
     * @return string|null
     */
    public function getNombrepago()
    {
        return $this->nombrepago;
    }
}
