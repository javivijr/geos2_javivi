<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailsautomaticosTime
 *
 * @ORM\Table(name="emailsautomaticos_time", indexes={@ORM\Index(name="idemails", columns={"idemail"}), @ORM\Index(name="idmailtomatico_FK_idx", columns={"idemail"})})
 * @ORM\Entity
 */
class EmailsautomaticosTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="idemailsautomaticos_time", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idemailsautomaticosTime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="idemail", type="string", length=45, nullable=true)
     */
    private $idemail;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_prox_envio", type="datetime", nullable=true)
     */
    private $fechaProxEnvio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombrelargo", type="string", length=100, nullable=true)
     */
    private $nombrelargo;

    /**
     * @var bool
     *
     * @ORM\Column(name="programado", type="boolean", nullable=false, options={"default"="1"})
     */
    private $programado = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="programacion", type="string", length=2500, nullable=true)
     */
    private $programacion;



    /**
     * Get idemailsautomaticosTime.
     *
     * @return int
     */
    public function getIdemailsautomaticosTime()
    {
        return $this->idemailsautomaticosTime;
    }

    /**
     * Set idemail.
     *
     * @param string|null $idemail
     *
     * @return EmailsautomaticosTime
     */
    public function setIdemail($idemail = null)
    {
        $this->idemail = $idemail;

        return $this;
    }

    /**
     * Get idemail.
     *
     * @return string|null
     */
    public function getIdemail()
    {
        return $this->idemail;
    }

    /**
     * Set fechaProxEnvio.
     *
     * @param \DateTime|null $fechaProxEnvio
     *
     * @return EmailsautomaticosTime
     */
    public function setFechaProxEnvio($fechaProxEnvio = null)
    {
        $this->fechaProxEnvio = $fechaProxEnvio;

        return $this;
    }

    /**
     * Get fechaProxEnvio.
     *
     * @return \DateTime|null
     */
    public function getFechaProxEnvio()
    {
        return $this->fechaProxEnvio;
    }

    /**
     * Set nombrelargo.
     *
     * @param string|null $nombrelargo
     *
     * @return EmailsautomaticosTime
     */
    public function setNombrelargo($nombrelargo = null)
    {
        $this->nombrelargo = $nombrelargo;

        return $this;
    }

    /**
     * Get nombrelargo.
     *
     * @return string|null
     */
    public function getNombrelargo()
    {
        return $this->nombrelargo;
    }

    /**
     * Set programado.
     *
     * @param bool $programado
     *
     * @return EmailsautomaticosTime
     */
    public function setProgramado($programado)
    {
        $this->programado = $programado;

        return $this;
    }

    /**
     * Get programado.
     *
     * @return bool
     */
    public function getProgramado()
    {
        return $this->programado;
    }

    /**
     * Set programacion.
     *
     * @param string|null $programacion
     *
     * @return EmailsautomaticosTime
     */
    public function setProgramacion($programacion = null)
    {
        $this->programacion = $programacion;

        return $this;
    }

    /**
     * Get programacion.
     *
     * @return string|null
     */
    public function getProgramacion()
    {
        return $this->programacion;
    }
}
