<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historicocontratos
 *
 * @ORM\Table(name="historicocontratos", indexes={@ORM\Index(name="idcontratos_HCT_FK_idx", columns={"idcontratos"}), @ORM\Index(name="idhistoricoVehiculos_HCT_FK_idx", columns={"idhistoricoVehiculos"})})
 * @ORM\Entity
 */
class Historicocontratos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhistoricoContratos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhistoricocontratos;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;

    /**
     * @var \Historicovehiculos
     *
     * @ORM\ManyToOne(targetEntity="Historicovehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoVehiculos", referencedColumnName="idhistoricoVehiculos")
     * })
     */
    private $idhistoricovehiculos;



    /**
     * Get idhistoricocontratos.
     *
     * @return int
     */
    public function getIdhistoricocontratos()
    {
        return $this->idhistoricocontratos;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Historicocontratos
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }

    /**
     * Set idhistoricovehiculos.
     *
     * @param \AppBundle\Entity\Globalcar\Historicovehiculos|null $idhistoricovehiculos
     *
     * @return Historicocontratos
     */
    public function setIdhistoricovehiculos(\AppBundle\Entity\Globalcar\Historicovehiculos $idhistoricovehiculos = null)
    {
        $this->idhistoricovehiculos = $idhistoricovehiculos;

        return $this;
    }

    /**
     * Get idhistoricovehiculos.
     *
     * @return \AppBundle\Entity\Globalcar\Historicovehiculos|null
     */
    public function getIdhistoricovehiculos()
    {
        return $this->idhistoricovehiculos;
    }
}
