<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmtemporalrenovacion
 *
 * @ORM\Table(name="mtmtemporalrenovacion", indexes={@ORM\Index(name="idcontratos_TR_FK_idx", columns={"idcontratos"})})
 * @ORM\Entity
 */
class Mtmtemporalrenovacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmTemporalRenovacion", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmtemporalrenovacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="objeto", type="text", length=65535, nullable=true)
     */
    private $objeto;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;



    /**
     * Get idmtmtemporalrenovacion.
     *
     * @return int
     */
    public function getIdmtmtemporalrenovacion()
    {
        return $this->idmtmtemporalrenovacion;
    }

    /**
     * Set objeto.
     *
     * @param string|null $objeto
     *
     * @return Mtmtemporalrenovacion
     */
    public function setObjeto($objeto = null)
    {
        $this->objeto = $objeto;

        return $this;
    }

    /**
     * Get objeto.
     *
     * @return string|null
     */
    public function getObjeto()
    {
        return $this->objeto;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Mtmtemporalrenovacion
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }
}
