<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmusuariosdispositivos
 *
 * @ORM\Table(name="mtmusuariosdispositivos", indexes={@ORM\Index(name="idusuariosmtm_MUD_FK_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Mtmusuariosdispositivos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmusuariosdispositivos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmusuariosdispositivos;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=400, nullable=false)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="plataforma", type="string", length=0, nullable=false)
     */
    private $plataforma;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmusuariosdispositivos.
     *
     * @return int
     */
    public function getIdmtmusuariosdispositivos()
    {
        return $this->idmtmusuariosdispositivos;
    }

    /**
     * Set token.
     *
     * @param string $token
     *
     * @return Mtmusuariosdispositivos
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set plataforma.
     *
     * @param string $plataforma
     *
     * @return Mtmusuariosdispositivos
     */
    public function setPlataforma($plataforma)
    {
        $this->plataforma = $plataforma;

        return $this;
    }

    /**
     * Get plataforma.
     *
     * @return string
     */
    public function getPlataforma()
    {
        return $this->plataforma;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmusuariosdispositivos
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
