<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organismospago
 *
 * @ORM\Table(name="organismospago", indexes={@ORM\Index(name="idorganismox", columns={"idorganismos"}), @ORM\Index(name="idbancos_org_pago_FK_idx", columns={"idbancos"})})
 * @ORM\Entity
 */
class Organismospago
{
    /**
     * @var int
     *
     * @ORM\Column(name="idorganismospago", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idorganismospago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="metodoPago", type="string", length=400, nullable=true)
     */
    private $metodopago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipopago", type="string", length=1, nullable=true)
     */
    private $tipopago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tiporemesa", type="string", length=30, nullable=true)
     */
    private $tiporemesa;

    /**
     * @var \Bancos
     *
     * @ORM\ManyToOne(targetEntity="Bancos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idbancos", referencedColumnName="idbancos")
     * })
     */
    private $idbancos;

    /**
     * @var \Organismos
     *
     * @ORM\ManyToOne(targetEntity="Organismos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idorganismos", referencedColumnName="idorganismos")
     * })
     */
    private $idorganismos;



    /**
     * Get idorganismospago.
     *
     * @return int
     */
    public function getIdorganismospago()
    {
        return $this->idorganismospago;
    }

    /**
     * Set metodopago.
     *
     * @param string|null $metodopago
     *
     * @return Organismospago
     */
    public function setMetodopago($metodopago = null)
    {
        $this->metodopago = $metodopago;

        return $this;
    }

    /**
     * Get metodopago.
     *
     * @return string|null
     */
    public function getMetodopago()
    {
        return $this->metodopago;
    }

    /**
     * Set tipopago.
     *
     * @param string|null $tipopago
     *
     * @return Organismospago
     */
    public function setTipopago($tipopago = null)
    {
        $this->tipopago = $tipopago;

        return $this;
    }

    /**
     * Get tipopago.
     *
     * @return string|null
     */
    public function getTipopago()
    {
        return $this->tipopago;
    }

    /**
     * Set tiporemesa.
     *
     * @param string|null $tiporemesa
     *
     * @return Organismospago
     */
    public function setTiporemesa($tiporemesa = null)
    {
        $this->tiporemesa = $tiporemesa;

        return $this;
    }

    /**
     * Get tiporemesa.
     *
     * @return string|null
     */
    public function getTiporemesa()
    {
        return $this->tiporemesa;
    }

    /**
     * Set idbancos.
     *
     * @param \AppBundle\Entity\Globalcar\Bancos|null $idbancos
     *
     * @return Organismospago
     */
    public function setIdbancos(\AppBundle\Entity\Globalcar\Bancos $idbancos = null)
    {
        $this->idbancos = $idbancos;

        return $this;
    }

    /**
     * Get idbancos.
     *
     * @return \AppBundle\Entity\Globalcar\Bancos|null
     */
    public function getIdbancos()
    {
        return $this->idbancos;
    }

    /**
     * Set idorganismos.
     *
     * @param \AppBundle\Entity\Globalcar\Organismos|null $idorganismos
     *
     * @return Organismospago
     */
    public function setIdorganismos(\AppBundle\Entity\Globalcar\Organismos $idorganismos = null)
    {
        $this->idorganismos = $idorganismos;

        return $this;
    }

    /**
     * Get idorganismos.
     *
     * @return \AppBundle\Entity\Globalcar\Organismos|null
     */
    public function getIdorganismos()
    {
        return $this->idorganismos;
    }
}
