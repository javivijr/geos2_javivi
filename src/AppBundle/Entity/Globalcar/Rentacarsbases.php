<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rentacarsbases
 *
 * @ORM\Table(name="rentacarsbases", indexes={@ORM\Index(name="FK_106CE844E09AF528", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Rentacarsbases
{
    /**
     * @var int
     *
     * @ORM\Column(name="idrentacarsbases", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrentacarsbases;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailareamanager", type="string", length=450, nullable=true)
     */
    private $emailareamanager;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombrecompleto", type="string", length=255, nullable=true)
     */
    private $nombrecompleto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="domicilio", type="string", length=255, nullable=true)
     */
    private $domicilio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="municipio", type="string", length=255, nullable=true)
     */
    private $municipio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=255, nullable=true)
     */
    private $provincia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais", type="string", length=255, nullable=true)
     */
    private $pais;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idrentacarsbases.
     *
     * @return int
     */
    public function getIdrentacarsbases()
    {
        return $this->idrentacarsbases;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Rentacarsbases
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Rentacarsbases
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set emailareamanager.
     *
     * @param string|null $emailareamanager
     *
     * @return Rentacarsbases
     */
    public function setEmailareamanager($emailareamanager = null)
    {
        $this->emailareamanager = $emailareamanager;

        return $this;
    }

    /**
     * Get emailareamanager.
     *
     * @return string|null
     */
    public function getEmailareamanager()
    {
        return $this->emailareamanager;
    }

    /**
     * Set telefono.
     *
     * @param string|null $telefono
     *
     * @return Rentacarsbases
     */
    public function setTelefono($telefono = null)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono.
     *
     * @return string|null
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set nombrecompleto.
     *
     * @param string|null $nombrecompleto
     *
     * @return Rentacarsbases
     */
    public function setNombrecompleto($nombrecompleto = null)
    {
        $this->nombrecompleto = $nombrecompleto;

        return $this;
    }

    /**
     * Get nombrecompleto.
     *
     * @return string|null
     */
    public function getNombrecompleto()
    {
        return $this->nombrecompleto;
    }

    /**
     * Set domicilio.
     *
     * @param string|null $domicilio
     *
     * @return Rentacarsbases
     */
    public function setDomicilio($domicilio = null)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio.
     *
     * @return string|null
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set municipio.
     *
     * @param string|null $municipio
     *
     * @return Rentacarsbases
     */
    public function setMunicipio($municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio.
     *
     * @return string|null
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Rentacarsbases
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Rentacarsbases
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set pais.
     *
     * @param string|null $pais
     *
     * @return Rentacarsbases
     */
    public function setPais($pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return string|null
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Rentacarsbases
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
