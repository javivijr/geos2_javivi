<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rentacars
 *
 * @ORM\Table(name="rentacars", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_UNIQUE", columns={"idexpedientes"})}, indexes={@ORM\Index(name="idexpediente_REC_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="iddestionoscartas_REC_FK_idx", columns={"destinoCarta"})})
 * @ORM\Entity
 */
class Rentacars
{
    /**
     * @var int
     *
     * @ORM\Column(name="idrentaCars", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrentacars;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaInicioContrato", type="datetime", nullable=true)
     */
    private $fechainiciocontrato;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaFinContrato", type="datetime", nullable=true)
     */
    private $fechafincontrato;

    /**
     * @var string
     *
     * @ORM\Column(name="adjCarrent", type="string", length=0, nullable=false)
     */
    private $adjcarrent;

    /**
     * @var string
     *
     * @ORM\Column(name="adjContrato", type="string", length=0, nullable=false)
     */
    private $adjcontrato;

    /**
     * @var string
     *
     * @ORM\Column(name="adjFactura", type="string", length=0, nullable=false)
     */
    private $adjfactura;

    /**
     * @var string
     *
     * @ORM\Column(name="adjCodCrnd", type="string", length=0, nullable=false)
     */
    private $adjcodcrnd;

    /**
     * @var string
     *
     * @ORM\Column(name="adjAnexo", type="string", length=0, nullable=false)
     */
    private $adjanexo;

    /**
     * @var string
     *
     * @ORM\Column(name="tieneFechaIni", type="string", length=0, nullable=false)
     */
    private $tienefechaini;

    /**
     * @var string
     *
     * @ORM\Column(name="tieneFechaFin", type="string", length=0, nullable=false)
     */
    private $tienefechafin;

    /**
     * @var string
     *
     * @ORM\Column(name="tieneHoraFin", type="string", length=0, nullable=false)
     */
    private $tienehorafin;

    /**
     * @var string
     *
     * @ORM\Column(name="esRnd", type="string", length=0, nullable=false)
     */
    private $esrnd;

    /**
     * @var string
     *
     * @ORM\Column(name="idiomaCarta", type="string", length=3, nullable=false)
     */
    private $idiomacarta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="miscelanea", type="string", length=100, nullable=true)
     */
    private $miscelanea;

    /**
     * @var string
     *
     * @ORM\Column(name="carrentfalso", type="string", length=0, nullable=false)
     */
    private $carrentfalso;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoescrito", type="string", length=0, nullable=false)
     */
    private $tipoescrito;

    /**
     * @var Destinoscartas
     *
     * @ORM\ManyToOne(targetEntity="Destinoscartas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="destinoCarta", referencedColumnName="iddestinosCartas")
     * })
     */
    private $destinocarta;

    /**
     * @var Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idrentacars.
     *
     * @return int
     */
    public function getIdrentacars()
    {
        return $this->idrentacars;
    }

    /**
     * Set fechainiciocontrato.
     *
     * @param \DateTime|null $fechainiciocontrato
     *
     * @return Rentacars
     */
    public function setFechainiciocontrato($fechainiciocontrato = null)
    {
        $this->fechainiciocontrato = $fechainiciocontrato;

        return $this;
    }

    /**
     * Get fechainiciocontrato.
     *
     * @return \DateTime|null
     */
    public function getFechainiciocontrato()
    {
        return $this->fechainiciocontrato;
    }

    /**
     * Set fechafincontrato.
     *
     * @param \DateTime|null $fechafincontrato
     *
     * @return Rentacars
     */
    public function setFechafincontrato($fechafincontrato = null)
    {
        $this->fechafincontrato = $fechafincontrato;

        return $this;
    }

    /**
     * Get fechafincontrato.
     *
     * @return \DateTime|null
     */
    public function getFechafincontrato()
    {
        return $this->fechafincontrato;
    }

    /**
     * Set adjcarrent.
     *
     * @param string $adjcarrent
     *
     * @return Rentacars
     */
    public function setAdjcarrent($adjcarrent)
    {
        $this->adjcarrent = $adjcarrent;

        return $this;
    }

    /**
     * Get adjcarrent.
     *
     * @return string
     */
    public function getAdjcarrent()
    {
        return $this->adjcarrent;
    }

    /**
     * Set adjcontrato.
     *
     * @param string $adjcontrato
     *
     * @return Rentacars
     */
    public function setAdjcontrato($adjcontrato)
    {
        $this->adjcontrato = $adjcontrato;

        return $this;
    }

    /**
     * Get adjcontrato.
     *
     * @return string
     */
    public function getAdjcontrato()
    {
        return $this->adjcontrato;
    }

    /**
     * Set adjfactura.
     *
     * @param string $adjfactura
     *
     * @return Rentacars
     */
    public function setAdjfactura($adjfactura)
    {
        $this->adjfactura = $adjfactura;

        return $this;
    }

    /**
     * Get adjfactura.
     *
     * @return string
     */
    public function getAdjfactura()
    {
        return $this->adjfactura;
    }

    /**
     * Set adjcodcrnd.
     *
     * @param string $adjcodcrnd
     *
     * @return Rentacars
     */
    public function setAdjcodcrnd($adjcodcrnd)
    {
        $this->adjcodcrnd = $adjcodcrnd;

        return $this;
    }

    /**
     * Get adjcodcrnd.
     *
     * @return string
     */
    public function getAdjcodcrnd()
    {
        return $this->adjcodcrnd;
    }

    /**
     * Set adjanexo.
     *
     * @param string $adjanexo
     *
     * @return Rentacars
     */
    public function setAdjanexo($adjanexo)
    {
        $this->adjanexo = $adjanexo;

        return $this;
    }

    /**
     * Get adjanexo.
     *
     * @return string
     */
    public function getAdjanexo()
    {
        return $this->adjanexo;
    }

    /**
     * Set tienefechaini.
     *
     * @param string $tienefechaini
     *
     * @return Rentacars
     */
    public function setTienefechaini($tienefechaini)
    {
        $this->tienefechaini = $tienefechaini;

        return $this;
    }

    /**
     * Get tienefechaini.
     *
     * @return string
     */
    public function getTienefechaini()
    {
        return $this->tienefechaini;
    }

    /**
     * Set tienefechafin.
     *
     * @param string $tienefechafin
     *
     * @return Rentacars
     */
    public function setTienefechafin($tienefechafin)
    {
        $this->tienefechafin = $tienefechafin;

        return $this;
    }

    /**
     * Get tienefechafin.
     *
     * @return string
     */
    public function getTienefechafin()
    {
        return $this->tienefechafin;
    }

    /**
     * Set tienehorafin.
     *
     * @param string $tienehorafin
     *
     * @return Rentacars
     */
    public function setTienehorafin($tienehorafin)
    {
        $this->tienehorafin = $tienehorafin;

        return $this;
    }

    /**
     * Get tienehorafin.
     *
     * @return string
     */
    public function getTienehorafin()
    {
        return $this->tienehorafin;
    }

    /**
     * Set esrnd.
     *
     * @param string $esrnd
     *
     * @return Rentacars
     */
    public function setEsrnd($esrnd)
    {
        $this->esrnd = $esrnd;

        return $this;
    }

    /**
     * Get esrnd.
     *
     * @return string
     */
    public function getEsrnd()
    {
        return $this->esrnd;
    }

    /**
     * Set idiomacarta.
     *
     * @param string $idiomacarta
     *
     * @return Rentacars
     */
    public function setIdiomacarta($idiomacarta)
    {
        $this->idiomacarta = $idiomacarta;

        return $this;
    }

    /**
     * Get idiomacarta.
     *
     * @return string
     */
    public function getIdiomacarta()
    {
        return $this->idiomacarta;
    }

    /**
     * Set miscelanea.
     *
     * @param string|null $miscelanea
     *
     * @return Rentacars
     */
    public function setMiscelanea($miscelanea = null)
    {
        $this->miscelanea = $miscelanea;

        return $this;
    }

    /**
     * Get miscelanea.
     *
     * @return string|null
     */
    public function getMiscelanea()
    {
        return $this->miscelanea;
    }

    /**
     * Set carrentfalso.
     *
     * @param string $carrentfalso
     *
     * @return Rentacars
     */
    public function setCarrentfalso($carrentfalso)
    {
        $this->carrentfalso = $carrentfalso;

        return $this;
    }

    /**
     * Get carrentfalso.
     *
     * @return string
     */
    public function getCarrentfalso()
    {
        return $this->carrentfalso;
    }

    /**
     * Set tipoescrito.
     *
     * @param string $tipoescrito
     *
     * @return Rentacars
     */
    public function setTipoescrito($tipoescrito)
    {
        $this->tipoescrito = $tipoescrito;

        return $this;
    }

    /**
     * Get tipoescrito.
     *
     * @return string
     */
    public function getTipoescrito()
    {
        return $this->tipoescrito;
    }

    /**
     * Set destinocarta.
     *
     * @param \AppBundle\Entity\Globalcar\Destinoscartas|null $destinocarta
     *
     * @return Rentacars
     */
    public function setDestinocarta(\AppBundle\Entity\Globalcar\Destinoscartas $destinocarta = null)
    {
        $this->destinocarta = $destinocarta;

        return $this;
    }

    /**
     * Get destinocarta.
     *
     * @return \AppBundle\Entity\Globalcar\Destinoscartas|null
     */
    public function getDestinocarta()
    {
        return $this->destinocarta;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Rentacars
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
