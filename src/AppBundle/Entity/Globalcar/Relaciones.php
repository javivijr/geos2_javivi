<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relaciones
 *
 * @ORM\Table(name="relaciones")
 * @ORM\Entity
 */
class Relaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="idrelaciones", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrelaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=50, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="formato", type="string", length=0, nullable=false)
     */
    private $formato;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmado", type="string", length=0, nullable=false)
     */
    private $confirmado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta", type="string", length=200, nullable=true)
     */
    private $ruta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoConfirmacion", type="string", length=10, nullable=true)
     */
    private $codigoconfirmacion;



    /**
     * Get idrelaciones.
     *
     * @return int
     */
    public function getIdrelaciones()
    {
        return $this->idrelaciones;
    }

    /**
     * Set usuario.
     *
     * @param string $usuario
     *
     * @return Relaciones
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set formato.
     *
     * @param string $formato
     *
     * @return Relaciones
     */
    public function setFormato($formato)
    {
        $this->formato = $formato;

        return $this;
    }

    /**
     * Get formato.
     *
     * @return string
     */
    public function getFormato()
    {
        return $this->formato;
    }

    /**
     * Set confirmado.
     *
     * @param string $confirmado
     *
     * @return Relaciones
     */
    public function setConfirmado($confirmado)
    {
        $this->confirmado = $confirmado;

        return $this;
    }

    /**
     * Get confirmado.
     *
     * @return string
     */
    public function getConfirmado()
    {
        return $this->confirmado;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Relaciones
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set ruta.
     *
     * @param string|null $ruta
     *
     * @return Relaciones
     */
    public function setRuta($ruta = null)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta.
     *
     * @return string|null
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set codigoconfirmacion.
     *
     * @param string|null $codigoconfirmacion
     *
     * @return Relaciones
     */
    public function setCodigoconfirmacion($codigoconfirmacion = null)
    {
        $this->codigoconfirmacion = $codigoconfirmacion;

        return $this;
    }

    /**
     * Get codigoconfirmacion.
     *
     * @return string|null
     */
    public function getCodigoconfirmacion()
    {
        return $this->codigoconfirmacion;
    }
}
