<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrentsixt
 *
 * @ORM\Table(name="carrentsixt", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_UNIQUE", columns={"idexpedientes"}), @ORM\UniqueConstraint(name="carrentsixt_poquepk", columns={"ticketdatetime", "licenseplate"})}, indexes={@ORM\Index(name="carrentsixt_ranumber", columns={"rentalagreement"}), @ORM\Index(name="carrentsixt_contratofirm", columns={"contratofirmadonecesario", "contratofirmadoadjuntado"}), @ORM\Index(name="carrentsixt_idexpediente_idx", columns={"idexpedientes"}), @ORM\Index(name="carrentsixt_desecho", columns={"gestdesecho"})})
 * @ORM\Entity
 */
class Carrentsixt
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrentsixt", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrentsixt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contratofirmadonecesario", type="string", length=1, nullable=true)
     */
    private $contratofirmadonecesario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contratofirmadoadjuntado", type="string", length=1, nullable=true)
     */
    private $contratofirmadoadjuntado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rentalagreement", type="string", length=200, nullable=true)
     */
    private $rentalagreement;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ticketdatetime", type="datetime", nullable=true)
     */
    private $ticketdatetime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ticketreferencenb", type="string", length=200, nullable=true)
     */
    private $ticketreferencenb;

    /**
     * @var string|null
     *
     * @ORM\Column(name="licenseplate", type="string", length=30, nullable=true)
     */
    private $licenseplate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="processeddateinviper", type="datetime", nullable=true)
     */
    private $processeddateinviper;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user", type="string", length=200, nullable=true)
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname1", type="string", length=200, nullable=true)
     */
    private $surname1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name1", type="string", length=200, nullable=true)
     */
    private $name1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address1", type="string", length=200, nullable=true)
     */
    private $address1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city1", type="string", length=200, nullable=true)
     */
    private $city1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postcode1", type="string", length=200, nullable=true)
     */
    private $postcode1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country1", type="string", length=200, nullable=true)
     */
    private $country1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="driverslicense1", type="string", length=200, nullable=true)
     */
    private $driverslicense1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="driverslicissuedate1", type="string", length=200, nullable=true)
     */
    private $driverslicissuedate1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="driverslicissueplace1", type="string", length=200, nullable=true)
     */
    private $driverslicissueplace1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname2", type="string", length=200, nullable=true)
     */
    private $surname2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name2", type="string", length=200, nullable=true)
     */
    private $name2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address2", type="string", length=200, nullable=true)
     */
    private $address2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city2", type="string", length=200, nullable=true)
     */
    private $city2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postcode2", type="string", length=200, nullable=true)
     */
    private $postcode2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country2", type="string", length=200, nullable=true)
     */
    private $country2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="driverslicense2", type="string", length=200, nullable=true)
     */
    private $driverslicense2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="driverslicissuedate2", type="string", length=200, nullable=true)
     */
    private $driverslicissuedate2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="driverslicissueplace2", type="string", length=200, nullable=true)
     */
    private $driverslicissueplace2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ticketfilename", type="string", length=200, nullable=true)
     */
    private $ticketfilename;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cartype", type="string", length=200, nullable=true)
     */
    private $cartype;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="checkoutdate", type="datetime", nullable=true)
     */
    private $checkoutdate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="checkindate", type="datetime", nullable=true)
     */
    private $checkindate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="checkoutplace", type="string", length=200, nullable=true)
     */
    private $checkoutplace;

    /**
     * @var string|null
     *
     * @ORM\Column(name="checkinplace", type="string", length=200, nullable=true)
     */
    private $checkinplace;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rafilename", type="string", length=200, nullable=true)
     */
    private $rafilename;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kmout", type="string", length=200, nullable=true)
     */
    private $kmout;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kmin", type="string", length=200, nullable=true)
     */
    private $kmin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sha1", type="string", length=40, nullable=true)
     */
    private $sha1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="passport1", type="string", length=200, nullable=true)
     */
    private $passport1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="passport2", type="string", length=200, nullable=true)
     */
    private $passport2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="barcode", type="string", length=200, nullable=true)
     */
    private $barcode;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tipyconsgenerationdate", type="datetime", nullable=true)
     */
    private $tipyconsgenerationdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gestdesecho", type="string", length=200, nullable=true)
     */
    private $gestdesecho;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idcarrentsixt.
     *
     * @return int
     */
    public function getIdcarrentsixt()
    {
        return $this->idcarrentsixt;
    }

    /**
     * Set contratofirmadonecesario.
     *
     * @param string|null $contratofirmadonecesario
     *
     * @return Carrentsixt
     */
    public function setContratofirmadonecesario($contratofirmadonecesario = null)
    {
        $this->contratofirmadonecesario = $contratofirmadonecesario;

        return $this;
    }

    /**
     * Get contratofirmadonecesario.
     *
     * @return string|null
     */
    public function getContratofirmadonecesario()
    {
        return $this->contratofirmadonecesario;
    }

    /**
     * Set contratofirmadoadjuntado.
     *
     * @param string|null $contratofirmadoadjuntado
     *
     * @return Carrentsixt
     */
    public function setContratofirmadoadjuntado($contratofirmadoadjuntado = null)
    {
        $this->contratofirmadoadjuntado = $contratofirmadoadjuntado;

        return $this;
    }

    /**
     * Get contratofirmadoadjuntado.
     *
     * @return string|null
     */
    public function getContratofirmadoadjuntado()
    {
        return $this->contratofirmadoadjuntado;
    }

    /**
     * Set rentalagreement.
     *
     * @param string|null $rentalagreement
     *
     * @return Carrentsixt
     */
    public function setRentalagreement($rentalagreement = null)
    {
        $this->rentalagreement = $rentalagreement;

        return $this;
    }

    /**
     * Get rentalagreement.
     *
     * @return string|null
     */
    public function getRentalagreement()
    {
        return $this->rentalagreement;
    }

    /**
     * Set ticketdatetime.
     *
     * @param \DateTime|null $ticketdatetime
     *
     * @return Carrentsixt
     */
    public function setTicketdatetime($ticketdatetime = null)
    {
        $this->ticketdatetime = $ticketdatetime;

        return $this;
    }

    /**
     * Get ticketdatetime.
     *
     * @return \DateTime|null
     */
    public function getTicketdatetime()
    {
        return $this->ticketdatetime;
    }

    /**
     * Set ticketreferencenb.
     *
     * @param string|null $ticketreferencenb
     *
     * @return Carrentsixt
     */
    public function setTicketreferencenb($ticketreferencenb = null)
    {
        $this->ticketreferencenb = $ticketreferencenb;

        return $this;
    }

    /**
     * Get ticketreferencenb.
     *
     * @return string|null
     */
    public function getTicketreferencenb()
    {
        return $this->ticketreferencenb;
    }

    /**
     * Set licenseplate.
     *
     * @param string|null $licenseplate
     *
     * @return Carrentsixt
     */
    public function setLicenseplate($licenseplate = null)
    {
        $this->licenseplate = $licenseplate;

        return $this;
    }

    /**
     * Get licenseplate.
     *
     * @return string|null
     */
    public function getLicenseplate()
    {
        return $this->licenseplate;
    }

    /**
     * Set processeddateinviper.
     *
     * @param \DateTime|null $processeddateinviper
     *
     * @return Carrentsixt
     */
    public function setProcesseddateinviper($processeddateinviper = null)
    {
        $this->processeddateinviper = $processeddateinviper;

        return $this;
    }

    /**
     * Get processeddateinviper.
     *
     * @return \DateTime|null
     */
    public function getProcesseddateinviper()
    {
        return $this->processeddateinviper;
    }

    /**
     * Set user.
     *
     * @param string|null $user
     *
     * @return Carrentsixt
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return string|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set surname1.
     *
     * @param string|null $surname1
     *
     * @return Carrentsixt
     */
    public function setSurname1($surname1 = null)
    {
        $this->surname1 = $surname1;

        return $this;
    }

    /**
     * Get surname1.
     *
     * @return string|null
     */
    public function getSurname1()
    {
        return $this->surname1;
    }

    /**
     * Set name1.
     *
     * @param string|null $name1
     *
     * @return Carrentsixt
     */
    public function setName1($name1 = null)
    {
        $this->name1 = $name1;

        return $this;
    }

    /**
     * Get name1.
     *
     * @return string|null
     */
    public function getName1()
    {
        return $this->name1;
    }

    /**
     * Set address1.
     *
     * @param string|null $address1
     *
     * @return Carrentsixt
     */
    public function setAddress1($address1 = null)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1.
     *
     * @return string|null
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set city1.
     *
     * @param string|null $city1
     *
     * @return Carrentsixt
     */
    public function setCity1($city1 = null)
    {
        $this->city1 = $city1;

        return $this;
    }

    /**
     * Get city1.
     *
     * @return string|null
     */
    public function getCity1()
    {
        return $this->city1;
    }

    /**
     * Set postcode1.
     *
     * @param string|null $postcode1
     *
     * @return Carrentsixt
     */
    public function setPostcode1($postcode1 = null)
    {
        $this->postcode1 = $postcode1;

        return $this;
    }

    /**
     * Get postcode1.
     *
     * @return string|null
     */
    public function getPostcode1()
    {
        return $this->postcode1;
    }

    /**
     * Set country1.
     *
     * @param string|null $country1
     *
     * @return Carrentsixt
     */
    public function setCountry1($country1 = null)
    {
        $this->country1 = $country1;

        return $this;
    }

    /**
     * Get country1.
     *
     * @return string|null
     */
    public function getCountry1()
    {
        return $this->country1;
    }

    /**
     * Set driverslicense1.
     *
     * @param string|null $driverslicense1
     *
     * @return Carrentsixt
     */
    public function setDriverslicense1($driverslicense1 = null)
    {
        $this->driverslicense1 = $driverslicense1;

        return $this;
    }

    /**
     * Get driverslicense1.
     *
     * @return string|null
     */
    public function getDriverslicense1()
    {
        return $this->driverslicense1;
    }

    /**
     * Set driverslicissuedate1.
     *
     * @param string|null $driverslicissuedate1
     *
     * @return Carrentsixt
     */
    public function setDriverslicissuedate1($driverslicissuedate1 = null)
    {
        $this->driverslicissuedate1 = $driverslicissuedate1;

        return $this;
    }

    /**
     * Get driverslicissuedate1.
     *
     * @return string|null
     */
    public function getDriverslicissuedate1()
    {
        return $this->driverslicissuedate1;
    }

    /**
     * Set driverslicissueplace1.
     *
     * @param string|null $driverslicissueplace1
     *
     * @return Carrentsixt
     */
    public function setDriverslicissueplace1($driverslicissueplace1 = null)
    {
        $this->driverslicissueplace1 = $driverslicissueplace1;

        return $this;
    }

    /**
     * Get driverslicissueplace1.
     *
     * @return string|null
     */
    public function getDriverslicissueplace1()
    {
        return $this->driverslicissueplace1;
    }

    /**
     * Set surname2.
     *
     * @param string|null $surname2
     *
     * @return Carrentsixt
     */
    public function setSurname2($surname2 = null)
    {
        $this->surname2 = $surname2;

        return $this;
    }

    /**
     * Get surname2.
     *
     * @return string|null
     */
    public function getSurname2()
    {
        return $this->surname2;
    }

    /**
     * Set name2.
     *
     * @param string|null $name2
     *
     * @return Carrentsixt
     */
    public function setName2($name2 = null)
    {
        $this->name2 = $name2;

        return $this;
    }

    /**
     * Get name2.
     *
     * @return string|null
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * Set address2.
     *
     * @param string|null $address2
     *
     * @return Carrentsixt
     */
    public function setAddress2($address2 = null)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2.
     *
     * @return string|null
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city2.
     *
     * @param string|null $city2
     *
     * @return Carrentsixt
     */
    public function setCity2($city2 = null)
    {
        $this->city2 = $city2;

        return $this;
    }

    /**
     * Get city2.
     *
     * @return string|null
     */
    public function getCity2()
    {
        return $this->city2;
    }

    /**
     * Set postcode2.
     *
     * @param string|null $postcode2
     *
     * @return Carrentsixt
     */
    public function setPostcode2($postcode2 = null)
    {
        $this->postcode2 = $postcode2;

        return $this;
    }

    /**
     * Get postcode2.
     *
     * @return string|null
     */
    public function getPostcode2()
    {
        return $this->postcode2;
    }

    /**
     * Set country2.
     *
     * @param string|null $country2
     *
     * @return Carrentsixt
     */
    public function setCountry2($country2 = null)
    {
        $this->country2 = $country2;

        return $this;
    }

    /**
     * Get country2.
     *
     * @return string|null
     */
    public function getCountry2()
    {
        return $this->country2;
    }

    /**
     * Set driverslicense2.
     *
     * @param string|null $driverslicense2
     *
     * @return Carrentsixt
     */
    public function setDriverslicense2($driverslicense2 = null)
    {
        $this->driverslicense2 = $driverslicense2;

        return $this;
    }

    /**
     * Get driverslicense2.
     *
     * @return string|null
     */
    public function getDriverslicense2()
    {
        return $this->driverslicense2;
    }

    /**
     * Set driverslicissuedate2.
     *
     * @param string|null $driverslicissuedate2
     *
     * @return Carrentsixt
     */
    public function setDriverslicissuedate2($driverslicissuedate2 = null)
    {
        $this->driverslicissuedate2 = $driverslicissuedate2;

        return $this;
    }

    /**
     * Get driverslicissuedate2.
     *
     * @return string|null
     */
    public function getDriverslicissuedate2()
    {
        return $this->driverslicissuedate2;
    }

    /**
     * Set driverslicissueplace2.
     *
     * @param string|null $driverslicissueplace2
     *
     * @return Carrentsixt
     */
    public function setDriverslicissueplace2($driverslicissueplace2 = null)
    {
        $this->driverslicissueplace2 = $driverslicissueplace2;

        return $this;
    }

    /**
     * Get driverslicissueplace2.
     *
     * @return string|null
     */
    public function getDriverslicissueplace2()
    {
        return $this->driverslicissueplace2;
    }

    /**
     * Set ticketfilename.
     *
     * @param string|null $ticketfilename
     *
     * @return Carrentsixt
     */
    public function setTicketfilename($ticketfilename = null)
    {
        $this->ticketfilename = $ticketfilename;

        return $this;
    }

    /**
     * Get ticketfilename.
     *
     * @return string|null
     */
    public function getTicketfilename()
    {
        return $this->ticketfilename;
    }

    /**
     * Set cartype.
     *
     * @param string|null $cartype
     *
     * @return Carrentsixt
     */
    public function setCartype($cartype = null)
    {
        $this->cartype = $cartype;

        return $this;
    }

    /**
     * Get cartype.
     *
     * @return string|null
     */
    public function getCartype()
    {
        return $this->cartype;
    }

    /**
     * Set checkoutdate.
     *
     * @param \DateTime|null $checkoutdate
     *
     * @return Carrentsixt
     */
    public function setCheckoutdate($checkoutdate = null)
    {
        $this->checkoutdate = $checkoutdate;

        return $this;
    }

    /**
     * Get checkoutdate.
     *
     * @return \DateTime|null
     */
    public function getCheckoutdate()
    {
        return $this->checkoutdate;
    }

    /**
     * Set checkindate.
     *
     * @param \DateTime|null $checkindate
     *
     * @return Carrentsixt
     */
    public function setCheckindate($checkindate = null)
    {
        $this->checkindate = $checkindate;

        return $this;
    }

    /**
     * Get checkindate.
     *
     * @return \DateTime|null
     */
    public function getCheckindate()
    {
        return $this->checkindate;
    }

    /**
     * Set checkoutplace.
     *
     * @param string|null $checkoutplace
     *
     * @return Carrentsixt
     */
    public function setCheckoutplace($checkoutplace = null)
    {
        $this->checkoutplace = $checkoutplace;

        return $this;
    }

    /**
     * Get checkoutplace.
     *
     * @return string|null
     */
    public function getCheckoutplace()
    {
        return $this->checkoutplace;
    }

    /**
     * Set checkinplace.
     *
     * @param string|null $checkinplace
     *
     * @return Carrentsixt
     */
    public function setCheckinplace($checkinplace = null)
    {
        $this->checkinplace = $checkinplace;

        return $this;
    }

    /**
     * Get checkinplace.
     *
     * @return string|null
     */
    public function getCheckinplace()
    {
        return $this->checkinplace;
    }

    /**
     * Set rafilename.
     *
     * @param string|null $rafilename
     *
     * @return Carrentsixt
     */
    public function setRafilename($rafilename = null)
    {
        $this->rafilename = $rafilename;

        return $this;
    }

    /**
     * Get rafilename.
     *
     * @return string|null
     */
    public function getRafilename()
    {
        return $this->rafilename;
    }

    /**
     * Set kmout.
     *
     * @param string|null $kmout
     *
     * @return Carrentsixt
     */
    public function setKmout($kmout = null)
    {
        $this->kmout = $kmout;

        return $this;
    }

    /**
     * Get kmout.
     *
     * @return string|null
     */
    public function getKmout()
    {
        return $this->kmout;
    }

    /**
     * Set kmin.
     *
     * @param string|null $kmin
     *
     * @return Carrentsixt
     */
    public function setKmin($kmin = null)
    {
        $this->kmin = $kmin;

        return $this;
    }

    /**
     * Get kmin.
     *
     * @return string|null
     */
    public function getKmin()
    {
        return $this->kmin;
    }

    /**
     * Set sha1.
     *
     * @param string|null $sha1
     *
     * @return Carrentsixt
     */
    public function setSha1($sha1 = null)
    {
        $this->sha1 = $sha1;

        return $this;
    }

    /**
     * Get sha1.
     *
     * @return string|null
     */
    public function getSha1()
    {
        return $this->sha1;
    }

    /**
     * Set passport1.
     *
     * @param string|null $passport1
     *
     * @return Carrentsixt
     */
    public function setPassport1($passport1 = null)
    {
        $this->passport1 = $passport1;

        return $this;
    }

    /**
     * Get passport1.
     *
     * @return string|null
     */
    public function getPassport1()
    {
        return $this->passport1;
    }

    /**
     * Set passport2.
     *
     * @param string|null $passport2
     *
     * @return Carrentsixt
     */
    public function setPassport2($passport2 = null)
    {
        $this->passport2 = $passport2;

        return $this;
    }

    /**
     * Get passport2.
     *
     * @return string|null
     */
    public function getPassport2()
    {
        return $this->passport2;
    }

    /**
     * Set barcode.
     *
     * @param string|null $barcode
     *
     * @return Carrentsixt
     */
    public function setBarcode($barcode = null)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode.
     *
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set tipyconsgenerationdate.
     *
     * @param \DateTime|null $tipyconsgenerationdate
     *
     * @return Carrentsixt
     */
    public function setTipyconsgenerationdate($tipyconsgenerationdate = null)
    {
        $this->tipyconsgenerationdate = $tipyconsgenerationdate;

        return $this;
    }

    /**
     * Get tipyconsgenerationdate.
     *
     * @return \DateTime|null
     */
    public function getTipyconsgenerationdate()
    {
        return $this->tipyconsgenerationdate;
    }

    /**
     * Set gestdesecho.
     *
     * @param string|null $gestdesecho
     *
     * @return Carrentsixt
     */
    public function setGestdesecho($gestdesecho = null)
    {
        $this->gestdesecho = $gestdesecho;

        return $this;
    }

    /**
     * Get gestdesecho.
     *
     * @return string|null
     */
    public function getGestdesecho()
    {
        return $this->gestdesecho;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Carrentsixt
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
