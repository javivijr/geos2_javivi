<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmpagotemp
 *
 * @ORM\Table(name="mtmpagotemp", indexes={@ORM\Index(name="idexpedientes_MPTd_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idusuariosmtm_MPTd_PK_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Mtmpagotemp
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmpagotemp", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmpagotemp;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=12, nullable=false)
     */
    private $referencia;

    /**
     * @var float
     *
     * @ORM\Column(name="importe", type="float", precision=6, scale=2, nullable=false)
     */
    private $importe;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuariosmtm", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idusuariosmtm;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idmtmpagotemp.
     *
     * @return int
     */
    public function getIdmtmpagotemp()
    {
        return $this->idmtmpagotemp;
    }

    /**
     * Set referencia.
     *
     * @param string $referencia
     *
     * @return Mtmpagotemp
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia.
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set importe.
     *
     * @param float $importe
     *
     * @return Mtmpagotemp
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe.
     *
     * @return float
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param int $idusuariosmtm
     *
     * @return Mtmpagotemp
     */
    public function setIdusuariosmtm($idusuariosmtm)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return int
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Mtmpagotemp
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
