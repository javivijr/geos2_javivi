<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historicopersonasportugal
 *
 * @ORM\Table(name="historicopersonasportugal")
 * @ORM\Entity
 */
class Historicopersonasportugal
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhistoricopersonasportugal", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhistoricopersonasportugal;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaexpedicion", type="datetime", nullable=true)
     */
    private $fechaexpedicion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lugarexpedicion", type="string", length=45, nullable=true)
     */
    private $lugarexpedicion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numdocumento2", type="string", length=25, nullable=true)
     */
    private $numdocumento2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipodocumento2", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $tipodocumento2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaexpedicion2", type="datetime", nullable=true)
     */
    private $fechaexpedicion2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lugarexpedicion2", type="string", length=45, nullable=true)
     */
    private $lugarexpedicion2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numdocumento3", type="string", length=25, nullable=true)
     */
    private $numdocumento3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipodocumento3", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $tipodocumento3;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaexpedicion3", type="datetime", nullable=true)
     */
    private $fechaexpedicion3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lugarexpedicion3", type="string", length=45, nullable=true)
     */
    private $lugarexpedicion3;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechadenacimiento", type="datetime", nullable=true)
     */
    private $fechadenacimiento;



    /**
     * Get idhistoricopersonasportugal.
     *
     * @return int
     */
    public function getIdhistoricopersonasportugal()
    {
        return $this->idhistoricopersonasportugal;
    }

    /**
     * Set fechaexpedicion.
     *
     * @param \DateTime|null $fechaexpedicion
     *
     * @return Historicopersonasportugal
     */
    public function setFechaexpedicion($fechaexpedicion = null)
    {
        $this->fechaexpedicion = $fechaexpedicion;

        return $this;
    }

    /**
     * Get fechaexpedicion.
     *
     * @return \DateTime|null
     */
    public function getFechaexpedicion()
    {
        return $this->fechaexpedicion;
    }

    /**
     * Set lugarexpedicion.
     *
     * @param string|null $lugarexpedicion
     *
     * @return Historicopersonasportugal
     */
    public function setLugarexpedicion($lugarexpedicion = null)
    {
        $this->lugarexpedicion = $lugarexpedicion;

        return $this;
    }

    /**
     * Get lugarexpedicion.
     *
     * @return string|null
     */
    public function getLugarexpedicion()
    {
        return $this->lugarexpedicion;
    }

    /**
     * Set numdocumento2.
     *
     * @param string|null $numdocumento2
     *
     * @return Historicopersonasportugal
     */
    public function setNumdocumento2($numdocumento2 = null)
    {
        $this->numdocumento2 = $numdocumento2;

        return $this;
    }

    /**
     * Get numdocumento2.
     *
     * @return string|null
     */
    public function getNumdocumento2()
    {
        return $this->numdocumento2;
    }

    /**
     * Set tipodocumento2.
     *
     * @param string|null $tipodocumento2
     *
     * @return Historicopersonasportugal
     */
    public function setTipodocumento2($tipodocumento2 = null)
    {
        $this->tipodocumento2 = $tipodocumento2;

        return $this;
    }

    /**
     * Get tipodocumento2.
     *
     * @return string|null
     */
    public function getTipodocumento2()
    {
        return $this->tipodocumento2;
    }

    /**
     * Set fechaexpedicion2.
     *
     * @param \DateTime|null $fechaexpedicion2
     *
     * @return Historicopersonasportugal
     */
    public function setFechaexpedicion2($fechaexpedicion2 = null)
    {
        $this->fechaexpedicion2 = $fechaexpedicion2;

        return $this;
    }

    /**
     * Get fechaexpedicion2.
     *
     * @return \DateTime|null
     */
    public function getFechaexpedicion2()
    {
        return $this->fechaexpedicion2;
    }

    /**
     * Set lugarexpedicion2.
     *
     * @param string|null $lugarexpedicion2
     *
     * @return Historicopersonasportugal
     */
    public function setLugarexpedicion2($lugarexpedicion2 = null)
    {
        $this->lugarexpedicion2 = $lugarexpedicion2;

        return $this;
    }

    /**
     * Get lugarexpedicion2.
     *
     * @return string|null
     */
    public function getLugarexpedicion2()
    {
        return $this->lugarexpedicion2;
    }

    /**
     * Set numdocumento3.
     *
     * @param string|null $numdocumento3
     *
     * @return Historicopersonasportugal
     */
    public function setNumdocumento3($numdocumento3 = null)
    {
        $this->numdocumento3 = $numdocumento3;

        return $this;
    }

    /**
     * Get numdocumento3.
     *
     * @return string|null
     */
    public function getNumdocumento3()
    {
        return $this->numdocumento3;
    }

    /**
     * Set tipodocumento3.
     *
     * @param string|null $tipodocumento3
     *
     * @return Historicopersonasportugal
     */
    public function setTipodocumento3($tipodocumento3 = null)
    {
        $this->tipodocumento3 = $tipodocumento3;

        return $this;
    }

    /**
     * Get tipodocumento3.
     *
     * @return string|null
     */
    public function getTipodocumento3()
    {
        return $this->tipodocumento3;
    }

    /**
     * Set fechaexpedicion3.
     *
     * @param \DateTime|null $fechaexpedicion3
     *
     * @return Historicopersonasportugal
     */
    public function setFechaexpedicion3($fechaexpedicion3 = null)
    {
        $this->fechaexpedicion3 = $fechaexpedicion3;

        return $this;
    }

    /**
     * Get fechaexpedicion3.
     *
     * @return \DateTime|null
     */
    public function getFechaexpedicion3()
    {
        return $this->fechaexpedicion3;
    }

    /**
     * Set lugarexpedicion3.
     *
     * @param string|null $lugarexpedicion3
     *
     * @return Historicopersonasportugal
     */
    public function setLugarexpedicion3($lugarexpedicion3 = null)
    {
        $this->lugarexpedicion3 = $lugarexpedicion3;

        return $this;
    }

    /**
     * Get lugarexpedicion3.
     *
     * @return string|null
     */
    public function getLugarexpedicion3()
    {
        return $this->lugarexpedicion3;
    }

    /**
     * Set fechadenacimiento.
     *
     * @param \DateTime|null $fechadenacimiento
     *
     * @return Historicopersonasportugal
     */
    public function setFechadenacimiento($fechadenacimiento = null)
    {
        $this->fechadenacimiento = $fechadenacimiento;

        return $this;
    }

    /**
     * Get fechadenacimiento.
     *
     * @return \DateTime|null
     */
    public function getFechadenacimiento()
    {
        return $this->fechadenacimiento;
    }
}
