<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Perfilesrequisitos
 *
 * @ORM\Table(name="perfilesrequisitos", indexes={@ORM\Index(name="idperfiles_PERE_FK_idx", columns={"idperfiles"}), @ORM\Index(name="idacciones_PERE_FK_idx", columns={"idacciones"})})
 * @ORM\Entity
 */
class Perfilesrequisitos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idperfilesRequisitos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idperfilesrequisitos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="permiso", type="integer", nullable=true, options={"unsigned"=true,"comment"="0-Nada,1-Lectura,2-Edicion,3-Borrado,4-Supervision"})
     */
    private $permiso;

    /**
     * @var \Acciones
     *
     * @ORM\ManyToOne(targetEntity="Acciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idacciones", referencedColumnName="idacciones")
     * })
     */
    private $idacciones;

    /**
     * @var \Perfiles
     *
     * @ORM\ManyToOne(targetEntity="Perfiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idperfiles", referencedColumnName="idperfiles")
     * })
     */
    private $idperfiles;



    /**
     * Get idperfilesrequisitos.
     *
     * @return int
     */
    public function getIdperfilesrequisitos()
    {
        return $this->idperfilesrequisitos;
    }

    /**
     * Set permiso.
     *
     * @param int|null $permiso
     *
     * @return Perfilesrequisitos
     */
    public function setPermiso($permiso = null)
    {
        $this->permiso = $permiso;

        return $this;
    }

    /**
     * Get permiso.
     *
     * @return int|null
     */
    public function getPermiso()
    {
        return $this->permiso;
    }

    /**
     * Set idacciones.
     *
     * @param \AppBundle\Entity\Globalcar\Acciones|null $idacciones
     *
     * @return Perfilesrequisitos
     */
    public function setIdacciones(\AppBundle\Entity\Globalcar\Acciones $idacciones = null)
    {
        $this->idacciones = $idacciones;

        return $this;
    }

    /**
     * Get idacciones.
     *
     * @return \AppBundle\Entity\Globalcar\Acciones|null
     */
    public function getIdacciones()
    {
        return $this->idacciones;
    }

    /**
     * Set idperfiles.
     *
     * @param \AppBundle\Entity\Globalcar\Perfiles|null $idperfiles
     *
     * @return Perfilesrequisitos
     */
    public function setIdperfiles(\AppBundle\Entity\Globalcar\Perfiles $idperfiles = null)
    {
        $this->idperfiles = $idperfiles;

        return $this;
    }

    /**
     * Get idperfiles.
     *
     * @return \AppBundle\Entity\Globalcar\Perfiles|null
     */
    public function getIdperfiles()
    {
        return $this->idperfiles;
    }
}
