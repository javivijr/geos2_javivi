<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Impresionesrelaciones
 *
 * @ORM\Table(name="impresionesrelaciones", indexes={@ORM\Index(name="idimpresiones_IR_FK_idx", columns={"idimpresiones"}), @ORM\Index(name="idrelaciones_IR_FK_idx", columns={"idrelaciones"})})
 * @ORM\Entity
 */
class Impresionesrelaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="idimpresionesRelaciones", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idimpresionesrelaciones;

    /**
     * @var \Impresiones
     *
     * @ORM\ManyToOne(targetEntity="Impresiones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idimpresiones", referencedColumnName="idimpresiones")
     * })
     */
    private $idimpresiones;

    /**
     * @var \Relaciones
     *
     * @ORM\ManyToOne(targetEntity="Relaciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idrelaciones", referencedColumnName="idrelaciones")
     * })
     */
    private $idrelaciones;



    /**
     * Get idimpresionesrelaciones.
     *
     * @return int
     */
    public function getIdimpresionesrelaciones()
    {
        return $this->idimpresionesrelaciones;
    }

    /**
     * Set idimpresiones.
     *
     * @param \AppBundle\Entity\Globalcar\Impresiones|null $idimpresiones
     *
     * @return Impresionesrelaciones
     */
    public function setIdimpresiones(\AppBundle\Entity\Globalcar\Impresiones $idimpresiones = null)
    {
        $this->idimpresiones = $idimpresiones;

        return $this;
    }

    /**
     * Get idimpresiones.
     *
     * @return \AppBundle\Entity\Globalcar\Impresiones|null
     */
    public function getIdimpresiones()
    {
        return $this->idimpresiones;
    }

    /**
     * Set idrelaciones.
     *
     * @param \AppBundle\Entity\Globalcar\Relaciones|null $idrelaciones
     *
     * @return Impresionesrelaciones
     */
    public function setIdrelaciones(\AppBundle\Entity\Globalcar\Relaciones $idrelaciones = null)
    {
        $this->idrelaciones = $idrelaciones;

        return $this;
    }

    /**
     * Get idrelaciones.
     *
     * @return \AppBundle\Entity\Globalcar\Relaciones|null
     */
    public function getIdrelaciones()
    {
        return $this->idrelaciones;
    }
}
