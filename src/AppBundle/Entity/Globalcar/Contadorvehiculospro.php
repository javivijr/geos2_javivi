<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contadorvehiculospro
 *
 * @ORM\Table(name="contadorvehiculospro", uniqueConstraints={@ORM\UniqueConstraint(name="cvtojunto", columns={"idtitulares", "fecha"})}, indexes={@ORM\Index(name="cvidtitulares", columns={"idtitulares"}), @ORM\Index(name="cvfecha", columns={"fecha"})})
 * @ORM\Entity
 */
class Contadorvehiculospro
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontadorvehiculospro", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontadorvehiculospro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="total", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $total;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idcontadorvehiculospro.
     *
     * @return int
     */
    public function getIdcontadorvehiculospro()
    {
        return $this->idcontadorvehiculospro;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Contadorvehiculospro
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set total.
     *
     * @param int $total
     *
     * @return Contadorvehiculospro
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Contadorvehiculospro
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
