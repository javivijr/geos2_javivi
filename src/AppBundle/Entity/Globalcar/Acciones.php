<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acciones
 *
 * @ORM\Table(name="acciones")
 * @ORM\Entity
 */
class Acciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="idacciones", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idacciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=70, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo", type="string", length=120, nullable=true)
     */
    private $titulo;



    /**
     * Get idacciones.
     *
     * @return int
     */
    public function getIdacciones()
    {
        return $this->idacciones;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Acciones
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set titulo.
     *
     * @param string|null $titulo
     *
     * @return Acciones
     */
    public function setTitulo($titulo = null)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo.
     *
     * @return string|null
     */
    public function getTitulo()
    {
        return $this->titulo;
    }
}
