<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Impresionescartas
 *
 * @ORM\Table(name="impresionescartas", indexes={@ORM\Index(name="idimpresiones_IMC_FK_idx", columns={"idimpresiones"}), @ORM\Index(name="destinocartas_IMC_FK_idx", columns={"destinoCarta"})})
 * @ORM\Entity
 */
class Impresionescartas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idimpresionesCartas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idimpresionescartas;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaEnvio", type="datetime", nullable=true)
     */
    private $fechaenvio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoCorreos", type="string", length=100, nullable=true)
     */
    private $codigocorreos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="idiomaCarta", type="string", length=0, nullable=true)
     */
    private $idiomacarta;

    /**
     * @var string
     *
     * @ORM\Column(name="motivopapel", type="string", length=0, nullable=false)
     */
    private $motivopapel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subdivision", type="string", length=200, nullable=true)
     */
    private $subdivision;

    /**
     * @var \Destinoscartas
     *
     * @ORM\ManyToOne(targetEntity="Destinoscartas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="destinoCarta", referencedColumnName="iddestinosCartas")
     * })
     */
    private $destinocarta;

    /**
     * @var \Impresiones
     *
     * @ORM\ManyToOne(targetEntity="Impresiones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idimpresiones", referencedColumnName="idimpresiones")
     * })
     */
    private $idimpresiones;



    /**
     * Get idimpresionescartas.
     *
     * @return int
     */
    public function getIdimpresionescartas()
    {
        return $this->idimpresionescartas;
    }

    /**
     * Set fechaenvio.
     *
     * @param \DateTime|null $fechaenvio
     *
     * @return Impresionescartas
     */
    public function setFechaenvio($fechaenvio = null)
    {
        $this->fechaenvio = $fechaenvio;

        return $this;
    }

    /**
     * Get fechaenvio.
     *
     * @return \DateTime|null
     */
    public function getFechaenvio()
    {
        return $this->fechaenvio;
    }

    /**
     * Set codigocorreos.
     *
     * @param string|null $codigocorreos
     *
     * @return Impresionescartas
     */
    public function setCodigocorreos($codigocorreos = null)
    {
        $this->codigocorreos = $codigocorreos;

        return $this;
    }

    /**
     * Get codigocorreos.
     *
     * @return string|null
     */
    public function getCodigocorreos()
    {
        return $this->codigocorreos;
    }

    /**
     * Set idiomacarta.
     *
     * @param string|null $idiomacarta
     *
     * @return Impresionescartas
     */
    public function setIdiomacarta($idiomacarta = null)
    {
        $this->idiomacarta = $idiomacarta;

        return $this;
    }

    /**
     * Get idiomacarta.
     *
     * @return string|null
     */
    public function getIdiomacarta()
    {
        return $this->idiomacarta;
    }

    /**
     * Set motivopapel.
     *
     * @param string $motivopapel
     *
     * @return Impresionescartas
     */
    public function setMotivopapel($motivopapel)
    {
        $this->motivopapel = $motivopapel;

        return $this;
    }

    /**
     * Get motivopapel.
     *
     * @return string
     */
    public function getMotivopapel()
    {
        return $this->motivopapel;
    }

    /**
     * Set subdivision.
     *
     * @param string|null $subdivision
     *
     * @return Impresionescartas
     */
    public function setSubdivision($subdivision = null)
    {
        $this->subdivision = $subdivision;

        return $this;
    }

    /**
     * Get subdivision.
     *
     * @return string|null
     */
    public function getSubdivision()
    {
        return $this->subdivision;
    }

    /**
     * Set destinocarta.
     *
     * @param \AppBundle\Entity\Globalcar\Destinoscartas|null $destinocarta
     *
     * @return Impresionescartas
     */
    public function setDestinocarta(\AppBundle\Entity\Globalcar\Destinoscartas $destinocarta = null)
    {
        $this->destinocarta = $destinocarta;

        return $this;
    }

    /**
     * Get destinocarta.
     *
     * @return \AppBundle\Entity\Globalcar\Destinoscartas|null
     */
    public function getDestinocarta()
    {
        return $this->destinocarta;
    }

    /**
     * Set idimpresiones.
     *
     * @param \AppBundle\Entity\Globalcar\Impresiones|null $idimpresiones
     *
     * @return Impresionescartas
     */
    public function setIdimpresiones(\AppBundle\Entity\Globalcar\Impresiones $idimpresiones = null)
    {
        $this->idimpresiones = $idimpresiones;

        return $this;
    }

    /**
     * Get idimpresiones.
     *
     * @return \AppBundle\Entity\Globalcar\Impresiones|null
     */
    public function getIdimpresiones()
    {
        return $this->idimpresiones;
    }
}
