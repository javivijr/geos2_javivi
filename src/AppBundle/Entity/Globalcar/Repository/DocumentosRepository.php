<?php

namespace AppBundle\Entity\Globalcar\Repository;


use AppBundle\Entity\Globalcar\Expedientes;
use Doctrine\ORM\EntityRepository;

class DocumentosRepository extends EntityRepository
{
    public function findDocumentoMultaExpediente(Expedientes $expediente) {

        $em = $this->getEntityManager();

//        $qb = $em
//            ->createQueryBuilder()
//            ->select('doc','docdesc', 'descr')
//            ->from('AppBundle:Documentosdescriptores' ,'docdesc')
//            ->join('docdesc.iddocumentos', 'doc')
//            ->join('docdesc.iddescriptores', 'descr')
//            ->where('doc.idexpedientes = :idexpediente')
//            ->andWhere('descr.clave = :clave')
//            ->setParameter('idexpediente', 9008297)
//            ->setParameter('clave', 'MUL')
//            ->getQuery()
//            ->getResult();

        $q = $em->createQuery(
            "SELECT doc
            FROM AppBundle:Documentos doc
            JOIN AppBundle:Documentosdescriptores docdesc WITH docdesc.iddocumentos = doc.iddocumentos
            JOIN docdesc.iddescriptores descr
            WHERE doc.idexpedientes = :idexpediente
            AND descr.clave = 'MUL'");
        $q->setParameter('idexpediente', $expediente->getIdexpedientes());
        return $q->getResult();

    }
}