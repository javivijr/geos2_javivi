<?php

namespace AppBundle\Entity\Globalcar\Repository;


use AppBundle\Entity\Globalcar\Vehiculos;
use Doctrine\ORM\EntityRepository;

class HistoricovehiculosRepository extends EntityRepository
{
    public function findHistoricosPorVehiculoFecha(Vehiculos $vehiculo, \DateTime $fecha) {

        $query = $this->createQueryBuilder('hv')
            ->where('hv.idvehiculo = :vehiculo')
            ->andWhere('hv.fechaalta <= :fecha')
            ->andWhere('hv.fechabaja >= :fecha')
            ->andWhere('hv.activo = :activo')
            ->setParameter('vehiculo',$vehiculo)
            ->setParameter('fecha',$fecha)
            ->setParameter('activo','s')
            ->getQuery();

        $result = $query->getResult();

        return $result;

    }
}