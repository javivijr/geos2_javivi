<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidenciasproc
 *
 * @ORM\Table(name="incidenciasproc", indexes={@ORM\Index(name="idprocedimientos_IPR_FK_idx", columns={"idprocedimientos"}), @ORM\Index(name="idescriptores_IPR_FK_idx", columns={"iddescriptores"})})
 * @ORM\Entity
 */
class Incidenciasproc
{
    /**
     * @var int
     *
     * @ORM\Column(name="idincidenciasProc", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idincidenciasproc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=0, nullable=false)
     */
    private $estado;

    /**
     * @var \Descriptores
     *
     * @ORM\ManyToOne(targetEntity="Descriptores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddescriptores", referencedColumnName="iddescriptores")
     * })
     */
    private $iddescriptores;

    /**
     * @var \Procedimientos
     *
     * @ORM\ManyToOne(targetEntity="Procedimientos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idprocedimientos", referencedColumnName="idprocedimientos")
     * })
     */
    private $idprocedimientos;



    /**
     * Get idincidenciasproc.
     *
     * @return int
     */
    public function getIdincidenciasproc()
    {
        return $this->idincidenciasproc;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Incidenciasproc
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Incidenciasproc
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado.
     *
     * @param string $estado
     *
     * @return Incidenciasproc
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado.
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set iddescriptores.
     *
     * @param \AppBundle\Entity\Globalcar\Descriptores|null $iddescriptores
     *
     * @return Incidenciasproc
     */
    public function setIddescriptores(\AppBundle\Entity\Globalcar\Descriptores $iddescriptores = null)
    {
        $this->iddescriptores = $iddescriptores;

        return $this;
    }

    /**
     * Get iddescriptores.
     *
     * @return \AppBundle\Entity\Globalcar\Descriptores|null
     */
    public function getIddescriptores()
    {
        return $this->iddescriptores;
    }

    /**
     * Set idprocedimientos.
     *
     * @param \AppBundle\Entity\Globalcar\Procedimientos|null $idprocedimientos
     *
     * @return Incidenciasproc
     */
    public function setIdprocedimientos(\AppBundle\Entity\Globalcar\Procedimientos $idprocedimientos = null)
    {
        $this->idprocedimientos = $idprocedimientos;

        return $this;
    }

    /**
     * Get idprocedimientos.
     *
     * @return \AppBundle\Entity\Globalcar\Procedimientos|null
     */
    public function getIdprocedimientos()
    {
        return $this->idprocedimientos;
    }
}
