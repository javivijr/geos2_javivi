<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmfaq
 *
 * @ORM\Table(name="mtmfaq")
 * @ORM\Entity
 */
class Mtmfaq
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmFaq", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmfaq;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pregunta", type="string", length=300, nullable=true)
     */
    private $pregunta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="respuesta", type="text", length=65535, nullable=true)
     */
    private $respuesta;

    /**
     * @var string
     *
     * @ORM\Column(name="arval", type="string", length=0, nullable=false)
     */
    private $arval;

    /**
     * @var string
     *
     * @ORM\Column(name="otros", type="string", length=0, nullable=false)
     */
    private $otros;

    /**
     * @var string
     *
     * @ORM\Column(name="conductor", type="string", length=0, nullable=false)
     */
    private $conductor;

    /**
     * @var string
     *
     * @ORM\Column(name="condempresa", type="string", length=0, nullable=false)
     */
    private $condempresa;



    /**
     * Get idmtmfaq.
     *
     * @return int
     */
    public function getIdmtmfaq()
    {
        return $this->idmtmfaq;
    }

    /**
     * Set pregunta.
     *
     * @param string|null $pregunta
     *
     * @return Mtmfaq
     */
    public function setPregunta($pregunta = null)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta.
     *
     * @return string|null
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set respuesta.
     *
     * @param string|null $respuesta
     *
     * @return Mtmfaq
     */
    public function setRespuesta($respuesta = null)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta.
     *
     * @return string|null
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set arval.
     *
     * @param string $arval
     *
     * @return Mtmfaq
     */
    public function setArval($arval)
    {
        $this->arval = $arval;

        return $this;
    }

    /**
     * Get arval.
     *
     * @return string
     */
    public function getArval()
    {
        return $this->arval;
    }

    /**
     * Set otros.
     *
     * @param string $otros
     *
     * @return Mtmfaq
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * Get otros.
     *
     * @return string
     */
    public function getOtros()
    {
        return $this->otros;
    }

    /**
     * Set conductor.
     *
     * @param string $conductor
     *
     * @return Mtmfaq
     */
    public function setConductor($conductor)
    {
        $this->conductor = $conductor;

        return $this;
    }

    /**
     * Get conductor.
     *
     * @return string
     */
    public function getConductor()
    {
        return $this->conductor;
    }

    /**
     * Set condempresa.
     *
     * @param string $condempresa
     *
     * @return Mtmfaq
     */
    public function setCondempresa($condempresa)
    {
        $this->condempresa = $condempresa;

        return $this;
    }

    /**
     * Get condempresa.
     *
     * @return string
     */
    public function getCondempresa()
    {
        return $this->condempresa;
    }
}
