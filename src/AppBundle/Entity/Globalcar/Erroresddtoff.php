<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Erroresddtoff
 *
 * @ORM\Table(name="erroresddtoff")
 * @ORM\Entity
 */
class Erroresddtoff
{
    /**
     * @var int
     *
     * @ORM\Column(name="iderroresDdtOff", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iderroresddtoff;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoSancion", type="string", length=30, nullable=true)
     */
    private $codigosancion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="matricula", type="string", length=10, nullable=true)
     */
    private $matricula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error", type="text", length=65535, nullable=true)
     */
    private $error;



    /**
     * Get iderroresddtoff.
     *
     * @return int
     */
    public function getIderroresddtoff()
    {
        return $this->iderroresddtoff;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Erroresddtoff
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set codigosancion.
     *
     * @param string|null $codigosancion
     *
     * @return Erroresddtoff
     */
    public function setCodigosancion($codigosancion = null)
    {
        $this->codigosancion = $codigosancion;

        return $this;
    }

    /**
     * Get codigosancion.
     *
     * @return string|null
     */
    public function getCodigosancion()
    {
        return $this->codigosancion;
    }

    /**
     * Set matricula.
     *
     * @param string|null $matricula
     *
     * @return Erroresddtoff
     */
    public function setMatricula($matricula = null)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula.
     *
     * @return string|null
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set error.
     *
     * @param string|null $error
     *
     * @return Erroresddtoff
     */
    public function setError($error = null)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error.
     *
     * @return string|null
     */
    public function getError()
    {
        return $this->error;
    }
}
