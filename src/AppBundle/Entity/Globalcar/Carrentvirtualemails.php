<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrentvirtualemails
 *
 * @ORM\Table(name="carrentvirtualemails", indexes={@ORM\Index(name="raNo_CVE_IDX", columns={"raNo"}), @ORM\Index(name="tarjeta_CVE_IDX", columns={"tarjeta"})})
 * @ORM\Entity
 */
class Carrentvirtualemails
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrentVirtualEmails", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrentvirtualemails;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raNo", type="string", length=50, nullable=true)
     */
    private $rano;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=250, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tarjeta", type="string", length=50, nullable=true)
     */
    private $tarjeta;



    /**
     * Get idcarrentvirtualemails.
     *
     * @return int
     */
    public function getIdcarrentvirtualemails()
    {
        return $this->idcarrentvirtualemails;
    }

    /**
     * Set rano.
     *
     * @param string|null $rano
     *
     * @return Carrentvirtualemails
     */
    public function setRano($rano = null)
    {
        $this->rano = $rano;

        return $this;
    }

    /**
     * Get rano.
     *
     * @return string|null
     */
    public function getRano()
    {
        return $this->rano;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Carrentvirtualemails
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tarjeta.
     *
     * @param string|null $tarjeta
     *
     * @return Carrentvirtualemails
     */
    public function setTarjeta($tarjeta = null)
    {
        $this->tarjeta = $tarjeta;

        return $this;
    }

    /**
     * Get tarjeta.
     *
     * @return string|null
     */
    public function getTarjeta()
    {
        return $this->tarjeta;
    }
}
