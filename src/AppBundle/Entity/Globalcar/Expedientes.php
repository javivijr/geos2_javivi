<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientes
 *
 * @ORM\Table(name="expedientes", indexes={@ORM\Index(name="idprocedimientos_E_FK_idx", columns={"idprocedimientos"}), @ORM\Index(name="idtitulares_E_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idescritoEntrada_E_FK_idx", columns={"idescritoEntrada"}), @ORM\Index(name="idorganismos_E_FK_idx", columns={"idorganismos"}), @ORM\Index(name="idestado_E_FK_idx", columns={"idestado"}), @ORM\Index(name="idhechos_E_FK_idx", columns={"idhechos"}), @ORM\Index(name="fechaVencimiento_E_IN", columns={"fechaVencimiento"}), @ORM\Index(name="fechaNotificacion_E_IN", columns={"fechaNotificacion"}), @ORM\Index(name="codigoSancion_E_IN", columns={"codigoSancion"})})
 * @ORM\Entity
 */
class Expedientes
{
    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientes", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idexpedientes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="importe", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $importe;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaPresenta", type="datetime", nullable=true)
     */
    private $fechapresenta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaAlta", type="datetime", nullable=true)
     */
    private $fechaalta;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoSancion", type="string", length=30, nullable=true)
     */
    private $codigosancion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numeroDenuncia", type="string", length=30, nullable=true)
     */
    private $numerodenuncia;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaNotificacion", type="datetime", nullable=true)
     */
    private $fechanotificacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaVencimiento", type="datetime", nullable=true)
     */
    private $fechavencimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="tiposalida", type="string", length=0, nullable=false)
     */
    private $tiposalida;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gravedad", type="string", length=0, nullable=true)
     */
    private $gravedad;

    /**
     * @var Escritoentrada
     *
     * @ORM\ManyToOne(targetEntity="Escritoentrada")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idescritoEntrada", referencedColumnName="idescritoEntrada")
     * })
     */
    private $idescritoentrada;

    /**
     * @var Estadosexpediente
     *
     * @ORM\ManyToOne(targetEntity="Estadosexpediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idestado", referencedColumnName="idestadosExpediente")
     * })
     */
    private $idestado;

    /**
     * @var Hechos
     *
     * @ORM\ManyToOne(targetEntity="Hechos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhechos", referencedColumnName="idhechos")
     * })
     */
    private $idhechos;

    /**
     * @var Organismos
     *
     * @ORM\ManyToOne(targetEntity="Organismos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idorganismos", referencedColumnName="idorganismos")
     * })
     */
    private $idorganismos;

    /**
     * @var Procedimientos
     *
     * @ORM\ManyToOne(targetEntity="Procedimientos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idprocedimientos", referencedColumnName="idprocedimientos")
     * })
     */
    private $idprocedimientos;

    /**
     * @var Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idexpedientes.
     *
     * @return int
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set importe.
     *
     * @param string|null $importe
     *
     * @return Expedientes
     */
    public function setImporte($importe = null)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe.
     *
     * @return string|null
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set fechapresenta.
     *
     * @param \DateTime|null $fechapresenta
     *
     * @return Expedientes
     */
    public function setFechapresenta($fechapresenta = null)
    {
        $this->fechapresenta = $fechapresenta;

        return $this;
    }

    /**
     * Get fechapresenta.
     *
     * @return \DateTime|null
     */
    public function getFechapresenta()
    {
        return $this->fechapresenta;
    }

    /**
     * Set fechaalta.
     *
     * @param \DateTime|null $fechaalta
     *
     * @return Expedientes
     */
    public function setFechaalta($fechaalta = null)
    {
        $this->fechaalta = $fechaalta;

        return $this;
    }

    /**
     * Get fechaalta.
     *
     * @return \DateTime|null
     */
    public function getFechaalta()
    {
        return $this->fechaalta;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Expedientes
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set codigosancion.
     *
     * @param string|null $codigosancion
     *
     * @return Expedientes
     */
    public function setCodigosancion($codigosancion = null)
    {
        $this->codigosancion = $codigosancion;

        return $this;
    }

    /**
     * Get codigosancion.
     *
     * @return string|null
     */
    public function getCodigosancion()
    {
        return $this->codigosancion;
    }

    /**
     * Set numerodenuncia.
     *
     * @param string|null $numerodenuncia
     *
     * @return Expedientes
     */
    public function setNumerodenuncia($numerodenuncia = null)
    {
        $this->numerodenuncia = $numerodenuncia;

        return $this;
    }

    /**
     * Get numerodenuncia.
     *
     * @return string|null
     */
    public function getNumerodenuncia()
    {
        return $this->numerodenuncia;
    }

    /**
     * Set fechanotificacion.
     *
     * @param \DateTime|null $fechanotificacion
     *
     * @return Expedientes
     */
    public function setFechanotificacion($fechanotificacion = null)
    {
        $this->fechanotificacion = $fechanotificacion;

        return $this;
    }

    /**
     * Get fechanotificacion.
     *
     * @return \DateTime|null
     */
    public function getFechanotificacion()
    {
        return $this->fechanotificacion;
    }

    /**
     * Set fechavencimiento.
     *
     * @param \DateTime|null $fechavencimiento
     *
     * @return Expedientes
     */
    public function setFechavencimiento($fechavencimiento = null)
    {
        $this->fechavencimiento = $fechavencimiento;

        return $this;
    }

    /**
     * Get fechavencimiento.
     *
     * @return \DateTime|null
     */
    public function getFechavencimiento()
    {
        return $this->fechavencimiento;
    }

    /**
     * Set tiposalida.
     *
     * @param string $tiposalida
     *
     * @return Expedientes
     */
    public function setTiposalida($tiposalida)
    {
        $this->tiposalida = $tiposalida;

        return $this;
    }

    /**
     * Get tiposalida.
     *
     * @return string
     */
    public function getTiposalida()
    {
        return $this->tiposalida;
    }

    /**
     * Set gravedad.
     *
     * @param string|null $gravedad
     *
     * @return Expedientes
     */
    public function setGravedad($gravedad = null)
    {
        $this->gravedad = $gravedad;

        return $this;
    }

    /**
     * Get gravedad.
     *
     * @return string|null
     */
    public function getGravedad()
    {
        return $this->gravedad;
    }

    /**
     * Set idescritoentrada.
     *
     * @param \AppBundle\Entity\Globalcar\Escritoentrada|null $idescritoentrada
     *
     * @return Expedientes
     */
    public function setIdescritoentrada(\AppBundle\Entity\Globalcar\Escritoentrada $idescritoentrada = null)
    {
        $this->idescritoentrada = $idescritoentrada;

        return $this;
    }

    /**
     * Get idescritoentrada.
     *
     * @return \AppBundle\Entity\Globalcar\Escritoentrada|null
     */
    public function getIdescritoentrada()
    {
        return $this->idescritoentrada;
    }

    /**
     * Set idestado.
     *
     * @param \AppBundle\Entity\Globalcar\Estadosexpediente|null $idestado
     *
     * @return Expedientes
     */
    public function setIdestado(\AppBundle\Entity\Globalcar\Estadosexpediente $idestado = null)
    {
        $this->idestado = $idestado;

        return $this;
    }

    /**
     * Get idestado.
     *
     * @return \AppBundle\Entity\Globalcar\Estadosexpediente|null
     */
    public function getIdestado()
    {
        return $this->idestado;
    }

    /**
     * Set idhechos.
     *
     * @param \AppBundle\Entity\Globalcar\Hechos|null $idhechos
     *
     * @return Expedientes
     */
    public function setIdhechos(\AppBundle\Entity\Globalcar\Hechos $idhechos = null)
    {
        $this->idhechos = $idhechos;

        return $this;
    }

    /**
     * Get idhechos.
     *
     * @return \AppBundle\Entity\Globalcar\Hechos|null
     */
    public function getIdhechos()
    {
        return $this->idhechos;
    }

    /**
     * Set idorganismos.
     *
     * @param \AppBundle\Entity\Globalcar\Organismos|null $idorganismos
     *
     * @return Expedientes
     */
    public function setIdorganismos(\AppBundle\Entity\Globalcar\Organismos $idorganismos = null)
    {
        $this->idorganismos = $idorganismos;

        return $this;
    }

    /**
     * Get idorganismos.
     *
     * @return \AppBundle\Entity\Globalcar\Organismos|null
     */
    public function getIdorganismos()
    {
        return $this->idorganismos;
    }

    /**
     * Set idprocedimientos.
     *
     * @param \AppBundle\Entity\Globalcar\Procedimientos|null $idprocedimientos
     *
     * @return Expedientes
     */
    public function setIdprocedimientos(\AppBundle\Entity\Globalcar\Procedimientos $idprocedimientos = null)
    {
        $this->idprocedimientos = $idprocedimientos;

        return $this;
    }

    /**
     * Get idprocedimientos.
     *
     * @return \AppBundle\Entity\Globalcar\Procedimientos|null
     */
    public function getIdprocedimientos()
    {
        return $this->idprocedimientos;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Expedientes
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
