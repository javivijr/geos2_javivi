<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrenteuropcar
 *
 * @ORM\Table(name="carrenteuropcar", indexes={@ORM\Index(name="matricula", columns={"matricula"}), @ORM\Index(name="fecha_inicio", columns={"fecha_checkout"}), @ORM\Index(name="fecha_fin", columns={"fecha_checkin"}), @ORM\Index(name="busquedachula", columns={"matricula", "fecha_checkout", "fecha_checkin"}), @ORM\Index(name="contrato", columns={"contrato"}), @ORM\Index(name="orarlrrlrl", columns={"tipo_movimiento"}), @ORM\Index(name="fawwlwwl", columns={"faw"}), @ORM\Index(name="sha1Europcar", columns={"sha1"})})
 * @ORM\Entity
 */
class Carrenteuropcar
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrenteuropcar", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrenteuropcar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contrato", type="string", length=45, nullable=true)
     */
    private $contrato;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idconductor_uie", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $idconductorUie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="matricula", type="string", length=12, nullable=true)
     */
    private $matricula;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehiculo_propietario", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $vehiculoPropietario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="oficina_checkout", type="string", length=45, nullable=true)
     */
    private $oficinaCheckout;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_checkout", type="datetime", nullable=true)
     */
    private $fechaCheckout;

    /**
     * @var string|null
     *
     * @ORM\Column(name="oficina_checkin", type="string", length=45, nullable=true)
     */
    private $oficinaCheckin;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_checkin", type="datetime", nullable=true)
     */
    private $fechaCheckin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modelo", type="string", length=45, nullable=true)
     */
    private $modelo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido_conductor", type="string", length=245, nullable=true)
     */
    private $apellidoConductor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre_conductor", type="string", length=245, nullable=true)
     */
    private $nombreConductor;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_nacimiento_conductor", type="datetime", nullable=true)
     */
    private $fechaNacimientoConductor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion1", type="string", length=250, nullable=true)
     */
    private $direccion1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion2", type="string", length=250, nullable=true)
     */
    private $direccion2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion3", type="string", length=250, nullable=true)
     */
    private $direccion3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion4", type="string", length=250, nullable=true)
     */
    private $direccion4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=45, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=145, nullable=true)
     */
    private $provincia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ciudad", type="string", length=145, nullable=true)
     */
    private $ciudad;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais_residencia", type="string", length=45, nullable=true)
     */
    private $paisResidencia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_permiso_conducir", type="string", length=45, nullable=true)
     */
    private $numeroPermisoConducir;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_expedicion_permiso", type="datetime", nullable=true)
     */
    private $fechaExpedicionPermiso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lugar_expedicion_permiso", type="string", length=45, nullable=true)
     */
    private $lugarExpedicionPermiso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo_pais_expedicion_permiso", type="string", length=5, nullable=true)
     */
    private $codigoPaisExpedicionPermiso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="faw", type="string", length=5, nullable=true)
     */
    private $faw;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_movimiento", type="string", length=5, nullable=true)
     */
    private $tipoMovimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="sha1", type="string", length=40, nullable=false, options={"fixed"=true})
     */
    private $sha1;



    /**
     * Get idcarrenteuropcar.
     *
     * @return int
     */
    public function getIdcarrenteuropcar()
    {
        return $this->idcarrenteuropcar;
    }

    /**
     * Set contrato.
     *
     * @param string|null $contrato
     *
     * @return Carrenteuropcar
     */
    public function setContrato($contrato = null)
    {
        $this->contrato = $contrato;

        return $this;
    }

    /**
     * Get contrato.
     *
     * @return string|null
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * Set idconductorUie.
     *
     * @param int|null $idconductorUie
     *
     * @return Carrenteuropcar
     */
    public function setIdconductorUie($idconductorUie = null)
    {
        $this->idconductorUie = $idconductorUie;

        return $this;
    }

    /**
     * Get idconductorUie.
     *
     * @return int|null
     */
    public function getIdconductorUie()
    {
        return $this->idconductorUie;
    }

    /**
     * Set matricula.
     *
     * @param string|null $matricula
     *
     * @return Carrenteuropcar
     */
    public function setMatricula($matricula = null)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula.
     *
     * @return string|null
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set vehiculoPropietario.
     *
     * @param int|null $vehiculoPropietario
     *
     * @return Carrenteuropcar
     */
    public function setVehiculoPropietario($vehiculoPropietario = null)
    {
        $this->vehiculoPropietario = $vehiculoPropietario;

        return $this;
    }

    /**
     * Get vehiculoPropietario.
     *
     * @return int|null
     */
    public function getVehiculoPropietario()
    {
        return $this->vehiculoPropietario;
    }

    /**
     * Set oficinaCheckout.
     *
     * @param string|null $oficinaCheckout
     *
     * @return Carrenteuropcar
     */
    public function setOficinaCheckout($oficinaCheckout = null)
    {
        $this->oficinaCheckout = $oficinaCheckout;

        return $this;
    }

    /**
     * Get oficinaCheckout.
     *
     * @return string|null
     */
    public function getOficinaCheckout()
    {
        return $this->oficinaCheckout;
    }

    /**
     * Set fechaCheckout.
     *
     * @param \DateTime|null $fechaCheckout
     *
     * @return Carrenteuropcar
     */
    public function setFechaCheckout($fechaCheckout = null)
    {
        $this->fechaCheckout = $fechaCheckout;

        return $this;
    }

    /**
     * Get fechaCheckout.
     *
     * @return \DateTime|null
     */
    public function getFechaCheckout()
    {
        return $this->fechaCheckout;
    }

    /**
     * Set oficinaCheckin.
     *
     * @param string|null $oficinaCheckin
     *
     * @return Carrenteuropcar
     */
    public function setOficinaCheckin($oficinaCheckin = null)
    {
        $this->oficinaCheckin = $oficinaCheckin;

        return $this;
    }

    /**
     * Get oficinaCheckin.
     *
     * @return string|null
     */
    public function getOficinaCheckin()
    {
        return $this->oficinaCheckin;
    }

    /**
     * Set fechaCheckin.
     *
     * @param \DateTime|null $fechaCheckin
     *
     * @return Carrenteuropcar
     */
    public function setFechaCheckin($fechaCheckin = null)
    {
        $this->fechaCheckin = $fechaCheckin;

        return $this;
    }

    /**
     * Get fechaCheckin.
     *
     * @return \DateTime|null
     */
    public function getFechaCheckin()
    {
        return $this->fechaCheckin;
    }

    /**
     * Set modelo.
     *
     * @param string|null $modelo
     *
     * @return Carrenteuropcar
     */
    public function setModelo($modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo.
     *
     * @return string|null
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set apellidoConductor.
     *
     * @param string|null $apellidoConductor
     *
     * @return Carrenteuropcar
     */
    public function setApellidoConductor($apellidoConductor = null)
    {
        $this->apellidoConductor = $apellidoConductor;

        return $this;
    }

    /**
     * Get apellidoConductor.
     *
     * @return string|null
     */
    public function getApellidoConductor()
    {
        return $this->apellidoConductor;
    }

    /**
     * Set nombreConductor.
     *
     * @param string|null $nombreConductor
     *
     * @return Carrenteuropcar
     */
    public function setNombreConductor($nombreConductor = null)
    {
        $this->nombreConductor = $nombreConductor;

        return $this;
    }

    /**
     * Get nombreConductor.
     *
     * @return string|null
     */
    public function getNombreConductor()
    {
        return $this->nombreConductor;
    }

    /**
     * Set fechaNacimientoConductor.
     *
     * @param \DateTime|null $fechaNacimientoConductor
     *
     * @return Carrenteuropcar
     */
    public function setFechaNacimientoConductor($fechaNacimientoConductor = null)
    {
        $this->fechaNacimientoConductor = $fechaNacimientoConductor;

        return $this;
    }

    /**
     * Get fechaNacimientoConductor.
     *
     * @return \DateTime|null
     */
    public function getFechaNacimientoConductor()
    {
        return $this->fechaNacimientoConductor;
    }

    /**
     * Set direccion1.
     *
     * @param string|null $direccion1
     *
     * @return Carrenteuropcar
     */
    public function setDireccion1($direccion1 = null)
    {
        $this->direccion1 = $direccion1;

        return $this;
    }

    /**
     * Get direccion1.
     *
     * @return string|null
     */
    public function getDireccion1()
    {
        return $this->direccion1;
    }

    /**
     * Set direccion2.
     *
     * @param string|null $direccion2
     *
     * @return Carrenteuropcar
     */
    public function setDireccion2($direccion2 = null)
    {
        $this->direccion2 = $direccion2;

        return $this;
    }

    /**
     * Get direccion2.
     *
     * @return string|null
     */
    public function getDireccion2()
    {
        return $this->direccion2;
    }

    /**
     * Set direccion3.
     *
     * @param string|null $direccion3
     *
     * @return Carrenteuropcar
     */
    public function setDireccion3($direccion3 = null)
    {
        $this->direccion3 = $direccion3;

        return $this;
    }

    /**
     * Get direccion3.
     *
     * @return string|null
     */
    public function getDireccion3()
    {
        return $this->direccion3;
    }

    /**
     * Set direccion4.
     *
     * @param string|null $direccion4
     *
     * @return Carrenteuropcar
     */
    public function setDireccion4($direccion4 = null)
    {
        $this->direccion4 = $direccion4;

        return $this;
    }

    /**
     * Get direccion4.
     *
     * @return string|null
     */
    public function getDireccion4()
    {
        return $this->direccion4;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Carrenteuropcar
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Carrenteuropcar
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set ciudad.
     *
     * @param string|null $ciudad
     *
     * @return Carrenteuropcar
     */
    public function setCiudad($ciudad = null)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad.
     *
     * @return string|null
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set paisResidencia.
     *
     * @param string|null $paisResidencia
     *
     * @return Carrenteuropcar
     */
    public function setPaisResidencia($paisResidencia = null)
    {
        $this->paisResidencia = $paisResidencia;

        return $this;
    }

    /**
     * Get paisResidencia.
     *
     * @return string|null
     */
    public function getPaisResidencia()
    {
        return $this->paisResidencia;
    }

    /**
     * Set numeroPermisoConducir.
     *
     * @param string|null $numeroPermisoConducir
     *
     * @return Carrenteuropcar
     */
    public function setNumeroPermisoConducir($numeroPermisoConducir = null)
    {
        $this->numeroPermisoConducir = $numeroPermisoConducir;

        return $this;
    }

    /**
     * Get numeroPermisoConducir.
     *
     * @return string|null
     */
    public function getNumeroPermisoConducir()
    {
        return $this->numeroPermisoConducir;
    }

    /**
     * Set fechaExpedicionPermiso.
     *
     * @param \DateTime|null $fechaExpedicionPermiso
     *
     * @return Carrenteuropcar
     */
    public function setFechaExpedicionPermiso($fechaExpedicionPermiso = null)
    {
        $this->fechaExpedicionPermiso = $fechaExpedicionPermiso;

        return $this;
    }

    /**
     * Get fechaExpedicionPermiso.
     *
     * @return \DateTime|null
     */
    public function getFechaExpedicionPermiso()
    {
        return $this->fechaExpedicionPermiso;
    }

    /**
     * Set lugarExpedicionPermiso.
     *
     * @param string|null $lugarExpedicionPermiso
     *
     * @return Carrenteuropcar
     */
    public function setLugarExpedicionPermiso($lugarExpedicionPermiso = null)
    {
        $this->lugarExpedicionPermiso = $lugarExpedicionPermiso;

        return $this;
    }

    /**
     * Get lugarExpedicionPermiso.
     *
     * @return string|null
     */
    public function getLugarExpedicionPermiso()
    {
        return $this->lugarExpedicionPermiso;
    }

    /**
     * Set codigoPaisExpedicionPermiso.
     *
     * @param string|null $codigoPaisExpedicionPermiso
     *
     * @return Carrenteuropcar
     */
    public function setCodigoPaisExpedicionPermiso($codigoPaisExpedicionPermiso = null)
    {
        $this->codigoPaisExpedicionPermiso = $codigoPaisExpedicionPermiso;

        return $this;
    }

    /**
     * Get codigoPaisExpedicionPermiso.
     *
     * @return string|null
     */
    public function getCodigoPaisExpedicionPermiso()
    {
        return $this->codigoPaisExpedicionPermiso;
    }

    /**
     * Set faw.
     *
     * @param string|null $faw
     *
     * @return Carrenteuropcar
     */
    public function setFaw($faw = null)
    {
        $this->faw = $faw;

        return $this;
    }

    /**
     * Get faw.
     *
     * @return string|null
     */
    public function getFaw()
    {
        return $this->faw;
    }

    /**
     * Set tipoMovimiento.
     *
     * @param string|null $tipoMovimiento
     *
     * @return Carrenteuropcar
     */
    public function setTipoMovimiento($tipoMovimiento = null)
    {
        $this->tipoMovimiento = $tipoMovimiento;

        return $this;
    }

    /**
     * Get tipoMovimiento.
     *
     * @return string|null
     */
    public function getTipoMovimiento()
    {
        return $this->tipoMovimiento;
    }

    /**
     * Set sha1.
     *
     * @param string $sha1
     *
     * @return Carrenteuropcar
     */
    public function setSha1($sha1)
    {
        $this->sha1 = $sha1;

        return $this;
    }

    /**
     * Get sha1.
     *
     * @return string
     */
    public function getSha1()
    {
        return $this->sha1;
    }
}
