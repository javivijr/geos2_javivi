<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuariosmtm
 *
 * @ORM\Table(name="usuariosmtm", indexes={@ORM\Index(name="usuario_UM_I", columns={"usuario"}), @ORM\Index(name="usuario_mtm_email_idx", columns={"email"})})
 * @ORM\Entity
 */
class Usuariosmtm
{
    /**
     * @var int
     *
     * @ORM\Column(name="idusuariosMtm", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuariosmtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=200, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pass", type="string", length=128, nullable=true, options={"fixed"=true})
     */
    private $pass;

    /**
     * @var string|null
     *
     * @ORM\Column(name="aleatorio", type="string", length=32, nullable=true, options={"fixed"=true})
     */
    private $aleatorio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="bloqueo", type="string", length=0, nullable=false)
     */
    private $bloqueo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ultimoAcceso", type="datetime", nullable=true)
     */
    private $ultimoacceso;

    /**
     * @var string
     *
     * @ORM\Column(name="noEnvio", type="string", length=0, nullable=false)
     */
    private $noenvio;

    /**
     * @var string
     *
     * @ORM\Column(name="noMailIdent", type="string", length=0, nullable=false)
     */
    private $nomailident;

    /**
     * @var string
     *
     * @ORM\Column(name="bloqueoMtm", type="string", length=0, nullable=false)
     */
    private $bloqueomtm;

    /**
     * @var string
     *
     * @ORM\Column(name="activo", type="string", length=0, nullable=false)
     */
    private $activo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idant", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $idant;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idtitulares", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $idtitulares;

    /**
     * @var int
     *
     * @ORM\Column(name="perfil", type="integer", nullable=false, options={"default"="500"})
     */
    private $perfil = '500';

    /**
     * @var string
     *
     * @ORM\Column(name="recibirinfo", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $recibirinfo = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="sololectura", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $sololectura = 'n';



    /**
     * Get idusuariosmtm.
     *
     * @return int
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Usuariosmtm
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set pass.
     *
     * @param string|null $pass
     *
     * @return Usuariosmtm
     */
    public function setPass($pass = null)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass.
     *
     * @return string|null
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set aleatorio.
     *
     * @param string|null $aleatorio
     *
     * @return Usuariosmtm
     */
    public function setAleatorio($aleatorio = null)
    {
        $this->aleatorio = $aleatorio;

        return $this;
    }

    /**
     * Get aleatorio.
     *
     * @return string|null
     */
    public function getAleatorio()
    {
        return $this->aleatorio;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Usuariosmtm
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Usuariosmtm
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set bloqueo.
     *
     * @param string $bloqueo
     *
     * @return Usuariosmtm
     */
    public function setBloqueo($bloqueo)
    {
        $this->bloqueo = $bloqueo;

        return $this;
    }

    /**
     * Get bloqueo.
     *
     * @return string
     */
    public function getBloqueo()
    {
        return $this->bloqueo;
    }

    /**
     * Set ultimoacceso.
     *
     * @param \DateTime|null $ultimoacceso
     *
     * @return Usuariosmtm
     */
    public function setUltimoacceso($ultimoacceso = null)
    {
        $this->ultimoacceso = $ultimoacceso;

        return $this;
    }

    /**
     * Get ultimoacceso.
     *
     * @return \DateTime|null
     */
    public function getUltimoacceso()
    {
        return $this->ultimoacceso;
    }

    /**
     * Set noenvio.
     *
     * @param string $noenvio
     *
     * @return Usuariosmtm
     */
    public function setNoenvio($noenvio)
    {
        $this->noenvio = $noenvio;

        return $this;
    }

    /**
     * Get noenvio.
     *
     * @return string
     */
    public function getNoenvio()
    {
        return $this->noenvio;
    }

    /**
     * Set nomailident.
     *
     * @param string $nomailident
     *
     * @return Usuariosmtm
     */
    public function setNomailident($nomailident)
    {
        $this->nomailident = $nomailident;

        return $this;
    }

    /**
     * Get nomailident.
     *
     * @return string
     */
    public function getNomailident()
    {
        return $this->nomailident;
    }

    /**
     * Set bloqueomtm.
     *
     * @param string $bloqueomtm
     *
     * @return Usuariosmtm
     */
    public function setBloqueomtm($bloqueomtm)
    {
        $this->bloqueomtm = $bloqueomtm;

        return $this;
    }

    /**
     * Get bloqueomtm.
     *
     * @return string
     */
    public function getBloqueomtm()
    {
        return $this->bloqueomtm;
    }

    /**
     * Set activo.
     *
     * @param string $activo
     *
     * @return Usuariosmtm
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo.
     *
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set idant.
     *
     * @param int|null $idant
     *
     * @return Usuariosmtm
     */
    public function setIdant($idant = null)
    {
        $this->idant = $idant;

        return $this;
    }

    /**
     * Get idant.
     *
     * @return int|null
     */
    public function getIdant()
    {
        return $this->idant;
    }

    /**
     * Set idtitulares.
     *
     * @param int|null $idtitulares
     *
     * @return Usuariosmtm
     */
    public function setIdtitulares($idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return int|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set perfil.
     *
     * @param int $perfil
     *
     * @return Usuariosmtm
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil.
     *
     * @return int
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set recibirinfo.
     *
     * @param string $recibirinfo
     *
     * @return Usuariosmtm
     */
    public function setRecibirinfo($recibirinfo)
    {
        $this->recibirinfo = $recibirinfo;

        return $this;
    }

    /**
     * Get recibirinfo.
     *
     * @return string
     */
    public function getRecibirinfo()
    {
        return $this->recibirinfo;
    }

    /**
     * Set sololectura.
     *
     * @param string $sololectura
     *
     * @return Usuariosmtm
     */
    public function setSololectura($sololectura)
    {
        $this->sololectura = $sololectura;

        return $this;
    }

    /**
     * Get sololectura.
     *
     * @return string
     */
    public function getSololectura()
    {
        return $this->sololectura;
    }
}
