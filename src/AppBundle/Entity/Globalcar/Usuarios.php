<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", uniqueConstraints={@ORM\UniqueConstraint(name="usuario_UNIQUE", columns={"usuario"})}, indexes={@ORM\Index(name="idperfiles_USU_FK_idx", columns={"idperfiles"})})
 * @ORM\Entity
 */
class Usuarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="idusuarios", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuarios;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var int|null
     *
     * @ORM\Column(name="extension", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $extension;

    /**
     * @var string|null
     *
     * @ORM\Column(name="banderas", type="string", length=40, nullable=true)
     */
    private $banderas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="avatar", type="string", length=45, nullable=true)
     */
    private $avatar;

    /**
     * @var Perfiles
     *
     * @ORM\ManyToOne(targetEntity="Perfiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idperfiles", referencedColumnName="idperfiles")
     * })
     */
    private $idperfiles;



    /**
     * Get idusuarios.
     *
     * @return int
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Usuarios
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Usuarios
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Usuarios
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set extension.
     *
     * @param int|null $extension
     *
     * @return Usuarios
     */
    public function setExtension($extension = null)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension.
     *
     * @return int|null
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set banderas.
     *
     * @param string|null $banderas
     *
     * @return Usuarios
     */
    public function setBanderas($banderas = null)
    {
        $this->banderas = $banderas;

        return $this;
    }

    /**
     * Get banderas.
     *
     * @return string|null
     */
    public function getBanderas()
    {
        return $this->banderas;
    }

    /**
     * Set avatar.
     *
     * @param string|null $avatar
     *
     * @return Usuarios
     */
    public function setAvatar($avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar.
     *
     * @return string|null
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set idperfiles.
     *
     * @param \AppBundle\Entity\Globalcar\Perfiles|null $idperfiles
     *
     * @return Usuarios
     */
    public function setIdperfiles(\AppBundle\Entity\Globalcar\Perfiles $idperfiles = null)
    {
        $this->idperfiles = $idperfiles;

        return $this;
    }

    /**
     * Get idperfiles.
     *
     * @return \AppBundle\Entity\Globalcar\Perfiles|null
     */
    public function getIdperfiles()
    {
        return $this->idperfiles;
    }
}
