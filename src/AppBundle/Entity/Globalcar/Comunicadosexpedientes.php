<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comunicadosexpedientes
 *
 * @ORM\Table(name="comunicadosexpedientes", indexes={@ORM\Index(name="idexpedientes_CE_FK_idx", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Comunicadosexpedientes
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcomunicadosExpedientes", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcomunicadosexpedientes;

    /**
     * @var string
     *
     * @ORM\Column(name="publico", type="string", length=0, nullable=false)
     */
    private $publico;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comunicado", type="text", length=65535, nullable=true)
     */
    private $comunicado;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idcomunicadosexpedientes.
     *
     * @return int
     */
    public function getIdcomunicadosexpedientes()
    {
        return $this->idcomunicadosexpedientes;
    }

    /**
     * Set publico.
     *
     * @param string $publico
     *
     * @return Comunicadosexpedientes
     */
    public function setPublico($publico)
    {
        $this->publico = $publico;

        return $this;
    }

    /**
     * Get publico.
     *
     * @return string
     */
    public function getPublico()
    {
        return $this->publico;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Comunicadosexpedientes
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Comunicadosexpedientes
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comunicado.
     *
     * @param string|null $comunicado
     *
     * @return Comunicadosexpedientes
     */
    public function setComunicado($comunicado = null)
    {
        $this->comunicado = $comunicado;

        return $this;
    }

    /**
     * Get comunicado.
     *
     * @return string|null
     */
    public function getComunicado()
    {
        return $this->comunicado;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Comunicadosexpedientes
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
