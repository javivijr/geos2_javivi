<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descriptorcategoria
 *
 * @ORM\Table(name="DescriptorCategoria")
 * @ORM\Entity
 */
class Descriptorcategoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddescriptorcategoria", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddescriptorcategoria;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=500, nullable=false)
     */
    private $categoria;



    /**
     * Get iddescriptorcategoria.
     *
     * @return int
     */
    public function getIddescriptorcategoria()
    {
        return $this->iddescriptorcategoria;
    }

    /**
     * Set categoria.
     *
     * @param string $categoria
     *
     * @return Descriptorcategoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria.
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
