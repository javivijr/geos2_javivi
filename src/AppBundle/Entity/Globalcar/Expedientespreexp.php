<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientespreexp
 *
 * @ORM\Table(name="expedientespreexp", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_UNIQUE", columns={"idexpedientes"})}, indexes={@ORM\Index(name="idexpedientes_EPE_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idpreExpedientes_EPE_FK_idx", columns={"idpreExpedientes"})})
 * @ORM\Entity
 */
class Expedientespreexp
{
    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientesPreExp", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idexpedientespreexp;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Preexpedientes
     *
     * @ORM\ManyToOne(targetEntity="Preexpedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpreExpedientes", referencedColumnName="idpreExpedientes")
     * })
     */
    private $idpreexpedientes;



    /**
     * Get idexpedientespreexp.
     *
     * @return int
     */
    public function getIdexpedientespreexp()
    {
        return $this->idexpedientespreexp;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Expedientespreexp
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idpreexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Preexpedientes|null $idpreexpedientes
     *
     * @return Expedientespreexp
     */
    public function setIdpreexpedientes(\AppBundle\Entity\Globalcar\Preexpedientes $idpreexpedientes = null)
    {
        $this->idpreexpedientes = $idpreexpedientes;

        return $this;
    }

    /**
     * Get idpreexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Preexpedientes|null
     */
    public function getIdpreexpedientes()
    {
        return $this->idpreexpedientes;
    }
}
