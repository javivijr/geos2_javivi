<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmregistrotemp
 *
 * @ORM\Table(name="mtmregistrotemp", indexes={@ORM\Index(name="idexpedientes_RTd_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idpreexpedientes_RTd_FK_idx", columns={"idpreexpedientes"}), @ORM\Index(name="idusuariosmtm_RTd_FK_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Mtmregistrotemp
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmregistrotemp", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmregistrotemp;

    /**
     * @var string
     *
     * @ORM\Column(name="clave", type="string", length=32, nullable=false, options={"fixed"=true})
     */
    private $clave;

    /**
     * @var string
     *
     * @ORM\Column(name="usado", type="string", length=0, nullable=false)
     */
    private $usado;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="idclientes", type="string", length=100, nullable=true)
     */
    private $idclientes;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Preexpedientes
     *
     * @ORM\ManyToOne(targetEntity="Preexpedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpreexpedientes", referencedColumnName="idpreExpedientes")
     * })
     */
    private $idpreexpedientes;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmregistrotemp.
     *
     * @return int
     */
    public function getIdmtmregistrotemp()
    {
        return $this->idmtmregistrotemp;
    }

    /**
     * Set clave.
     *
     * @param string $clave
     *
     * @return Mtmregistrotemp
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave.
     *
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set usado.
     *
     * @param string $usado
     *
     * @return Mtmregistrotemp
     */
    public function setUsado($usado)
    {
        $this->usado = $usado;

        return $this;
    }

    /**
     * Get usado.
     *
     * @return string
     */
    public function getUsado()
    {
        return $this->usado;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Mtmregistrotemp
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set idclientes.
     *
     * @param string|null $idclientes
     *
     * @return Mtmregistrotemp
     */
    public function setIdclientes($idclientes = null)
    {
        $this->idclientes = $idclientes;

        return $this;
    }

    /**
     * Get idclientes.
     *
     * @return string|null
     */
    public function getIdclientes()
    {
        return $this->idclientes;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Mtmregistrotemp
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idpreexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Preexpedientes|null $idpreexpedientes
     *
     * @return Mtmregistrotemp
     */
    public function setIdpreexpedientes(\AppBundle\Entity\Globalcar\Preexpedientes $idpreexpedientes = null)
    {
        $this->idpreexpedientes = $idpreexpedientes;

        return $this;
    }

    /**
     * Get idpreexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Preexpedientes|null
     */
    public function getIdpreexpedientes()
    {
        return $this->idpreexpedientes;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmregistrotemp
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
