<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmlogsubidamasiva
 *
 * @ORM\Table(name="mtmlogsubidamasiva", indexes={@ORM\Index(name="idusuariosmtm_MLSM_FK_idx", columns={"idusuariosmtm"}), @ORM\Index(name="fecha_MLSM_IDX", columns={"fecha"})})
 * @ORM\Entity
 */
class Mtmlogsubidamasiva
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmlogsubidamasiva", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmlogsubidamasiva;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="fichero", type="string", length=200, nullable=false)
     */
    private $fichero;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmlogsubidamasiva.
     *
     * @return int
     */
    public function getIdmtmlogsubidamasiva()
    {
        return $this->idmtmlogsubidamasiva;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Mtmlogsubidamasiva
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fichero.
     *
     * @param string $fichero
     *
     * @return Mtmlogsubidamasiva
     */
    public function setFichero($fichero)
    {
        $this->fichero = $fichero;

        return $this;
    }

    /**
     * Get fichero.
     *
     * @return string
     */
    public function getFichero()
    {
        return $this->fichero;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmlogsubidamasiva
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
