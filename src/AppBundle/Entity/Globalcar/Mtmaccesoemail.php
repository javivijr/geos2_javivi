<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmaccesoemail
 *
 * @ORM\Table(name="mtmaccesoemail", indexes={@ORM\Index(name="idusuariosmtm_MAE_FK_idx", columns={"idusuariosmtm"}), @ORM\Index(name="idtitulares_MAE_FK_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Mtmaccesoemail
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmaccesoemail", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmaccesoemail;

    /**
     * @var int
     *
     * @ORM\Column(name="perfil", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $perfil;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoCBase", type="string", length=0, nullable=false)
     */
    private $tipocbase;

    /**
     * @var string
     *
     * @ORM\Column(name="envMailCB", type="string", length=0, nullable=false)
     */
    private $envmailcb;

    /**
     * @var string
     *
     * @ORM\Column(name="copias_emails", type="string", length=500, nullable=false)
     */
    private $copiasEmails;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmaccesoemail.
     *
     * @return int
     */
    public function getIdmtmaccesoemail()
    {
        return $this->idmtmaccesoemail;
    }

    /**
     * Set perfil.
     *
     * @param int $perfil
     *
     * @return Mtmaccesoemail
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil.
     *
     * @return int
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set tipocbase.
     *
     * @param string $tipocbase
     *
     * @return Mtmaccesoemail
     */
    public function setTipocbase($tipocbase)
    {
        $this->tipocbase = $tipocbase;

        return $this;
    }

    /**
     * Get tipocbase.
     *
     * @return string
     */
    public function getTipocbase()
    {
        return $this->tipocbase;
    }

    /**
     * Set envmailcb.
     *
     * @param string $envmailcb
     *
     * @return Mtmaccesoemail
     */
    public function setEnvmailcb($envmailcb)
    {
        $this->envmailcb = $envmailcb;

        return $this;
    }

    /**
     * Get envmailcb.
     *
     * @return string
     */
    public function getEnvmailcb()
    {
        return $this->envmailcb;
    }

    /**
     * Set copiasEmails.
     *
     * @param string $copiasEmails
     *
     * @return Mtmaccesoemail
     */
    public function setCopiasEmails($copiasEmails)
    {
        $this->copiasEmails = $copiasEmails;

        return $this;
    }

    /**
     * Get copiasEmails.
     *
     * @return string
     */
    public function getCopiasEmails()
    {
        return $this->copiasEmails;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmaccesoemail
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmaccesoemail
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
