<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tmunicipiosservei
 *
 * @ORM\Table(name="tmunicipiosservei", uniqueConstraints={@ORM\UniqueConstraint(name="pk_muniserveimierda", columns={"cp", "municipio"})})
 * @ORM\Entity
 */
class Tmunicipiosservei
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtmunicipiosservei", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtmunicipiosservei;

    /**
     * @var int
     *
     * @ORM\Column(name="cp", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="municipio", type="string", length=250, nullable=true)
     */
    private $municipio;



    /**
     * Get idtmunicipiosservei.
     *
     * @return int
     */
    public function getIdtmunicipiosservei()
    {
        return $this->idtmunicipiosservei;
    }

    /**
     * Set cp.
     *
     * @param int $cp
     *
     * @return Tmunicipiosservei
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return int
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set municipio.
     *
     * @param string|null $municipio
     *
     * @return Tmunicipiosservei
     */
    public function setMunicipio($municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio.
     *
     * @return string|null
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }
}
