<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipocalle
 *
 * @ORM\Table(name="tipocalle")
 * @ORM\Entity
 */
class Tipocalle
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtipoCalle", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtipocalle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="clave", type="string", length=4, nullable=true)
     */
    private $clave;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="string", length=50, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valore", type="string", length=50, nullable=true)
     */
    private $valore;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valorp", type="string", length=50, nullable=true)
     */
    private $valorp;



    /**
     * Get idtipocalle.
     *
     * @return int
     */
    public function getIdtipocalle()
    {
        return $this->idtipocalle;
    }

    /**
     * Set clave.
     *
     * @param string|null $clave
     *
     * @return Tipocalle
     */
    public function setClave($clave = null)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave.
     *
     * @return string|null
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set valor.
     *
     * @param string|null $valor
     *
     * @return Tipocalle
     */
    public function setValor($valor = null)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor.
     *
     * @return string|null
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set valore.
     *
     * @param string|null $valore
     *
     * @return Tipocalle
     */
    public function setValore($valore = null)
    {
        $this->valore = $valore;

        return $this;
    }

    /**
     * Get valore.
     *
     * @return string|null
     */
    public function getValore()
    {
        return $this->valore;
    }

    /**
     * Set valorp.
     *
     * @param string|null $valorp
     *
     * @return Tipocalle
     */
    public function setValorp($valorp = null)
    {
        $this->valorp = $valorp;

        return $this;
    }

    /**
     * Get valorp.
     *
     * @return string|null
     */
    public function getValorp()
    {
        return $this->valorp;
    }
}
