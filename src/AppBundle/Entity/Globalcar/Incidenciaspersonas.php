<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidenciaspersonas
 *
 * @ORM\Table(name="incidenciaspersonas", indexes={@ORM\Index(name="idpersonas_IP_FK_idx", columns={"idpersonas"}), @ORM\Index(name="iddescriptores_IP_FK_idx", columns={"iddescriptores"})})
 * @ORM\Entity
 */
class Incidenciaspersonas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idincidenciasPersonas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idincidenciaspersonas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=0, nullable=false)
     */
    private $estado;

    /**
     * @var \Descriptores
     *
     * @ORM\ManyToOne(targetEntity="Descriptores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddescriptores", referencedColumnName="iddescriptores")
     * })
     */
    private $iddescriptores;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersonas", referencedColumnName="idpersonas")
     * })
     */
    private $idpersonas;



    /**
     * Get idincidenciaspersonas.
     *
     * @return int
     */
    public function getIdincidenciaspersonas()
    {
        return $this->idincidenciaspersonas;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Incidenciaspersonas
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Incidenciaspersonas
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado.
     *
     * @param string $estado
     *
     * @return Incidenciaspersonas
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado.
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set iddescriptores.
     *
     * @param \AppBundle\Entity\Globalcar\Descriptores|null $iddescriptores
     *
     * @return Incidenciaspersonas
     */
    public function setIddescriptores(\AppBundle\Entity\Globalcar\Descriptores $iddescriptores = null)
    {
        $this->iddescriptores = $iddescriptores;

        return $this;
    }

    /**
     * Get iddescriptores.
     *
     * @return \AppBundle\Entity\Globalcar\Descriptores|null
     */
    public function getIddescriptores()
    {
        return $this->iddescriptores;
    }

    /**
     * Set idpersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Personas|null $idpersonas
     *
     * @return Incidenciaspersonas
     */
    public function setIdpersonas(\AppBundle\Entity\Globalcar\Personas $idpersonas = null)
    {
        $this->idpersonas = $idpersonas;

        return $this;
    }

    /**
     * Get idpersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Personas|null
     */
    public function getIdpersonas()
    {
        return $this->idpersonas;
    }
}
