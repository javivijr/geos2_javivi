<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailsautomaticosReceptores
 *
 * @ORM\Table(name="emailsautomaticos_receptores", indexes={@ORM\Index(name="id_emailFKTime", columns={"idemailsautomaticos"}), @ORM\Index(name="id_emailFKUsu", columns={"idusuarios"})})
 * @ORM\Entity
 */
class EmailsautomaticosReceptores
{
    /**
     * @var int
     *
     * @ORM\Column(name="idemailsautomaticos_receptores", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idemailsautomaticosReceptores;

    /**
     * @var int
     *
     * @ORM\Column(name="idemailsautomaticos", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idemailsautomaticos;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuarios", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idusuarios;



    /**
     * Get idemailsautomaticosReceptores.
     *
     * @return int
     */
    public function getIdemailsautomaticosReceptores()
    {
        return $this->idemailsautomaticosReceptores;
    }

    /**
     * Set idemailsautomaticos.
     *
     * @param int $idemailsautomaticos
     *
     * @return EmailsautomaticosReceptores
     */
    public function setIdemailsautomaticos($idemailsautomaticos)
    {
        $this->idemailsautomaticos = $idemailsautomaticos;

        return $this;
    }

    /**
     * Get idemailsautomaticos.
     *
     * @return int
     */
    public function getIdemailsautomaticos()
    {
        return $this->idemailsautomaticos;
    }

    /**
     * Set idusuarios.
     *
     * @param int $idusuarios
     *
     * @return EmailsautomaticosReceptores
     */
    public function setIdusuarios($idusuarios)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return int
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }
}
