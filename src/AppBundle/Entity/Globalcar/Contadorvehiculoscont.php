<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contadorvehiculoscont
 *
 * @ORM\Table(name="contadorvehiculoscont", uniqueConstraints={@ORM\UniqueConstraint(name="idtojunto_idx", columns={"idcontratos", "fecha"})}, indexes={@ORM\Index(name="idcontratoscvc_FK_idx", columns={"idcontratos"}), @ORM\Index(name="idfecha_idx", columns={"fecha"})})
 * @ORM\Entity
 */
class Contadorvehiculoscont
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontadorvehiculoscont", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontadorvehiculoscont;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="total", type="integer", nullable=false)
     */
    private $total;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;



    /**
     * Get idcontadorvehiculoscont.
     *
     * @return int
     */
    public function getIdcontadorvehiculoscont()
    {
        return $this->idcontadorvehiculoscont;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Contadorvehiculoscont
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set total.
     *
     * @param int $total
     *
     * @return Contadorvehiculoscont
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Contadorvehiculoscont
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }
}
