<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmcontratacionesparticulares
 *
 * @ORM\Table(name="mtmcontratacionesparticulares", indexes={@ORM\Index(name="contrataciones_personas_mtm_FK_idx", columns={"idpersona"})})
 * @ORM\Entity
 */
class Mtmcontratacionesparticulares
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmcontratacionesparticulares", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmcontratacionesparticulares;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_alta", type="date", nullable=false)
     */
    private $fechaAlta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=false)
     */
    private $fechaInicio;

    /**
     * @var int
     *
     * @ORM\Column(name="meses", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $meses;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_plan", type="string", length=50, nullable=false)
     */
    private $nombrePlan;

    /**
     * @var string
     *
     * @ORM\Column(name="precio_plan", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $precioPlan;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersona", referencedColumnName="idpersonas")
     * })
     */
    private $idpersona;



    /**
     * Get idmtmcontratacionesparticulares.
     *
     * @return int
     */
    public function getIdmtmcontratacionesparticulares()
    {
        return $this->idmtmcontratacionesparticulares;
    }

    /**
     * Set fechaAlta.
     *
     * @param \DateTime $fechaAlta
     *
     * @return Mtmcontratacionesparticulares
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta.
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set fechaInicio.
     *
     * @param \DateTime $fechaInicio
     *
     * @return Mtmcontratacionesparticulares
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio.
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set meses.
     *
     * @param int $meses
     *
     * @return Mtmcontratacionesparticulares
     */
    public function setMeses($meses)
    {
        $this->meses = $meses;

        return $this;
    }

    /**
     * Get meses.
     *
     * @return int
     */
    public function getMeses()
    {
        return $this->meses;
    }

    /**
     * Set nombrePlan.
     *
     * @param string $nombrePlan
     *
     * @return Mtmcontratacionesparticulares
     */
    public function setNombrePlan($nombrePlan)
    {
        $this->nombrePlan = $nombrePlan;

        return $this;
    }

    /**
     * Get nombrePlan.
     *
     * @return string
     */
    public function getNombrePlan()
    {
        return $this->nombrePlan;
    }

    /**
     * Set precioPlan.
     *
     * @param string $precioPlan
     *
     * @return Mtmcontratacionesparticulares
     */
    public function setPrecioPlan($precioPlan)
    {
        $this->precioPlan = $precioPlan;

        return $this;
    }

    /**
     * Get precioPlan.
     *
     * @return string
     */
    public function getPrecioPlan()
    {
        return $this->precioPlan;
    }

    /**
     * Set idpersona.
     *
     * @param \AppBundle\Entity\Globalcar\Personas|null $idpersona
     *
     * @return Mtmcontratacionesparticulares
     */
    public function setIdpersona(\AppBundle\Entity\Globalcar\Personas $idpersona = null)
    {
        $this->idpersona = $idpersona;

        return $this;
    }

    /**
     * Get idpersona.
     *
     * @return \AppBundle\Entity\Globalcar\Personas|null
     */
    public function getIdpersona()
    {
        return $this->idpersona;
    }
}
