<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Legalescritosalida
 *
 * @ORM\Table(name="legalescritosalida")
 * @ORM\Entity
 */
class Legalescritosalida
{
    /**
     * @var int
     *
     * @ORM\Column(name="idlegalEscritoSalida", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlegalescritosalida;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=5, nullable=true, options={"fixed"=true})
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=50, nullable=true)
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptor", type="string", length=15, nullable=true)
     */
    private $descriptor;

    /**
     * @var string
     *
     * @ORM\Column(name="usaidentificado", type="string", length=0, nullable=false)
     */
    private $usaidentificado;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="string", length=0, nullable=false, options={"default"="s"})
     */
    private $visible = 's';



    /**
     * Get idlegalescritosalida.
     *
     * @return int
     */
    public function getIdlegalescritosalida()
    {
        return $this->idlegalescritosalida;
    }

    /**
     * Set codigo.
     *
     * @param string|null $codigo
     *
     * @return Legalescritosalida
     */
    public function setCodigo($codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo.
     *
     * @return string|null
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Legalescritosalida
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipo.
     *
     * @param string|null $tipo
     *
     * @return Legalescritosalida
     */
    public function setTipo($tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string|null
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set descriptor.
     *
     * @param string|null $descriptor
     *
     * @return Legalescritosalida
     */
    public function setDescriptor($descriptor = null)
    {
        $this->descriptor = $descriptor;

        return $this;
    }

    /**
     * Get descriptor.
     *
     * @return string|null
     */
    public function getDescriptor()
    {
        return $this->descriptor;
    }

    /**
     * Set usaidentificado.
     *
     * @param string $usaidentificado
     *
     * @return Legalescritosalida
     */
    public function setUsaidentificado($usaidentificado)
    {
        $this->usaidentificado = $usaidentificado;

        return $this;
    }

    /**
     * Get usaidentificado.
     *
     * @return string
     */
    public function getUsaidentificado()
    {
        return $this->usaidentificado;
    }

    /**
     * Set visible.
     *
     * @param string $visible
     *
     * @return Legalescritosalida
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible.
     *
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
