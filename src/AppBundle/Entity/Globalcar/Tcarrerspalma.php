<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tcarrerspalma
 *
 * @ORM\Table(name="tcarrerspalma")
 * @ORM\Entity
 */
class Tcarrerspalma
{
    /**
     * @var int
     *
     * @ORM\Column(name="idCarrer", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="siglaCarrer", type="string", length=3, nullable=true)
     */
    private $siglacarrer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreCarrer", type="string", length=100, nullable=true)
     */
    private $nombrecarrer;



    /**
     * Get idcarrer.
     *
     * @return int
     */
    public function getIdcarrer()
    {
        return $this->idcarrer;
    }

    /**
     * Set siglacarrer.
     *
     * @param string|null $siglacarrer
     *
     * @return Tcarrerspalma
     */
    public function setSiglacarrer($siglacarrer = null)
    {
        $this->siglacarrer = $siglacarrer;

        return $this;
    }

    /**
     * Get siglacarrer.
     *
     * @return string|null
     */
    public function getSiglacarrer()
    {
        return $this->siglacarrer;
    }

    /**
     * Set nombrecarrer.
     *
     * @param string|null $nombrecarrer
     *
     * @return Tcarrerspalma
     */
    public function setNombrecarrer($nombrecarrer = null)
    {
        $this->nombrecarrer = $nombrecarrer;

        return $this;
    }

    /**
     * Get nombrecarrer.
     *
     * @return string|null
     */
    public function getNombrecarrer()
    {
        return $this->nombrecarrer;
    }
}
