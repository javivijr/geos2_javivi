<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrenteuropcarexpediente
 *
 * @ORM\Table(name="carrenteuropcarexpediente", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_4E411FD259A462DE", columns={"idexpedientes"}), @ORM\UniqueConstraint(name="IDX_4E411FD2BDF66A79", columns={"idcarrenteuropcar"})})
 * @ORM\Entity
 */
class Carrenteuropcarexpediente
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrenteuropcarexpediente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrenteuropcarexpediente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=1000, nullable=true)
     */
    private $observaciones;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Carrenteuropcar
     *
     * @ORM\ManyToOne(targetEntity="Carrenteuropcar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcarrenteuropcar", referencedColumnName="idcarrenteuropcar")
     * })
     */
    private $idcarrenteuropcar;



    /**
     * Get idcarrenteuropcarexpediente.
     *
     * @return int
     */
    public function getIdcarrenteuropcarexpediente()
    {
        return $this->idcarrenteuropcarexpediente;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Carrenteuropcarexpediente
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Carrenteuropcarexpediente
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Carrenteuropcarexpediente
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idcarrenteuropcar.
     *
     * @param \AppBundle\Entity\Globalcar\Carrenteuropcar|null $idcarrenteuropcar
     *
     * @return Carrenteuropcarexpediente
     */
    public function setIdcarrenteuropcar(\AppBundle\Entity\Globalcar\Carrenteuropcar $idcarrenteuropcar = null)
    {
        $this->idcarrenteuropcar = $idcarrenteuropcar;

        return $this;
    }

    /**
     * Get idcarrenteuropcar.
     *
     * @return \AppBundle\Entity\Globalcar\Carrenteuropcar|null
     */
    public function getIdcarrenteuropcar()
    {
        return $this->idcarrenteuropcar;
    }
}
