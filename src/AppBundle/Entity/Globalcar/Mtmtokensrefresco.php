<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmtokensrefresco
 *
 * @ORM\Table(name="mtmtokensrefresco", uniqueConstraints={@ORM\UniqueConstraint(name="iddispositivo_idx", columns={"iddispositivo"})}, indexes={@ORM\Index(name="idusuariosmtm_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Mtmtokensrefresco
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmtokensrefresco", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmtokensrefresco;

    /**
     * @var string
     *
     * @ORM\Column(name="iddispositivo", type="string", length=50, nullable=false, options={"fixed"=true})
     */
    private $iddispositivo;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=128, nullable=false, options={"fixed"=true})
     */
    private $token;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmtokensrefresco.
     *
     * @return int
     */
    public function getIdmtmtokensrefresco()
    {
        return $this->idmtmtokensrefresco;
    }

    /**
     * Set iddispositivo.
     *
     * @param string $iddispositivo
     *
     * @return Mtmtokensrefresco
     */
    public function setIddispositivo($iddispositivo)
    {
        $this->iddispositivo = $iddispositivo;

        return $this;
    }

    /**
     * Get iddispositivo.
     *
     * @return string
     */
    public function getIddispositivo()
    {
        return $this->iddispositivo;
    }

    /**
     * Set token.
     *
     * @param string $token
     *
     * @return Mtmtokensrefresco
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmtokensrefresco
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
