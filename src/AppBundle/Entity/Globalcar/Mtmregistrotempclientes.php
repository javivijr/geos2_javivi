<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmregistrotempclientes
 *
 * @ORM\Table(name="mtmregistrotempclientes", indexes={@ORM\Index(name="idregistrotemp_RTC_FK_idx", columns={"idregistrotemp"}), @ORM\Index(name="idtitulares_RTC_FK_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Mtmregistrotempclientes
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmregistrotempclientes", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmregistrotempclientes;

    /**
     * @var \Mtmregistrotemp
     *
     * @ORM\ManyToOne(targetEntity="Mtmregistrotemp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idregistrotemp", referencedColumnName="idmtmregistrotemp")
     * })
     */
    private $idregistrotemp;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idmtmregistrotempclientes.
     *
     * @return int
     */
    public function getIdmtmregistrotempclientes()
    {
        return $this->idmtmregistrotempclientes;
    }

    /**
     * Set idregistrotemp.
     *
     * @param \AppBundle\Entity\Globalcar\Mtmregistrotemp|null $idregistrotemp
     *
     * @return Mtmregistrotempclientes
     */
    public function setIdregistrotemp(\AppBundle\Entity\Globalcar\Mtmregistrotemp $idregistrotemp = null)
    {
        $this->idregistrotemp = $idregistrotemp;

        return $this;
    }

    /**
     * Get idregistrotemp.
     *
     * @return \AppBundle\Entity\Globalcar\Mtmregistrotemp|null
     */
    public function getIdregistrotemp()
    {
        return $this->idregistrotemp;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmregistrotempclientes
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
