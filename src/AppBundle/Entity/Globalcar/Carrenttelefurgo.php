<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrenttelefurgo
 *
 * @ORM\Table(name="carrenttelefurgo", indexes={@ORM\Index(name="idexpedientes", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Carrenttelefurgo
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrenttelefurgo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrenttelefurgo;

    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientes", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idexpedientes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="documento", type="string", length=200, nullable=true)
     */
    private $documento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=200, nullable=true)
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="matricula", type="string", length=200, nullable=true)
     */
    private $matricula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehiculo", type="string", length=200, nullable=true)
     */
    private $vehiculo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaentrega", type="datetime", nullable=true)
     */
    private $fechaentrega;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaeecogida", type="datetime", nullable=true)
     */
    private $fechaeecogida;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Empresa", type="string", length=400, nullable=true)
     */
    private $empresa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresacod", type="string", length=200, nullable=true)
     */
    private $empresacod;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresadocumento", type="string", length=200, nullable=true)
     */
    private $empresadocumento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresadir1", type="string", length=500, nullable=true)
     */
    private $empresadir1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresadir2", type="string", length=500, nullable=true)
     */
    private $empresadir2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresaeir3", type="string", length=500, nullable=true)
     */
    private $empresaeir3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conductor", type="string", length=400, nullable=true)
     */
    private $conductor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conductorcod", type="string", length=200, nullable=true)
     */
    private $conductorcod;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conductordocumento", type="string", length=200, nullable=true)
     */
    private $conductordocumento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conductordir1", type="string", length=500, nullable=true)
     */
    private $conductordir1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conductordir2", type="string", length=500, nullable=true)
     */
    private $conductordir2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conductordir3", type="string", length=500, nullable=true)
     */
    private $conductordir3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pdf", type="string", length=3500, nullable=true)
     */
    private $pdf;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="conductorfechanacimiento", type="datetime", nullable=true)
     */
    private $conductorfechanacimiento;



    /**
     * Get idcarrenttelefurgo.
     *
     * @return int
     */
    public function getIdcarrenttelefurgo()
    {
        return $this->idcarrenttelefurgo;
    }

    /**
     * Set idexpedientes.
     *
     * @param int $idexpedientes
     *
     * @return Carrenttelefurgo
     */
    public function setIdexpedientes($idexpedientes)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return int
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set documento.
     *
     * @param string|null $documento
     *
     * @return Carrenttelefurgo
     */
    public function setDocumento($documento = null)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento.
     *
     * @return string|null
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set tipo.
     *
     * @param string|null $tipo
     *
     * @return Carrenttelefurgo
     */
    public function setTipo($tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string|null
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set matricula.
     *
     * @param string|null $matricula
     *
     * @return Carrenttelefurgo
     */
    public function setMatricula($matricula = null)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula.
     *
     * @return string|null
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set vehiculo.
     *
     * @param string|null $vehiculo
     *
     * @return Carrenttelefurgo
     */
    public function setVehiculo($vehiculo = null)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo.
     *
     * @return string|null
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set fechaentrega.
     *
     * @param \DateTime|null $fechaentrega
     *
     * @return Carrenttelefurgo
     */
    public function setFechaentrega($fechaentrega = null)
    {
        $this->fechaentrega = $fechaentrega;

        return $this;
    }

    /**
     * Get fechaentrega.
     *
     * @return \DateTime|null
     */
    public function getFechaentrega()
    {
        return $this->fechaentrega;
    }

    /**
     * Set fechaeecogida.
     *
     * @param \DateTime|null $fechaeecogida
     *
     * @return Carrenttelefurgo
     */
    public function setFechaeecogida($fechaeecogida = null)
    {
        $this->fechaeecogida = $fechaeecogida;

        return $this;
    }

    /**
     * Get fechaeecogida.
     *
     * @return \DateTime|null
     */
    public function getFechaeecogida()
    {
        return $this->fechaeecogida;
    }

    /**
     * Set empresa.
     *
     * @param string|null $empresa
     *
     * @return Carrenttelefurgo
     */
    public function setEmpresa($empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa.
     *
     * @return string|null
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set empresacod.
     *
     * @param string|null $empresacod
     *
     * @return Carrenttelefurgo
     */
    public function setEmpresacod($empresacod = null)
    {
        $this->empresacod = $empresacod;

        return $this;
    }

    /**
     * Get empresacod.
     *
     * @return string|null
     */
    public function getEmpresacod()
    {
        return $this->empresacod;
    }

    /**
     * Set empresadocumento.
     *
     * @param string|null $empresadocumento
     *
     * @return Carrenttelefurgo
     */
    public function setEmpresadocumento($empresadocumento = null)
    {
        $this->empresadocumento = $empresadocumento;

        return $this;
    }

    /**
     * Get empresadocumento.
     *
     * @return string|null
     */
    public function getEmpresadocumento()
    {
        return $this->empresadocumento;
    }

    /**
     * Set empresadir1.
     *
     * @param string|null $empresadir1
     *
     * @return Carrenttelefurgo
     */
    public function setEmpresadir1($empresadir1 = null)
    {
        $this->empresadir1 = $empresadir1;

        return $this;
    }

    /**
     * Get empresadir1.
     *
     * @return string|null
     */
    public function getEmpresadir1()
    {
        return $this->empresadir1;
    }

    /**
     * Set empresadir2.
     *
     * @param string|null $empresadir2
     *
     * @return Carrenttelefurgo
     */
    public function setEmpresadir2($empresadir2 = null)
    {
        $this->empresadir2 = $empresadir2;

        return $this;
    }

    /**
     * Get empresadir2.
     *
     * @return string|null
     */
    public function getEmpresadir2()
    {
        return $this->empresadir2;
    }

    /**
     * Set empresaeir3.
     *
     * @param string|null $empresaeir3
     *
     * @return Carrenttelefurgo
     */
    public function setEmpresaeir3($empresaeir3 = null)
    {
        $this->empresaeir3 = $empresaeir3;

        return $this;
    }

    /**
     * Get empresaeir3.
     *
     * @return string|null
     */
    public function getEmpresaeir3()
    {
        return $this->empresaeir3;
    }

    /**
     * Set conductor.
     *
     * @param string|null $conductor
     *
     * @return Carrenttelefurgo
     */
    public function setConductor($conductor = null)
    {
        $this->conductor = $conductor;

        return $this;
    }

    /**
     * Get conductor.
     *
     * @return string|null
     */
    public function getConductor()
    {
        return $this->conductor;
    }

    /**
     * Set conductorcod.
     *
     * @param string|null $conductorcod
     *
     * @return Carrenttelefurgo
     */
    public function setConductorcod($conductorcod = null)
    {
        $this->conductorcod = $conductorcod;

        return $this;
    }

    /**
     * Get conductorcod.
     *
     * @return string|null
     */
    public function getConductorcod()
    {
        return $this->conductorcod;
    }

    /**
     * Set conductordocumento.
     *
     * @param string|null $conductordocumento
     *
     * @return Carrenttelefurgo
     */
    public function setConductordocumento($conductordocumento = null)
    {
        $this->conductordocumento = $conductordocumento;

        return $this;
    }

    /**
     * Get conductordocumento.
     *
     * @return string|null
     */
    public function getConductordocumento()
    {
        return $this->conductordocumento;
    }

    /**
     * Set conductordir1.
     *
     * @param string|null $conductordir1
     *
     * @return Carrenttelefurgo
     */
    public function setConductordir1($conductordir1 = null)
    {
        $this->conductordir1 = $conductordir1;

        return $this;
    }

    /**
     * Get conductordir1.
     *
     * @return string|null
     */
    public function getConductordir1()
    {
        return $this->conductordir1;
    }

    /**
     * Set conductordir2.
     *
     * @param string|null $conductordir2
     *
     * @return Carrenttelefurgo
     */
    public function setConductordir2($conductordir2 = null)
    {
        $this->conductordir2 = $conductordir2;

        return $this;
    }

    /**
     * Get conductordir2.
     *
     * @return string|null
     */
    public function getConductordir2()
    {
        return $this->conductordir2;
    }

    /**
     * Set conductordir3.
     *
     * @param string|null $conductordir3
     *
     * @return Carrenttelefurgo
     */
    public function setConductordir3($conductordir3 = null)
    {
        $this->conductordir3 = $conductordir3;

        return $this;
    }

    /**
     * Get conductordir3.
     *
     * @return string|null
     */
    public function getConductordir3()
    {
        return $this->conductordir3;
    }

    /**
     * Set pdf.
     *
     * @param string|null $pdf
     *
     * @return Carrenttelefurgo
     */
    public function setPdf($pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf.
     *
     * @return string|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set conductorfechanacimiento.
     *
     * @param \DateTime|null $conductorfechanacimiento
     *
     * @return Carrenttelefurgo
     */
    public function setConductorfechanacimiento($conductorfechanacimiento = null)
    {
        $this->conductorfechanacimiento = $conductorfechanacimiento;

        return $this;
    }

    /**
     * Get conductorfechanacimiento.
     *
     * @return \DateTime|null
     */
    public function getConductorfechanacimiento()
    {
        return $this->conductorfechanacimiento;
    }
}
