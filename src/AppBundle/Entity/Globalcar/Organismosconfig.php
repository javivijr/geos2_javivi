<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organismosconfig
 *
 * @ORM\Table(name="organismosconfig", uniqueConstraints={@ORM\UniqueConstraint(name="idorganismos", columns={"idorganismos", "idtitulares"})}, indexes={@ORM\Index(name="idorganismos_OC_FK_idx", columns={"idorganismos"}), @ORM\Index(name="idtitulares_OC_FK_idx", columns={"idtitulares"}), @ORM\Index(name="aceptafirmaescaneada", columns={"aceptafirmaescaneada", "aceptaselloescaneado"})})
 * @ORM\Entity
 */
class Organismosconfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="idorganismosConfig", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idorganismosconfig;

    /**
     * @var string
     *
     * @ORM\Column(name="copiaContratoRenting", type="string", length=0, nullable=false)
     */
    private $copiacontratorenting;

    /**
     * @var string
     *
     * @ORM\Column(name="copiaContratoRentacar", type="string", length=0, nullable=false)
     */
    private $copiacontratorentacar;

    /**
     * @var string
     *
     * @ORM\Column(name="escrito", type="string", length=0, nullable=false)
     */
    private $escrito;

    /**
     * @var string
     *
     * @ORM\Column(name="relacion", type="string", length=0, nullable=false)
     */
    private $relacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailContacto", type="string", length=100, nullable=true)
     */
    private $emailcontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="faxContacto", type="string", length=13, nullable=true)
     */
    private $faxcontacto;

    /**
     * @var string
     *
     * @ORM\Column(name="plantillaUsar", type="string", length=0, nullable=false)
     */
    private $plantillausar;

    /**
     * @var string
     *
     * @ORM\Column(name="connie", type="string", length=0, nullable=false)
     */
    private $connie;

    /**
     * @var string
     *
     * @ORM\Column(name="salida", type="string", length=0, nullable=false)
     */
    private $salida;

    /**
     * @var string
     *
     * @ORM\Column(name="salidaextranjeros", type="string", length=0, nullable=false)
     */
    private $salidaextranjeros;

    /**
     * @var string|null
     *
     * @ORM\Column(name="representanteescritos", type="string", length=100, nullable=true)
     */
    private $representanteescritos;

    /**
     * @var string
     *
     * @ORM\Column(name="salidalegal", type="string", length=0, nullable=false)
     */
    private $salidalegal;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="escritodoble", type="string", length=0, nullable=false)
     */
    private $escritodoble;

    /**
     * @var string
     *
     * @ORM\Column(name="usafirmajaime", type="string", length=0, nullable=false)
     */
    private $usafirmajaime;

    /**
     * @var string
     *
     * @ORM\Column(name="contratoFirmadoRentacar", type="string", length=0, nullable=false)
     */
    private $contratofirmadorentacar;

    /**
     * @var string
     *
     * @ORM\Column(name="emailMasivo", type="string", length=0, nullable=false)
     */
    private $emailmasivo;

    /**
     * @var string
     *
     * @ORM\Column(name="aceptafirmaescaneada", type="string", length=0, nullable=false, options={"default"="s"})
     */
    private $aceptafirmaescaneada = 's';

    /**
     * @var string
     *
     * @ORM\Column(name="aceptaselloescaneado", type="string", length=0, nullable=false, options={"default"="s"})
     */
    private $aceptaselloescaneado = 's';

    /**
     * @var string
     *
     * @ORM\Column(name="pagarnombrearrendatario", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $pagarnombrearrendatario = 'n';

    /**
     * @var \Organismos
     *
     * @ORM\ManyToOne(targetEntity="Organismos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idorganismos", referencedColumnName="idorganismos")
     * })
     */
    private $idorganismos;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idorganismosconfig.
     *
     * @return int
     */
    public function getIdorganismosconfig()
    {
        return $this->idorganismosconfig;
    }

    /**
     * Set copiacontratorenting.
     *
     * @param string $copiacontratorenting
     *
     * @return Organismosconfig
     */
    public function setCopiacontratorenting($copiacontratorenting)
    {
        $this->copiacontratorenting = $copiacontratorenting;

        return $this;
    }

    /**
     * Get copiacontratorenting.
     *
     * @return string
     */
    public function getCopiacontratorenting()
    {
        return $this->copiacontratorenting;
    }

    /**
     * Set copiacontratorentacar.
     *
     * @param string $copiacontratorentacar
     *
     * @return Organismosconfig
     */
    public function setCopiacontratorentacar($copiacontratorentacar)
    {
        $this->copiacontratorentacar = $copiacontratorentacar;

        return $this;
    }

    /**
     * Get copiacontratorentacar.
     *
     * @return string
     */
    public function getCopiacontratorentacar()
    {
        return $this->copiacontratorentacar;
    }

    /**
     * Set escrito.
     *
     * @param string $escrito
     *
     * @return Organismosconfig
     */
    public function setEscrito($escrito)
    {
        $this->escrito = $escrito;

        return $this;
    }

    /**
     * Get escrito.
     *
     * @return string
     */
    public function getEscrito()
    {
        return $this->escrito;
    }

    /**
     * Set relacion.
     *
     * @param string $relacion
     *
     * @return Organismosconfig
     */
    public function setRelacion($relacion)
    {
        $this->relacion = $relacion;

        return $this;
    }

    /**
     * Get relacion.
     *
     * @return string
     */
    public function getRelacion()
    {
        return $this->relacion;
    }

    /**
     * Set emailcontacto.
     *
     * @param string|null $emailcontacto
     *
     * @return Organismosconfig
     */
    public function setEmailcontacto($emailcontacto = null)
    {
        $this->emailcontacto = $emailcontacto;

        return $this;
    }

    /**
     * Get emailcontacto.
     *
     * @return string|null
     */
    public function getEmailcontacto()
    {
        return $this->emailcontacto;
    }

    /**
     * Set faxcontacto.
     *
     * @param string|null $faxcontacto
     *
     * @return Organismosconfig
     */
    public function setFaxcontacto($faxcontacto = null)
    {
        $this->faxcontacto = $faxcontacto;

        return $this;
    }

    /**
     * Get faxcontacto.
     *
     * @return string|null
     */
    public function getFaxcontacto()
    {
        return $this->faxcontacto;
    }

    /**
     * Set plantillausar.
     *
     * @param string $plantillausar
     *
     * @return Organismosconfig
     */
    public function setPlantillausar($plantillausar)
    {
        $this->plantillausar = $plantillausar;

        return $this;
    }

    /**
     * Get plantillausar.
     *
     * @return string
     */
    public function getPlantillausar()
    {
        return $this->plantillausar;
    }

    /**
     * Set connie.
     *
     * @param string $connie
     *
     * @return Organismosconfig
     */
    public function setConnie($connie)
    {
        $this->connie = $connie;

        return $this;
    }

    /**
     * Get connie.
     *
     * @return string
     */
    public function getConnie()
    {
        return $this->connie;
    }

    /**
     * Set salida.
     *
     * @param string $salida
     *
     * @return Organismosconfig
     */
    public function setSalida($salida)
    {
        $this->salida = $salida;

        return $this;
    }

    /**
     * Get salida.
     *
     * @return string
     */
    public function getSalida()
    {
        return $this->salida;
    }

    /**
     * Set salidaextranjeros.
     *
     * @param string $salidaextranjeros
     *
     * @return Organismosconfig
     */
    public function setSalidaextranjeros($salidaextranjeros)
    {
        $this->salidaextranjeros = $salidaextranjeros;

        return $this;
    }

    /**
     * Get salidaextranjeros.
     *
     * @return string
     */
    public function getSalidaextranjeros()
    {
        return $this->salidaextranjeros;
    }

    /**
     * Set representanteescritos.
     *
     * @param string|null $representanteescritos
     *
     * @return Organismosconfig
     */
    public function setRepresentanteescritos($representanteescritos = null)
    {
        $this->representanteescritos = $representanteescritos;

        return $this;
    }

    /**
     * Get representanteescritos.
     *
     * @return string|null
     */
    public function getRepresentanteescritos()
    {
        return $this->representanteescritos;
    }

    /**
     * Set salidalegal.
     *
     * @param string $salidalegal
     *
     * @return Organismosconfig
     */
    public function setSalidalegal($salidalegal)
    {
        $this->salidalegal = $salidalegal;

        return $this;
    }

    /**
     * Get salidalegal.
     *
     * @return string
     */
    public function getSalidalegal()
    {
        return $this->salidalegal;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Organismosconfig
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set escritodoble.
     *
     * @param string $escritodoble
     *
     * @return Organismosconfig
     */
    public function setEscritodoble($escritodoble)
    {
        $this->escritodoble = $escritodoble;

        return $this;
    }

    /**
     * Get escritodoble.
     *
     * @return string
     */
    public function getEscritodoble()
    {
        return $this->escritodoble;
    }

    /**
     * Set usafirmajaime.
     *
     * @param string $usafirmajaime
     *
     * @return Organismosconfig
     */
    public function setUsafirmajaime($usafirmajaime)
    {
        $this->usafirmajaime = $usafirmajaime;

        return $this;
    }

    /**
     * Get usafirmajaime.
     *
     * @return string
     */
    public function getUsafirmajaime()
    {
        return $this->usafirmajaime;
    }

    /**
     * Set contratofirmadorentacar.
     *
     * @param string $contratofirmadorentacar
     *
     * @return Organismosconfig
     */
    public function setContratofirmadorentacar($contratofirmadorentacar)
    {
        $this->contratofirmadorentacar = $contratofirmadorentacar;

        return $this;
    }

    /**
     * Get contratofirmadorentacar.
     *
     * @return string
     */
    public function getContratofirmadorentacar()
    {
        return $this->contratofirmadorentacar;
    }

    /**
     * Set emailmasivo.
     *
     * @param string $emailmasivo
     *
     * @return Organismosconfig
     */
    public function setEmailmasivo($emailmasivo)
    {
        $this->emailmasivo = $emailmasivo;

        return $this;
    }

    /**
     * Get emailmasivo.
     *
     * @return string
     */
    public function getEmailmasivo()
    {
        return $this->emailmasivo;
    }

    /**
     * Set aceptafirmaescaneada.
     *
     * @param string $aceptafirmaescaneada
     *
     * @return Organismosconfig
     */
    public function setAceptafirmaescaneada($aceptafirmaescaneada)
    {
        $this->aceptafirmaescaneada = $aceptafirmaescaneada;

        return $this;
    }

    /**
     * Get aceptafirmaescaneada.
     *
     * @return string
     */
    public function getAceptafirmaescaneada()
    {
        return $this->aceptafirmaescaneada;
    }

    /**
     * Set aceptaselloescaneado.
     *
     * @param string $aceptaselloescaneado
     *
     * @return Organismosconfig
     */
    public function setAceptaselloescaneado($aceptaselloescaneado)
    {
        $this->aceptaselloescaneado = $aceptaselloescaneado;

        return $this;
    }

    /**
     * Get aceptaselloescaneado.
     *
     * @return string
     */
    public function getAceptaselloescaneado()
    {
        return $this->aceptaselloescaneado;
    }

    /**
     * Set pagarnombrearrendatario.
     *
     * @param string $pagarnombrearrendatario
     *
     * @return Organismosconfig
     */
    public function setPagarnombrearrendatario($pagarnombrearrendatario)
    {
        $this->pagarnombrearrendatario = $pagarnombrearrendatario;

        return $this;
    }

    /**
     * Get pagarnombrearrendatario.
     *
     * @return string
     */
    public function getPagarnombrearrendatario()
    {
        return $this->pagarnombrearrendatario;
    }

    /**
     * Set idorganismos.
     *
     * @param \AppBundle\Entity\Globalcar\Organismos|null $idorganismos
     *
     * @return Organismosconfig
     */
    public function setIdorganismos(\AppBundle\Entity\Globalcar\Organismos $idorganismos = null)
    {
        $this->idorganismos = $idorganismos;

        return $this;
    }

    /**
     * Get idorganismos.
     *
     * @return \AppBundle\Entity\Globalcar\Organismos|null
     */
    public function getIdorganismos()
    {
        return $this->idorganismos;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Organismosconfig
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
