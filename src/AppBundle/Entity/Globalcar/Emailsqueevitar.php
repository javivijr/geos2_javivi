<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Emailsqueevitar
 *
 * @ORM\Table(name="emailsqueevitar")
 * @ORM\Entity
 */
class Emailsqueevitar
{
    /**
     * @var int
     *
     * @ORM\Column(name="idemailsqueevitar", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idemailsqueevitar;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor", type="string", length=30, nullable=false)
     */
    private $proveedor;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="autorizado", type="string", length=1, nullable=false, options={"default"="n","fixed"=true})
     */
    private $autorizado = 'n';



    /**
     * Get idemailsqueevitar.
     *
     * @return int
     */
    public function getIdemailsqueevitar()
    {
        return $this->idemailsqueevitar;
    }

    /**
     * Set proveedor.
     *
     * @param string $proveedor
     *
     * @return Emailsqueevitar
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor.
     *
     * @return string
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Emailsqueevitar
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set autorizado.
     *
     * @param string $autorizado
     *
     * @return Emailsqueevitar
     */
    public function setAutorizado($autorizado)
    {
        $this->autorizado = $autorizado;

        return $this;
    }

    /**
     * Get autorizado.
     *
     * @return string
     */
    public function getAutorizado()
    {
        return $this->autorizado;
    }
}
