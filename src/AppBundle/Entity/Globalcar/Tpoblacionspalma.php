<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tpoblacionspalma
 *
 * @ORM\Table(name="tpoblacionspalma", indexes={@ORM\Index(name="idprovincia_tpoblacionsPalma_FK_idx", columns={"idProvincia"})})
 * @ORM\Entity
 */
class Tpoblacionspalma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idMunicipio", type="integer", nullable=true)
     */
    private $idmunicipio;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idPoblacion", type="integer", nullable=true)
     */
    private $idpoblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombrePoblacion", type="string", length=100, nullable=true)
     */
    private $nombrepoblacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dispos", type="integer", nullable=true)
     */
    private $dispos;

    /**
     * @var \Tprovincias
     *
     * @ORM\ManyToOne(targetEntity="Tprovincias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idProvincia", referencedColumnName="idtProvincias")
     * })
     */
    private $idprovincia;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idmunicipio.
     *
     * @param int|null $idmunicipio
     *
     * @return Tpoblacionspalma
     */
    public function setIdmunicipio($idmunicipio = null)
    {
        $this->idmunicipio = $idmunicipio;

        return $this;
    }

    /**
     * Get idmunicipio.
     *
     * @return int|null
     */
    public function getIdmunicipio()
    {
        return $this->idmunicipio;
    }

    /**
     * Set idpoblacion.
     *
     * @param int|null $idpoblacion
     *
     * @return Tpoblacionspalma
     */
    public function setIdpoblacion($idpoblacion = null)
    {
        $this->idpoblacion = $idpoblacion;

        return $this;
    }

    /**
     * Get idpoblacion.
     *
     * @return int|null
     */
    public function getIdpoblacion()
    {
        return $this->idpoblacion;
    }

    /**
     * Set nombrepoblacion.
     *
     * @param string|null $nombrepoblacion
     *
     * @return Tpoblacionspalma
     */
    public function setNombrepoblacion($nombrepoblacion = null)
    {
        $this->nombrepoblacion = $nombrepoblacion;

        return $this;
    }

    /**
     * Get nombrepoblacion.
     *
     * @return string|null
     */
    public function getNombrepoblacion()
    {
        return $this->nombrepoblacion;
    }

    /**
     * Set dispos.
     *
     * @param int|null $dispos
     *
     * @return Tpoblacionspalma
     */
    public function setDispos($dispos = null)
    {
        $this->dispos = $dispos;

        return $this;
    }

    /**
     * Get dispos.
     *
     * @return int|null
     */
    public function getDispos()
    {
        return $this->dispos;
    }

    /**
     * Set idprovincia.
     *
     * @param \AppBundle\Entity\Globalcar\Tprovincias|null $idprovincia
     *
     * @return Tpoblacionspalma
     */
    public function setIdprovincia(\AppBundle\Entity\Globalcar\Tprovincias $idprovincia = null)
    {
        $this->idprovincia = $idprovincia;

        return $this;
    }

    /**
     * Get idprovincia.
     *
     * @return \AppBundle\Entity\Globalcar\Tprovincias|null
     */
    public function getIdprovincia()
    {
        return $this->idprovincia;
    }
}
