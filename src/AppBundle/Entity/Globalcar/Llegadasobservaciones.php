<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Llegadasobservaciones
 *
 * @ORM\Table(name="llegadasobservaciones", indexes={@ORM\Index(name="idllegadas_LLO_FK_idx", columns={"idllegadas"})})
 * @ORM\Entity
 */
class Llegadasobservaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="idllegadasObservaciones", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idllegadasobservaciones;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comunicado", type="string", length=500, nullable=true)
     */
    private $comunicado;

    /**
     * @var \Llegadas
     *
     * @ORM\ManyToOne(targetEntity="Llegadas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idllegadas", referencedColumnName="idllegadas")
     * })
     */
    private $idllegadas;



    /**
     * Get idllegadasobservaciones.
     *
     * @return int
     */
    public function getIdllegadasobservaciones()
    {
        return $this->idllegadasobservaciones;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Llegadasobservaciones
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Llegadasobservaciones
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comunicado.
     *
     * @param string|null $comunicado
     *
     * @return Llegadasobservaciones
     */
    public function setComunicado($comunicado = null)
    {
        $this->comunicado = $comunicado;

        return $this;
    }

    /**
     * Get comunicado.
     *
     * @return string|null
     */
    public function getComunicado()
    {
        return $this->comunicado;
    }

    /**
     * Set idllegadas.
     *
     * @param \AppBundle\Entity\Globalcar\Llegadas|null $idllegadas
     *
     * @return Llegadasobservaciones
     */
    public function setIdllegadas(\AppBundle\Entity\Globalcar\Llegadas $idllegadas = null)
    {
        $this->idllegadas = $idllegadas;

        return $this;
    }

    /**
     * Get idllegadas.
     *
     * @return \AppBundle\Entity\Globalcar\Llegadas|null
     */
    public function getIdllegadas()
    {
        return $this->idllegadas;
    }
}
