<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmcomunicadosvistos
 *
 * @ORM\Table(name="mtmcomunicadosvistos", uniqueConstraints={@ORM\UniqueConstraint(name="idcomunicadosexpedientesyidusuariosmtm", columns={"idcomunicadosexpedientes", "idusuariosmtm"})}, indexes={@ORM\Index(name="idcomunicadosexpedientes_MCV_FK_idx", columns={"idcomunicadosexpedientes"}), @ORM\Index(name="idusuariosmtm_MCV_FK_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Mtmcomunicadosvistos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmcomunicadosvistos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmcomunicadosvistos;

    /**
     * @var \Comunicadosexpedientes
     *
     * @ORM\ManyToOne(targetEntity="Comunicadosexpedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcomunicadosexpedientes", referencedColumnName="idcomunicadosExpedientes")
     * })
     */
    private $idcomunicadosexpedientes;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmcomunicadosvistos.
     *
     * @return int
     */
    public function getIdmtmcomunicadosvistos()
    {
        return $this->idmtmcomunicadosvistos;
    }

    /**
     * Set idcomunicadosexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Comunicadosexpedientes|null $idcomunicadosexpedientes
     *
     * @return Mtmcomunicadosvistos
     */
    public function setIdcomunicadosexpedientes(\AppBundle\Entity\Globalcar\Comunicadosexpedientes $idcomunicadosexpedientes = null)
    {
        $this->idcomunicadosexpedientes = $idcomunicadosexpedientes;

        return $this;
    }

    /**
     * Get idcomunicadosexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Comunicadosexpedientes|null
     */
    public function getIdcomunicadosexpedientes()
    {
        return $this->idcomunicadosexpedientes;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmcomunicadosvistos
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
