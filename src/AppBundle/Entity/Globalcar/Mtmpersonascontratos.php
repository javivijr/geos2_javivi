<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmpersonascontratos
 *
 * @ORM\Table(name="mtmpersonascontratos", indexes={@ORM\Index(name="idcontratos_PC_FK_idx", columns={"idcontratos"}), @ORM\Index(name="idhistoricoPersonas_PC_FK_idx", columns={"idhistoricoPersonas"})})
 * @ORM\Entity
 */
class Mtmpersonascontratos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpersonasContratos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonascontratos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="visible", type="string", length=0, nullable=true)
     */
    private $visible;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;

    /**
     * @var \Historicopersonas
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoPersonas", referencedColumnName="idhistoricoPersonas")
     * })
     */
    private $idhistoricopersonas;



    /**
     * Get idpersonascontratos.
     *
     * @return int
     */
    public function getIdpersonascontratos()
    {
        return $this->idpersonascontratos;
    }

    /**
     * Set visible.
     *
     * @param string|null $visible
     *
     * @return Mtmpersonascontratos
     */
    public function setVisible($visible = null)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible.
     *
     * @return string|null
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Mtmpersonascontratos
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }

    /**
     * Set idhistoricopersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonas|null $idhistoricopersonas
     *
     * @return Mtmpersonascontratos
     */
    public function setIdhistoricopersonas(\AppBundle\Entity\Globalcar\Historicopersonas $idhistoricopersonas = null)
    {
        $this->idhistoricopersonas = $idhistoricopersonas;

        return $this;
    }

    /**
     * Get idhistoricopersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonas|null
     */
    public function getIdhistoricopersonas()
    {
        return $this->idhistoricopersonas;
    }
}
