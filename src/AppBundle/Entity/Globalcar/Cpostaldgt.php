<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cpostaldgt
 *
 * @ORM\Table(name="cpostaldgt")
 * @ORM\Entity
 */
class Cpostaldgt
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcpostaldgt", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcpostaldgt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=5, nullable=true, options={"fixed"=true})
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo1", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private $codigo1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="municipio", type="string", length=50, nullable=true)
     */
    private $municipio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo2", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $codigo2;



    /**
     * Get idcpostaldgt.
     *
     * @return int
     */
    public function getIdcpostaldgt()
    {
        return $this->idcpostaldgt;
    }

    /**
     * Set codigo.
     *
     * @param string|null $codigo
     *
     * @return Cpostaldgt
     */
    public function setCodigo($codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo.
     *
     * @return string|null
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set codigo1.
     *
     * @param string|null $codigo1
     *
     * @return Cpostaldgt
     */
    public function setCodigo1($codigo1 = null)
    {
        $this->codigo1 = $codigo1;

        return $this;
    }

    /**
     * Get codigo1.
     *
     * @return string|null
     */
    public function getCodigo1()
    {
        return $this->codigo1;
    }

    /**
     * Set municipio.
     *
     * @param string|null $municipio
     *
     * @return Cpostaldgt
     */
    public function setMunicipio($municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio.
     *
     * @return string|null
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set codigo2.
     *
     * @param string|null $codigo2
     *
     * @return Cpostaldgt
     */
    public function setCodigo2($codigo2 = null)
    {
        $this->codigo2 = $codigo2;

        return $this;
    }

    /**
     * Get codigo2.
     *
     * @return string|null
     */
    public function getCodigo2()
    {
        return $this->codigo2;
    }
}
