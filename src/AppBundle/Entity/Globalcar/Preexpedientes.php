<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preexpedientes
 *
 * @ORM\Table(name="preexpedientes", indexes={@ORM\Index(name="idvehiculos_PRE_FK_idx", columns={"idvehiculos"}), @ORM\Index(name="idusuariosMtm_PRE_FK_idx", columns={"idusuariosMtm"}), @ORM\Index(name="idproveedor_PRE_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idhistoricopersonas_PRE_fk_idx", columns={"idhistoricopersonas"}), @ORM\Index(name="idarrandatario_PRE_FK_idx", columns={"idarrendatario"})})
 * @ORM\Entity
 */
class Preexpedientes
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpreExpedientes", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpreexpedientes;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaProntoPago", type="datetime", nullable=true)
     */
    private $fechaprontopago;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaAlta", type="datetime", nullable=true)
     */
    private $fechaalta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaNotificacion", type="datetime", nullable=true)
     */
    private $fechanotificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=false)
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta", type="string", length=200, nullable=true)
     */
    private $ruta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rutadocuslegal", type="text", length=65535, nullable=true)
     */
    private $rutadocuslegal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="text", length=65535, nullable=true)
     */
    private $observaciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=true)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rutajustificante", type="string", length=200, nullable=true)
     */
    private $rutajustificante;

    /**
     * @var Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idarrendatario", referencedColumnName="idtitulares")
     * })
     */
    private $idarrendatario;

    /**
     * @var Historicopersonas
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricopersonas", referencedColumnName="idhistoricoPersonas")
     * })
     */
    private $idhistoricopersonas;

    /**
     * @var Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;

    /**
     * @var Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosMtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;

    /**
     * @var Vehiculos
     *
     * @ORM\ManyToOne(targetEntity="Vehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idvehiculos", referencedColumnName="idvehiculos")
     * })
     */
    private $idvehiculos;



    /**
     * Get idpreexpedientes.
     *
     * @return int
     */
    public function getIdpreexpedientes()
    {
        return $this->idpreexpedientes;
    }

    /**
     * Set fechaprontopago.
     *
     * @param \DateTime|null $fechaprontopago
     *
     * @return Preexpedientes
     */
    public function setFechaprontopago($fechaprontopago = null)
    {
        $this->fechaprontopago = $fechaprontopago;

        return $this;
    }

    /**
     * Get fechaprontopago.
     *
     * @return \DateTime|null
     */
    public function getFechaprontopago()
    {
        return $this->fechaprontopago;
    }

    /**
     * Set fechaalta.
     *
     * @param \DateTime|null $fechaalta
     *
     * @return Preexpedientes
     */
    public function setFechaalta($fechaalta = null)
    {
        $this->fechaalta = $fechaalta;

        return $this;
    }

    /**
     * Get fechaalta.
     *
     * @return \DateTime|null
     */
    public function getFechaalta()
    {
        return $this->fechaalta;
    }

    /**
     * Set fechanotificacion.
     *
     * @param \DateTime|null $fechanotificacion
     *
     * @return Preexpedientes
     */
    public function setFechanotificacion($fechanotificacion = null)
    {
        $this->fechanotificacion = $fechanotificacion;

        return $this;
    }

    /**
     * Get fechanotificacion.
     *
     * @return \DateTime|null
     */
    public function getFechanotificacion()
    {
        return $this->fechanotificacion;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Preexpedientes
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set ruta.
     *
     * @param string|null $ruta
     *
     * @return Preexpedientes
     */
    public function setRuta($ruta = null)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta.
     *
     * @return string|null
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set rutadocuslegal.
     *
     * @param string|null $rutadocuslegal
     *
     * @return Preexpedientes
     */
    public function setRutadocuslegal($rutadocuslegal = null)
    {
        $this->rutadocuslegal = $rutadocuslegal;

        return $this;
    }

    /**
     * Get rutadocuslegal.
     *
     * @return string|null
     */
    public function getRutadocuslegal()
    {
        return $this->rutadocuslegal;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Preexpedientes
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return Preexpedientes
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set rutajustificante.
     *
     * @param string|null $rutajustificante
     *
     * @return Preexpedientes
     */
    public function setRutajustificante($rutajustificante = null)
    {
        $this->rutajustificante = $rutajustificante;

        return $this;
    }

    /**
     * Get rutajustificante.
     *
     * @return string|null
     */
    public function getRutajustificante()
    {
        return $this->rutajustificante;
    }

    /**
     * Set idarrendatario.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idarrendatario
     *
     * @return Preexpedientes
     */
    public function setIdarrendatario(\AppBundle\Entity\Globalcar\Titulares $idarrendatario = null)
    {
        $this->idarrendatario = $idarrendatario;

        return $this;
    }

    /**
     * Get idarrendatario.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdarrendatario()
    {
        return $this->idarrendatario;
    }

    /**
     * Set idhistoricopersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonas|null $idhistoricopersonas
     *
     * @return Preexpedientes
     */
    public function setIdhistoricopersonas(\AppBundle\Entity\Globalcar\Historicopersonas $idhistoricopersonas = null)
    {
        $this->idhistoricopersonas = $idhistoricopersonas;

        return $this;
    }

    /**
     * Get idhistoricopersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonas|null
     */
    public function getIdhistoricopersonas()
    {
        return $this->idhistoricopersonas;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Preexpedientes
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Preexpedientes
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }

    /**
     * Set idvehiculos.
     *
     * @param \AppBundle\Entity\Globalcar\Vehiculos|null $idvehiculos
     *
     * @return Preexpedientes
     */
    public function setIdvehiculos(\AppBundle\Entity\Globalcar\Vehiculos $idvehiculos = null)
    {
        $this->idvehiculos = $idvehiculos;

        return $this;
    }

    /**
     * Get idvehiculos.
     *
     * @return \AppBundle\Entity\Globalcar\Vehiculos|null
     */
    public function getIdvehiculos()
    {
        return $this->idvehiculos;
    }
}
