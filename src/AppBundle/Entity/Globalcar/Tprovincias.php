<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tprovincias
 *
 * @ORM\Table(name="tprovincias")
 * @ORM\Entity
 */
class Tprovincias
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtProvincias", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtprovincias;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=150, nullable=true)
     */
    private $provincia;



    /**
     * Get idtprovincias.
     *
     * @return int
     */
    public function getIdtprovincias()
    {
        return $this->idtprovincias;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Tprovincias
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
}
