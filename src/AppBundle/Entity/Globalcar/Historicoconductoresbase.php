<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historicoconductoresbase
 *
 * @ORM\Table(name="historicoconductoresbase", indexes={@ORM\Index(name="idpersonas_HC_FK_idx", columns={"idpersonas"}), @ORM\Index(name="idhistoricoVehiculos_HC_FK_idx", columns={"idhistoricoVehiculos"}), @ORM\Index(name="idx_Fechas_Vehiculo", columns={"fechaInicio", "fechaFin", "idhistoricoVehiculos", "idpersonas"}), @ORM\Index(name="idx_FechaInicio", columns={"fechaInicio"}), @ORM\Index(name="idx_FechaFin", columns={"fechaFin"}), @ORM\Index(name="idx_Fechas", columns={"fechaInicio", "fechaFin"})})
 * @ORM\Entity
 */
class Historicoconductoresbase
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhistoricoConductoresBase", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhistoricoconductoresbase;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaInicio", type="datetime", nullable=true)
     */
    private $fechainicio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaFin", type="datetime", nullable=true)
     */
    private $fechafin;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=500, nullable=true)
     */
    private $observaciones;

    /**
     * @var \Historicovehiculos
     *
     * @ORM\ManyToOne(targetEntity="Historicovehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoVehiculos", referencedColumnName="idhistoricoVehiculos")
     * })
     */
    private $idhistoricovehiculos;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersonas", referencedColumnName="idpersonas")
     * })
     */
    private $idpersonas;



    /**
     * Get idhistoricoconductoresbase.
     *
     * @return int
     */
    public function getIdhistoricoconductoresbase()
    {
        return $this->idhistoricoconductoresbase;
    }

    /**
     * Set fechainicio.
     *
     * @param \DateTime|null $fechainicio
     *
     * @return Historicoconductoresbase
     */
    public function setFechainicio($fechainicio = null)
    {
        $this->fechainicio = $fechainicio;

        return $this;
    }

    /**
     * Get fechainicio.
     *
     * @return \DateTime|null
     */
    public function getFechainicio()
    {
        return $this->fechainicio;
    }

    /**
     * Set fechafin.
     *
     * @param \DateTime|null $fechafin
     *
     * @return Historicoconductoresbase
     */
    public function setFechafin($fechafin = null)
    {
        $this->fechafin = $fechafin;

        return $this;
    }

    /**
     * Get fechafin.
     *
     * @return \DateTime|null
     */
    public function getFechafin()
    {
        return $this->fechafin;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Historicoconductoresbase
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Historicoconductoresbase
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Historicoconductoresbase
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set idhistoricovehiculos.
     *
     * @param \AppBundle\Entity\Globalcar\Historicovehiculos|null $idhistoricovehiculos
     *
     * @return Historicoconductoresbase
     */
    public function setIdhistoricovehiculos(\AppBundle\Entity\Globalcar\Historicovehiculos $idhistoricovehiculos = null)
    {
        $this->idhistoricovehiculos = $idhistoricovehiculos;

        return $this;
    }

    /**
     * Get idhistoricovehiculos.
     *
     * @return \AppBundle\Entity\Globalcar\Historicovehiculos|null
     */
    public function getIdhistoricovehiculos()
    {
        return $this->idhistoricovehiculos;
    }

    /**
     * Set idpersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Personas|null $idpersonas
     *
     * @return Historicoconductoresbase
     */
    public function setIdpersonas(\AppBundle\Entity\Globalcar\Personas $idpersonas = null)
    {
        $this->idpersonas = $idpersonas;

        return $this;
    }

    /**
     * Get idpersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Personas|null
     */
    public function getIdpersonas()
    {
        return $this->idpersonas;
    }
}
