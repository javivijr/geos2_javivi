<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country", uniqueConstraints={@ORM\UniqueConstraint(name="iso_UNIQUE", columns={"iso"}), @ORM\UniqueConstraint(name="iso3index", columns={"iso3"})}, indexes={@ORM\Index(name="nombreesp_idx", columns={"nombreEsp"}), @ORM\Index(name="nombrepor_idx", columns={"nombrePor"})})
 * @ORM\Entity
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontry", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontry;

    /**
     * @var string
     *
     * @ORM\Column(name="iso", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $iso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="printableName", type="string", length=80, nullable=true)
     */
    private $printablename;

    /**
     * @var string|null
     *
     * @ORM\Column(name="iso3", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $iso3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numcode", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $numcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreEsp", type="string", length=100, nullable=true)
     */
    private $nombreesp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombrePor", type="string", length=100, nullable=true)
     */
    private $nombrepor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="prefijoServei", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $prefijoservei;

    /**
     * @var int|null
     *
     * @ORM\Column(name="prefijoPalma", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $prefijopalma;



    /**
     * Get idcontry.
     *
     * @return int
     */
    public function getIdcontry()
    {
        return $this->idcontry;
    }

    /**
     * Set iso.
     *
     * @param string $iso
     *
     * @return Country
     */
    public function setIso($iso)
    {
        $this->iso = $iso;

        return $this;
    }

    /**
     * Get iso.
     *
     * @return string
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Country
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set printablename.
     *
     * @param string|null $printablename
     *
     * @return Country
     */
    public function setPrintablename($printablename = null)
    {
        $this->printablename = $printablename;

        return $this;
    }

    /**
     * Get printablename.
     *
     * @return string|null
     */
    public function getPrintablename()
    {
        return $this->printablename;
    }

    /**
     * Set iso3.
     *
     * @param string|null $iso3
     *
     * @return Country
     */
    public function setIso3($iso3 = null)
    {
        $this->iso3 = $iso3;

        return $this;
    }

    /**
     * Get iso3.
     *
     * @return string|null
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * Set numcode.
     *
     * @param int|null $numcode
     *
     * @return Country
     */
    public function setNumcode($numcode = null)
    {
        $this->numcode = $numcode;

        return $this;
    }

    /**
     * Get numcode.
     *
     * @return int|null
     */
    public function getNumcode()
    {
        return $this->numcode;
    }

    /**
     * Set nombreesp.
     *
     * @param string|null $nombreesp
     *
     * @return Country
     */
    public function setNombreesp($nombreesp = null)
    {
        $this->nombreesp = $nombreesp;

        return $this;
    }

    /**
     * Get nombreesp.
     *
     * @return string|null
     */
    public function getNombreesp()
    {
        return $this->nombreesp;
    }

    /**
     * Set nombrepor.
     *
     * @param string|null $nombrepor
     *
     * @return Country
     */
    public function setNombrepor($nombrepor = null)
    {
        $this->nombrepor = $nombrepor;

        return $this;
    }

    /**
     * Get nombrepor.
     *
     * @return string|null
     */
    public function getNombrepor()
    {
        return $this->nombrepor;
    }

    /**
     * Set prefijoservei.
     *
     * @param int|null $prefijoservei
     *
     * @return Country
     */
    public function setPrefijoservei($prefijoservei = null)
    {
        $this->prefijoservei = $prefijoservei;

        return $this;
    }

    /**
     * Get prefijoservei.
     *
     * @return int|null
     */
    public function getPrefijoservei()
    {
        return $this->prefijoservei;
    }

    /**
     * Set prefijopalma.
     *
     * @param int|null $prefijopalma
     *
     * @return Country
     */
    public function setPrefijopalma($prefijopalma = null)
    {
        $this->prefijopalma = $prefijopalma;

        return $this;
    }

    /**
     * Get prefijopalma.
     *
     * @return int|null
     */
    public function getPrefijopalma()
    {
        return $this->prefijopalma;
    }
}
