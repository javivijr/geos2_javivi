<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmrecuperarpass
 *
 * @ORM\Table(name="mtmrecuperarpass", indexes={@ORM\Index(name="idusuariosMtm_RCP_FK_idx", columns={"idusuariosMtm"})})
 * @ORM\Entity
 */
class Mtmrecuperarpass
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmRecuperarPass", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmrecuperarpass;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keyEnlace", type="string", length=128, nullable=true, options={"fixed"=true})
     */
    private $keyenlace;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pass", type="string", length=128, nullable=true, options={"fixed"=true})
     */
    private $pass;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=true)
     */
    private $ip;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosMtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmrecuperarpass.
     *
     * @return int
     */
    public function getIdmtmrecuperarpass()
    {
        return $this->idmtmrecuperarpass;
    }

    /**
     * Set keyenlace.
     *
     * @param string|null $keyenlace
     *
     * @return Mtmrecuperarpass
     */
    public function setKeyenlace($keyenlace = null)
    {
        $this->keyenlace = $keyenlace;

        return $this;
    }

    /**
     * Get keyenlace.
     *
     * @return string|null
     */
    public function getKeyenlace()
    {
        return $this->keyenlace;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Mtmrecuperarpass
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set pass.
     *
     * @param string|null $pass
     *
     * @return Mtmrecuperarpass
     */
    public function setPass($pass = null)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass.
     *
     * @return string|null
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return Mtmrecuperarpass
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmrecuperarpass
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
