<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Titularconfig
 *
 * @ORM\Table(name="titularconfig", indexes={@ORM\Index(name="idtitulares_TC_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idusuariook1_TC_FK_idx", columns={"idusuariook1"}), @ORM\Index(name="idusuariook2_TC_FK_idx", columns={"idusuariook2"})})
 * @ORM\Entity
 */
class Titularconfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtitularConfig", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtitularconfig;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="comunicacionesMail", type="string", length=0, nullable=false)
     */
    private $comunicacionesmail;

    /**
     * @var string
     *
     * @ORM\Column(name="carta", type="string", length=0, nullable=false)
     */
    private $carta;

    /**
     * @var string
     *
     * @ORM\Column(name="comunicacionesCarta", type="string", length=0, nullable=false)
     */
    private $comunicacionescarta;

    /**
     * @var string
     *
     * @ORM\Column(name="arrendatarioLocal", type="string", length=0, nullable=false)
     */
    private $arrendatariolocal;

    /**
     * @var string
     *
     * @ORM\Column(name="carrentLocal", type="string", length=0, nullable=false)
     */
    private $carrentlocal;

    /**
     * @var string
     *
     * @ORM\Column(name="exportacionArrendatario", type="string", length=0, nullable=false)
     */
    private $exportacionarrendatario;

    /**
     * @var string
     *
     * @ORM\Column(name="usaApiIdentificado", type="string", length=0, nullable=false)
     */
    private $usaapiidentificado;

    /**
     * @var string
     *
     * @ORM\Column(name="usaReporteLlegadas", type="string", length=0, nullable=false)
     */
    private $usareportellegadas;

    /**
     * @var string
     *
     * @ORM\Column(name="usaLineasAmarillasLlegadas", type="string", length=0, nullable=false)
     */
    private $usalineasamarillasllegadas;

    /**
     * @var string
     *
     * @ORM\Column(name="usaConfirmacionLlegadas", type="string", length=0, nullable=false)
     */
    private $usaconfirmacionllegadas;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuariook1", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idusuariook1;

    /**
     * @var string
     *
     * @ORM\Column(name="usaMTMParcial", type="string", length=0, nullable=false)
     */
    private $usamtmparcial;

    /**
     * @var string
     *
     * @ORM\Column(name="usaDenunciado", type="string", length=0, nullable=false)
     */
    private $usadenunciado;

    /**
     * @var string
     *
     * @ORM\Column(name="usaGestorFlota", type="string", length=0, nullable=false)
     */
    private $usagestorflota;

    /**
     * @var string
     *
     * @ORM\Column(name="usaCbase", type="string", length=0, nullable=false)
     */
    private $usacbase;

    /**
     * @var string
     *
     * @ORM\Column(name="condEmpresa", type="string", length=0, nullable=false)
     */
    private $condempresa;

    /**
     * @var string
     *
     * @ORM\Column(name="idiomaComunicaciones", type="string", length=0, nullable=false, options={"default"="ES"})
     */
    private $idiomacomunicaciones = 'ES';

    /**
     * @var string
     *
     * @ORM\Column(name="tipocuentavehiculos", type="string", length=0, nullable=false, options={"default"="RC"})
     */
    private $tipocuentavehiculos = 'RC';

    /**
     * @var string
     *
     * @ORM\Column(name="clienteOtrosPorDefecto", type="string", length=0, nullable=false)
     */
    private $clienteotrospordefecto;

    /**
     * @var string
     *
     * @ORM\Column(name="aceptaPago", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $aceptapago = 'n';

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariook2", referencedColumnName="idusuarios")
     * })
     */
    private $idusuariook2;


    /**
     * @var string
     *
     * @ORM\Column(name="apiext", type="string", length=50, nullable=true)
     */
    private $apiext;


    /**
     * Get idtitularconfig.
     *
     * @return int
     */
    public function getIdtitularconfig()
    {
        return $this->idtitularconfig;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Titularconfig
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set comunicacionesmail.
     *
     * @param string $comunicacionesmail
     *
     * @return Titularconfig
     */
    public function setComunicacionesmail($comunicacionesmail)
    {
        $this->comunicacionesmail = $comunicacionesmail;

        return $this;
    }

    /**
     * Get comunicacionesmail.
     *
     * @return string
     */
    public function getComunicacionesmail()
    {
        return $this->comunicacionesmail;
    }

    /**
     * Set carta.
     *
     * @param string $carta
     *
     * @return Titularconfig
     */
    public function setCarta($carta)
    {
        $this->carta = $carta;

        return $this;
    }

    /**
     * Get carta.
     *
     * @return string
     */
    public function getCarta()
    {
        return $this->carta;
    }

    /**
     * Set comunicacionescarta.
     *
     * @param string $comunicacionescarta
     *
     * @return Titularconfig
     */
    public function setComunicacionescarta($comunicacionescarta)
    {
        $this->comunicacionescarta = $comunicacionescarta;

        return $this;
    }

    /**
     * Get comunicacionescarta.
     *
     * @return string
     */
    public function getComunicacionescarta()
    {
        return $this->comunicacionescarta;
    }

    /**
     * Set arrendatariolocal.
     *
     * @param string $arrendatariolocal
     *
     * @return Titularconfig
     */
    public function setArrendatariolocal($arrendatariolocal)
    {
        $this->arrendatariolocal = $arrendatariolocal;

        return $this;
    }

    /**
     * Get arrendatariolocal.
     *
     * @return string
     */
    public function getArrendatariolocal()
    {
        return $this->arrendatariolocal;
    }

    /**
     * Set carrentlocal.
     *
     * @param string $carrentlocal
     *
     * @return Titularconfig
     */
    public function setCarrentlocal($carrentlocal)
    {
        $this->carrentlocal = $carrentlocal;

        return $this;
    }

    /**
     * Get carrentlocal.
     *
     * @return string
     */
    public function getCarrentlocal()
    {
        return $this->carrentlocal;
    }

    /**
     * Set exportacionarrendatario.
     *
     * @param string $exportacionarrendatario
     *
     * @return Titularconfig
     */
    public function setExportacionarrendatario($exportacionarrendatario)
    {
        $this->exportacionarrendatario = $exportacionarrendatario;

        return $this;
    }

    /**
     * Get exportacionarrendatario.
     *
     * @return string
     */
    public function getExportacionarrendatario()
    {
        return $this->exportacionarrendatario;
    }

    /**
     * Set usaapiidentificado.
     *
     * @param string $usaapiidentificado
     *
     * @return Titularconfig
     */
    public function setUsaapiidentificado($usaapiidentificado)
    {
        $this->usaapiidentificado = $usaapiidentificado;

        return $this;
    }

    /**
     * Get usaapiidentificado.
     *
     * @return string
     */
    public function getUsaapiidentificado()
    {
        return $this->usaapiidentificado;
    }

    /**
     * Set usareportellegadas.
     *
     * @param string $usareportellegadas
     *
     * @return Titularconfig
     */
    public function setUsareportellegadas($usareportellegadas)
    {
        $this->usareportellegadas = $usareportellegadas;

        return $this;
    }

    /**
     * Get usareportellegadas.
     *
     * @return string
     */
    public function getUsareportellegadas()
    {
        return $this->usareportellegadas;
    }

    /**
     * Set usalineasamarillasllegadas.
     *
     * @param string $usalineasamarillasllegadas
     *
     * @return Titularconfig
     */
    public function setUsalineasamarillasllegadas($usalineasamarillasllegadas)
    {
        $this->usalineasamarillasllegadas = $usalineasamarillasllegadas;

        return $this;
    }

    /**
     * Get usalineasamarillasllegadas.
     *
     * @return string
     */
    public function getUsalineasamarillasllegadas()
    {
        return $this->usalineasamarillasllegadas;
    }

    /**
     * Set usaconfirmacionllegadas.
     *
     * @param string $usaconfirmacionllegadas
     *
     * @return Titularconfig
     */
    public function setUsaconfirmacionllegadas($usaconfirmacionllegadas)
    {
        $this->usaconfirmacionllegadas = $usaconfirmacionllegadas;

        return $this;
    }

    /**
     * Get usaconfirmacionllegadas.
     *
     * @return string
     */
    public function getUsaconfirmacionllegadas()
    {
        return $this->usaconfirmacionllegadas;
    }

    /**
     * Set idusuariook1.
     *
     * @param int $idusuariook1
     *
     * @return Titularconfig
     */
    public function setIdusuariook1($idusuariook1)
    {
        $this->idusuariook1 = $idusuariook1;

        return $this;
    }

    /**
     * Get idusuariook1.
     *
     * @return int
     */
    public function getIdusuariook1()
    {
        return $this->idusuariook1;
    }

    /**
     * Set usamtmparcial.
     *
     * @param string $usamtmparcial
     *
     * @return Titularconfig
     */
    public function setUsamtmparcial($usamtmparcial)
    {
        $this->usamtmparcial = $usamtmparcial;

        return $this;
    }

    /**
     * Get usamtmparcial.
     *
     * @return string
     */
    public function getUsamtmparcial()
    {
        return $this->usamtmparcial;
    }

    /**
     * Set usadenunciado.
     *
     * @param string $usadenunciado
     *
     * @return Titularconfig
     */
    public function setUsadenunciado($usadenunciado)
    {
        $this->usadenunciado = $usadenunciado;

        return $this;
    }

    /**
     * Get usadenunciado.
     *
     * @return string
     */
    public function getUsadenunciado()
    {
        return $this->usadenunciado;
    }

    /**
     * Set usagestorflota.
     *
     * @param string $usagestorflota
     *
     * @return Titularconfig
     */
    public function setUsagestorflota($usagestorflota)
    {
        $this->usagestorflota = $usagestorflota;

        return $this;
    }

    /**
     * Get usagestorflota.
     *
     * @return string
     */
    public function getUsagestorflota()
    {
        return $this->usagestorflota;
    }

    /**
     * Set usacbase.
     *
     * @param string $usacbase
     *
     * @return Titularconfig
     */
    public function setUsacbase($usacbase)
    {
        $this->usacbase = $usacbase;

        return $this;
    }

    /**
     * Get usacbase.
     *
     * @return string
     */
    public function getUsacbase()
    {
        return $this->usacbase;
    }

    /**
     * Set condempresa.
     *
     * @param string $condempresa
     *
     * @return Titularconfig
     */
    public function setCondempresa($condempresa)
    {
        $this->condempresa = $condempresa;

        return $this;
    }

    /**
     * Get condempresa.
     *
     * @return string
     */
    public function getCondempresa()
    {
        return $this->condempresa;
    }

    /**
     * Set idiomacomunicaciones.
     *
     * @param string $idiomacomunicaciones
     *
     * @return Titularconfig
     */
    public function setIdiomacomunicaciones($idiomacomunicaciones)
    {
        $this->idiomacomunicaciones = $idiomacomunicaciones;

        return $this;
    }

    /**
     * Get idiomacomunicaciones.
     *
     * @return string
     */
    public function getIdiomacomunicaciones()
    {
        return $this->idiomacomunicaciones;
    }

    /**
     * Set tipocuentavehiculos.
     *
     * @param string $tipocuentavehiculos
     *
     * @return Titularconfig
     */
    public function setTipocuentavehiculos($tipocuentavehiculos)
    {
        $this->tipocuentavehiculos = $tipocuentavehiculos;

        return $this;
    }

    /**
     * Get tipocuentavehiculos.
     *
     * @return string
     */
    public function getTipocuentavehiculos()
    {
        return $this->tipocuentavehiculos;
    }

    /**
     * Set clienteotrospordefecto.
     *
     * @param string $clienteotrospordefecto
     *
     * @return Titularconfig
     */
    public function setClienteotrospordefecto($clienteotrospordefecto)
    {
        $this->clienteotrospordefecto = $clienteotrospordefecto;

        return $this;
    }

    /**
     * Get clienteotrospordefecto.
     *
     * @return string
     */
    public function getClienteotrospordefecto()
    {
        return $this->clienteotrospordefecto;
    }

    /**
     * Set aceptapago.
     *
     * @param string $aceptapago
     *
     * @return Titularconfig
     */
    public function setAceptapago($aceptapago)
    {
        $this->aceptapago = $aceptapago;

        return $this;
    }

    /**
     * Get aceptapago.
     *
     * @return string
     */
    public function getAceptapago()
    {
        return $this->aceptapago;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Titularconfig
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set idusuariook2.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuariook2
     *
     * @return Titularconfig
     */
    public function setIdusuariook2(\AppBundle\Entity\Globalcar\Usuarios $idusuariook2 = null)
    {
        $this->idusuariook2 = $idusuariook2;

        return $this;
    }

    /**
     * Get idusuariook2.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuariook2()
    {
        return $this->idusuariook2;
    }

    /**
     * @return string|null
     */
    public function getApiext()
    {
        return $this->apiext;
    }

    /**
     * @param string $apiext
     */
    public function setApiext(string $apiext): void
    {
        $this->apiext = $apiext;
    }


}
