<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientesdenunciado
 *
 * @ORM\Table(name="expedientesdenunciado", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_ED_FK_idx", columns={"idexpedientes"})}, indexes={@ORM\Index(name="idhistoricoPersonas_ED_FK_idx", columns={"idhistoricoPersonas"})})
 * @ORM\Entity
 */
class Expedientesdenunciado
{
    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientesDenunciado", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idexpedientesdenunciado;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Historicopersonas
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoPersonas", referencedColumnName="idhistoricoPersonas")
     * })
     */
    private $idhistoricopersonas;



    /**
     * Get idexpedientesdenunciado.
     *
     * @return int
     */
    public function getIdexpedientesdenunciado()
    {
        return $this->idexpedientesdenunciado;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Expedientesdenunciado
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idhistoricopersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonas|null $idhistoricopersonas
     *
     * @return Expedientesdenunciado
     */
    public function setIdhistoricopersonas(\AppBundle\Entity\Globalcar\Historicopersonas $idhistoricopersonas = null)
    {
        $this->idhistoricopersonas = $idhistoricopersonas;

        return $this;
    }

    /**
     * Get idhistoricopersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonas|null
     */
    public function getIdhistoricopersonas()
    {
        return $this->idhistoricopersonas;
    }
}
