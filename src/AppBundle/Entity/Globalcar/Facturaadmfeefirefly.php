<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facturaadmfeefirefly
 *
 * @ORM\Table(name="facturaadmfeefirefly", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_UNIQUE", columns={"idexpedientes"})}, indexes={@ORM\Index(name="IDX_D76D732B59A462DE", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Facturaadmfeefirefly
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Cust_no", type="string", length=20, nullable=false)
     */
    private $custNo;

    /**
     * @var string
     *
     * @ORM\Column(name="Last_name", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="First_name", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="Invoice_no", type="string", length=50, nullable=false)
     */
    private $invoiceNo;

    /**
     * @var string
     *
     * @ORM\Column(name="Email_address", type="string", length=255, nullable=false)
     */
    private $emailAddress;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="Tax_ID", type="string", length=255, nullable=false)
     */
    private $taxId;

    /**
     * @var string
     *
     * @ORM\Column(name="Address_1", type="string", length=255, nullable=false)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="Address_2", type="string", length=255, nullable=false)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="St_desc", type="string", length=255, nullable=false)
     */
    private $stDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="Post_code", type="string", length=255, nullable=false)
     */
    private $postCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rutadoc", type="string", length=1000, nullable=true)
     */
    private $rutadoc;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set custNo.
     *
     * @param string $custNo
     *
     * @return Facturaadmfeefirefly
     */
    public function setCustNo($custNo)
    {
        $this->custNo = $custNo;

        return $this;
    }

    /**
     * Get custNo.
     *
     * @return string
     */
    public function getCustNo()
    {
        return $this->custNo;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return Facturaadmfeefirefly
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return Facturaadmfeefirefly
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set invoiceNo.
     *
     * @param string $invoiceNo
     *
     * @return Facturaadmfeefirefly
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo.
     *
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set emailAddress.
     *
     * @param string $emailAddress
     *
     * @return Facturaadmfeefirefly
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress.
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Facturaadmfeefirefly
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set taxId.
     *
     * @param string $taxId
     *
     * @return Facturaadmfeefirefly
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;

        return $this;
    }

    /**
     * Get taxId.
     *
     * @return string
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * Set address1.
     *
     * @param string $address1
     *
     * @return Facturaadmfeefirefly
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1.
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2.
     *
     * @param string $address2
     *
     * @return Facturaadmfeefirefly
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2.
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Facturaadmfeefirefly
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set stDesc.
     *
     * @param string $stDesc
     *
     * @return Facturaadmfeefirefly
     */
    public function setStDesc($stDesc)
    {
        $this->stDesc = $stDesc;

        return $this;
    }

    /**
     * Get stDesc.
     *
     * @return string
     */
    public function getStDesc()
    {
        return $this->stDesc;
    }

    /**
     * Set postCode.
     *
     * @param string $postCode
     *
     * @return Facturaadmfeefirefly
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode.
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set rutadoc.
     *
     * @param string|null $rutadoc
     *
     * @return Facturaadmfeefirefly
     */
    public function setRutadoc($rutadoc = null)
    {
        $this->rutadoc = $rutadoc;

        return $this;
    }

    /**
     * Get rutadoc.
     *
     * @return string|null
     */
    public function getRutadoc()
    {
        return $this->rutadoc;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Facturaadmfeefirefly
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
