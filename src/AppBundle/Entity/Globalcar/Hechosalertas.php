<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hechosalertas
 *
 * @ORM\Table(name="hechosalertas", indexes={@ORM\Index(name="idhechosalerta_FK_idx", columns={"idhechoalerta"})})
 * @ORM\Entity
 */
class Hechosalertas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhechosalertas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhechosalertas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="textoalerta", type="string", length=255, nullable=true)
     */
    private $textoalerta;

    /**
     * @var \Hechos
     *
     * @ORM\ManyToOne(targetEntity="Hechos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhechoalerta", referencedColumnName="idhechos")
     * })
     */
    private $idhechoalerta;



    /**
     * Get idhechosalertas.
     *
     * @return int
     */
    public function getIdhechosalertas()
    {
        return $this->idhechosalertas;
    }

    /**
     * Set textoalerta.
     *
     * @param string|null $textoalerta
     *
     * @return Hechosalertas
     */
    public function setTextoalerta($textoalerta = null)
    {
        $this->textoalerta = $textoalerta;

        return $this;
    }

    /**
     * Get textoalerta.
     *
     * @return string|null
     */
    public function getTextoalerta()
    {
        return $this->textoalerta;
    }

    /**
     * Set idhechoalerta.
     *
     * @param \AppBundle\Entity\Globalcar\Hechos|null $idhechoalerta
     *
     * @return Hechosalertas
     */
    public function setIdhechoalerta(\AppBundle\Entity\Globalcar\Hechos $idhechoalerta = null)
    {
        $this->idhechoalerta = $idhechoalerta;

        return $this;
    }

    /**
     * Get idhechoalerta.
     *
     * @return \AppBundle\Entity\Globalcar\Hechos|null
     */
    public function getIdhechoalerta()
    {
        return $this->idhechoalerta;
    }
}
