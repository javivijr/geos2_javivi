<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Respuestatelematica
 *
 * @ORM\Table(name="respuestatelematica", indexes={@ORM\Index(name="idexpedientes_RT_FK_idx", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Respuestatelematica
{
    /**
     * @var int
     *
     * @ORM\Column(name="idrespuestaTelematica", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrespuestatelematica;

    /**
     * @var string
     *
     * @ORM\Column(name="ok", type="string", length=0, nullable=false)
     */
    private $ok;

    /**
     * @var string|null
     *
     * @ORM\Column(name="respuestaTxt", type="string", length=500, nullable=true)
     */
    private $respuestatxt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaRespuesta", type="datetime", nullable=true)
     */
    private $fecharespuesta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaComprobacion", type="datetime", nullable=true)
     */
    private $fechacomprobacion;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idrespuestatelematica.
     *
     * @return int
     */
    public function getIdrespuestatelematica()
    {
        return $this->idrespuestatelematica;
    }

    /**
     * Set ok.
     *
     * @param string $ok
     *
     * @return Respuestatelematica
     */
    public function setOk($ok)
    {
        $this->ok = $ok;

        return $this;
    }

    /**
     * Get ok.
     *
     * @return string
     */
    public function getOk()
    {
        return $this->ok;
    }

    /**
     * Set respuestatxt.
     *
     * @param string|null $respuestatxt
     *
     * @return Respuestatelematica
     */
    public function setRespuestatxt($respuestatxt = null)
    {
        $this->respuestatxt = $respuestatxt;

        return $this;
    }

    /**
     * Get respuestatxt.
     *
     * @return string|null
     */
    public function getRespuestatxt()
    {
        return $this->respuestatxt;
    }

    /**
     * Set fecharespuesta.
     *
     * @param \DateTime|null $fecharespuesta
     *
     * @return Respuestatelematica
     */
    public function setFecharespuesta($fecharespuesta = null)
    {
        $this->fecharespuesta = $fecharespuesta;

        return $this;
    }

    /**
     * Get fecharespuesta.
     *
     * @return \DateTime|null
     */
    public function getFecharespuesta()
    {
        return $this->fecharespuesta;
    }

    /**
     * Set fechacomprobacion.
     *
     * @param \DateTime|null $fechacomprobacion
     *
     * @return Respuestatelematica
     */
    public function setFechacomprobacion($fechacomprobacion = null)
    {
        $this->fechacomprobacion = $fechacomprobacion;

        return $this;
    }

    /**
     * Get fechacomprobacion.
     *
     * @return \DateTime|null
     */
    public function getFechacomprobacion()
    {
        return $this->fechacomprobacion;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Respuestatelematica
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
