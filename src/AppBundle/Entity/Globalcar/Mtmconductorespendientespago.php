<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmconductorespendientespago
 *
 * @ORM\Table(name="mtmconductorespendientespago", indexes={@ORM\Index(name="idcontratos_CPP_FK_idx", columns={"idcontratos"}), @ORM\Index(name="idtipoCalle_CPP_FK_idx", columns={"idtipoCalle"})})
 * @ORM\Entity
 */
class Mtmconductorespendientespago
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmConductoresPendientesPago", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmconductorespendientespago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nif", type="string", length=20, nullable=true)
     */
    private $nif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=70, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido1", type="string", length=50, nullable=true)
     */
    private $apellido1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido2", type="string", length=50, nullable=true)
     */
    private $apellido2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="calle", type="string", length=100, nullable=true)
     */
    private $calle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=11, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="portal", type="string", length=3, nullable=true)
     */
    private $portal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="escalera", type="string", length=3, nullable=true)
     */
    private $escalera;

    /**
     * @var string|null
     *
     * @ORM\Column(name="piso", type="string", length=2, nullable=true)
     */
    private $piso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="puerta", type="string", length=2, nullable=true)
     */
    private $puerta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=10, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poblacion", type="string", length=60, nullable=true)
     */
    private $poblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=60, nullable=true)
     */
    private $provincia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais", type="string", length=20, nullable=true)
     */
    private $pais;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoDoc", type="string", length=0, nullable=false)
     */
    private $tipodoc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", length=13, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referencia", type="string", length=100, nullable=true)
     */
    private $referencia;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;

    /**
     * @var \Tipocalle
     *
     * @ORM\ManyToOne(targetEntity="Tipocalle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipoCalle", referencedColumnName="idtipoCalle")
     * })
     */
    private $idtipocalle;



    /**
     * Get idmtmconductorespendientespago.
     *
     * @return int
     */
    public function getIdmtmconductorespendientespago()
    {
        return $this->idmtmconductorespendientespago;
    }

    /**
     * Set nif.
     *
     * @param string|null $nif
     *
     * @return Mtmconductorespendientespago
     */
    public function setNif($nif = null)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif.
     *
     * @return string|null
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Mtmconductorespendientespago
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1.
     *
     * @param string|null $apellido1
     *
     * @return Mtmconductorespendientespago
     */
    public function setApellido1($apellido1 = null)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1.
     *
     * @return string|null
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2.
     *
     * @param string|null $apellido2
     *
     * @return Mtmconductorespendientespago
     */
    public function setApellido2($apellido2 = null)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2.
     *
     * @return string|null
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set calle.
     *
     * @param string|null $calle
     *
     * @return Mtmconductorespendientespago
     */
    public function setCalle($calle = null)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle.
     *
     * @return string|null
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numero.
     *
     * @param string|null $numero
     *
     * @return Mtmconductorespendientespago
     */
    public function setNumero($numero = null)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero.
     *
     * @return string|null
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set portal.
     *
     * @param string|null $portal
     *
     * @return Mtmconductorespendientespago
     */
    public function setPortal($portal = null)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal.
     *
     * @return string|null
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * Set escalera.
     *
     * @param string|null $escalera
     *
     * @return Mtmconductorespendientespago
     */
    public function setEscalera($escalera = null)
    {
        $this->escalera = $escalera;

        return $this;
    }

    /**
     * Get escalera.
     *
     * @return string|null
     */
    public function getEscalera()
    {
        return $this->escalera;
    }

    /**
     * Set piso.
     *
     * @param string|null $piso
     *
     * @return Mtmconductorespendientespago
     */
    public function setPiso($piso = null)
    {
        $this->piso = $piso;

        return $this;
    }

    /**
     * Get piso.
     *
     * @return string|null
     */
    public function getPiso()
    {
        return $this->piso;
    }

    /**
     * Set puerta.
     *
     * @param string|null $puerta
     *
     * @return Mtmconductorespendientespago
     */
    public function setPuerta($puerta = null)
    {
        $this->puerta = $puerta;

        return $this;
    }

    /**
     * Get puerta.
     *
     * @return string|null
     */
    public function getPuerta()
    {
        return $this->puerta;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Mtmconductorespendientespago
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set poblacion.
     *
     * @param string|null $poblacion
     *
     * @return Mtmconductorespendientespago
     */
    public function setPoblacion($poblacion = null)
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * Get poblacion.
     *
     * @return string|null
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Mtmconductorespendientespago
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set pais.
     *
     * @param string|null $pais
     *
     * @return Mtmconductorespendientespago
     */
    public function setPais($pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return string|null
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Mtmconductorespendientespago
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tipodoc.
     *
     * @param string $tipodoc
     *
     * @return Mtmconductorespendientespago
     */
    public function setTipodoc($tipodoc)
    {
        $this->tipodoc = $tipodoc;

        return $this;
    }

    /**
     * Get tipodoc.
     *
     * @return string
     */
    public function getTipodoc()
    {
        return $this->tipodoc;
    }

    /**
     * Set telefono.
     *
     * @param string|null $telefono
     *
     * @return Mtmconductorespendientespago
     */
    public function setTelefono($telefono = null)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono.
     *
     * @return string|null
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set referencia.
     *
     * @param string|null $referencia
     *
     * @return Mtmconductorespendientespago
     */
    public function setReferencia($referencia = null)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia.
     *
     * @return string|null
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Mtmconductorespendientespago
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }

    /**
     * Set idtipocalle.
     *
     * @param \AppBundle\Entity\Globalcar\Tipocalle|null $idtipocalle
     *
     * @return Mtmconductorespendientespago
     */
    public function setIdtipocalle(\AppBundle\Entity\Globalcar\Tipocalle $idtipocalle = null)
    {
        $this->idtipocalle = $idtipocalle;

        return $this;
    }

    /**
     * Get idtipocalle.
     *
     * @return \AppBundle\Entity\Globalcar\Tipocalle|null
     */
    public function getIdtipocalle()
    {
        return $this->idtipocalle;
    }
}
