<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Titularesproveedores
 *
 * @ORM\Table(name="titularesproveedores", indexes={@ORM\Index(name="idtitulares_TIP_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idproveedores_TIP_FK_idx", columns={"idproveedor"})})
 * @ORM\Entity
 */
class Titularesproveedores
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtitularesProveedores", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtitularesproveedores;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idproveedor", referencedColumnName="idtitulares")
     * })
     */
    private $idproveedor;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idtitularesproveedores.
     *
     * @return int
     */
    public function getIdtitularesproveedores()
    {
        return $this->idtitularesproveedores;
    }

    /**
     * Set idproveedor.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idproveedor
     *
     * @return Titularesproveedores
     */
    public function setIdproveedor(\AppBundle\Entity\Globalcar\Titulares $idproveedor = null)
    {
        $this->idproveedor = $idproveedor;

        return $this;
    }

    /**
     * Get idproveedor.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdproveedor()
    {
        return $this->idproveedor;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Titularesproveedores
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
