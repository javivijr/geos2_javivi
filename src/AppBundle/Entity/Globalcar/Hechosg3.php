<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hechosg3
 *
 * @ORM\Table(name="hechosg3", indexes={@ORM\Index(name="idhechos_HG3_FK_idx", columns={"idhechos"})})
 * @ORM\Entity
 */
class Hechosg3
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhechosg3", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhechosg3;

    /**
     * @var string
     *
     * @ORM\Column(name="codhechoant", type="string", length=15, nullable=false)
     */
    private $codhechoant;

    /**
     * @var string
     *
     * @ORM\Column(name="madrid", type="string", length=0, nullable=false)
     */
    private $madrid;

    /**
     * @var \Hechos
     *
     * @ORM\ManyToOne(targetEntity="Hechos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhechos", referencedColumnName="idhechos")
     * })
     */
    private $idhechos;



    /**
     * Get idhechosg3.
     *
     * @return int
     */
    public function getIdhechosg3()
    {
        return $this->idhechosg3;
    }

    /**
     * Set codhechoant.
     *
     * @param string $codhechoant
     *
     * @return Hechosg3
     */
    public function setCodhechoant($codhechoant)
    {
        $this->codhechoant = $codhechoant;

        return $this;
    }

    /**
     * Get codhechoant.
     *
     * @return string
     */
    public function getCodhechoant()
    {
        return $this->codhechoant;
    }

    /**
     * Set madrid.
     *
     * @param string $madrid
     *
     * @return Hechosg3
     */
    public function setMadrid($madrid)
    {
        $this->madrid = $madrid;

        return $this;
    }

    /**
     * Get madrid.
     *
     * @return string
     */
    public function getMadrid()
    {
        return $this->madrid;
    }

    /**
     * Set idhechos.
     *
     * @param \AppBundle\Entity\Globalcar\Hechos|null $idhechos
     *
     * @return Hechosg3
     */
    public function setIdhechos(\AppBundle\Entity\Globalcar\Hechos $idhechos = null)
    {
        $this->idhechos = $idhechos;

        return $this;
    }

    /**
     * Get idhechos.
     *
     * @return \AppBundle\Entity\Globalcar\Hechos|null
     */
    public function getIdhechos()
    {
        return $this->idhechos;
    }
}
