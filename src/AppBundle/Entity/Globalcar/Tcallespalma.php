<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tcallespalma
 *
 * @ORM\Table(name="tcallespalma", indexes={@ORM\Index(name="nombreCalle_UNIQUE", columns={"nombreCalle"})})
 * @ORM\Entity
 */
class Tcallespalma
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtcallespalma", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtcallespalma;

    /**
     * @var string
     *
     * @ORM\Column(name="codCalle", type="string", length=10, nullable=false)
     */
    private $codcalle;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreCalle", type="string", length=70, nullable=false)
     */
    private $nombrecalle;



    /**
     * Get idtcallespalma.
     *
     * @return int
     */
    public function getIdtcallespalma()
    {
        return $this->idtcallespalma;
    }

    /**
     * Set codcalle.
     *
     * @param string $codcalle
     *
     * @return Tcallespalma
     */
    public function setCodcalle($codcalle)
    {
        $this->codcalle = $codcalle;

        return $this;
    }

    /**
     * Get codcalle.
     *
     * @return string
     */
    public function getCodcalle()
    {
        return $this->codcalle;
    }

    /**
     * Set nombrecalle.
     *
     * @param string $nombrecalle
     *
     * @return Tcallespalma
     */
    public function setNombrecalle($nombrecalle)
    {
        $this->nombrecalle = $nombrecalle;

        return $this;
    }

    /**
     * Get nombrecalle.
     *
     * @return string
     */
    public function getNombrecalle()
    {
        return $this->nombrecalle;
    }
}
