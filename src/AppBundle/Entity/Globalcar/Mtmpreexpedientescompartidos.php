<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmpreexpedientescompartidos
 *
 * @ORM\Table(name="mtmpreexpedientescompartidos", uniqueConstraints={@ORM\UniqueConstraint(name="idpreexpedientes_PEC_FK_idx", columns={"idpreexpedientes"})}, indexes={@ORM\Index(name="idcontratos_PEC_FK_idx", columns={"idcontratos"})})
 * @ORM\Entity
 */
class Mtmpreexpedientescompartidos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmpreexpedientescompartidos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmpreexpedientescompartidos;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=false)
     */
    private $tipo;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;

    /**
     * @var \Preexpedientes
     *
     * @ORM\ManyToOne(targetEntity="Preexpedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpreexpedientes", referencedColumnName="idpreExpedientes")
     * })
     */
    private $idpreexpedientes;



    /**
     * Get idmtmpreexpedientescompartidos.
     *
     * @return int
     */
    public function getIdmtmpreexpedientescompartidos()
    {
        return $this->idmtmpreexpedientescompartidos;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Mtmpreexpedientescompartidos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Mtmpreexpedientescompartidos
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }

    /**
     * Set idpreexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Preexpedientes|null $idpreexpedientes
     *
     * @return Mtmpreexpedientescompartidos
     */
    public function setIdpreexpedientes(\AppBundle\Entity\Globalcar\Preexpedientes $idpreexpedientes = null)
    {
        $this->idpreexpedientes = $idpreexpedientes;

        return $this;
    }

    /**
     * Get idpreexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Preexpedientes|null
     */
    public function getIdpreexpedientes()
    {
        return $this->idpreexpedientes;
    }
}
