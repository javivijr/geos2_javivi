<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculos
 *
 * @ORM\Table(name="vehiculos", uniqueConstraints={@ORM\UniqueConstraint(name="matricula_UNIQUE", columns={"matricula"})})
 * @ORM\Entity
 */
class Vehiculos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idvehiculos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idvehiculos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bastidor", type="string", length=25, nullable=true)
     */
    private $bastidor;

    /**
     * @var string
     *
     * @ORM\Column(name="matricula", type="string", length=10, nullable=false)
     */
    private $matricula;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaMatriculacion", type="datetime", nullable=true)
     */
    private $fechamatriculacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="marca", type="string", length=20, nullable=true)
     */
    private $marca;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modelo", type="string", length=22, nullable=true)
     */
    private $modelo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=20, nullable=true)
     */
    private $color;



    /**
     * Get idvehiculos.
     *
     * @return int
     */
    public function getIdvehiculos()
    {
        return $this->idvehiculos;
    }

    /**
     * Set bastidor.
     *
     * @param string|null $bastidor
     *
     * @return Vehiculos
     */
    public function setBastidor($bastidor = null)
    {
        $this->bastidor = $bastidor;

        return $this;
    }

    /**
     * Get bastidor.
     *
     * @return string|null
     */
    public function getBastidor()
    {
        return $this->bastidor;
    }

    /**
     * Set matricula.
     *
     * @param string $matricula
     *
     * @return Vehiculos
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula.
     *
     * @return string
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set fechamatriculacion.
     *
     * @param \DateTime|null $fechamatriculacion
     *
     * @return Vehiculos
     */
    public function setFechamatriculacion($fechamatriculacion = null)
    {
        $this->fechamatriculacion = $fechamatriculacion;

        return $this;
    }

    /**
     * Get fechamatriculacion.
     *
     * @return \DateTime|null
     */
    public function getFechamatriculacion()
    {
        return $this->fechamatriculacion;
    }

    /**
     * Set marca.
     *
     * @param string|null $marca
     *
     * @return Vehiculos
     */
    public function setMarca($marca = null)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca.
     *
     * @return string|null
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo.
     *
     * @param string|null $modelo
     *
     * @return Vehiculos
     */
    public function setModelo($modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo.
     *
     * @return string|null
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set color.
     *
     * @param string|null $color
     *
     * @return Vehiculos
     */
    public function setColor($color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }
}
