<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Perfiles
 *
 * @ORM\Table(name="perfiles")
 * @ORM\Entity
 */
class Perfiles
{
    /**
     * @var int
     *
     * @ORM\Column(name="idperfiles", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idperfiles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="valor", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $valor;



    /**
     * Get idperfiles.
     *
     * @return int
     */
    public function getIdperfiles()
    {
        return $this->idperfiles;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Perfiles
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set valor.
     *
     * @param int|null $valor
     *
     * @return Perfiles
     */
    public function setValor($valor = null)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor.
     *
     * @return int|null
     */
    public function getValor()
    {
        return $this->valor;
    }
}
