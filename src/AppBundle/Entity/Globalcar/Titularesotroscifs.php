<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Titularesotroscifs
 *
 * @ORM\Table(name="titularesotroscifs", indexes={@ORM\Index(name="idtitulares_FK_toc_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Titularesotroscifs
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtitularesotroscifs", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtitularesotroscifs;

    /**
     * @var string
     *
     * @ORM\Column(name="cif", type="string", length=20, nullable=false)
     */
    private $cif;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idtitularesotroscifs.
     *
     * @return int
     */
    public function getIdtitularesotroscifs()
    {
        return $this->idtitularesotroscifs;
    }

    /**
     * Set cif.
     *
     * @param string $cif
     *
     * @return Titularesotroscifs
     */
    public function setCif($cif)
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * Get cif.
     *
     * @return string
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Titularesotroscifs
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
