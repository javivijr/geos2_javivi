<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrentfurgoline
 *
 * @ORM\Table(name="carrentfurgoline", indexes={@ORM\Index(name="tcarrentfurgoline_idexp_IDX", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Carrentfurgoline
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrentfurgoline", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrentfurgoline;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idexpedientes", type="integer", nullable=true)
     */
    private $idexpedientes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecharespuesta", type="datetime", nullable=false)
     */
    private $fecharespuesta;

    /**
     * @var string
     *
     * @ORM\Column(name="numcontrato", type="string", length=50, nullable=false)
     */
    private $numcontrato;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresacif", type="string", length=30, nullable=true)
     */
    private $empresacif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresanombre", type="string", length=500, nullable=true)
     */
    private $empresanombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresaapellido1", type="string", length=250, nullable=true)
     */
    private $empresaapellido1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresaapellido2", type="string", length=250, nullable=true)
     */
    private $empresaapellido2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresadomiciliocompleto", type="string", length=1000, nullable=true)
     */
    private $empresadomiciliocompleto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cif", type="string", length=30, nullable=true)
     */
    private $cif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=500, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido1", type="string", length=250, nullable=true)
     */
    private $apellido1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido2", type="string", length=250, nullable=true)
     */
    private $apellido2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="domiciliocompleto", type="string", length=1000, nullable=true)
     */
    private $domiciliocompleto;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="conductorfechanacimiento", type="datetime", nullable=true)
     */
    private $conductorfechanacimiento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=500, nullable=true)
     */
    private $observaciones;



    /**
     * Get idcarrentfurgoline.
     *
     * @return int
     */
    public function getIdcarrentfurgoline()
    {
        return $this->idcarrentfurgoline;
    }

    /**
     * Set idexpedientes.
     *
     * @param int|null $idexpedientes
     *
     * @return Carrentfurgoline
     */
    public function setIdexpedientes($idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return int|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set fecharespuesta.
     *
     * @param \DateTime $fecharespuesta
     *
     * @return Carrentfurgoline
     */
    public function setFecharespuesta($fecharespuesta)
    {
        $this->fecharespuesta = $fecharespuesta;

        return $this;
    }

    /**
     * Get fecharespuesta.
     *
     * @return \DateTime
     */
    public function getFecharespuesta()
    {
        return $this->fecharespuesta;
    }

    /**
     * Set numcontrato.
     *
     * @param string $numcontrato
     *
     * @return Carrentfurgoline
     */
    public function setNumcontrato($numcontrato)
    {
        $this->numcontrato = $numcontrato;

        return $this;
    }

    /**
     * Get numcontrato.
     *
     * @return string
     */
    public function getNumcontrato()
    {
        return $this->numcontrato;
    }

    /**
     * Set empresacif.
     *
     * @param string|null $empresacif
     *
     * @return Carrentfurgoline
     */
    public function setEmpresacif($empresacif = null)
    {
        $this->empresacif = $empresacif;

        return $this;
    }

    /**
     * Get empresacif.
     *
     * @return string|null
     */
    public function getEmpresacif()
    {
        return $this->empresacif;
    }

    /**
     * Set empresanombre.
     *
     * @param string|null $empresanombre
     *
     * @return Carrentfurgoline
     */
    public function setEmpresanombre($empresanombre = null)
    {
        $this->empresanombre = $empresanombre;

        return $this;
    }

    /**
     * Get empresanombre.
     *
     * @return string|null
     */
    public function getEmpresanombre()
    {
        return $this->empresanombre;
    }

    /**
     * Set empresaapellido1.
     *
     * @param string|null $empresaapellido1
     *
     * @return Carrentfurgoline
     */
    public function setEmpresaapellido1($empresaapellido1 = null)
    {
        $this->empresaapellido1 = $empresaapellido1;

        return $this;
    }

    /**
     * Get empresaapellido1.
     *
     * @return string|null
     */
    public function getEmpresaapellido1()
    {
        return $this->empresaapellido1;
    }

    /**
     * Set empresaapellido2.
     *
     * @param string|null $empresaapellido2
     *
     * @return Carrentfurgoline
     */
    public function setEmpresaapellido2($empresaapellido2 = null)
    {
        $this->empresaapellido2 = $empresaapellido2;

        return $this;
    }

    /**
     * Get empresaapellido2.
     *
     * @return string|null
     */
    public function getEmpresaapellido2()
    {
        return $this->empresaapellido2;
    }

    /**
     * Set empresadomiciliocompleto.
     *
     * @param string|null $empresadomiciliocompleto
     *
     * @return Carrentfurgoline
     */
    public function setEmpresadomiciliocompleto($empresadomiciliocompleto = null)
    {
        $this->empresadomiciliocompleto = $empresadomiciliocompleto;

        return $this;
    }

    /**
     * Get empresadomiciliocompleto.
     *
     * @return string|null
     */
    public function getEmpresadomiciliocompleto()
    {
        return $this->empresadomiciliocompleto;
    }

    /**
     * Set cif.
     *
     * @param string|null $cif
     *
     * @return Carrentfurgoline
     */
    public function setCif($cif = null)
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * Get cif.
     *
     * @return string|null
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Carrentfurgoline
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1.
     *
     * @param string|null $apellido1
     *
     * @return Carrentfurgoline
     */
    public function setApellido1($apellido1 = null)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1.
     *
     * @return string|null
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2.
     *
     * @param string|null $apellido2
     *
     * @return Carrentfurgoline
     */
    public function setApellido2($apellido2 = null)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2.
     *
     * @return string|null
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set domiciliocompleto.
     *
     * @param string|null $domiciliocompleto
     *
     * @return Carrentfurgoline
     */
    public function setDomiciliocompleto($domiciliocompleto = null)
    {
        $this->domiciliocompleto = $domiciliocompleto;

        return $this;
    }

    /**
     * Get domiciliocompleto.
     *
     * @return string|null
     */
    public function getDomiciliocompleto()
    {
        return $this->domiciliocompleto;
    }

    /**
     * Set conductorfechanacimiento.
     *
     * @param \DateTime|null $conductorfechanacimiento
     *
     * @return Carrentfurgoline
     */
    public function setConductorfechanacimiento($conductorfechanacimiento = null)
    {
        $this->conductorfechanacimiento = $conductorfechanacimiento;

        return $this;
    }

    /**
     * Get conductorfechanacimiento.
     *
     * @return \DateTime|null
     */
    public function getConductorfechanacimiento()
    {
        return $this->conductorfechanacimiento;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Carrentfurgoline
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }
}
