<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Legal
 *
 * @ORM\Table(name="legal", indexes={@ORM\Index(name="idexpedientes_LG_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idlegalEscritosSalida_LG_FK_idx", columns={"idlegalEscritosSalida"}), @ORM\Index(name="idusuarios_fkk", columns={"idusuarios"})})
 * @ORM\Entity
 */
class Legal
{
    /**
     * @var int
     *
     * @ORM\Column(name="idlegal", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlegal;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaRecepcion", type="datetime", nullable=true)
     */
    private $fecharecepcion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaIncoaccion", type="datetime", nullable=true)
     */
    private $fechaincoaccion;

    /**
     * @var string
     *
     * @ORM\Column(name="urgente", type="string", length=0, nullable=false)
     */
    private $urgente;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Legalescritosalida
     *
     * @ORM\ManyToOne(targetEntity="Legalescritosalida")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idlegalEscritosSalida", referencedColumnName="idlegalEscritoSalida")
     * })
     */
    private $idlegalescritossalida;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarios", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarios;



    /**
     * Get idlegal.
     *
     * @return int
     */
    public function getIdlegal()
    {
        return $this->idlegal;
    }

    /**
     * Set fecharecepcion.
     *
     * @param \DateTime|null $fecharecepcion
     *
     * @return Legal
     */
    public function setFecharecepcion($fecharecepcion = null)
    {
        $this->fecharecepcion = $fecharecepcion;

        return $this;
    }

    /**
     * Get fecharecepcion.
     *
     * @return \DateTime|null
     */
    public function getFecharecepcion()
    {
        return $this->fecharecepcion;
    }

    /**
     * Set fechaincoaccion.
     *
     * @param \DateTime|null $fechaincoaccion
     *
     * @return Legal
     */
    public function setFechaincoaccion($fechaincoaccion = null)
    {
        $this->fechaincoaccion = $fechaincoaccion;

        return $this;
    }

    /**
     * Get fechaincoaccion.
     *
     * @return \DateTime|null
     */
    public function getFechaincoaccion()
    {
        return $this->fechaincoaccion;
    }

    /**
     * Set urgente.
     *
     * @param string $urgente
     *
     * @return Legal
     */
    public function setUrgente($urgente)
    {
        $this->urgente = $urgente;

        return $this;
    }

    /**
     * Get urgente.
     *
     * @return string
     */
    public function getUrgente()
    {
        return $this->urgente;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Legal
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idlegalescritossalida.
     *
     * @param \AppBundle\Entity\Globalcar\Legalescritosalida|null $idlegalescritossalida
     *
     * @return Legal
     */
    public function setIdlegalescritossalida(\AppBundle\Entity\Globalcar\Legalescritosalida $idlegalescritossalida = null)
    {
        $this->idlegalescritossalida = $idlegalescritossalida;

        return $this;
    }

    /**
     * Get idlegalescritossalida.
     *
     * @return \AppBundle\Entity\Globalcar\Legalescritosalida|null
     */
    public function getIdlegalescritossalida()
    {
        return $this->idlegalescritossalida;
    }

    /**
     * Set idusuarios.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarios
     *
     * @return Legal
     */
    public function setIdusuarios(\AppBundle\Entity\Globalcar\Usuarios $idusuarios = null)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }
}
