<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Escritoentrada
 *
 * @ORM\Table(name="escritoentrada", uniqueConstraints={@ORM\UniqueConstraint(name="codigo_UNIQUE", columns={"codigo"})})
 * @ORM\Entity
 */
class Escritoentrada
{
    /**
     * @var int
     *
     * @ORM\Column(name="idescritoEntrada", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idescritoentrada;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=50, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="reporteRecursos", type="string", length=0, nullable=false)
     */
    private $reporterecursos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vencimiento", type="integer", nullable=true, options={"default"="10","unsigned"=true})
     */
    private $vencimiento = '10';

    /**
     * @var string
     *
     * @ORM\Column(name="naturales", type="string", length=0, nullable=false)
     */
    private $naturales;



    /**
     * Get idescritoentrada.
     *
     * @return int
     */
    public function getIdescritoentrada()
    {
        return $this->idescritoentrada;
    }

    /**
     * Set codigo.
     *
     * @param string|null $codigo
     *
     * @return Escritoentrada
     */
    public function setCodigo($codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo.
     *
     * @return string|null
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Escritoentrada
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipo.
     *
     * @param string|null $tipo
     *
     * @return Escritoentrada
     */
    public function setTipo($tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string|null
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set reporterecursos.
     *
     * @param string $reporterecursos
     *
     * @return Escritoentrada
     */
    public function setReporterecursos($reporterecursos)
    {
        $this->reporterecursos = $reporterecursos;

        return $this;
    }

    /**
     * Get reporterecursos.
     *
     * @return string
     */
    public function getReporterecursos()
    {
        return $this->reporterecursos;
    }

    /**
     * Set vencimiento.
     *
     * @param int|null $vencimiento
     *
     * @return Escritoentrada
     */
    public function setVencimiento($vencimiento = null)
    {
        $this->vencimiento = $vencimiento;

        return $this;
    }

    /**
     * Get vencimiento.
     *
     * @return int|null
     */
    public function getVencimiento()
    {
        return $this->vencimiento;
    }

    /**
     * Set naturales.
     *
     * @param string $naturales
     *
     * @return Escritoentrada
     */
    public function setNaturales($naturales)
    {
        $this->naturales = $naturales;

        return $this;
    }

    /**
     * Get naturales.
     *
     * @return string
     */
    public function getNaturales()
    {
        return $this->naturales;
    }
}
