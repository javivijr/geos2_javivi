<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documentospreexp
 *
 * @ORM\Table(name="documentospreexp", indexes={@ORM\Index(name="idpreexpedientes_DPE_FK_idx", columns={"idpreexpedientes"})})
 * @ORM\Entity
 */
class Documentospreexp
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddocumentospreexp", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddocumentospreexp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta", type="string", length=200, nullable=true)
     */
    private $ruta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="alta", type="datetime", nullable=true)
     */
    private $alta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=100, nullable=true)
     */
    private $usuario;

    /**
     * @var \Preexpedientes
     *
     * @ORM\ManyToOne(targetEntity="Preexpedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpreexpedientes", referencedColumnName="idpreExpedientes")
     * })
     */
    private $idpreexpedientes;



    /**
     * Get iddocumentospreexp.
     *
     * @return int
     */
    public function getIddocumentospreexp()
    {
        return $this->iddocumentospreexp;
    }

    /**
     * Set ruta.
     *
     * @param string|null $ruta
     *
     * @return Documentospreexp
     */
    public function setRuta($ruta = null)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta.
     *
     * @return string|null
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set alta.
     *
     * @param \DateTime|null $alta
     *
     * @return Documentospreexp
     */
    public function setAlta($alta = null)
    {
        $this->alta = $alta;

        return $this;
    }

    /**
     * Get alta.
     *
     * @return \DateTime|null
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Documentospreexp
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set idpreexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Preexpedientes|null $idpreexpedientes
     *
     * @return Documentospreexp
     */
    public function setIdpreexpedientes(\AppBundle\Entity\Globalcar\Preexpedientes $idpreexpedientes = null)
    {
        $this->idpreexpedientes = $idpreexpedientes;

        return $this;
    }

    /**
     * Get idpreexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Preexpedientes|null
     */
    public function getIdpreexpedientes()
    {
        return $this->idpreexpedientes;
    }
}
