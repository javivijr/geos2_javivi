<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuariosrequisitos
 *
 * @ORM\Table(name="usuariosrequisitos", indexes={@ORM\Index(name="idusuarios_USR_FK_idx", columns={"idusuarios"}), @ORM\Index(name="idacciones_USR_FK_idx", columns={"idacciones"})})
 * @ORM\Entity
 */
class Usuariosrequisitos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idusuariosRequisitos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuariosrequisitos;

    /**
     * @var int
     *
     * @ORM\Column(name="permiso", type="integer", nullable=false, options={"unsigned"=true,"comment"="0-Nada,1-Lectura,2-Edicion,3-Borrado,4-Supervision"})
     */
    private $permiso;

    /**
     * @var \Acciones
     *
     * @ORM\ManyToOne(targetEntity="Acciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idacciones", referencedColumnName="idacciones")
     * })
     */
    private $idacciones;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarios", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarios;



    /**
     * Get idusuariosrequisitos.
     *
     * @return int
     */
    public function getIdusuariosrequisitos()
    {
        return $this->idusuariosrequisitos;
    }

    /**
     * Set permiso.
     *
     * @param int $permiso
     *
     * @return Usuariosrequisitos
     */
    public function setPermiso($permiso)
    {
        $this->permiso = $permiso;

        return $this;
    }

    /**
     * Get permiso.
     *
     * @return int
     */
    public function getPermiso()
    {
        return $this->permiso;
    }

    /**
     * Set idacciones.
     *
     * @param \AppBundle\Entity\Globalcar\Acciones|null $idacciones
     *
     * @return Usuariosrequisitos
     */
    public function setIdacciones(\AppBundle\Entity\Globalcar\Acciones $idacciones = null)
    {
        $this->idacciones = $idacciones;

        return $this;
    }

    /**
     * Get idacciones.
     *
     * @return \AppBundle\Entity\Globalcar\Acciones|null
     */
    public function getIdacciones()
    {
        return $this->idacciones;
    }

    /**
     * Set idusuarios.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarios
     *
     * @return Usuariosrequisitos
     */
    public function setIdusuarios(\AppBundle\Entity\Globalcar\Usuarios $idusuarios = null)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }
}
