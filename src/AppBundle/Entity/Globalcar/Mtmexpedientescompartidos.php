<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmexpedientescompartidos
 *
 * @ORM\Table(name="mtmexpedientescompartidos", uniqueConstraints={@ORM\UniqueConstraint(name="idexpedientes_EC_FK_idx", columns={"idexpedientes"})}, indexes={@ORM\Index(name="idexpedientes_ECd_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idcontratos_ECd_FK_idx", columns={"idcontratos"})})
 * @ORM\Entity
 */
class Mtmexpedientescompartidos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmexpedientescompartidos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmexpedientescompartidos;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=false)
     */
    private $tipo;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontratos", referencedColumnName="idcontratos")
     * })
     */
    private $idcontratos;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idmtmexpedientescompartidos.
     *
     * @return int
     */
    public function getIdmtmexpedientescompartidos()
    {
        return $this->idmtmexpedientescompartidos;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Mtmexpedientescompartidos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set idcontratos.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontratos
     *
     * @return Mtmexpedientescompartidos
     */
    public function setIdcontratos(\AppBundle\Entity\Globalcar\Contratos $idcontratos = null)
    {
        $this->idcontratos = $idcontratos;

        return $this;
    }

    /**
     * Get idcontratos.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Mtmexpedientescompartidos
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
