<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documentos
 *
 * @ORM\Table(name="documentos", indexes={@ORM\Index(name="idexpedientes_DO_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="ruta_DO_IND", columns={"ruta"}), @ORM\Index(name="existe", columns={"existe"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Globalcar\Repository\DocumentosRepository")
 */
class Documentos
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddocumentos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddocumentos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta", type="string", length=200, nullable=true)
     */
    private $ruta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="alta", type="datetime", nullable=true)
     */
    private $alta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="altaReal", type="datetime", nullable=true)
     */
    private $altareal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sha1", type="string", length=100, nullable=true)
     */
    private $sha1;

    /**
     * @var string
     *
     * @ORM\Column(name="existe", type="string", length=0, nullable=false)
     */
    private $existe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rutamini", type="string", length=200, nullable=true)
     */
    private $rutamini;

    /**
     * @var string
     *
     * @ORM\Column(name="visiblemtm", type="string", length=0, nullable=false, options={"default"="auto"})
     */
    private $visiblemtm = 'auto';

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get iddocumentos.
     *
     * @return int
     */
    public function getIddocumentos()
    {
        return $this->iddocumentos;
    }

    /**
     * Set ruta.
     *
     * @param string|null $ruta
     *
     * @return Documentos
     */
    public function setRuta($ruta = null)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta.
     *
     * @return string|null
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set alta.
     *
     * @param \DateTime|null $alta
     *
     * @return Documentos
     */
    public function setAlta($alta = null)
    {
        $this->alta = $alta;

        return $this;
    }

    /**
     * Get alta.
     *
     * @return \DateTime|null
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * Set altareal.
     *
     * @param \DateTime|null $altareal
     *
     * @return Documentos
     */
    public function setAltareal($altareal = null)
    {
        $this->altareal = $altareal;

        return $this;
    }

    /**
     * Get altareal.
     *
     * @return \DateTime|null
     */
    public function getAltareal()
    {
        return $this->altareal;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Documentos
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set sha1.
     *
     * @param string|null $sha1
     *
     * @return Documentos
     */
    public function setSha1($sha1 = null)
    {
        $this->sha1 = $sha1;

        return $this;
    }

    /**
     * Get sha1.
     *
     * @return string|null
     */
    public function getSha1()
    {
        return $this->sha1;
    }

    /**
     * Set existe.
     *
     * @param string $existe
     *
     * @return Documentos
     */
    public function setExiste($existe)
    {
        $this->existe = $existe;

        return $this;
    }

    /**
     * Get existe.
     *
     * @return string
     */
    public function getExiste()
    {
        return $this->existe;
    }

    /**
     * Set rutamini.
     *
     * @param string|null $rutamini
     *
     * @return Documentos
     */
    public function setRutamini($rutamini = null)
    {
        $this->rutamini = $rutamini;

        return $this;
    }

    /**
     * Get rutamini.
     *
     * @return string|null
     */
    public function getRutamini()
    {
        return $this->rutamini;
    }

    /**
     * Set visiblemtm.
     *
     * @param string $visiblemtm
     *
     * @return Documentos
     */
    public function setVisiblemtm($visiblemtm)
    {
        $this->visiblemtm = $visiblemtm;

        return $this;
    }

    /**
     * Get visiblemtm.
     *
     * @return string
     */
    public function getVisiblemtm()
    {
        return $this->visiblemtm;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Documentos
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
