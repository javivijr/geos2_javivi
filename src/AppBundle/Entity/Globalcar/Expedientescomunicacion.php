<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientescomunicacion
 *
 * @ORM\Table(name="expedientescomunicacion", indexes={@ORM\Index(name="idexpedientes_EC_FK_idx", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Expedientescomunicacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientesComunicacion", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idexpedientescomunicacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaEnvio", type="datetime", nullable=true)
     */
    private $fechaenvio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="plantillaUsada", type="string", length=150, nullable=true)
     */
    private $plantillausada;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idexpedientescomunicacion.
     *
     * @return int
     */
    public function getIdexpedientescomunicacion()
    {
        return $this->idexpedientescomunicacion;
    }

    /**
     * Set fechaenvio.
     *
     * @param \DateTime|null $fechaenvio
     *
     * @return Expedientescomunicacion
     */
    public function setFechaenvio($fechaenvio = null)
    {
        $this->fechaenvio = $fechaenvio;

        return $this;
    }

    /**
     * Get fechaenvio.
     *
     * @return \DateTime|null
     */
    public function getFechaenvio()
    {
        return $this->fechaenvio;
    }

    /**
     * Set plantillausada.
     *
     * @param string|null $plantillausada
     *
     * @return Expedientescomunicacion
     */
    public function setPlantillausada($plantillausada = null)
    {
        $this->plantillausada = $plantillausada;

        return $this;
    }

    /**
     * Get plantillausada.
     *
     * @return string|null
     */
    public function getPlantillausada()
    {
        return $this->plantillausada;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Expedientescomunicacion
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
