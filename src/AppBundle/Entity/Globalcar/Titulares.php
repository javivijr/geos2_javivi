<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Titulares
 *
 * @ORM\Table(name="titulares", indexes={@ORM\Index(name="nif_TIT_idx", columns={"nif"}), @ORM\Index(name="nombreTIT_idx", columns={"nombre"}), @ORM\Index(name="apellido1TIT_idx", columns={"apellido1"}), @ORM\Index(name="apellido2TIT_idx", columns={"apellido2"}), @ORM\Index(name="codigoArval", columns={"codigoArval"}), @ORM\Index(name="codigoOtros", columns={"codigoOtros"})})
 * @ORM\Entity
 */
class Titulares
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtitulares", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtitulares;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=70, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido1", type="string", length=50, nullable=true)
     */
    private $apellido1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido2", type="string", length=50, nullable=true)
     */
    private $apellido2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nif", type="string", length=20, nullable=true)
     */
    private $nif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="domicilio", type="string", length=100, nullable=true)
     */
    private $domicilio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=10, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poblacion", type="string", length=60, nullable=true)
     */
    private $poblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais", type="string", length=20, nullable=true)
     */
    private $pais;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telPrivado", type="string", length=13, nullable=true)
     */
    private $telprivado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telPro", type="string", length=13, nullable=true)
     */
    private $telpro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="movil", type="string", length=13, nullable=true)
     */
    private $movil;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fax", type="string", length=13, nullable=true)
     */
    private $fax;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email2", type="string", length=100, nullable=true)
     */
    private $email2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=70, nullable=true)
     */
    private $provincia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origen", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoParticular", type="string", length=0, nullable=false)
     */
    private $tipoparticular;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor", type="string", length=0, nullable=false)
     */
    private $proveedor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombregestor", type="string", length=100, nullable=true)
     */
    private $nombregestor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefonogestor", type="string", length=50, nullable=true)
     */
    private $telefonogestor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigocompraarval", type="string", length=20, nullable=true)
     */
    private $codigocompraarval;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoArval", type="string", length=20, nullable=true)
     */
    private $codigoarval;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoOtros", type="integer", nullable=true)
     */
    private $codigootros;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correocopiacomunicaciones", type="string", length=100, nullable=true)
     */
    private $correocopiacomunicaciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apoderado", type="string", length=0, nullable=true, options={"default"="n"})
     */
    private $apoderado = 'n';

    /**
     * @ORM\OneToOne(targetEntity="Titularconfig", mappedBy="idtitulares", fetch="EAGER")
     */
    protected $titularconfig;

    /**
     * Get titularconfig
     *
     * @return TitularConfig
     */
    public function getTitularconfig()
    {
        return $this->titularconfig;
    }

    /**
     * Get idtitulares.
     *
     * @return int
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Titulares
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1.
     *
     * @param string|null $apellido1
     *
     * @return Titulares
     */
    public function setApellido1($apellido1 = null)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1.
     *
     * @return string|null
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2.
     *
     * @param string|null $apellido2
     *
     * @return Titulares
     */
    public function setApellido2($apellido2 = null)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2.
     *
     * @return string|null
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set nif.
     *
     * @param string|null $nif
     *
     * @return Titulares
     */
    public function setNif($nif = null)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif.
     *
     * @return string|null
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * Set domicilio.
     *
     * @param string|null $domicilio
     *
     * @return Titulares
     */
    public function setDomicilio($domicilio = null)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio.
     *
     * @return string|null
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Titulares
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set poblacion.
     *
     * @param string|null $poblacion
     *
     * @return Titulares
     */
    public function setPoblacion($poblacion = null)
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * Get poblacion.
     *
     * @return string|null
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * Set pais.
     *
     * @param string|null $pais
     *
     * @return Titulares
     */
    public function setPais($pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return string|null
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set telprivado.
     *
     * @param string|null $telprivado
     *
     * @return Titulares
     */
    public function setTelprivado($telprivado = null)
    {
        $this->telprivado = $telprivado;

        return $this;
    }

    /**
     * Get telprivado.
     *
     * @return string|null
     */
    public function getTelprivado()
    {
        return $this->telprivado;
    }

    /**
     * Set telpro.
     *
     * @param string|null $telpro
     *
     * @return Titulares
     */
    public function setTelpro($telpro = null)
    {
        $this->telpro = $telpro;

        return $this;
    }

    /**
     * Get telpro.
     *
     * @return string|null
     */
    public function getTelpro()
    {
        return $this->telpro;
    }

    /**
     * Set movil.
     *
     * @param string|null $movil
     *
     * @return Titulares
     */
    public function setMovil($movil = null)
    {
        $this->movil = $movil;

        return $this;
    }

    /**
     * Get movil.
     *
     * @return string|null
     */
    public function getMovil()
    {
        return $this->movil;
    }

    /**
     * Set fax.
     *
     * @param string|null $fax
     *
     * @return Titulares
     */
    public function setFax($fax = null)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string|null
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Titulares
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email2.
     *
     * @param string|null $email2
     *
     * @return Titulares
     */
    public function setEmail2($email2 = null)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2.
     *
     * @return string|null
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Titulares
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set origen.
     *
     * @param string|null $origen
     *
     * @return Titulares
     */
    public function setOrigen($origen = null)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string|null
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set tipoparticular.
     *
     * @param string $tipoparticular
     *
     * @return Titulares
     */
    public function setTipoparticular($tipoparticular)
    {
        $this->tipoparticular = $tipoparticular;

        return $this;
    }

    /**
     * Get tipoparticular.
     *
     * @return string
     */
    public function getTipoparticular()
    {
        return $this->tipoparticular;
    }

    /**
     * Set proveedor.
     *
     * @param string $proveedor
     *
     * @return Titulares
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor.
     *
     * @return string
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set nombregestor.
     *
     * @param string|null $nombregestor
     *
     * @return Titulares
     */
    public function setNombregestor($nombregestor = null)
    {
        $this->nombregestor = $nombregestor;

        return $this;
    }

    /**
     * Get nombregestor.
     *
     * @return string|null
     */
    public function getNombregestor()
    {
        return $this->nombregestor;
    }

    /**
     * Set telefonogestor.
     *
     * @param string|null $telefonogestor
     *
     * @return Titulares
     */
    public function setTelefonogestor($telefonogestor = null)
    {
        $this->telefonogestor = $telefonogestor;

        return $this;
    }

    /**
     * Get telefonogestor.
     *
     * @return string|null
     */
    public function getTelefonogestor()
    {
        return $this->telefonogestor;
    }

    /**
     * Set codigocompraarval.
     *
     * @param string|null $codigocompraarval
     *
     * @return Titulares
     */
    public function setCodigocompraarval($codigocompraarval = null)
    {
        $this->codigocompraarval = $codigocompraarval;

        return $this;
    }

    /**
     * Get codigocompraarval.
     *
     * @return string|null
     */
    public function getCodigocompraarval()
    {
        return $this->codigocompraarval;
    }

    /**
     * Set codigoarval.
     *
     * @param string|null $codigoarval
     *
     * @return Titulares
     */
    public function setCodigoarval($codigoarval = null)
    {
        $this->codigoarval = $codigoarval;

        return $this;
    }

    /**
     * Get codigoarval.
     *
     * @return string|null
     */
    public function getCodigoarval()
    {
        return $this->codigoarval;
    }

    /**
     * Set codigootros.
     *
     * @param int|null $codigootros
     *
     * @return Titulares
     */
    public function setCodigootros($codigootros = null)
    {
        $this->codigootros = $codigootros;

        return $this;
    }

    /**
     * Get codigootros.
     *
     * @return int|null
     */
    public function getCodigootros()
    {
        return $this->codigootros;
    }

    /**
     * Set correocopiacomunicaciones.
     *
     * @param string|null $correocopiacomunicaciones
     *
     * @return Titulares
     */
    public function setCorreocopiacomunicaciones($correocopiacomunicaciones = null)
    {
        $this->correocopiacomunicaciones = $correocopiacomunicaciones;

        return $this;
    }

    /**
     * Get correocopiacomunicaciones.
     *
     * @return string|null
     */
    public function getCorreocopiacomunicaciones()
    {
        return $this->correocopiacomunicaciones;
    }

    /**
     * Set apoderado.
     *
     * @param string|null $apoderado
     *
     * @return Titulares
     */
    public function setApoderado($apoderado = null)
    {
        $this->apoderado = $apoderado;

        return $this;
    }

    /**
     * Get apoderado.
     *
     * @return string|null
     */
    public function getApoderado()
    {
        return $this->apoderado;
    }
}
