<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organismos
 *
 * @ORM\Table(name="organismos", indexes={@ORM\Index(name="nombre_ORG_IDX", columns={"nombre"}), @ORM\Index(name="fukfkfukfukfukcountry", columns={"pais"})})
 * @ORM\Entity
 */
class Organismos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idorganismos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idorganismos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="plazo", type="integer", nullable=true, options={"default"="20","unsigned"=true})
     */
    private $plazo = '20';

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion", type="string", length=100, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="aceptaAutoid", type="string", length=0, nullable=false)
     */
    private $aceptaautoid;

    /**
     * @var string
     *
     * @ORM\Column(name="naturales", type="string", length=0, nullable=false)
     */
    private $naturales;

    /**
     * @var string|null
     *
     * @ORM\Column(name="metodoPago", type="string", length=400, nullable=true)
     */
    private $metodopago;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idbancos", type="integer", nullable=true)
     */
    private $idbancos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipopago", type="string", length=1, nullable=true)
     */
    private $tipopago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", length=200, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="personacontacto", type="string", length=200, nullable=true)
     */
    private $personacontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailpersonacontacto", type="string", length=200, nullable=true)
     */
    private $emailpersonacontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefonopersonacontacto", type="string", length=200, nullable=true)
     */
    private $telefonopersonacontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="personacontactopago", type="string", length=200, nullable=true)
     */
    private $personacontactopago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailpersonacontactopago", type="string", length=200, nullable=true)
     */
    private $emailpersonacontactopago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefonopersonacontactopago", type="string", length=200, nullable=true)
     */
    private $telefonopersonacontactopago;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=0, nullable=false, options={"default"="ES"})
     */
    private $idioma = 'ES';

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pais", referencedColumnName="idcontry")
     * })
     */
    private $pais;



    /**
     * Get idorganismos.
     *
     * @return int
     */
    public function getIdorganismos()
    {
        return $this->idorganismos;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Organismos
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set plazo.
     *
     * @param int|null $plazo
     *
     * @return Organismos
     */
    public function setPlazo($plazo = null)
    {
        $this->plazo = $plazo;

        return $this;
    }

    /**
     * Get plazo.
     *
     * @return int|null
     */
    public function getPlazo()
    {
        return $this->plazo;
    }

    /**
     * Set direccion.
     *
     * @param string|null $direccion
     *
     * @return Organismos
     */
    public function setDireccion($direccion = null)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion.
     *
     * @return string|null
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set aceptaautoid.
     *
     * @param string $aceptaautoid
     *
     * @return Organismos
     */
    public function setAceptaautoid($aceptaautoid)
    {
        $this->aceptaautoid = $aceptaautoid;

        return $this;
    }

    /**
     * Get aceptaautoid.
     *
     * @return string
     */
    public function getAceptaautoid()
    {
        return $this->aceptaautoid;
    }

    /**
     * Set naturales.
     *
     * @param string $naturales
     *
     * @return Organismos
     */
    public function setNaturales($naturales)
    {
        $this->naturales = $naturales;

        return $this;
    }

    /**
     * Get naturales.
     *
     * @return string
     */
    public function getNaturales()
    {
        return $this->naturales;
    }

    /**
     * Set metodopago.
     *
     * @param string|null $metodopago
     *
     * @return Organismos
     */
    public function setMetodopago($metodopago = null)
    {
        $this->metodopago = $metodopago;

        return $this;
    }

    /**
     * Get metodopago.
     *
     * @return string|null
     */
    public function getMetodopago()
    {
        return $this->metodopago;
    }

    /**
     * Set idbancos.
     *
     * @param int|null $idbancos
     *
     * @return Organismos
     */
    public function setIdbancos($idbancos = null)
    {
        $this->idbancos = $idbancos;

        return $this;
    }

    /**
     * Get idbancos.
     *
     * @return int|null
     */
    public function getIdbancos()
    {
        return $this->idbancos;
    }

    /**
     * Set tipopago.
     *
     * @param string|null $tipopago
     *
     * @return Organismos
     */
    public function setTipopago($tipopago = null)
    {
        $this->tipopago = $tipopago;

        return $this;
    }

    /**
     * Get tipopago.
     *
     * @return string|null
     */
    public function getTipopago()
    {
        return $this->tipopago;
    }

    /**
     * Set telefono.
     *
     * @param string|null $telefono
     *
     * @return Organismos
     */
    public function setTelefono($telefono = null)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono.
     *
     * @return string|null
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set personacontacto.
     *
     * @param string|null $personacontacto
     *
     * @return Organismos
     */
    public function setPersonacontacto($personacontacto = null)
    {
        $this->personacontacto = $personacontacto;

        return $this;
    }

    /**
     * Get personacontacto.
     *
     * @return string|null
     */
    public function getPersonacontacto()
    {
        return $this->personacontacto;
    }

    /**
     * Set emailpersonacontacto.
     *
     * @param string|null $emailpersonacontacto
     *
     * @return Organismos
     */
    public function setEmailpersonacontacto($emailpersonacontacto = null)
    {
        $this->emailpersonacontacto = $emailpersonacontacto;

        return $this;
    }

    /**
     * Get emailpersonacontacto.
     *
     * @return string|null
     */
    public function getEmailpersonacontacto()
    {
        return $this->emailpersonacontacto;
    }

    /**
     * Set telefonopersonacontacto.
     *
     * @param string|null $telefonopersonacontacto
     *
     * @return Organismos
     */
    public function setTelefonopersonacontacto($telefonopersonacontacto = null)
    {
        $this->telefonopersonacontacto = $telefonopersonacontacto;

        return $this;
    }

    /**
     * Get telefonopersonacontacto.
     *
     * @return string|null
     */
    public function getTelefonopersonacontacto()
    {
        return $this->telefonopersonacontacto;
    }

    /**
     * Set personacontactopago.
     *
     * @param string|null $personacontactopago
     *
     * @return Organismos
     */
    public function setPersonacontactopago($personacontactopago = null)
    {
        $this->personacontactopago = $personacontactopago;

        return $this;
    }

    /**
     * Get personacontactopago.
     *
     * @return string|null
     */
    public function getPersonacontactopago()
    {
        return $this->personacontactopago;
    }

    /**
     * Set emailpersonacontactopago.
     *
     * @param string|null $emailpersonacontactopago
     *
     * @return Organismos
     */
    public function setEmailpersonacontactopago($emailpersonacontactopago = null)
    {
        $this->emailpersonacontactopago = $emailpersonacontactopago;

        return $this;
    }

    /**
     * Get emailpersonacontactopago.
     *
     * @return string|null
     */
    public function getEmailpersonacontactopago()
    {
        return $this->emailpersonacontactopago;
    }

    /**
     * Set telefonopersonacontactopago.
     *
     * @param string|null $telefonopersonacontactopago
     *
     * @return Organismos
     */
    public function setTelefonopersonacontactopago($telefonopersonacontactopago = null)
    {
        $this->telefonopersonacontactopago = $telefonopersonacontactopago;

        return $this;
    }

    /**
     * Get telefonopersonacontactopago.
     *
     * @return string|null
     */
    public function getTelefonopersonacontactopago()
    {
        return $this->telefonopersonacontactopago;
    }

    /**
     * Set idioma.
     *
     * @param string $idioma
     *
     * @return Organismos
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma.
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set pais.
     *
     * @param \AppBundle\Entity\Globalcar\Country|null $pais
     *
     * @return Organismos
     */
    public function setPais(\AppBundle\Entity\Globalcar\Country $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return \AppBundle\Entity\Globalcar\Country|null
     */
    public function getPais()
    {
        return $this->pais;
    }
}
