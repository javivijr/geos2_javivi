<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmfaqproveedores
 *
 * @ORM\Table(name="mtmfaqproveedores", indexes={@ORM\Index(name="idmtmfaq_MFP_FK_idx", columns={"idmtmfaq"}), @ORM\Index(name="idtitulares_MFP_FK_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Mtmfaqproveedores
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmfaqproveedores", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmfaqproveedores;

    /**
     * @var \Mtmfaq
     *
     * @ORM\ManyToOne(targetEntity="Mtmfaq")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idmtmfaq", referencedColumnName="idmtmFaq")
     * })
     */
    private $idmtmfaq;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idmtmfaqproveedores.
     *
     * @return int
     */
    public function getIdmtmfaqproveedores()
    {
        return $this->idmtmfaqproveedores;
    }

    /**
     * Set idmtmfaq.
     *
     * @param \AppBundle\Entity\Globalcar\Mtmfaq|null $idmtmfaq
     *
     * @return Mtmfaqproveedores
     */
    public function setIdmtmfaq(\AppBundle\Entity\Globalcar\Mtmfaq $idmtmfaq = null)
    {
        $this->idmtmfaq = $idmtmfaq;

        return $this;
    }

    /**
     * Get idmtmfaq.
     *
     * @return \AppBundle\Entity\Globalcar\Mtmfaq|null
     */
    public function getIdmtmfaq()
    {
        return $this->idmtmfaq;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmfaqproveedores
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
