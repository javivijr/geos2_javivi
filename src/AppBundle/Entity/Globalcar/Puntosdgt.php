<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Puntosdgt
 *
 * @ORM\Table(name="puntosdgt", indexes={@ORM\Index(name="idusuariosmtm_PDGT_FK_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Puntosdgt
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpuntosdgt", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpuntosdgt;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=15, nullable=false)
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="clave", type="string", length=45, nullable=false)
     */
    private $clave;

    /**
     * @var int
     *
     * @ORM\Column(name="puntos", type="integer", nullable=false, options={"default"="-1"})
     */
    private $puntos = '-1';

    /**
     * @var int
     *
     * @ORM\Column(name="puntosanteriores", type="integer", nullable=false, options={"default"="-1"})
     */
    private $puntosanteriores = '-1';

    /**
     * @var string
     *
     * @ORM\Column(name="cambio", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $cambio = 'n';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actualizado", type="datetime", nullable=false, options={"default"="1970-01-01 00:00:00"})
     */
    private $actualizado = '1970-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="string", length=80, nullable=false)
     */
    private $error = '';

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idpuntosdgt.
     *
     * @return int
     */
    public function getIdpuntosdgt()
    {
        return $this->idpuntosdgt;
    }

    /**
     * Set dni.
     *
     * @param string $dni
     *
     * @return Puntosdgt
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni.
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set clave.
     *
     * @param string $clave
     *
     * @return Puntosdgt
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave.
     *
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set puntos.
     *
     * @param int $puntos
     *
     * @return Puntosdgt
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos.
     *
     * @return int
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set puntosanteriores.
     *
     * @param int $puntosanteriores
     *
     * @return Puntosdgt
     */
    public function setPuntosanteriores($puntosanteriores)
    {
        $this->puntosanteriores = $puntosanteriores;

        return $this;
    }

    /**
     * Get puntosanteriores.
     *
     * @return int
     */
    public function getPuntosanteriores()
    {
        return $this->puntosanteriores;
    }

    /**
     * Set cambio.
     *
     * @param string $cambio
     *
     * @return Puntosdgt
     */
    public function setCambio($cambio)
    {
        $this->cambio = $cambio;

        return $this;
    }

    /**
     * Get cambio.
     *
     * @return string
     */
    public function getCambio()
    {
        return $this->cambio;
    }

    /**
     * Set actualizado.
     *
     * @param \DateTime $actualizado
     *
     * @return Puntosdgt
     */
    public function setActualizado($actualizado)
    {
        $this->actualizado = $actualizado;

        return $this;
    }

    /**
     * Get actualizado.
     *
     * @return \DateTime
     */
    public function getActualizado()
    {
        return $this->actualizado;
    }

    /**
     * Set error.
     *
     * @param string $error
     *
     * @return Puntosdgt
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error.
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Puntosdgt
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
