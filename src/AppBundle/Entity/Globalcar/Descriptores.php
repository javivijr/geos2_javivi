<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descriptores
 *
 * @ORM\Table(name="descriptores", uniqueConstraints={@ORM\UniqueConstraint(name="clave_2", columns={"clave"})}, indexes={@ORM\Index(name="tipo", columns={"tipo"}), @ORM\Index(name="clave", columns={"clave"}), @ORM\Index(name="visible", columns={"visible"}), @ORM\Index(name="IDX_1B0F1882D7FE4266", columns={"iddescriptorcategoria"})})
 * @ORM\Entity
 */
class Descriptores
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddescriptores", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddescriptores;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="clave", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $clave;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="string", length=0, nullable=false)
     */
    private $visible;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoAnt", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $codigoant;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcionPT", type="string", length=100, nullable=true)
     */
    private $descripcionpt;

    /**
     * @var \Descriptorcategoria
     *
     * @ORM\ManyToOne(targetEntity="Descriptorcategoria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddescriptorcategoria", referencedColumnName="iddescriptorcategoria")
     * })
     */
    private $iddescriptorcategoria;



    /**
     * Get iddescriptores.
     *
     * @return int
     */
    public function getIddescriptores()
    {
        return $this->iddescriptores;
    }

    /**
     * Set descripcion.
     *
     * @param string|null $descripcion
     *
     * @return Descriptores
     */
    public function setDescripcion($descripcion = null)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string|null
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipo.
     *
     * @param string|null $tipo
     *
     * @return Descriptores
     */
    public function setTipo($tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string|null
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set clave.
     *
     * @param string|null $clave
     *
     * @return Descriptores
     */
    public function setClave($clave = null)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave.
     *
     * @return string|null
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set visible.
     *
     * @param string $visible
     *
     * @return Descriptores
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible.
     *
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set codigoant.
     *
     * @param int|null $codigoant
     *
     * @return Descriptores
     */
    public function setCodigoant($codigoant = null)
    {
        $this->codigoant = $codigoant;

        return $this;
    }

    /**
     * Get codigoant.
     *
     * @return int|null
     */
    public function getCodigoant()
    {
        return $this->codigoant;
    }

    /**
     * Set descripcionpt.
     *
     * @param string|null $descripcionpt
     *
     * @return Descriptores
     */
    public function setDescripcionpt($descripcionpt = null)
    {
        $this->descripcionpt = $descripcionpt;

        return $this;
    }

    /**
     * Get descripcionpt.
     *
     * @return string|null
     */
    public function getDescripcionpt()
    {
        return $this->descripcionpt;
    }

    /**
     * Set iddescriptorcategoria.
     *
     * @param \AppBundle\Entity\Globalcar\Descriptorcategoria|null $iddescriptorcategoria
     *
     * @return Descriptores
     */
    public function setIddescriptorcategoria(\AppBundle\Entity\Globalcar\Descriptorcategoria $iddescriptorcategoria = null)
    {
        $this->iddescriptorcategoria = $iddescriptorcategoria;

        return $this;
    }

    /**
     * Get iddescriptorcategoria.
     *
     * @return \AppBundle\Entity\Globalcar\Descriptorcategoria|null
     */
    public function getIddescriptorcategoria()
    {
        return $this->iddescriptorcategoria;
    }
}
