<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documentosdescriptores
 *
 * @ORM\Table(name="documentosdescriptores", indexes={@ORM\Index(name="iddocumentos_DD_FK_idx", columns={"iddocumentos"}), @ORM\Index(name="idescriptores_DD_FK_idx", columns={"iddescriptores"})})
 * @ORM\Entity
 */
class Documentosdescriptores
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddocumentosDescriptores", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddocumentosdescriptores;

    /**
     * @var \Documentos
     *
     * @ORM\ManyToOne(targetEntity="Documentos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddocumentos", referencedColumnName="iddocumentos")
     * })
     */
    private $iddocumentos;

    /**
     * @var \Descriptores
     *
     * @ORM\ManyToOne(targetEntity="Descriptores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddescriptores", referencedColumnName="iddescriptores")
     * })
     */
    private $iddescriptores;



    /**
     * Get iddocumentosdescriptores.
     *
     * @return int
     */
    public function getIddocumentosdescriptores()
    {
        return $this->iddocumentosdescriptores;
    }

    /**
     * Set iddocumentos.
     *
     * @param \AppBundle\Entity\Globalcar\Documentos|null $iddocumentos
     *
     * @return Documentosdescriptores
     */
    public function setIddocumentos(\AppBundle\Entity\Globalcar\Documentos $iddocumentos = null)
    {
        $this->iddocumentos = $iddocumentos;

        return $this;
    }

    /**
     * Get iddocumentos.
     *
     * @return \AppBundle\Entity\Globalcar\Documentos|null
     */
    public function getIddocumentos()
    {
        return $this->iddocumentos;
    }

    /**
     * Set iddescriptores.
     *
     * @param \AppBundle\Entity\Globalcar\Descriptores|null $iddescriptores
     *
     * @return Documentosdescriptores
     */
    public function setIddescriptores(\AppBundle\Entity\Globalcar\Descriptores $iddescriptores = null)
    {
        $this->iddescriptores = $iddescriptores;

        return $this;
    }

    /**
     * Get iddescriptores.
     *
     * @return \AppBundle\Entity\Globalcar\Descriptores|null
     */
    public function getIddescriptores()
    {
        return $this->iddescriptores;
    }
}
