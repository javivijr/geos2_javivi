<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Infraccionesdgt
 *
 * @ORM\Table(name="infraccionesdgt")
 * @ORM\Entity
 */
class Infraccionesdgt
{
    /**
     * @var int
     *
     * @ORM\Column(name="idinfraccionesdgt", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idinfraccionesdgt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="norma", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $norma;

    /**
     * @var string|null
     *
     * @ORM\Column(name="articulo", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $articulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apartado", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $apartado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="opc", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $opc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="puntos", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $puntos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="calificacion", type="string", length=30, nullable=true)
     */
    private $calificacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="texto", type="string", length=300, nullable=true)
     */
    private $texto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="importe", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $importe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="importereducido", type="integer", nullable=true)
     */
    private $importereducido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="responsable", type="string", length=45, nullable=true)
     */
    private $responsable;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comentario", type="string", length=300, nullable=true)
     */
    private $comentario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="materia", type="string", length=100, nullable=true)
     */
    private $materia;



    /**
     * Get idinfraccionesdgt.
     *
     * @return int
     */
    public function getIdinfraccionesdgt()
    {
        return $this->idinfraccionesdgt;
    }

    /**
     * Set norma.
     *
     * @param string|null $norma
     *
     * @return Infraccionesdgt
     */
    public function setNorma($norma = null)
    {
        $this->norma = $norma;

        return $this;
    }

    /**
     * Get norma.
     *
     * @return string|null
     */
    public function getNorma()
    {
        return $this->norma;
    }

    /**
     * Set articulo.
     *
     * @param string|null $articulo
     *
     * @return Infraccionesdgt
     */
    public function setArticulo($articulo = null)
    {
        $this->articulo = $articulo;

        return $this;
    }

    /**
     * Get articulo.
     *
     * @return string|null
     */
    public function getArticulo()
    {
        return $this->articulo;
    }

    /**
     * Set apartado.
     *
     * @param string|null $apartado
     *
     * @return Infraccionesdgt
     */
    public function setApartado($apartado = null)
    {
        $this->apartado = $apartado;

        return $this;
    }

    /**
     * Get apartado.
     *
     * @return string|null
     */
    public function getApartado()
    {
        return $this->apartado;
    }

    /**
     * Set opc.
     *
     * @param string|null $opc
     *
     * @return Infraccionesdgt
     */
    public function setOpc($opc = null)
    {
        $this->opc = $opc;

        return $this;
    }

    /**
     * Get opc.
     *
     * @return string|null
     */
    public function getOpc()
    {
        return $this->opc;
    }

    /**
     * Set puntos.
     *
     * @param int|null $puntos
     *
     * @return Infraccionesdgt
     */
    public function setPuntos($puntos = null)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos.
     *
     * @return int|null
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set calificacion.
     *
     * @param string|null $calificacion
     *
     * @return Infraccionesdgt
     */
    public function setCalificacion($calificacion = null)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Get calificacion.
     *
     * @return string|null
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set texto.
     *
     * @param string|null $texto
     *
     * @return Infraccionesdgt
     */
    public function setTexto($texto = null)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto.
     *
     * @return string|null
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set importe.
     *
     * @param int|null $importe
     *
     * @return Infraccionesdgt
     */
    public function setImporte($importe = null)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe.
     *
     * @return int|null
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set importereducido.
     *
     * @param int|null $importereducido
     *
     * @return Infraccionesdgt
     */
    public function setImportereducido($importereducido = null)
    {
        $this->importereducido = $importereducido;

        return $this;
    }

    /**
     * Get importereducido.
     *
     * @return int|null
     */
    public function getImportereducido()
    {
        return $this->importereducido;
    }

    /**
     * Set responsable.
     *
     * @param string|null $responsable
     *
     * @return Infraccionesdgt
     */
    public function setResponsable($responsable = null)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable.
     *
     * @return string|null
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set comentario.
     *
     * @param string|null $comentario
     *
     * @return Infraccionesdgt
     */
    public function setComentario($comentario = null)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario.
     *
     * @return string|null
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set materia.
     *
     * @param string|null $materia
     *
     * @return Infraccionesdgt
     */
    public function setMateria($materia = null)
    {
        $this->materia = $materia;

        return $this;
    }

    /**
     * Get materia.
     *
     * @return string|null
     */
    public function getMateria()
    {
        return $this->materia;
    }
}
