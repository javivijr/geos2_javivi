<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registros
 *
 * @ORM\Table(name="registros")
 * @ORM\Entity
 */
class Registros
{
    /**
     * @var int
     *
     * @ORM\Column(name="idregistros", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idregistros;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=0, nullable=false)
     */
    private $ciudad;



    /**
     * Get idregistros.
     *
     * @return int
     */
    public function getIdregistros()
    {
        return $this->idregistros;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Registros
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ciudad.
     *
     * @param string $ciudad
     *
     * @return Registros
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad.
     *
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }
}
