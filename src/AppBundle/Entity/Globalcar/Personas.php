<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personas
 *
 * @ORM\Table(name="personas", uniqueConstraints={@ORM\UniqueConstraint(name="personas_P_IND", columns={"nombre", "apellido1", "apellido2", "nif", "tipoDoc"})}, indexes={@ORM\Index(name="nif_P_IND", columns={"nif"})})
 * @ORM\Entity
 */
class Personas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpersonas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonas;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido1", type="string", length=50, nullable=false)
     */
    private $apellido1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido2", type="string", length=50, nullable=true)
     */
    private $apellido2;

    /**
     * @var string
     *
     * @ORM\Column(name="nif", type="string", length=30, nullable=false)
     */
    private $nif;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoDoc", type="string", length=0, nullable=false)
     */
    private $tipodoc;



    /**
     * Get idpersonas.
     *
     * @return int
     */
    public function getIdpersonas()
    {
        return $this->idpersonas;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Personas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1.
     *
     * @param string $apellido1
     *
     * @return Personas
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1.
     *
     * @return string
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2.
     *
     * @param string|null $apellido2
     *
     * @return Personas
     */
    public function setApellido2($apellido2 = null)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2.
     *
     * @return string|null
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set nif.
     *
     * @param string $nif
     *
     * @return Personas
     */
    public function setNif($nif)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif.
     *
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * Set tipodoc.
     *
     * @param string $tipodoc
     *
     * @return Personas
     */
    public function setTipodoc($tipodoc)
    {
        $this->tipodoc = $tipodoc;

        return $this;
    }

    /**
     * Get tipodoc.
     *
     * @return string
     */
    public function getTipodoc()
    {
        return $this->tipodoc;
    }
}
