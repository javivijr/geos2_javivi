<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuariosmtmcuenta
 *
 * @ORM\Table(name="usuariosmtmcuenta", indexes={@ORM\Index(name="idusuariosmtm_UMCd_FK_idx", columns={"idusuariosmtm"})})
 * @ORM\Entity
 */
class Usuariosmtmcuenta
{
    /**
     * @var int
     *
     * @ORM\Column(name="idusuariosmtmcuenta", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idusuariosmtmcuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="identificador", type="string", length=200, nullable=false)
     */
    private $identificador;

    /**
     * @var string
     *
     * @ORM\Column(name="tipocuenta", type="string", length=0, nullable=false)
     */
    private $tipocuenta;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idusuariosmtmcuenta.
     *
     * @return int
     */
    public function getIdusuariosmtmcuenta()
    {
        return $this->idusuariosmtmcuenta;
    }

    /**
     * Set identificador.
     *
     * @param string $identificador
     *
     * @return Usuariosmtmcuenta
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador.
     *
     * @return string
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }

    /**
     * Set tipocuenta.
     *
     * @param string $tipocuenta
     *
     * @return Usuariosmtmcuenta
     */
    public function setTipocuenta($tipocuenta)
    {
        $this->tipocuenta = $tipocuenta;

        return $this;
    }

    /**
     * Get tipocuenta.
     *
     * @return string
     */
    public function getTipocuenta()
    {
        return $this->tipocuenta;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Usuariosmtmcuenta
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
