<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmclientesotrosarval
 *
 * @ORM\Table(name="mtmclientesotrosarval", indexes={@ORM\Index(name="idtitulares_a_COA_FK_idx", columns={"idtitulares_a"}), @ORM\Index(name="idtitulares_o_COA_FK_idx", columns={"idtitulares_o"})})
 * @ORM\Entity
 */
class Mtmclientesotrosarval
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmClientesOtrosArval", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmclientesotrosarval;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares_a", referencedColumnName="idtitulares")
     * })
     */
    private $idtitularesA;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares_o", referencedColumnName="idtitulares")
     * })
     */
    private $idtitularesO;



    /**
     * Get idmtmclientesotrosarval.
     *
     * @return int
     */
    public function getIdmtmclientesotrosarval()
    {
        return $this->idmtmclientesotrosarval;
    }

    /**
     * Set idtitularesA.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitularesA
     *
     * @return Mtmclientesotrosarval
     */
    public function setIdtitularesA(\AppBundle\Entity\Globalcar\Titulares $idtitularesA = null)
    {
        $this->idtitularesA = $idtitularesA;

        return $this;
    }

    /**
     * Get idtitularesA.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitularesA()
    {
        return $this->idtitularesA;
    }

    /**
     * Set idtitularesO.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitularesO
     *
     * @return Mtmclientesotrosarval
     */
    public function setIdtitularesO(\AppBundle\Entity\Globalcar\Titulares $idtitularesO = null)
    {
        $this->idtitularesO = $idtitularesO;

        return $this;
    }

    /**
     * Get idtitularesO.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitularesO()
    {
        return $this->idtitularesO;
    }
}
