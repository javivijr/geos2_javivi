<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historicovehiculos
 *
 * @ORM\Table(name="historicovehiculos", indexes={@ORM\Index(name="idvehiculo_HV_FK_idx", columns={"idvehiculo"}), @ORM\Index(name="idtitular_HV_FK_idx", columns={"idtitular"}), @ORM\Index(name="fechaAlta", columns={"fechaAlta"}), @ORM\Index(name="fechaBaja", columns={"fechaBaja"}), @ORM\Index(name="tipoTitular", columns={"tipoTitular"}), @ORM\Index(name="fechaInsercion", columns={"fechaInsercion"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Globalcar\Repository\HistoricovehiculosRepository")
 */
class Historicovehiculos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhistoricoVehiculos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhistoricovehiculos;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaAlta", type="datetime", nullable=true)
     */
    private $fechaalta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaBaja", type="datetime", nullable=true)
     */
    private $fechabaja;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaInsercion", type="datetime", nullable=true)
     */
    private $fechainsercion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="empresaRenting", type="string", length=50, nullable=true)
     */
    private $empresarenting;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoTitular", type="string", length=0, nullable=false)
     */
    private $tipotitular;

    /**
     * @var string
     *
     * @ORM\Column(name="temporal", type="string", length=0, nullable=false)
     */
    private $temporal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=500, nullable=true)
     */
    private $observaciones;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoarval", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $codigoarval;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codhistootros", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $codhistootros;

    /**
     * @var string
     *
     * @ORM\Column(name="activo", type="string", length=0, nullable=false)
     */
    private $activo;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitular", referencedColumnName="idtitulares")
     * })
     */
    private $idtitular;

    /**
     * @var \Vehiculos
     *
     * @ORM\ManyToOne(targetEntity="Vehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idvehiculo", referencedColumnName="idvehiculos")
     * })
     */
    private $idvehiculo;



    /**
     * Get idhistoricovehiculos.
     *
     * @return int
     */
    public function getIdhistoricovehiculos()
    {
        return $this->idhistoricovehiculos;
    }

    /**
     * Set fechaalta.
     *
     * @param \DateTime|null $fechaalta
     *
     * @return Historicovehiculos
     */
    public function setFechaalta($fechaalta = null)
    {
        $this->fechaalta = $fechaalta;

        return $this;
    }

    /**
     * Get fechaalta.
     *
     * @return \DateTime|null
     */
    public function getFechaalta()
    {
        return $this->fechaalta;
    }

    /**
     * Set fechabaja.
     *
     * @param \DateTime|null $fechabaja
     *
     * @return Historicovehiculos
     */
    public function setFechabaja($fechabaja = null)
    {
        $this->fechabaja = $fechabaja;

        return $this;
    }

    /**
     * Get fechabaja.
     *
     * @return \DateTime|null
     */
    public function getFechabaja()
    {
        return $this->fechabaja;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Historicovehiculos
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set fechainsercion.
     *
     * @param \DateTime|null $fechainsercion
     *
     * @return Historicovehiculos
     */
    public function setFechainsercion($fechainsercion = null)
    {
        $this->fechainsercion = $fechainsercion;

        return $this;
    }

    /**
     * Get fechainsercion.
     *
     * @return \DateTime|null
     */
    public function getFechainsercion()
    {
        return $this->fechainsercion;
    }

    /**
     * Set empresarenting.
     *
     * @param string|null $empresarenting
     *
     * @return Historicovehiculos
     */
    public function setEmpresarenting($empresarenting = null)
    {
        $this->empresarenting = $empresarenting;

        return $this;
    }

    /**
     * Get empresarenting.
     *
     * @return string|null
     */
    public function getEmpresarenting()
    {
        return $this->empresarenting;
    }

    /**
     * Set tipotitular.
     *
     * @param string $tipotitular
     *
     * @return Historicovehiculos
     */
    public function setTipotitular($tipotitular)
    {
        $this->tipotitular = $tipotitular;

        return $this;
    }

    /**
     * Get tipotitular.
     *
     * @return string
     */
    public function getTipotitular()
    {
        return $this->tipotitular;
    }

    /**
     * Set temporal.
     *
     * @param string $temporal
     *
     * @return Historicovehiculos
     */
    public function setTemporal($temporal)
    {
        $this->temporal = $temporal;

        return $this;
    }

    /**
     * Get temporal.
     *
     * @return string
     */
    public function getTemporal()
    {
        return $this->temporal;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Historicovehiculos
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set codigoarval.
     *
     * @param int|null $codigoarval
     *
     * @return Historicovehiculos
     */
    public function setCodigoarval($codigoarval = null)
    {
        $this->codigoarval = $codigoarval;

        return $this;
    }

    /**
     * Get codigoarval.
     *
     * @return int|null
     */
    public function getCodigoarval()
    {
        return $this->codigoarval;
    }

    /**
     * Set codhistootros.
     *
     * @param int|null $codhistootros
     *
     * @return Historicovehiculos
     */
    public function setCodhistootros($codhistootros = null)
    {
        $this->codhistootros = $codhistootros;

        return $this;
    }

    /**
     * Get codhistootros.
     *
     * @return int|null
     */
    public function getCodhistootros()
    {
        return $this->codhistootros;
    }

    /**
     * Set activo.
     *
     * @param string $activo
     *
     * @return Historicovehiculos
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo.
     *
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set idtitular.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitular
     *
     * @return Historicovehiculos
     */
    public function setIdtitular(\AppBundle\Entity\Globalcar\Titulares $idtitular = null)
    {
        $this->idtitular = $idtitular;

        return $this;
    }

    /**
     * Get idtitular.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitular()
    {
        return $this->idtitular;
    }

    /**
     * Set idvehiculo.
     *
     * @param \AppBundle\Entity\Globalcar\Vehiculos|null $idvehiculo
     *
     * @return Historicovehiculos
     */
    public function setIdvehiculo(\AppBundle\Entity\Globalcar\Vehiculos $idvehiculo = null)
    {
        $this->idvehiculo = $idvehiculo;

        return $this;
    }

    /**
     * Get idvehiculo.
     *
     * @return \AppBundle\Entity\Globalcar\Vehiculos|null
     */
    public function getIdvehiculo()
    {
        return $this->idvehiculo;
    }
}
