<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Abogados
 *
 * @ORM\Table(name="abogados", indexes={@ORM\Index(name="IDX_50D79026E09AF528", columns={"idtitulares"}), @ORM\Index(name="IDX_50D79026A423CFFF", columns={"idusuarios"}), @ORM\Index(name="IDX_50D7902677802C39", columns={"idusuariosSuplente"})})
 * @ORM\Entity
 */
class Abogados
{
    /**
     * @var int
     *
     * @ORM\Column(name="idabogados", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idabogados;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaAsignacion", type="datetime", nullable=true)
     */
    private $fechaasignacion;

    /**
     * @var Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosSuplente", referencedColumnName="idusuarios")
     * })
     */
    private $idusuariossuplente;

    /**
     * @var Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarios", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarios;

    /**
     * @var Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idabogados.
     *
     * @return int
     */
    public function getIdabogados()
    {
        return $this->idabogados;
    }

    /**
     * Set fechaasignacion.
     *
     * @param \DateTime|null $fechaasignacion
     *
     * @return Abogados
     */
    public function setFechaasignacion($fechaasignacion = null)
    {
        $this->fechaasignacion = $fechaasignacion;

        return $this;
    }

    /**
     * Get fechaasignacion.
     *
     * @return \DateTime|null
     */
    public function getFechaasignacion()
    {
        return $this->fechaasignacion;
    }

    /**
     * Set idusuariossuplente.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuariossuplente
     *
     * @return Abogados
     */
    public function setIdusuariossuplente(\AppBundle\Entity\Globalcar\Usuarios $idusuariossuplente = null)
    {
        $this->idusuariossuplente = $idusuariossuplente;

        return $this;
    }

    /**
     * Get idusuariossuplente.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuariossuplente()
    {
        return $this->idusuariossuplente;
    }

    /**
     * Set idusuarios.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarios
     *
     * @return Abogados
     */
    public function setIdusuarios(\AppBundle\Entity\Globalcar\Usuarios $idusuarios = null)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Abogados
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
