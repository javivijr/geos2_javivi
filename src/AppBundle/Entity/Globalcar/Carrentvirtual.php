<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrentvirtual
 *
 * @ORM\Table(name="carrentvirtual", indexes={@ORM\Index(name="idexpedientes_CV_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="raNo_CV_IDX", columns={"raNo"})})
 * @ORM\Entity
 */
class Carrentvirtual
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrentVirtual", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrentvirtual;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoSancion", type="string", length=30, nullable=true)
     */
    private $codigosancion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raNo", type="string", length=50, nullable=true)
     */
    private $rano;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unit", type="string", length=50, nullable=true)
     */
    private $unit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="license", type="string", length=50, nullable=true)
     */
    private $license;

    /**
     * @var string|null
     *
     * @ORM\Column(name="license2", type="string", length=50, nullable=true)
     */
    private $license2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ownWwd", type="string", length=50, nullable=true)
     */
    private $ownwwd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payCode", type="string", length=50, nullable=true)
     */
    private $paycode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehClass", type="string", length=50, nullable=true)
     */
    private $vehclass;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehYr", type="string", length=50, nullable=true)
     */
    private $vehyr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dateOut", type="string", length=15, nullable=true)
     */
    private $dateout;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dateIn", type="string", length=15, nullable=true)
     */
    private $datein;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dateDue", type="string", length=15, nullable=true)
     */
    private $datedue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="areaLoc1", type="string", length=50, nullable=true)
     */
    private $arealoc1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="areaLoc2", type="string", length=50, nullable=true)
     */
    private $arealoc2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="areaLoc3", type="string", length=50, nullable=true)
     */
    private $arealoc3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kmsMls1", type="string", length=50, nullable=true)
     */
    private $kmsmls1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kmsMls2", type="string", length=50, nullable=true)
     */
    private $kmsmls2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gn1Club", type="string", length=50, nullable=true)
     */
    private $gn1club;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cdpid", type="string", length=50, nullable=true)
     */
    private $cdpid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer1", type="string", length=50, nullable=true)
     */
    private $customer1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer2", type="string", length=50, nullable=true)
     */
    private $customer2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL1F1", type="string", length=50, nullable=true)
     */
    private $addressl1f1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL1F2", type="string", length=50, nullable=true)
     */
    private $addressl1f2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL2F1", type="string", length=50, nullable=true)
     */
    private $addressl2f1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL2F2", type="string", length=50, nullable=true)
     */
    private $addressl2f2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL3F1", type="string", length=50, nullable=true)
     */
    private $addressl3f1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL3F2", type="string", length=50, nullable=true)
     */
    private $addressl3f2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL4F1", type="string", length=50, nullable=true)
     */
    private $addressl4f1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addressL4F2", type="string", length=50, nullable=true)
     */
    private $addressl4f2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone1", type="string", length=50, nullable=true)
     */
    private $phone1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone2", type="string", length=50, nullable=true)
     */
    private $phone2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mop", type="string", length=50, nullable=true)
     */
    private $mop;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ccNn", type="string", length=50, nullable=true)
     */
    private $ccnn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="exprires", type="string", length=50, nullable=true)
     */
    private $exprires;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dlNo", type="string", length=50, nullable=true)
     */
    private $dlno;

    /**
     * @var string|null
     *
     * @ORM\Column(name="issued", type="string", length=50, nullable=true)
     */
    private $issued;

    /**
     * @var string|null
     *
     * @ORM\Column(name="expires", type="string", length=50, nullable=true)
     */
    private $expires;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dlIss", type="string", length=50, nullable=true)
     */
    private $dliss;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pob", type="string", length=50, nullable=true)
     */
    private $pob;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dob", type="string", length=50, nullable=true)
     */
    private $dob;

    /**
     * @var string|null
     *
     * @ORM\Column(name="passport", type="string", length=50, nullable=true)
     */
    private $passport;

    /**
     * @var string|null
     *
     * @ORM\Column(name="issBy", type="string", length=50, nullable=true)
     */
    private $issby;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NextRagn", type="string", length=50, nullable=true)
     */
    private $nextragn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nextAreaUnit", type="string", length=50, nullable=true)
     */
    private $nextareaunit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="section", type="string", length=50, nullable=true)
     */
    private $section;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioritario", type="string", length=50, nullable=true)
     */
    private $prioritario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tarjeta", type="string", length=50, nullable=true)
     */
    private $tarjeta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=500, nullable=true)
     */
    private $observaciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailcarta", type="string", length=500, nullable=true)
     */
    private $emailcarta;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idcarrentvirtual.
     *
     * @return int
     */
    public function getIdcarrentvirtual()
    {
        return $this->idcarrentvirtual;
    }

    /**
     * Set codigosancion.
     *
     * @param string|null $codigosancion
     *
     * @return Carrentvirtual
     */
    public function setCodigosancion($codigosancion = null)
    {
        $this->codigosancion = $codigosancion;

        return $this;
    }

    /**
     * Get codigosancion.
     *
     * @return string|null
     */
    public function getCodigosancion()
    {
        return $this->codigosancion;
    }

    /**
     * Set rano.
     *
     * @param string|null $rano
     *
     * @return Carrentvirtual
     */
    public function setRano($rano = null)
    {
        $this->rano = $rano;

        return $this;
    }

    /**
     * Get rano.
     *
     * @return string|null
     */
    public function getRano()
    {
        return $this->rano;
    }

    /**
     * Set unit.
     *
     * @param string|null $unit
     *
     * @return Carrentvirtual
     */
    public function setUnit($unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit.
     *
     * @return string|null
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set license.
     *
     * @param string|null $license
     *
     * @return Carrentvirtual
     */
    public function setLicense($license = null)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license.
     *
     * @return string|null
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set license2.
     *
     * @param string|null $license2
     *
     * @return Carrentvirtual
     */
    public function setLicense2($license2 = null)
    {
        $this->license2 = $license2;

        return $this;
    }

    /**
     * Get license2.
     *
     * @return string|null
     */
    public function getLicense2()
    {
        return $this->license2;
    }

    /**
     * Set status.
     *
     * @param string|null $status
     *
     * @return Carrentvirtual
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set ownwwd.
     *
     * @param string|null $ownwwd
     *
     * @return Carrentvirtual
     */
    public function setOwnwwd($ownwwd = null)
    {
        $this->ownwwd = $ownwwd;

        return $this;
    }

    /**
     * Get ownwwd.
     *
     * @return string|null
     */
    public function getOwnwwd()
    {
        return $this->ownwwd;
    }

    /**
     * Set paycode.
     *
     * @param string|null $paycode
     *
     * @return Carrentvirtual
     */
    public function setPaycode($paycode = null)
    {
        $this->paycode = $paycode;

        return $this;
    }

    /**
     * Get paycode.
     *
     * @return string|null
     */
    public function getPaycode()
    {
        return $this->paycode;
    }

    /**
     * Set vehclass.
     *
     * @param string|null $vehclass
     *
     * @return Carrentvirtual
     */
    public function setVehclass($vehclass = null)
    {
        $this->vehclass = $vehclass;

        return $this;
    }

    /**
     * Get vehclass.
     *
     * @return string|null
     */
    public function getVehclass()
    {
        return $this->vehclass;
    }

    /**
     * Set vehyr.
     *
     * @param string|null $vehyr
     *
     * @return Carrentvirtual
     */
    public function setVehyr($vehyr = null)
    {
        $this->vehyr = $vehyr;

        return $this;
    }

    /**
     * Get vehyr.
     *
     * @return string|null
     */
    public function getVehyr()
    {
        return $this->vehyr;
    }

    /**
     * Set dateout.
     *
     * @param string|null $dateout
     *
     * @return Carrentvirtual
     */
    public function setDateout($dateout = null)
    {
        $this->dateout = $dateout;

        return $this;
    }

    /**
     * Get dateout.
     *
     * @return string|null
     */
    public function getDateout()
    {
        return $this->dateout;
    }

    /**
     * Set datein.
     *
     * @param string|null $datein
     *
     * @return Carrentvirtual
     */
    public function setDatein($datein = null)
    {
        $this->datein = $datein;

        return $this;
    }

    /**
     * Get datein.
     *
     * @return string|null
     */
    public function getDatein()
    {
        return $this->datein;
    }

    /**
     * Set datedue.
     *
     * @param string|null $datedue
     *
     * @return Carrentvirtual
     */
    public function setDatedue($datedue = null)
    {
        $this->datedue = $datedue;

        return $this;
    }

    /**
     * Get datedue.
     *
     * @return string|null
     */
    public function getDatedue()
    {
        return $this->datedue;
    }

    /**
     * Set arealoc1.
     *
     * @param string|null $arealoc1
     *
     * @return Carrentvirtual
     */
    public function setArealoc1($arealoc1 = null)
    {
        $this->arealoc1 = $arealoc1;

        return $this;
    }

    /**
     * Get arealoc1.
     *
     * @return string|null
     */
    public function getArealoc1()
    {
        return $this->arealoc1;
    }

    /**
     * Set arealoc2.
     *
     * @param string|null $arealoc2
     *
     * @return Carrentvirtual
     */
    public function setArealoc2($arealoc2 = null)
    {
        $this->arealoc2 = $arealoc2;

        return $this;
    }

    /**
     * Get arealoc2.
     *
     * @return string|null
     */
    public function getArealoc2()
    {
        return $this->arealoc2;
    }

    /**
     * Set arealoc3.
     *
     * @param string|null $arealoc3
     *
     * @return Carrentvirtual
     */
    public function setArealoc3($arealoc3 = null)
    {
        $this->arealoc3 = $arealoc3;

        return $this;
    }

    /**
     * Get arealoc3.
     *
     * @return string|null
     */
    public function getArealoc3()
    {
        return $this->arealoc3;
    }

    /**
     * Set kmsmls1.
     *
     * @param string|null $kmsmls1
     *
     * @return Carrentvirtual
     */
    public function setKmsmls1($kmsmls1 = null)
    {
        $this->kmsmls1 = $kmsmls1;

        return $this;
    }

    /**
     * Get kmsmls1.
     *
     * @return string|null
     */
    public function getKmsmls1()
    {
        return $this->kmsmls1;
    }

    /**
     * Set kmsmls2.
     *
     * @param string|null $kmsmls2
     *
     * @return Carrentvirtual
     */
    public function setKmsmls2($kmsmls2 = null)
    {
        $this->kmsmls2 = $kmsmls2;

        return $this;
    }

    /**
     * Get kmsmls2.
     *
     * @return string|null
     */
    public function getKmsmls2()
    {
        return $this->kmsmls2;
    }

    /**
     * Set gn1club.
     *
     * @param string|null $gn1club
     *
     * @return Carrentvirtual
     */
    public function setGn1club($gn1club = null)
    {
        $this->gn1club = $gn1club;

        return $this;
    }

    /**
     * Get gn1club.
     *
     * @return string|null
     */
    public function getGn1club()
    {
        return $this->gn1club;
    }

    /**
     * Set cdpid.
     *
     * @param string|null $cdpid
     *
     * @return Carrentvirtual
     */
    public function setCdpid($cdpid = null)
    {
        $this->cdpid = $cdpid;

        return $this;
    }

    /**
     * Get cdpid.
     *
     * @return string|null
     */
    public function getCdpid()
    {
        return $this->cdpid;
    }

    /**
     * Set customer1.
     *
     * @param string|null $customer1
     *
     * @return Carrentvirtual
     */
    public function setCustomer1($customer1 = null)
    {
        $this->customer1 = $customer1;

        return $this;
    }

    /**
     * Get customer1.
     *
     * @return string|null
     */
    public function getCustomer1()
    {
        return $this->customer1;
    }

    /**
     * Set customer2.
     *
     * @param string|null $customer2
     *
     * @return Carrentvirtual
     */
    public function setCustomer2($customer2 = null)
    {
        $this->customer2 = $customer2;

        return $this;
    }

    /**
     * Get customer2.
     *
     * @return string|null
     */
    public function getCustomer2()
    {
        return $this->customer2;
    }

    /**
     * Set addressl1f1.
     *
     * @param string|null $addressl1f1
     *
     * @return Carrentvirtual
     */
    public function setAddressl1f1($addressl1f1 = null)
    {
        $this->addressl1f1 = $addressl1f1;

        return $this;
    }

    /**
     * Get addressl1f1.
     *
     * @return string|null
     */
    public function getAddressl1f1()
    {
        return $this->addressl1f1;
    }

    /**
     * Set addressl1f2.
     *
     * @param string|null $addressl1f2
     *
     * @return Carrentvirtual
     */
    public function setAddressl1f2($addressl1f2 = null)
    {
        $this->addressl1f2 = $addressl1f2;

        return $this;
    }

    /**
     * Get addressl1f2.
     *
     * @return string|null
     */
    public function getAddressl1f2()
    {
        return $this->addressl1f2;
    }

    /**
     * Set addressl2f1.
     *
     * @param string|null $addressl2f1
     *
     * @return Carrentvirtual
     */
    public function setAddressl2f1($addressl2f1 = null)
    {
        $this->addressl2f1 = $addressl2f1;

        return $this;
    }

    /**
     * Get addressl2f1.
     *
     * @return string|null
     */
    public function getAddressl2f1()
    {
        return $this->addressl2f1;
    }

    /**
     * Set addressl2f2.
     *
     * @param string|null $addressl2f2
     *
     * @return Carrentvirtual
     */
    public function setAddressl2f2($addressl2f2 = null)
    {
        $this->addressl2f2 = $addressl2f2;

        return $this;
    }

    /**
     * Get addressl2f2.
     *
     * @return string|null
     */
    public function getAddressl2f2()
    {
        return $this->addressl2f2;
    }

    /**
     * Set addressl3f1.
     *
     * @param string|null $addressl3f1
     *
     * @return Carrentvirtual
     */
    public function setAddressl3f1($addressl3f1 = null)
    {
        $this->addressl3f1 = $addressl3f1;

        return $this;
    }

    /**
     * Get addressl3f1.
     *
     * @return string|null
     */
    public function getAddressl3f1()
    {
        return $this->addressl3f1;
    }

    /**
     * Set addressl3f2.
     *
     * @param string|null $addressl3f2
     *
     * @return Carrentvirtual
     */
    public function setAddressl3f2($addressl3f2 = null)
    {
        $this->addressl3f2 = $addressl3f2;

        return $this;
    }

    /**
     * Get addressl3f2.
     *
     * @return string|null
     */
    public function getAddressl3f2()
    {
        return $this->addressl3f2;
    }

    /**
     * Set addressl4f1.
     *
     * @param string|null $addressl4f1
     *
     * @return Carrentvirtual
     */
    public function setAddressl4f1($addressl4f1 = null)
    {
        $this->addressl4f1 = $addressl4f1;

        return $this;
    }

    /**
     * Get addressl4f1.
     *
     * @return string|null
     */
    public function getAddressl4f1()
    {
        return $this->addressl4f1;
    }

    /**
     * Set addressl4f2.
     *
     * @param string|null $addressl4f2
     *
     * @return Carrentvirtual
     */
    public function setAddressl4f2($addressl4f2 = null)
    {
        $this->addressl4f2 = $addressl4f2;

        return $this;
    }

    /**
     * Get addressl4f2.
     *
     * @return string|null
     */
    public function getAddressl4f2()
    {
        return $this->addressl4f2;
    }

    /**
     * Set phone1.
     *
     * @param string|null $phone1
     *
     * @return Carrentvirtual
     */
    public function setPhone1($phone1 = null)
    {
        $this->phone1 = $phone1;

        return $this;
    }

    /**
     * Get phone1.
     *
     * @return string|null
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Set phone2.
     *
     * @param string|null $phone2
     *
     * @return Carrentvirtual
     */
    public function setPhone2($phone2 = null)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2.
     *
     * @return string|null
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set mop.
     *
     * @param string|null $mop
     *
     * @return Carrentvirtual
     */
    public function setMop($mop = null)
    {
        $this->mop = $mop;

        return $this;
    }

    /**
     * Get mop.
     *
     * @return string|null
     */
    public function getMop()
    {
        return $this->mop;
    }

    /**
     * Set ccnn.
     *
     * @param string|null $ccnn
     *
     * @return Carrentvirtual
     */
    public function setCcnn($ccnn = null)
    {
        $this->ccnn = $ccnn;

        return $this;
    }

    /**
     * Get ccnn.
     *
     * @return string|null
     */
    public function getCcnn()
    {
        return $this->ccnn;
    }

    /**
     * Set exprires.
     *
     * @param string|null $exprires
     *
     * @return Carrentvirtual
     */
    public function setExprires($exprires = null)
    {
        $this->exprires = $exprires;

        return $this;
    }

    /**
     * Get exprires.
     *
     * @return string|null
     */
    public function getExprires()
    {
        return $this->exprires;
    }

    /**
     * Set dlno.
     *
     * @param string|null $dlno
     *
     * @return Carrentvirtual
     */
    public function setDlno($dlno = null)
    {
        $this->dlno = $dlno;

        return $this;
    }

    /**
     * Get dlno.
     *
     * @return string|null
     */
    public function getDlno()
    {
        return $this->dlno;
    }

    /**
     * Set issued.
     *
     * @param string|null $issued
     *
     * @return Carrentvirtual
     */
    public function setIssued($issued = null)
    {
        $this->issued = $issued;

        return $this;
    }

    /**
     * Get issued.
     *
     * @return string|null
     */
    public function getIssued()
    {
        return $this->issued;
    }

    /**
     * Set expires.
     *
     * @param string|null $expires
     *
     * @return Carrentvirtual
     */
    public function setExpires($expires = null)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires.
     *
     * @return string|null
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * Set dliss.
     *
     * @param string|null $dliss
     *
     * @return Carrentvirtual
     */
    public function setDliss($dliss = null)
    {
        $this->dliss = $dliss;

        return $this;
    }

    /**
     * Get dliss.
     *
     * @return string|null
     */
    public function getDliss()
    {
        return $this->dliss;
    }

    /**
     * Set pob.
     *
     * @param string|null $pob
     *
     * @return Carrentvirtual
     */
    public function setPob($pob = null)
    {
        $this->pob = $pob;

        return $this;
    }

    /**
     * Get pob.
     *
     * @return string|null
     */
    public function getPob()
    {
        return $this->pob;
    }

    /**
     * Set dob.
     *
     * @param string|null $dob
     *
     * @return Carrentvirtual
     */
    public function setDob($dob = null)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob.
     *
     * @return string|null
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set passport.
     *
     * @param string|null $passport
     *
     * @return Carrentvirtual
     */
    public function setPassport($passport = null)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport.
     *
     * @return string|null
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set issby.
     *
     * @param string|null $issby
     *
     * @return Carrentvirtual
     */
    public function setIssby($issby = null)
    {
        $this->issby = $issby;

        return $this;
    }

    /**
     * Get issby.
     *
     * @return string|null
     */
    public function getIssby()
    {
        return $this->issby;
    }

    /**
     * Set nextragn.
     *
     * @param string|null $nextragn
     *
     * @return Carrentvirtual
     */
    public function setNextragn($nextragn = null)
    {
        $this->nextragn = $nextragn;

        return $this;
    }

    /**
     * Get nextragn.
     *
     * @return string|null
     */
    public function getNextragn()
    {
        return $this->nextragn;
    }

    /**
     * Set nextareaunit.
     *
     * @param string|null $nextareaunit
     *
     * @return Carrentvirtual
     */
    public function setNextareaunit($nextareaunit = null)
    {
        $this->nextareaunit = $nextareaunit;

        return $this;
    }

    /**
     * Get nextareaunit.
     *
     * @return string|null
     */
    public function getNextareaunit()
    {
        return $this->nextareaunit;
    }

    /**
     * Set section.
     *
     * @param string|null $section
     *
     * @return Carrentvirtual
     */
    public function setSection($section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section.
     *
     * @return string|null
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set prioritario.
     *
     * @param string|null $prioritario
     *
     * @return Carrentvirtual
     */
    public function setPrioritario($prioritario = null)
    {
        $this->prioritario = $prioritario;

        return $this;
    }

    /**
     * Get prioritario.
     *
     * @return string|null
     */
    public function getPrioritario()
    {
        return $this->prioritario;
    }

    /**
     * Set tarjeta.
     *
     * @param string|null $tarjeta
     *
     * @return Carrentvirtual
     */
    public function setTarjeta($tarjeta = null)
    {
        $this->tarjeta = $tarjeta;

        return $this;
    }

    /**
     * Get tarjeta.
     *
     * @return string|null
     */
    public function getTarjeta()
    {
        return $this->tarjeta;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Carrentvirtual
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set emailcarta.
     *
     * @param string|null $emailcarta
     *
     * @return Carrentvirtual
     */
    public function setEmailcarta($emailcarta = null)
    {
        $this->emailcarta = $emailcarta;

        return $this;
    }

    /**
     * Get emailcarta.
     *
     * @return string|null
     */
    public function getEmailcarta()
    {
        return $this->emailcarta;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Carrentvirtual
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
