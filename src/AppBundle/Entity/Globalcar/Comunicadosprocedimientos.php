<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comunicadosprocedimientos
 *
 * @ORM\Table(name="comunicadosprocedimientos", indexes={@ORM\Index(name="idprocedimientos_CP_FK_idx", columns={"idprocedimientos"})})
 * @ORM\Entity
 */
class Comunicadosprocedimientos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcomunicadosProcedimientos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcomunicadosprocedimientos;

    /**
     * @var string
     *
     * @ORM\Column(name="publico", type="string", length=0, nullable=false)
     */
    private $publico;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comunicado", type="text", length=65535, nullable=true)
     */
    private $comunicado;

    /**
     * @var \Procedimientos
     *
     * @ORM\ManyToOne(targetEntity="Procedimientos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idprocedimientos", referencedColumnName="idprocedimientos")
     * })
     */
    private $idprocedimientos;



    /**
     * Get idcomunicadosprocedimientos.
     *
     * @return int
     */
    public function getIdcomunicadosprocedimientos()
    {
        return $this->idcomunicadosprocedimientos;
    }

    /**
     * Set publico.
     *
     * @param string $publico
     *
     * @return Comunicadosprocedimientos
     */
    public function setPublico($publico)
    {
        $this->publico = $publico;

        return $this;
    }

    /**
     * Get publico.
     *
     * @return string
     */
    public function getPublico()
    {
        return $this->publico;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Comunicadosprocedimientos
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Comunicadosprocedimientos
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comunicado.
     *
     * @param string|null $comunicado
     *
     * @return Comunicadosprocedimientos
     */
    public function setComunicado($comunicado = null)
    {
        $this->comunicado = $comunicado;

        return $this;
    }

    /**
     * Get comunicado.
     *
     * @return string|null
     */
    public function getComunicado()
    {
        return $this->comunicado;
    }

    /**
     * Set idprocedimientos.
     *
     * @param \AppBundle\Entity\Globalcar\Procedimientos|null $idprocedimientos
     *
     * @return Comunicadosprocedimientos
     */
    public function setIdprocedimientos(\AppBundle\Entity\Globalcar\Procedimientos $idprocedimientos = null)
    {
        $this->idprocedimientos = $idprocedimientos;

        return $this;
    }

    /**
     * Get idprocedimientos.
     *
     * @return \AppBundle\Entity\Globalcar\Procedimientos|null
     */
    public function getIdprocedimientos()
    {
        return $this->idprocedimientos;
    }
}
