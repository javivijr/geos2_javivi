<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrenthertzportugal
 *
 * @ORM\Table(name="carrenthertzportugal")
 * @ORM\Entity
 */
class Carrenthertzportugal
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrenthertzportugal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrenthertzportugal;

    /**
     * @var string
     *
     * @ORM\Column(name="drivertype", type="string", length=255, nullable=false)
     */
    private $drivertype;

    /**
     * @var int
     *
     * @ORM\Column(name="drivercode", type="integer", nullable=false)
     */
    private $drivercode;

    /**
     * @var string
     *
     * @ORM\Column(name="drivername", type="string", length=255, nullable=false)
     */
    private $drivername;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=255, nullable=false)
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="datetime", nullable=false)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="birthplace", type="string", length=255, nullable=false)
     */
    private $birthplace;

    /**
     * @var int
     *
     * @ORM\Column(name="age", type="integer", nullable=false)
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="documenttype", type="string", length=255, nullable=false)
     */
    private $documenttype;

    /**
     * @var string
     *
     * @ORM\Column(name="documentnr", type="string", length=255, nullable=false)
     */
    private $documentnr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="issueddate", type="datetime", nullable=false)
     */
    private $issueddate;

    /**
     * @var string
     *
     * @ORM\Column(name="issueplace", type="string", length=255, nullable=false)
     */
    private $issueplace;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=255, nullable=false)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="watchlisteddriver", type="string", length=255, nullable=false)
     */
    private $watchlisteddriver;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=false)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="reservationumber", type="string", length=255, nullable=false)
     */
    private $reservationumber;

    /**
     * @var string
     *
     * @ORM\Column(name="rentalagreementnumber", type="string", length=255, nullable=false)
     */
    private $rentalagreementnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="platenumber", type="string", length=255, nullable=false)
     */
    private $platenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="colour", type="string", length=255, nullable=false)
     */
    private $colour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateouthp", type="datetime", nullable=false)
     */
    private $dateouthp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateinhp", type="datetime", nullable=false)
     */
    private $dateinhp;

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", length=255, nullable=false)
     */
    private $period;

    /**
     * @var int
     *
     * @ORM\Column(name="countrycode", type="integer", nullable=false)
     */
    private $countrycode;

    /**
     * @var string
     *
     * @ORM\Column(name="drivertaxnumber", type="string", length=255, nullable=false)
     */
    private $drivertaxnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="driverlicensenumber", type="string", length=255, nullable=false)
     */
    private $driverlicensenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="driverlicenseissueplace", type="string", length=255, nullable=false)
     */
    private $driverlicenseissueplace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="driverlicenseissuedate", type="datetime", nullable=false)
     */
    private $driverlicenseissuedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="driverlicenseexpirydate", type="datetime", nullable=false)
     */
    private $driverlicenseexpirydate;

    /**
     * @var string
     *
     * @ORM\Column(name="sendcomunication", type="string", length=255, nullable=false)
     */
    private $sendcomunication;

    /**
     * @var string
     *
     * @ORM\Column(name="sha1", type="string", length=40, nullable=false)
     */
    private $sha1;



    /**
     * Get idcarrenthertzportugal.
     *
     * @return int
     */
    public function getIdcarrenthertzportugal()
    {
        return $this->idcarrenthertzportugal;
    }

    /**
     * Set drivertype.
     *
     * @param string $drivertype
     *
     * @return Carrenthertzportugal
     */
    public function setDrivertype($drivertype)
    {
        $this->drivertype = $drivertype;

        return $this;
    }

    /**
     * Get drivertype.
     *
     * @return string
     */
    public function getDrivertype()
    {
        return $this->drivertype;
    }

    /**
     * Set drivercode.
     *
     * @param int $drivercode
     *
     * @return Carrenthertzportugal
     */
    public function setDrivercode($drivercode)
    {
        $this->drivercode = $drivercode;

        return $this;
    }

    /**
     * Get drivercode.
     *
     * @return int
     */
    public function getDrivercode()
    {
        return $this->drivercode;
    }

    /**
     * Set drivername.
     *
     * @param string $drivername
     *
     * @return Carrenthertzportugal
     */
    public function setDrivername($drivername)
    {
        $this->drivername = $drivername;

        return $this;
    }

    /**
     * Get drivername.
     *
     * @return string
     */
    public function getDrivername()
    {
        return $this->drivername;
    }

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return Carrenthertzportugal
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zipcode.
     *
     * @param string $zipcode
     *
     * @return Carrenthertzportugal
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode.
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Carrenthertzportugal
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return Carrenthertzportugal
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set birthdate.
     *
     * @param \DateTime $birthdate
     *
     * @return Carrenthertzportugal
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate.
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set birthplace.
     *
     * @param string $birthplace
     *
     * @return Carrenthertzportugal
     */
    public function setBirthplace($birthplace)
    {
        $this->birthplace = $birthplace;

        return $this;
    }

    /**
     * Get birthplace.
     *
     * @return string
     */
    public function getBirthplace()
    {
        return $this->birthplace;
    }

    /**
     * Set age.
     *
     * @param int $age
     *
     * @return Carrenthertzportugal
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age.
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set documenttype.
     *
     * @param string $documenttype
     *
     * @return Carrenthertzportugal
     */
    public function setDocumenttype($documenttype)
    {
        $this->documenttype = $documenttype;

        return $this;
    }

    /**
     * Get documenttype.
     *
     * @return string
     */
    public function getDocumenttype()
    {
        return $this->documenttype;
    }

    /**
     * Set documentnr.
     *
     * @param string $documentnr
     *
     * @return Carrenthertzportugal
     */
    public function setDocumentnr($documentnr)
    {
        $this->documentnr = $documentnr;

        return $this;
    }

    /**
     * Get documentnr.
     *
     * @return string
     */
    public function getDocumentnr()
    {
        return $this->documentnr;
    }

    /**
     * Set issueddate.
     *
     * @param \DateTime $issueddate
     *
     * @return Carrenthertzportugal
     */
    public function setIssueddate($issueddate)
    {
        $this->issueddate = $issueddate;

        return $this;
    }

    /**
     * Get issueddate.
     *
     * @return \DateTime
     */
    public function getIssueddate()
    {
        return $this->issueddate;
    }

    /**
     * Set issueplace.
     *
     * @param string $issueplace
     *
     * @return Carrenthertzportugal
     */
    public function setIssueplace($issueplace)
    {
        $this->issueplace = $issueplace;

        return $this;
    }

    /**
     * Get issueplace.
     *
     * @return string
     */
    public function getIssueplace()
    {
        return $this->issueplace;
    }

    /**
     * Set sex.
     *
     * @param string $sex
     *
     * @return Carrenthertzportugal
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex.
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set watchlisteddriver.
     *
     * @param string $watchlisteddriver
     *
     * @return Carrenthertzportugal
     */
    public function setWatchlisteddriver($watchlisteddriver)
    {
        $this->watchlisteddriver = $watchlisteddriver;

        return $this;
    }

    /**
     * Get watchlisteddriver.
     *
     * @return string
     */
    public function getWatchlisteddriver()
    {
        return $this->watchlisteddriver;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Carrenthertzportugal
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Carrenthertzportugal
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set reservationumber.
     *
     * @param string $reservationumber
     *
     * @return Carrenthertzportugal
     */
    public function setReservationumber($reservationumber)
    {
        $this->reservationumber = $reservationumber;

        return $this;
    }

    /**
     * Get reservationumber.
     *
     * @return string
     */
    public function getReservationumber()
    {
        return $this->reservationumber;
    }

    /**
     * Set rentalagreementnumber.
     *
     * @param string $rentalagreementnumber
     *
     * @return Carrenthertzportugal
     */
    public function setRentalagreementnumber($rentalagreementnumber)
    {
        $this->rentalagreementnumber = $rentalagreementnumber;

        return $this;
    }

    /**
     * Get rentalagreementnumber.
     *
     * @return string
     */
    public function getRentalagreementnumber()
    {
        return $this->rentalagreementnumber;
    }

    /**
     * Set platenumber.
     *
     * @param string $platenumber
     *
     * @return Carrenthertzportugal
     */
    public function setPlatenumber($platenumber)
    {
        $this->platenumber = $platenumber;

        return $this;
    }

    /**
     * Get platenumber.
     *
     * @return string
     */
    public function getPlatenumber()
    {
        return $this->platenumber;
    }

    /**
     * Set colour.
     *
     * @param string $colour
     *
     * @return Carrenthertzportugal
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour.
     *
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * Set dateouthp.
     *
     * @param \DateTime $dateouthp
     *
     * @return Carrenthertzportugal
     */
    public function setDateouthp($dateouthp)
    {
        $this->dateouthp = $dateouthp;

        return $this;
    }

    /**
     * Get dateouthp.
     *
     * @return \DateTime
     */
    public function getDateouthp()
    {
        return $this->dateouthp;
    }

    /**
     * Set dateinhp.
     *
     * @param \DateTime $dateinhp
     *
     * @return Carrenthertzportugal
     */
    public function setDateinhp($dateinhp)
    {
        $this->dateinhp = $dateinhp;

        return $this;
    }

    /**
     * Get dateinhp.
     *
     * @return \DateTime
     */
    public function getDateinhp()
    {
        return $this->dateinhp;
    }

    /**
     * Set period.
     *
     * @param string $period
     *
     * @return Carrenthertzportugal
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period.
     *
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set countrycode.
     *
     * @param int $countrycode
     *
     * @return Carrenthertzportugal
     */
    public function setCountrycode($countrycode)
    {
        $this->countrycode = $countrycode;

        return $this;
    }

    /**
     * Get countrycode.
     *
     * @return int
     */
    public function getCountrycode()
    {
        return $this->countrycode;
    }

    /**
     * Set drivertaxnumber.
     *
     * @param string $drivertaxnumber
     *
     * @return Carrenthertzportugal
     */
    public function setDrivertaxnumber($drivertaxnumber)
    {
        $this->drivertaxnumber = $drivertaxnumber;

        return $this;
    }

    /**
     * Get drivertaxnumber.
     *
     * @return string
     */
    public function getDrivertaxnumber()
    {
        return $this->drivertaxnumber;
    }

    /**
     * Set driverlicensenumber.
     *
     * @param string $driverlicensenumber
     *
     * @return Carrenthertzportugal
     */
    public function setDriverlicensenumber($driverlicensenumber)
    {
        $this->driverlicensenumber = $driverlicensenumber;

        return $this;
    }

    /**
     * Get driverlicensenumber.
     *
     * @return string
     */
    public function getDriverlicensenumber()
    {
        return $this->driverlicensenumber;
    }

    /**
     * Set driverlicenseissueplace.
     *
     * @param string $driverlicenseissueplace
     *
     * @return Carrenthertzportugal
     */
    public function setDriverlicenseissueplace($driverlicenseissueplace)
    {
        $this->driverlicenseissueplace = $driverlicenseissueplace;

        return $this;
    }

    /**
     * Get driverlicenseissueplace.
     *
     * @return string
     */
    public function getDriverlicenseissueplace()
    {
        return $this->driverlicenseissueplace;
    }

    /**
     * Set driverlicenseissuedate.
     *
     * @param \DateTime $driverlicenseissuedate
     *
     * @return Carrenthertzportugal
     */
    public function setDriverlicenseissuedate($driverlicenseissuedate)
    {
        $this->driverlicenseissuedate = $driverlicenseissuedate;

        return $this;
    }

    /**
     * Get driverlicenseissuedate.
     *
     * @return \DateTime
     */
    public function getDriverlicenseissuedate()
    {
        return $this->driverlicenseissuedate;
    }

    /**
     * Set driverlicenseexpirydate.
     *
     * @param \DateTime $driverlicenseexpirydate
     *
     * @return Carrenthertzportugal
     */
    public function setDriverlicenseexpirydate($driverlicenseexpirydate)
    {
        $this->driverlicenseexpirydate = $driverlicenseexpirydate;

        return $this;
    }

    /**
     * Get driverlicenseexpirydate.
     *
     * @return \DateTime
     */
    public function getDriverlicenseexpirydate()
    {
        return $this->driverlicenseexpirydate;
    }

    /**
     * Set sendcomunication.
     *
     * @param string $sendcomunication
     *
     * @return Carrenthertzportugal
     */
    public function setSendcomunication($sendcomunication)
    {
        $this->sendcomunication = $sendcomunication;

        return $this;
    }

    /**
     * Get sendcomunication.
     *
     * @return string
     */
    public function getSendcomunication()
    {
        return $this->sendcomunication;
    }

    /**
     * Set sha1.
     *
     * @param string $sha1
     *
     * @return Carrenthertzportugal
     */
    public function setSha1($sha1)
    {
        $this->sha1 = $sha1;

        return $this;
    }

    /**
     * Get sha1.
     *
     * @return string
     */
    public function getSha1()
    {
        return $this->sha1;
    }
}
