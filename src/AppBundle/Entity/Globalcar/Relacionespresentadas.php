<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relacionespresentadas
 *
 * @ORM\Table(name="relacionespresentadas", uniqueConstraints={@ORM\UniqueConstraint(name="idrelaciones_REL_FK_idx", columns={"idrelaciones"})}, indexes={@ORM\Index(name="idusuarios_REL_FK_idx", columns={"idusuarios"}), @ORM\Index(name="idregistros_REL_FK_idx", columns={"idregistros"})})
 * @ORM\Entity
 */
class Relacionespresentadas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idelacionesPresentadas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idelacionespresentadas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=300, nullable=true)
     */
    private $observaciones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSalida", type="datetime", nullable=false)
     */
    private $fechasalida;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaRecuperacion", type="datetime", nullable=true)
     */
    private $fecharecuperacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="relacionespresentadascol", type="string", length=45, nullable=true)
     */
    private $relacionespresentadascol;

    /**
     * @var \Registros
     *
     * @ORM\ManyToOne(targetEntity="Registros")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idregistros", referencedColumnName="idregistros")
     * })
     */
    private $idregistros;

    /**
     * @var \Relaciones
     *
     * @ORM\ManyToOne(targetEntity="Relaciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idrelaciones", referencedColumnName="idrelaciones")
     * })
     */
    private $idrelaciones;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarios", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarios;



    /**
     * Get idelacionespresentadas.
     *
     * @return int
     */
    public function getIdelacionespresentadas()
    {
        return $this->idelacionespresentadas;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Relacionespresentadas
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set fechasalida.
     *
     * @param \DateTime $fechasalida
     *
     * @return Relacionespresentadas
     */
    public function setFechasalida($fechasalida)
    {
        $this->fechasalida = $fechasalida;

        return $this;
    }

    /**
     * Get fechasalida.
     *
     * @return \DateTime
     */
    public function getFechasalida()
    {
        return $this->fechasalida;
    }

    /**
     * Set fecharecuperacion.
     *
     * @param \DateTime|null $fecharecuperacion
     *
     * @return Relacionespresentadas
     */
    public function setFecharecuperacion($fecharecuperacion = null)
    {
        $this->fecharecuperacion = $fecharecuperacion;

        return $this;
    }

    /**
     * Get fecharecuperacion.
     *
     * @return \DateTime|null
     */
    public function getFecharecuperacion()
    {
        return $this->fecharecuperacion;
    }

    /**
     * Set relacionespresentadascol.
     *
     * @param string|null $relacionespresentadascol
     *
     * @return Relacionespresentadas
     */
    public function setRelacionespresentadascol($relacionespresentadascol = null)
    {
        $this->relacionespresentadascol = $relacionespresentadascol;

        return $this;
    }

    /**
     * Get relacionespresentadascol.
     *
     * @return string|null
     */
    public function getRelacionespresentadascol()
    {
        return $this->relacionespresentadascol;
    }

    /**
     * Set idregistros.
     *
     * @param \AppBundle\Entity\Globalcar\Registros|null $idregistros
     *
     * @return Relacionespresentadas
     */
    public function setIdregistros(\AppBundle\Entity\Globalcar\Registros $idregistros = null)
    {
        $this->idregistros = $idregistros;

        return $this;
    }

    /**
     * Get idregistros.
     *
     * @return \AppBundle\Entity\Globalcar\Registros|null
     */
    public function getIdregistros()
    {
        return $this->idregistros;
    }

    /**
     * Set idrelaciones.
     *
     * @param \AppBundle\Entity\Globalcar\Relaciones|null $idrelaciones
     *
     * @return Relacionespresentadas
     */
    public function setIdrelaciones(\AppBundle\Entity\Globalcar\Relaciones $idrelaciones = null)
    {
        $this->idrelaciones = $idrelaciones;

        return $this;
    }

    /**
     * Get idrelaciones.
     *
     * @return \AppBundle\Entity\Globalcar\Relaciones|null
     */
    public function getIdrelaciones()
    {
        return $this->idrelaciones;
    }

    /**
     * Set idusuarios.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarios
     *
     * @return Relacionespresentadas
     */
    public function setIdusuarios(\AppBundle\Entity\Globalcar\Usuarios $idusuarios = null)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }
}
