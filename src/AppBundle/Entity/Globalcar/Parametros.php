<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parametros
 *
 * @ORM\Table(name="parametros", uniqueConstraints={@ORM\UniqueConstraint(name="tiponombre_IN_P", columns={"tipo", "nombre"})})
 * @ORM\Entity
 */
class Parametros
{
    /**
     * @var int
     *
     * @ORM\Column(name="idparametros", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idparametros;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=10, nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="text", length=65535, nullable=true)
     */
    private $valor;



    /**
     * Get idparametros.
     *
     * @return int
     */
    public function getIdparametros()
    {
        return $this->idparametros;
    }

    /**
     * Set tipo.
     *
     * @param string $tipo
     *
     * @return Parametros
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Parametros
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set valor.
     *
     * @param string|null $valor
     *
     * @return Parametros
     */
    public function setValor($valor = null)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor.
     *
     * @return string|null
     */
    public function getValor()
    {
        return $this->valor;
    }
}
