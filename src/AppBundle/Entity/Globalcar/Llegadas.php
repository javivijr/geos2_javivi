<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Llegadas
 *
 * @ORM\Table(name="llegadas", indexes={@ORM\Index(name="idpersonas_LLE_FK_idx", columns={"idpersonas"}), @ORM\Index(name="idescritoentrada_LL_FK_IDX", columns={"idescritoEntrada"})})
 * @ORM\Entity
 */
class Llegadas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idllegadas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idllegadas;

    /**
     * @var string
     *
     * @ORM\Column(name="nifdenunciado", type="string", length=15, nullable=false)
     */
    private $nifdenunciado;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaNotificacion", type="datetime", nullable=true)
     */
    private $fechanotificacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaVencimiento", type="datetime", nullable=true)
     */
    private $fechavencimiento;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaAlta", type="datetime", nullable=true)
     */
    private $fechaalta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaUltModificacion", type="datetime", nullable=true)
     */
    private $fechaultmodificacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaOKr1", type="datetime", nullable=true)
     */
    private $fechaokr1;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaOkr2", type="datetime", nullable=true)
     */
    private $fechaokr2;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="llegadascol", type="string", length=45, nullable=true)
     */
    private $llegadascol;

    /**
     * @var string|null
     *
     * @ORM\Column(name="llegadascol1", type="string", length=45, nullable=true)
     */
    private $llegadascol1;

    /**
     * @var \Escritoentrada
     *
     * @ORM\ManyToOne(targetEntity="Escritoentrada")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idescritoEntrada", referencedColumnName="idescritoEntrada")
     * })
     */
    private $idescritoentrada;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersonas", referencedColumnName="idpersonas")
     * })
     */
    private $idpersonas;



    /**
     * Get idllegadas.
     *
     * @return int
     */
    public function getIdllegadas()
    {
        return $this->idllegadas;
    }

    /**
     * Set nifdenunciado.
     *
     * @param string $nifdenunciado
     *
     * @return Llegadas
     */
    public function setNifdenunciado($nifdenunciado)
    {
        $this->nifdenunciado = $nifdenunciado;

        return $this;
    }

    /**
     * Get nifdenunciado.
     *
     * @return string
     */
    public function getNifdenunciado()
    {
        return $this->nifdenunciado;
    }

    /**
     * Set fechanotificacion.
     *
     * @param \DateTime|null $fechanotificacion
     *
     * @return Llegadas
     */
    public function setFechanotificacion($fechanotificacion = null)
    {
        $this->fechanotificacion = $fechanotificacion;

        return $this;
    }

    /**
     * Get fechanotificacion.
     *
     * @return \DateTime|null
     */
    public function getFechanotificacion()
    {
        return $this->fechanotificacion;
    }

    /**
     * Set fechavencimiento.
     *
     * @param \DateTime|null $fechavencimiento
     *
     * @return Llegadas
     */
    public function setFechavencimiento($fechavencimiento = null)
    {
        $this->fechavencimiento = $fechavencimiento;

        return $this;
    }

    /**
     * Get fechavencimiento.
     *
     * @return \DateTime|null
     */
    public function getFechavencimiento()
    {
        return $this->fechavencimiento;
    }

    /**
     * Set cantidad.
     *
     * @param int|null $cantidad
     *
     * @return Llegadas
     */
    public function setCantidad($cantidad = null)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad.
     *
     * @return int|null
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set fechaalta.
     *
     * @param \DateTime|null $fechaalta
     *
     * @return Llegadas
     */
    public function setFechaalta($fechaalta = null)
    {
        $this->fechaalta = $fechaalta;

        return $this;
    }

    /**
     * Get fechaalta.
     *
     * @return \DateTime|null
     */
    public function getFechaalta()
    {
        return $this->fechaalta;
    }

    /**
     * Set fechaultmodificacion.
     *
     * @param \DateTime|null $fechaultmodificacion
     *
     * @return Llegadas
     */
    public function setFechaultmodificacion($fechaultmodificacion = null)
    {
        $this->fechaultmodificacion = $fechaultmodificacion;

        return $this;
    }

    /**
     * Get fechaultmodificacion.
     *
     * @return \DateTime|null
     */
    public function getFechaultmodificacion()
    {
        return $this->fechaultmodificacion;
    }

    /**
     * Set fechaokr1.
     *
     * @param \DateTime|null $fechaokr1
     *
     * @return Llegadas
     */
    public function setFechaokr1($fechaokr1 = null)
    {
        $this->fechaokr1 = $fechaokr1;

        return $this;
    }

    /**
     * Get fechaokr1.
     *
     * @return \DateTime|null
     */
    public function getFechaokr1()
    {
        return $this->fechaokr1;
    }

    /**
     * Set fechaokr2.
     *
     * @param \DateTime|null $fechaokr2
     *
     * @return Llegadas
     */
    public function setFechaokr2($fechaokr2 = null)
    {
        $this->fechaokr2 = $fechaokr2;

        return $this;
    }

    /**
     * Get fechaokr2.
     *
     * @return \DateTime|null
     */
    public function getFechaokr2()
    {
        return $this->fechaokr2;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Llegadas
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set llegadascol.
     *
     * @param string|null $llegadascol
     *
     * @return Llegadas
     */
    public function setLlegadascol($llegadascol = null)
    {
        $this->llegadascol = $llegadascol;

        return $this;
    }

    /**
     * Get llegadascol.
     *
     * @return string|null
     */
    public function getLlegadascol()
    {
        return $this->llegadascol;
    }

    /**
     * Set llegadascol1.
     *
     * @param string|null $llegadascol1
     *
     * @return Llegadas
     */
    public function setLlegadascol1($llegadascol1 = null)
    {
        $this->llegadascol1 = $llegadascol1;

        return $this;
    }

    /**
     * Get llegadascol1.
     *
     * @return string|null
     */
    public function getLlegadascol1()
    {
        return $this->llegadascol1;
    }

    /**
     * Set idescritoentrada.
     *
     * @param \AppBundle\Entity\Globalcar\Escritoentrada|null $idescritoentrada
     *
     * @return Llegadas
     */
    public function setIdescritoentrada(\AppBundle\Entity\Globalcar\Escritoentrada $idescritoentrada = null)
    {
        $this->idescritoentrada = $idescritoentrada;

        return $this;
    }

    /**
     * Get idescritoentrada.
     *
     * @return \AppBundle\Entity\Globalcar\Escritoentrada|null
     */
    public function getIdescritoentrada()
    {
        return $this->idescritoentrada;
    }

    /**
     * Set idpersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Personas|null $idpersonas
     *
     * @return Llegadas
     */
    public function setIdpersonas(\AppBundle\Entity\Globalcar\Personas $idpersonas = null)
    {
        $this->idpersonas = $idpersonas;

        return $this;
    }

    /**
     * Get idpersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Personas|null
     */
    public function getIdpersonas()
    {
        return $this->idpersonas;
    }
}
