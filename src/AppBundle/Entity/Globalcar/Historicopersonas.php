<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historicopersonas
 *
 * @ORM\Table(name="historicopersonas", indexes={@ORM\Index(name="idpersonas_HP_FK_idx", columns={"idpersonas"}), @ORM\Index(name="idtipoCalle_HP_FK_idx", columns={"idtipoCalle"}), @ORM\Index(name="idhistoricopersportu_IDX", columns={"idhistoricopersonasportugal"})})
 * @ORM\Entity
 */
class Historicopersonas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhistoricoPersonas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhistoricopersonas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="calle", type="string", length=250, nullable=true)
     */
    private $calle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=11, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="portal", type="string", length=3, nullable=true)
     */
    private $portal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="escalera", type="string", length=3, nullable=true)
     */
    private $escalera;

    /**
     * @var string|null
     *
     * @ORM\Column(name="piso", type="string", length=2, nullable=true)
     */
    private $piso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="puerta", type="string", length=2, nullable=true)
     */
    private $puerta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=10, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poblacion", type="string", length=60, nullable=true)
     */
    private $poblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=60, nullable=true)
     */
    private $provincia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="paisResidencia", type="string", length=20, nullable=true)
     */
    private $paisresidencia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email2", type="string", length=100, nullable=true)
     */
    private $email2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", length=13, nullable=true)
     */
    private $telefono;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referencia", type="string", length=100, nullable=true)
     */
    private $referencia;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="envCorreo", type="string", length=0, nullable=false)
     */
    private $envcorreo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=500, nullable=true)
     */
    private $observaciones;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoNCA", type="integer", nullable=true)
     */
    private $codigonca;

    /**
     * @var Historicopersonasportugal
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonasportugal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricopersonasportugal", referencedColumnName="idhistoricopersonasportugal")
     * })
     */
    private $idhistoricopersonasportugal;

    /**
     * @var Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersonas", referencedColumnName="idpersonas")
     * })
     */
    private $idpersonas;

    /**
     * @var Tipocalle
     *
     * @ORM\ManyToOne(targetEntity="Tipocalle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipoCalle", referencedColumnName="idtipoCalle")
     * })
     */
    private $idtipocalle;



    /**
     * Get idhistoricopersonas.
     *
     * @return int
     */
    public function getIdhistoricopersonas()
    {
        return $this->idhistoricopersonas;
    }

    /**
     * Set calle.
     *
     * @param string|null $calle
     *
     * @return Historicopersonas
     */
    public function setCalle($calle = null)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle.
     *
     * @return string|null
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numero.
     *
     * @param string|null $numero
     *
     * @return Historicopersonas
     */
    public function setNumero($numero = null)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero.
     *
     * @return string|null
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set portal.
     *
     * @param string|null $portal
     *
     * @return Historicopersonas
     */
    public function setPortal($portal = null)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal.
     *
     * @return string|null
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * Set escalera.
     *
     * @param string|null $escalera
     *
     * @return Historicopersonas
     */
    public function setEscalera($escalera = null)
    {
        $this->escalera = $escalera;

        return $this;
    }

    /**
     * Get escalera.
     *
     * @return string|null
     */
    public function getEscalera()
    {
        return $this->escalera;
    }

    /**
     * Set piso.
     *
     * @param string|null $piso
     *
     * @return Historicopersonas
     */
    public function setPiso($piso = null)
    {
        $this->piso = $piso;

        return $this;
    }

    /**
     * Get piso.
     *
     * @return string|null
     */
    public function getPiso()
    {
        return $this->piso;
    }

    /**
     * Set puerta.
     *
     * @param string|null $puerta
     *
     * @return Historicopersonas
     */
    public function setPuerta($puerta = null)
    {
        $this->puerta = $puerta;

        return $this;
    }

    /**
     * Get puerta.
     *
     * @return string|null
     */
    public function getPuerta()
    {
        return $this->puerta;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Historicopersonas
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set poblacion.
     *
     * @param string|null $poblacion
     *
     * @return Historicopersonas
     */
    public function setPoblacion($poblacion = null)
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * Get poblacion.
     *
     * @return string|null
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Historicopersonas
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set paisresidencia.
     *
     * @param string|null $paisresidencia
     *
     * @return Historicopersonas
     */
    public function setPaisresidencia($paisresidencia = null)
    {
        $this->paisresidencia = $paisresidencia;

        return $this;
    }

    /**
     * Get paisresidencia.
     *
     * @return string|null
     */
    public function getPaisresidencia()
    {
        return $this->paisresidencia;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Historicopersonas
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email2.
     *
     * @param string|null $email2
     *
     * @return Historicopersonas
     */
    public function setEmail2($email2 = null)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2.
     *
     * @return string|null
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set telefono.
     *
     * @param string|null $telefono
     *
     * @return Historicopersonas
     */
    public function setTelefono($telefono = null)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono.
     *
     * @return string|null
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set referencia.
     *
     * @param string|null $referencia
     *
     * @return Historicopersonas
     */
    public function setReferencia($referencia = null)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia.
     *
     * @return string|null
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Historicopersonas
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set envcorreo.
     *
     * @param string $envcorreo
     *
     * @return Historicopersonas
     */
    public function setEnvcorreo($envcorreo)
    {
        $this->envcorreo = $envcorreo;

        return $this;
    }

    /**
     * Get envcorreo.
     *
     * @return string
     */
    public function getEnvcorreo()
    {
        return $this->envcorreo;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Historicopersonas
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set codigonca.
     *
     * @param int|null $codigonca
     *
     * @return Historicopersonas
     */
    public function setCodigonca($codigonca = null)
    {
        $this->codigonca = $codigonca;

        return $this;
    }

    /**
     * Get codigonca.
     *
     * @return int|null
     */
    public function getCodigonca()
    {
        return $this->codigonca;
    }

    /**
     * Set idhistoricopersonasportugal.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonasportugal|null $idhistoricopersonasportugal
     *
     * @return Historicopersonas
     */
    public function setIdhistoricopersonasportugal(\AppBundle\Entity\Globalcar\Historicopersonasportugal $idhistoricopersonasportugal = null)
    {
        $this->idhistoricopersonasportugal = $idhistoricopersonasportugal;

        return $this;
    }

    /**
     * Get idhistoricopersonasportugal.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonasportugal|null
     */
    public function getIdhistoricopersonasportugal()
    {
        return $this->idhistoricopersonasportugal;
    }

    /**
     * Set idpersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Personas|null $idpersonas
     *
     * @return Historicopersonas
     */
    public function setIdpersonas(\AppBundle\Entity\Globalcar\Personas $idpersonas = null)
    {
        $this->idpersonas = $idpersonas;

        return $this;
    }

    /**
     * Get idpersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Personas|null
     */
    public function getIdpersonas()
    {
        return $this->idpersonas;
    }

    /**
     * Set idtipocalle.
     *
     * @param \AppBundle\Entity\Globalcar\Tipocalle|null $idtipocalle
     *
     * @return Historicopersonas
     */
    public function setIdtipocalle(\AppBundle\Entity\Globalcar\Tipocalle $idtipocalle = null)
    {
        $this->idtipocalle = $idtipocalle;

        return $this;
    }

    /**
     * Get idtipocalle.
     *
     * @return \AppBundle\Entity\Globalcar\Tipocalle|null
     */
    public function getIdtipocalle()
    {
        return $this->idtipocalle;
    }
}
