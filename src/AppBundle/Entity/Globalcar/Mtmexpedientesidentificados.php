<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmexpedientesidentificados
 *
 * @ORM\Table(name="mtmexpedientesidentificados", indexes={@ORM\Index(name="idexpedientes_MEI_FK_idx", columns={"idexpedientes"}), @ORM\Index(name="idtitulares_MEI_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idhistoricoPersonas_MEI_FK_idx", columns={"idhistoricoPersonas"})})
 * @ORM\Entity
 */
class Mtmexpedientesidentificados
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmExpedientesIdentificados", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmexpedientesidentificados;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=true)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuarioMtm", type="string", length=30, nullable=true)
     */
    private $usuariomtm;

    /**
     * @var string
     *
     * @ORM\Column(name="borrado", type="string", length=0, nullable=false)
     */
    private $borrado;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Historicopersonas
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoPersonas", referencedColumnName="idhistoricoPersonas")
     * })
     */
    private $idhistoricopersonas;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idmtmexpedientesidentificados.
     *
     * @return int
     */
    public function getIdmtmexpedientesidentificados()
    {
        return $this->idmtmexpedientesidentificados;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Mtmexpedientesidentificados
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return Mtmexpedientesidentificados
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set usuariomtm.
     *
     * @param string|null $usuariomtm
     *
     * @return Mtmexpedientesidentificados
     */
    public function setUsuariomtm($usuariomtm = null)
    {
        $this->usuariomtm = $usuariomtm;

        return $this;
    }

    /**
     * Get usuariomtm.
     *
     * @return string|null
     */
    public function getUsuariomtm()
    {
        return $this->usuariomtm;
    }

    /**
     * Set borrado.
     *
     * @param string $borrado
     *
     * @return Mtmexpedientesidentificados
     */
    public function setBorrado($borrado)
    {
        $this->borrado = $borrado;

        return $this;
    }

    /**
     * Get borrado.
     *
     * @return string
     */
    public function getBorrado()
    {
        return $this->borrado;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Mtmexpedientesidentificados
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idhistoricopersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonas|null $idhistoricopersonas
     *
     * @return Mtmexpedientesidentificados
     */
    public function setIdhistoricopersonas(\AppBundle\Entity\Globalcar\Historicopersonas $idhistoricopersonas = null)
    {
        $this->idhistoricopersonas = $idhistoricopersonas;

        return $this;
    }

    /**
     * Get idhistoricopersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonas|null
     */
    public function getIdhistoricopersonas()
    {
        return $this->idhistoricopersonas;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmexpedientesidentificados
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
