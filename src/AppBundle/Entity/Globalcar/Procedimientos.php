<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procedimientos
 *
 * @ORM\Table(name="procedimientos", indexes={@ORM\Index(name="idhistoricoVehiculos_PRO_FK_idx", columns={"idhistoricoVehiculos"})})
 * @ORM\Entity
 */
class Procedimientos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idprocedimientos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idprocedimientos;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaDenuncia", type="datetime", nullable=true)
     */
    private $fechadenuncia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lugarDenuncia", type="string", length=80, nullable=true)
     */
    private $lugardenuncia;

    /**
     * @var \Historicovehiculos
     *
     * @ORM\ManyToOne(targetEntity="Historicovehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoVehiculos", referencedColumnName="idhistoricoVehiculos")
     * })
     */
    private $idhistoricovehiculos;



    /**
     * Get idprocedimientos.
     *
     * @return int
     */
    public function getIdprocedimientos()
    {
        return $this->idprocedimientos;
    }

    /**
     * Set fechadenuncia.
     *
     * @param \DateTime|null $fechadenuncia
     *
     * @return Procedimientos
     */
    public function setFechadenuncia($fechadenuncia = null)
    {
        $this->fechadenuncia = $fechadenuncia;

        return $this;
    }

    /**
     * Get fechadenuncia.
     *
     * @return \DateTime|null
     */
    public function getFechadenuncia()
    {
        return $this->fechadenuncia;
    }

    /**
     * Set lugardenuncia.
     *
     * @param string|null $lugardenuncia
     *
     * @return Procedimientos
     */
    public function setLugardenuncia($lugardenuncia = null)
    {
        $this->lugardenuncia = $lugardenuncia;

        return $this;
    }

    /**
     * Get lugardenuncia.
     *
     * @return string|null
     */
    public function getLugardenuncia()
    {
        return $this->lugardenuncia;
    }

    /**
     * Set idhistoricovehiculos.
     *
     * @param \AppBundle\Entity\Globalcar\Historicovehiculos|null $idhistoricovehiculos
     *
     * @return Procedimientos
     */
    public function setIdhistoricovehiculos(\AppBundle\Entity\Globalcar\Historicovehiculos $idhistoricovehiculos = null)
    {
        $this->idhistoricovehiculos = $idhistoricovehiculos;

        return $this;
    }

    /**
     * Get idhistoricovehiculos.
     *
     * @return \AppBundle\Entity\Globalcar\Historicovehiculos|null
     */
    public function getIdhistoricovehiculos()
    {
        return $this->idhistoricovehiculos;
    }
}
