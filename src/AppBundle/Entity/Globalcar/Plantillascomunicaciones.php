<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Plantillascomunicaciones
 *
 * @ORM\Table(name="plantillascomunicaciones", indexes={@ORM\Index(name="PlantComTitu_idx", columns={"idtitulares"}), @ORM\Index(name="PlantComPlant_idx", columns={"idplantillas"}), @ORM\Index(name="TipoPlantCom", columns={"tipocom"})})
 * @ORM\Entity
 */
class Plantillascomunicaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="idplantillascomunicaciones", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idplantillascomunicaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="tipocom", type="string", length=10, nullable=false, options={"default"="I1"})
     */
    private $tipocom = 'I1';

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=2, nullable=false, options={"default"="ES","fixed"=true})
     */
    private $idioma = 'ES';

    /**
     * @var string
     *
     * @ORM\Column(name="tipocomu", type="string", length=255, nullable=false, options={"default"="JARL"})
     */
    private $tipocomu = 'JARL';

    /**
     * @var int
     *
     * @ORM\Column(name="numeroenvio", type="smallint", nullable=false, options={"default"="1"})
     */
    private $numeroenvio = '1';

    /**
     * @var \Plantillas
     *
     * @ORM\ManyToOne(targetEntity="Plantillas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idplantillas", referencedColumnName="idplantillas")
     * })
     */
    private $idplantillas;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idplantillascomunicaciones.
     *
     * @return int
     */
    public function getIdplantillascomunicaciones()
    {
        return $this->idplantillascomunicaciones;
    }

    /**
     * Set tipocom.
     *
     * @param string $tipocom
     *
     * @return Plantillascomunicaciones
     */
    public function setTipocom($tipocom)
    {
        $this->tipocom = $tipocom;

        return $this;
    }

    /**
     * Get tipocom.
     *
     * @return string
     */
    public function getTipocom()
    {
        return $this->tipocom;
    }

    /**
     * Set idioma.
     *
     * @param string $idioma
     *
     * @return Plantillascomunicaciones
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma.
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set tipocomu.
     *
     * @param string $tipocomu
     *
     * @return Plantillascomunicaciones
     */
    public function setTipocomu($tipocomu)
    {
        $this->tipocomu = $tipocomu;

        return $this;
    }

    /**
     * Get tipocomu.
     *
     * @return string
     */
    public function getTipocomu()
    {
        return $this->tipocomu;
    }

    /**
     * Set numeroenvio.
     *
     * @param int $numeroenvio
     *
     * @return Plantillascomunicaciones
     */
    public function setNumeroenvio($numeroenvio)
    {
        $this->numeroenvio = $numeroenvio;

        return $this;
    }

    /**
     * Get numeroenvio.
     *
     * @return int
     */
    public function getNumeroenvio()
    {
        return $this->numeroenvio;
    }

    /**
     * Set idplantillas.
     *
     * @param \AppBundle\Entity\Globalcar\Plantillas|null $idplantillas
     *
     * @return Plantillascomunicaciones
     */
    public function setIdplantillas(\AppBundle\Entity\Globalcar\Plantillas $idplantillas = null)
    {
        $this->idplantillas = $idplantillas;

        return $this;
    }

    /**
     * Get idplantillas.
     *
     * @return \AppBundle\Entity\Globalcar\Plantillas|null
     */
    public function getIdplantillas()
    {
        return $this->idplantillas;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Plantillascomunicaciones
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
