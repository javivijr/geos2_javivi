<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmaccesoadmin
 *
 * @ORM\Table(name="mtmaccesoadmin", indexes={@ORM\Index(name="idusuariosmtm_MAA_FK_idx", columns={"idusuariosmtm"}), @ORM\Index(name="idtitulares_MAA_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idhistoricoPersonasCond", columns={"idhistoricoPersonasCond"})})
 * @ORM\Entity
 */
class Mtmaccesoadmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmaccesoadmin", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmaccesoadmin;

    /**
     * @var int
     *
     * @ORM\Column(name="perfil", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $perfil;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;

    /**
     * @var \Historicopersonas
     *
     * @ORM\ManyToOne(targetEntity="Historicopersonas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhistoricoPersonasCond", referencedColumnName="idhistoricoPersonas")
     * })
     */
    private $idhistoricopersonascond;



    /**
     * Get idmtmaccesoadmin.
     *
     * @return int
     */
    public function getIdmtmaccesoadmin()
    {
        return $this->idmtmaccesoadmin;
    }

    /**
     * Set perfil.
     *
     * @param int $perfil
     *
     * @return Mtmaccesoadmin
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil.
     *
     * @return int
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmaccesoadmin
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmaccesoadmin
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }

    /**
     * Set idhistoricopersonascond.
     *
     * @param \AppBundle\Entity\Globalcar\Historicopersonas|null $idhistoricopersonascond
     *
     * @return Mtmaccesoadmin
     */
    public function setIdhistoricopersonascond(\AppBundle\Entity\Globalcar\Historicopersonas $idhistoricopersonascond = null)
    {
        $this->idhistoricopersonascond = $idhistoricopersonascond;

        return $this;
    }

    /**
     * Get idhistoricopersonascond.
     *
     * @return \AppBundle\Entity\Globalcar\Historicopersonas|null
     */
    public function getIdhistoricopersonascond()
    {
        return $this->idhistoricopersonascond;
    }
}
