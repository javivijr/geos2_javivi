<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrentavis
 *
 * @ORM\Table(name="carrentavis", indexes={@ORM\Index(name="vehlicnum", columns={"vehlicnum"}), @ORM\Index(name="coutdatepluscouttime", columns={"coutdatepluscouttime"}), @ORM\Index(name="ranumber_2", columns={"ranumber", "vehlicnum", "coutdatepluscouttime"}), @ORM\Index(name="ranumber", columns={"ranumber", "vehlicnum", "coutdatepluscouttime", "cindatepluscintime", "abiertocerrado"}), @ORM\Index(name="sha1", columns={"sha1"}), @ORM\Index(name="fechafichero", columns={"fechafichero"})})
 * @ORM\Entity
 */
class Carrentavis
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrentavis", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrentavis;

    /**
     * @var string
     *
     * @ORM\Column(name="sha1", type="string", length=40, nullable=false, options={"fixed"=true})
     */
    private $sha1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechafichero", type="datetime", nullable=false, options={"default"="0001-00-00 00:00:00"})
     */
    private $fechafichero = '0001-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ranumber", type="string", length=11, nullable=false)
     */
    private $ranumber;

    /**
     * @var string
     *
     * @ORM\Column(name="mvanum", type="string", length=9, nullable=false)
     */
    private $mvanum;

    /**
     * @var string|null
     *
     * @ORM\Column(name="companyname", type="string", length=30, nullable=true)
     */
    private $companyname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hdqtraddress1", type="string", length=30, nullable=true)
     */
    private $hdqtraddress1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hdqtraddress2", type="string", length=30, nullable=true)
     */
    private $hdqtraddress2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taxnumber", type="string", length=16, nullable=true)
     */
    private $taxnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rentername", type="string", length=30, nullable=true)
     */
    private $rentername;

    /**
     * @var string|null
     *
     * @ORM\Column(name="renteraddress1", type="string", length=30, nullable=true)
     */
    private $renteraddress1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="renteraddress2", type="string", length=30, nullable=true)
     */
    private $renteraddress2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="renteraddress3", type="string", length=30, nullable=true)
     */
    private $renteraddress3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="localcontact", type="string", length=34, nullable=true)
     */
    private $localcontact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="renterbirthdate", type="string", length=10, nullable=true)
     */
    private $renterbirthdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="drivinglicense", type="string", length=27, nullable=true)
     */
    private $drivinglicense;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coutlocname", type="string", length=17, nullable=true)
     */
    private $coutlocname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cinlocname", type="string", length=17, nullable=true)
     */
    private $cinlocname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehmakemoddes", type="string", length=16, nullable=true)
     */
    private $vehmakemoddes;

    /**
     * @var string
     *
     * @ORM\Column(name="vehlicnum", type="string", length=10, nullable=false)
     */
    private $vehlicnum;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numaut", type="string", length=10, nullable=true)
     */
    private $numaut;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fltvehid", type="string", length=32, nullable=true)
     */
    private $fltvehid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehgrprented", type="string", length=1, nullable=true)
     */
    private $vehgrprented;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deslgasind", type="string", length=1, nullable=true)
     */
    private $deslgasind;

    /**
     * @var string|null
     *
     * @ORM\Column(name="countfuellevel", type="string", length=1, nullable=true)
     */
    private $countfuellevel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="countdistcount", type="decimal", precision=9, scale=1, nullable=true)
     */
    private $countdistcount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cindistcount", type="decimal", precision=9, scale=1, nullable=true)
     */
    private $cindistcount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="coutdatepluscouttime", type="datetime", nullable=false)
     */
    private $coutdatepluscouttime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="cindatepluscintime", type="datetime", nullable=true)
     */
    private $cindatepluscintime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rentallength", type="string", length=20, nullable=true)
     */
    private $rentallength;

    /**
     * @var int|null
     *
     * @ORM\Column(name="billeddays", type="integer", nullable=true)
     */
    private $billeddays;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pretaxrentalamt", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $pretaxrentalamt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taxpct", type="decimal", precision=9, scale=3, nullable=true)
     */
    private $taxpct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taxamt", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $taxamt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="totalrentalchgamt", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $totalrentalchgamt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reservationnumber", type="string", length=11, nullable=true)
     */
    private $reservationnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="creditclubcd", type="string", length=2, nullable=true)
     */
    private $creditclubcd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="creditcardnum", type="string", length=22, nullable=true)
     */
    private $creditcardnum;

    /**
     * @var string|null
     *
     * @ORM\Column(name="entcoutdate", type="string", length=10, nullable=true)
     */
    private $entcoutdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coutauthnum", type="string", length=10, nullable=true)
     */
    private $coutauthnum;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wizarddate", type="string", length=10, nullable=true)
     */
    private $wizarddate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="abiertocerrado", type="string", length=1, nullable=true)
     */
    private $abiertocerrado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehexchcount", type="decimal", precision=5, scale=0, nullable=true)
     */
    private $vehexchcount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="costcontrolnumber", type="string", length=15, nullable=true)
     */
    private $costcontrolnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="remarks", type="string", length=25, nullable=true)
     */
    private $remarks;

    /**
     * @var string|null
     *
     * @ORM\Column(name="corpcode", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $corpcode;



    /**
     * Get idcarrentavis.
     *
     * @return int
     */
    public function getIdcarrentavis()
    {
        return $this->idcarrentavis;
    }

    /**
     * Set sha1.
     *
     * @param string $sha1
     *
     * @return Carrentavis
     */
    public function setSha1($sha1)
    {
        $this->sha1 = $sha1;

        return $this;
    }

    /**
     * Get sha1.
     *
     * @return string
     */
    public function getSha1()
    {
        return $this->sha1;
    }

    /**
     * Set fechafichero.
     *
     * @param \DateTime $fechafichero
     *
     * @return Carrentavis
     */
    public function setFechafichero($fechafichero)
    {
        $this->fechafichero = $fechafichero;

        return $this;
    }

    /**
     * Get fechafichero.
     *
     * @return \DateTime
     */
    public function getFechafichero()
    {
        return $this->fechafichero;
    }

    /**
     * Set ranumber.
     *
     * @param string $ranumber
     *
     * @return Carrentavis
     */
    public function setRanumber($ranumber)
    {
        $this->ranumber = $ranumber;

        return $this;
    }

    /**
     * Get ranumber.
     *
     * @return string
     */
    public function getRanumber()
    {
        return $this->ranumber;
    }

    /**
     * Set mvanum.
     *
     * @param string $mvanum
     *
     * @return Carrentavis
     */
    public function setMvanum($mvanum)
    {
        $this->mvanum = $mvanum;

        return $this;
    }

    /**
     * Get mvanum.
     *
     * @return string
     */
    public function getMvanum()
    {
        return $this->mvanum;
    }

    /**
     * Set companyname.
     *
     * @param string|null $companyname
     *
     * @return Carrentavis
     */
    public function setCompanyname($companyname = null)
    {
        $this->companyname = $companyname;

        return $this;
    }

    /**
     * Get companyname.
     *
     * @return string|null
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Set hdqtraddress1.
     *
     * @param string|null $hdqtraddress1
     *
     * @return Carrentavis
     */
    public function setHdqtraddress1($hdqtraddress1 = null)
    {
        $this->hdqtraddress1 = $hdqtraddress1;

        return $this;
    }

    /**
     * Get hdqtraddress1.
     *
     * @return string|null
     */
    public function getHdqtraddress1()
    {
        return $this->hdqtraddress1;
    }

    /**
     * Set hdqtraddress2.
     *
     * @param string|null $hdqtraddress2
     *
     * @return Carrentavis
     */
    public function setHdqtraddress2($hdqtraddress2 = null)
    {
        $this->hdqtraddress2 = $hdqtraddress2;

        return $this;
    }

    /**
     * Get hdqtraddress2.
     *
     * @return string|null
     */
    public function getHdqtraddress2()
    {
        return $this->hdqtraddress2;
    }

    /**
     * Set taxnumber.
     *
     * @param string|null $taxnumber
     *
     * @return Carrentavis
     */
    public function setTaxnumber($taxnumber = null)
    {
        $this->taxnumber = $taxnumber;

        return $this;
    }

    /**
     * Get taxnumber.
     *
     * @return string|null
     */
    public function getTaxnumber()
    {
        return $this->taxnumber;
    }

    /**
     * Set rentername.
     *
     * @param string|null $rentername
     *
     * @return Carrentavis
     */
    public function setRentername($rentername = null)
    {
        $this->rentername = $rentername;

        return $this;
    }

    /**
     * Get rentername.
     *
     * @return string|null
     */
    public function getRentername()
    {
        return $this->rentername;
    }

    /**
     * Set renteraddress1.
     *
     * @param string|null $renteraddress1
     *
     * @return Carrentavis
     */
    public function setRenteraddress1($renteraddress1 = null)
    {
        $this->renteraddress1 = $renteraddress1;

        return $this;
    }

    /**
     * Get renteraddress1.
     *
     * @return string|null
     */
    public function getRenteraddress1()
    {
        return $this->renteraddress1;
    }

    /**
     * Set renteraddress2.
     *
     * @param string|null $renteraddress2
     *
     * @return Carrentavis
     */
    public function setRenteraddress2($renteraddress2 = null)
    {
        $this->renteraddress2 = $renteraddress2;

        return $this;
    }

    /**
     * Get renteraddress2.
     *
     * @return string|null
     */
    public function getRenteraddress2()
    {
        return $this->renteraddress2;
    }

    /**
     * Set renteraddress3.
     *
     * @param string|null $renteraddress3
     *
     * @return Carrentavis
     */
    public function setRenteraddress3($renteraddress3 = null)
    {
        $this->renteraddress3 = $renteraddress3;

        return $this;
    }

    /**
     * Get renteraddress3.
     *
     * @return string|null
     */
    public function getRenteraddress3()
    {
        return $this->renteraddress3;
    }

    /**
     * Set localcontact.
     *
     * @param string|null $localcontact
     *
     * @return Carrentavis
     */
    public function setLocalcontact($localcontact = null)
    {
        $this->localcontact = $localcontact;

        return $this;
    }

    /**
     * Get localcontact.
     *
     * @return string|null
     */
    public function getLocalcontact()
    {
        return $this->localcontact;
    }

    /**
     * Set renterbirthdate.
     *
     * @param string|null $renterbirthdate
     *
     * @return Carrentavis
     */
    public function setRenterbirthdate($renterbirthdate = null)
    {
        $this->renterbirthdate = $renterbirthdate;

        return $this;
    }

    /**
     * Get renterbirthdate.
     *
     * @return string|null
     */
    public function getRenterbirthdate()
    {
        return $this->renterbirthdate;
    }

    /**
     * Set drivinglicense.
     *
     * @param string|null $drivinglicense
     *
     * @return Carrentavis
     */
    public function setDrivinglicense($drivinglicense = null)
    {
        $this->drivinglicense = $drivinglicense;

        return $this;
    }

    /**
     * Get drivinglicense.
     *
     * @return string|null
     */
    public function getDrivinglicense()
    {
        return $this->drivinglicense;
    }

    /**
     * Set coutlocname.
     *
     * @param string|null $coutlocname
     *
     * @return Carrentavis
     */
    public function setCoutlocname($coutlocname = null)
    {
        $this->coutlocname = $coutlocname;

        return $this;
    }

    /**
     * Get coutlocname.
     *
     * @return string|null
     */
    public function getCoutlocname()
    {
        return $this->coutlocname;
    }

    /**
     * Set cinlocname.
     *
     * @param string|null $cinlocname
     *
     * @return Carrentavis
     */
    public function setCinlocname($cinlocname = null)
    {
        $this->cinlocname = $cinlocname;

        return $this;
    }

    /**
     * Get cinlocname.
     *
     * @return string|null
     */
    public function getCinlocname()
    {
        return $this->cinlocname;
    }

    /**
     * Set vehmakemoddes.
     *
     * @param string|null $vehmakemoddes
     *
     * @return Carrentavis
     */
    public function setVehmakemoddes($vehmakemoddes = null)
    {
        $this->vehmakemoddes = $vehmakemoddes;

        return $this;
    }

    /**
     * Get vehmakemoddes.
     *
     * @return string|null
     */
    public function getVehmakemoddes()
    {
        return $this->vehmakemoddes;
    }

    /**
     * Set vehlicnum.
     *
     * @param string $vehlicnum
     *
     * @return Carrentavis
     */
    public function setVehlicnum($vehlicnum)
    {
        $this->vehlicnum = $vehlicnum;

        return $this;
    }

    /**
     * Get vehlicnum.
     *
     * @return string
     */
    public function getVehlicnum()
    {
        return $this->vehlicnum;
    }

    /**
     * Set numaut.
     *
     * @param string|null $numaut
     *
     * @return Carrentavis
     */
    public function setNumaut($numaut = null)
    {
        $this->numaut = $numaut;

        return $this;
    }

    /**
     * Get numaut.
     *
     * @return string|null
     */
    public function getNumaut()
    {
        return $this->numaut;
    }

    /**
     * Set fltvehid.
     *
     * @param string|null $fltvehid
     *
     * @return Carrentavis
     */
    public function setFltvehid($fltvehid = null)
    {
        $this->fltvehid = $fltvehid;

        return $this;
    }

    /**
     * Get fltvehid.
     *
     * @return string|null
     */
    public function getFltvehid()
    {
        return $this->fltvehid;
    }

    /**
     * Set vehgrprented.
     *
     * @param string|null $vehgrprented
     *
     * @return Carrentavis
     */
    public function setVehgrprented($vehgrprented = null)
    {
        $this->vehgrprented = $vehgrprented;

        return $this;
    }

    /**
     * Get vehgrprented.
     *
     * @return string|null
     */
    public function getVehgrprented()
    {
        return $this->vehgrprented;
    }

    /**
     * Set deslgasind.
     *
     * @param string|null $deslgasind
     *
     * @return Carrentavis
     */
    public function setDeslgasind($deslgasind = null)
    {
        $this->deslgasind = $deslgasind;

        return $this;
    }

    /**
     * Get deslgasind.
     *
     * @return string|null
     */
    public function getDeslgasind()
    {
        return $this->deslgasind;
    }

    /**
     * Set countfuellevel.
     *
     * @param string|null $countfuellevel
     *
     * @return Carrentavis
     */
    public function setCountfuellevel($countfuellevel = null)
    {
        $this->countfuellevel = $countfuellevel;

        return $this;
    }

    /**
     * Get countfuellevel.
     *
     * @return string|null
     */
    public function getCountfuellevel()
    {
        return $this->countfuellevel;
    }

    /**
     * Set countdistcount.
     *
     * @param string|null $countdistcount
     *
     * @return Carrentavis
     */
    public function setCountdistcount($countdistcount = null)
    {
        $this->countdistcount = $countdistcount;

        return $this;
    }

    /**
     * Get countdistcount.
     *
     * @return string|null
     */
    public function getCountdistcount()
    {
        return $this->countdistcount;
    }

    /**
     * Set cindistcount.
     *
     * @param string|null $cindistcount
     *
     * @return Carrentavis
     */
    public function setCindistcount($cindistcount = null)
    {
        $this->cindistcount = $cindistcount;

        return $this;
    }

    /**
     * Get cindistcount.
     *
     * @return string|null
     */
    public function getCindistcount()
    {
        return $this->cindistcount;
    }

    /**
     * Set coutdatepluscouttime.
     *
     * @param \DateTime $coutdatepluscouttime
     *
     * @return Carrentavis
     */
    public function setCoutdatepluscouttime($coutdatepluscouttime)
    {
        $this->coutdatepluscouttime = $coutdatepluscouttime;

        return $this;
    }

    /**
     * Get coutdatepluscouttime.
     *
     * @return \DateTime
     */
    public function getCoutdatepluscouttime()
    {
        return $this->coutdatepluscouttime;
    }

    /**
     * Set cindatepluscintime.
     *
     * @param \DateTime|null $cindatepluscintime
     *
     * @return Carrentavis
     */
    public function setCindatepluscintime($cindatepluscintime = null)
    {
        $this->cindatepluscintime = $cindatepluscintime;

        return $this;
    }

    /**
     * Get cindatepluscintime.
     *
     * @return \DateTime|null
     */
    public function getCindatepluscintime()
    {
        return $this->cindatepluscintime;
    }

    /**
     * Set rentallength.
     *
     * @param string|null $rentallength
     *
     * @return Carrentavis
     */
    public function setRentallength($rentallength = null)
    {
        $this->rentallength = $rentallength;

        return $this;
    }

    /**
     * Get rentallength.
     *
     * @return string|null
     */
    public function getRentallength()
    {
        return $this->rentallength;
    }

    /**
     * Set billeddays.
     *
     * @param int|null $billeddays
     *
     * @return Carrentavis
     */
    public function setBilleddays($billeddays = null)
    {
        $this->billeddays = $billeddays;

        return $this;
    }

    /**
     * Get billeddays.
     *
     * @return int|null
     */
    public function getBilleddays()
    {
        return $this->billeddays;
    }

    /**
     * Set pretaxrentalamt.
     *
     * @param string|null $pretaxrentalamt
     *
     * @return Carrentavis
     */
    public function setPretaxrentalamt($pretaxrentalamt = null)
    {
        $this->pretaxrentalamt = $pretaxrentalamt;

        return $this;
    }

    /**
     * Get pretaxrentalamt.
     *
     * @return string|null
     */
    public function getPretaxrentalamt()
    {
        return $this->pretaxrentalamt;
    }

    /**
     * Set taxpct.
     *
     * @param string|null $taxpct
     *
     * @return Carrentavis
     */
    public function setTaxpct($taxpct = null)
    {
        $this->taxpct = $taxpct;

        return $this;
    }

    /**
     * Get taxpct.
     *
     * @return string|null
     */
    public function getTaxpct()
    {
        return $this->taxpct;
    }

    /**
     * Set taxamt.
     *
     * @param string|null $taxamt
     *
     * @return Carrentavis
     */
    public function setTaxamt($taxamt = null)
    {
        $this->taxamt = $taxamt;

        return $this;
    }

    /**
     * Get taxamt.
     *
     * @return string|null
     */
    public function getTaxamt()
    {
        return $this->taxamt;
    }

    /**
     * Set totalrentalchgamt.
     *
     * @param string|null $totalrentalchgamt
     *
     * @return Carrentavis
     */
    public function setTotalrentalchgamt($totalrentalchgamt = null)
    {
        $this->totalrentalchgamt = $totalrentalchgamt;

        return $this;
    }

    /**
     * Get totalrentalchgamt.
     *
     * @return string|null
     */
    public function getTotalrentalchgamt()
    {
        return $this->totalrentalchgamt;
    }

    /**
     * Set reservationnumber.
     *
     * @param string|null $reservationnumber
     *
     * @return Carrentavis
     */
    public function setReservationnumber($reservationnumber = null)
    {
        $this->reservationnumber = $reservationnumber;

        return $this;
    }

    /**
     * Get reservationnumber.
     *
     * @return string|null
     */
    public function getReservationnumber()
    {
        return $this->reservationnumber;
    }

    /**
     * Set creditclubcd.
     *
     * @param string|null $creditclubcd
     *
     * @return Carrentavis
     */
    public function setCreditclubcd($creditclubcd = null)
    {
        $this->creditclubcd = $creditclubcd;

        return $this;
    }

    /**
     * Get creditclubcd.
     *
     * @return string|null
     */
    public function getCreditclubcd()
    {
        return $this->creditclubcd;
    }

    /**
     * Set creditcardnum.
     *
     * @param string|null $creditcardnum
     *
     * @return Carrentavis
     */
    public function setCreditcardnum($creditcardnum = null)
    {
        $this->creditcardnum = $creditcardnum;

        return $this;
    }

    /**
     * Get creditcardnum.
     *
     * @return string|null
     */
    public function getCreditcardnum()
    {
        return $this->creditcardnum;
    }

    /**
     * Set entcoutdate.
     *
     * @param string|null $entcoutdate
     *
     * @return Carrentavis
     */
    public function setEntcoutdate($entcoutdate = null)
    {
        $this->entcoutdate = $entcoutdate;

        return $this;
    }

    /**
     * Get entcoutdate.
     *
     * @return string|null
     */
    public function getEntcoutdate()
    {
        return $this->entcoutdate;
    }

    /**
     * Set coutauthnum.
     *
     * @param string|null $coutauthnum
     *
     * @return Carrentavis
     */
    public function setCoutauthnum($coutauthnum = null)
    {
        $this->coutauthnum = $coutauthnum;

        return $this;
    }

    /**
     * Get coutauthnum.
     *
     * @return string|null
     */
    public function getCoutauthnum()
    {
        return $this->coutauthnum;
    }

    /**
     * Set wizarddate.
     *
     * @param string|null $wizarddate
     *
     * @return Carrentavis
     */
    public function setWizarddate($wizarddate = null)
    {
        $this->wizarddate = $wizarddate;

        return $this;
    }

    /**
     * Get wizarddate.
     *
     * @return string|null
     */
    public function getWizarddate()
    {
        return $this->wizarddate;
    }

    /**
     * Set abiertocerrado.
     *
     * @param string|null $abiertocerrado
     *
     * @return Carrentavis
     */
    public function setAbiertocerrado($abiertocerrado = null)
    {
        $this->abiertocerrado = $abiertocerrado;

        return $this;
    }

    /**
     * Get abiertocerrado.
     *
     * @return string|null
     */
    public function getAbiertocerrado()
    {
        return $this->abiertocerrado;
    }

    /**
     * Set vehexchcount.
     *
     * @param string|null $vehexchcount
     *
     * @return Carrentavis
     */
    public function setVehexchcount($vehexchcount = null)
    {
        $this->vehexchcount = $vehexchcount;

        return $this;
    }

    /**
     * Get vehexchcount.
     *
     * @return string|null
     */
    public function getVehexchcount()
    {
        return $this->vehexchcount;
    }

    /**
     * Set costcontrolnumber.
     *
     * @param string|null $costcontrolnumber
     *
     * @return Carrentavis
     */
    public function setCostcontrolnumber($costcontrolnumber = null)
    {
        $this->costcontrolnumber = $costcontrolnumber;

        return $this;
    }

    /**
     * Get costcontrolnumber.
     *
     * @return string|null
     */
    public function getCostcontrolnumber()
    {
        return $this->costcontrolnumber;
    }

    /**
     * Set remarks.
     *
     * @param string|null $remarks
     *
     * @return Carrentavis
     */
    public function setRemarks($remarks = null)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks.
     *
     * @return string|null
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set corpcode.
     *
     * @param string|null $corpcode
     *
     * @return Carrentavis
     */
    public function setCorpcode($corpcode = null)
    {
        $this->corpcode = $corpcode;

        return $this;
    }

    /**
     * Get corpcode.
     *
     * @return string|null
     */
    public function getCorpcode()
    {
        return $this->corpcode;
    }
}
