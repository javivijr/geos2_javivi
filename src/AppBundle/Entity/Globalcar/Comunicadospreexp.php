<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comunicadospreexp
 *
 * @ORM\Table(name="comunicadospreexp", indexes={@ORM\Index(name="idpreExpedientes_CPE_FK_idx", columns={"idpreExpedientes"})})
 * @ORM\Entity
 */
class Comunicadospreexp
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcomunicadosPreExp", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcomunicadospreexp;

    /**
     * @var string
     *
     * @ORM\Column(name="publico", type="string", length=0, nullable=false)
     */
    private $publico;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=30, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comunicado", type="text", length=65535, nullable=true)
     */
    private $comunicado;

    /**
     * @var \Preexpedientes
     *
     * @ORM\ManyToOne(targetEntity="Preexpedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpreExpedientes", referencedColumnName="idpreExpedientes")
     * })
     */
    private $idpreexpedientes;



    /**
     * Get idcomunicadospreexp.
     *
     * @return int
     */
    public function getIdcomunicadospreexp()
    {
        return $this->idcomunicadospreexp;
    }

    /**
     * Set publico.
     *
     * @param string $publico
     *
     * @return Comunicadospreexp
     */
    public function setPublico($publico)
    {
        $this->publico = $publico;

        return $this;
    }

    /**
     * Get publico.
     *
     * @return string
     */
    public function getPublico()
    {
        return $this->publico;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Comunicadospreexp
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario.
     *
     * @param string|null $usuario
     *
     * @return Comunicadospreexp
     */
    public function setUsuario($usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario.
     *
     * @return string|null
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comunicado.
     *
     * @param string|null $comunicado
     *
     * @return Comunicadospreexp
     */
    public function setComunicado($comunicado = null)
    {
        $this->comunicado = $comunicado;

        return $this;
    }

    /**
     * Get comunicado.
     *
     * @return string|null
     */
    public function getComunicado()
    {
        return $this->comunicado;
    }

    /**
     * Set idpreexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Preexpedientes|null $idpreexpedientes
     *
     * @return Comunicadospreexp
     */
    public function setIdpreexpedientes(\AppBundle\Entity\Globalcar\Preexpedientes $idpreexpedientes = null)
    {
        $this->idpreexpedientes = $idpreexpedientes;

        return $this;
    }

    /**
     * Get idpreexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Preexpedientes|null
     */
    public function getIdpreexpedientes()
    {
        return $this->idpreexpedientes;
    }
}
