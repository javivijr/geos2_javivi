<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contratos
 *
 * @ORM\Table(name="contratos", indexes={@ORM\Index(name="idtitulares_CON_FK_idx", columns={"idtitulares"}), @ORM\Index(name="cif_CON_idx", columns={"cif"}), @ORM\Index(name="nombrecontrato_idx", columns={"nombre"}), @ORM\Index(name="aliascontrato_idx", columns={"alias"}), @ORM\Index(name="emailcontrato_idx", columns={"emailContacto"}), @ORM\Index(name="codigoArval", columns={"codigoArval"}), @ORM\Index(name="codigoOtros", columns={"codigoOtros"})})
 * @ORM\Entity
 */
class Contratos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcontratos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontratos;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=0, nullable=false)
     */
    private $origen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cif", type="string", length=20, nullable=true)
     */
    private $cif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="alias", type="string", length=45, nullable=true)
     */
    private $alias;

    /**
     * @var string|null
     *
     * @ORM\Column(name="domicilio", type="string", length=100, nullable=true)
     */
    private $domicilio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cp", type="string", length=10, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poblacion", type="string", length=60, nullable=true)
     */
    private $poblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais", type="string", length=20, nullable=true)
     */
    private $pais;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombreContacto", type="string", length=100, nullable=true)
     */
    private $nombrecontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telContacto", type="string", length=13, nullable=true)
     */
    private $telcontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="faxContacto", type="string", length=13, nullable=true)
     */
    private $faxcontacto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailContacto", type="string", length=100, nullable=true)
     */
    private $emailcontacto;

    /**
     * @var string
     *
     * @ORM\Column(name="noEnvio", type="string", length=0, nullable=false)
     */
    private $noenvio;

    /**
     * @var string
     *
     * @ORM\Column(name="autoIdnt", type="string", length=0, nullable=false)
     */
    private $autoidnt;

    /**
     * @var string
     *
     * @ORM\Column(name="condBase", type="string", length=0, nullable=false)
     */
    private $condbase;

    /**
     * @var string
     *
     * @ORM\Column(name="sendMail", type="string", length=0, nullable=false)
     */
    private $sendmail;

    /**
     * @var string
     *
     * @ORM\Column(name="sendFax", type="string", length=0, nullable=false)
     */
    private $sendfax;

    /**
     * @var string
     *
     * @ORM\Column(name="copiaMultaMail", type="string", length=0, nullable=false)
     */
    private $copiamultamail;

    /**
     * @var string
     *
     * @ORM\Column(name="copiaMultaFax", type="string", length=0, nullable=false)
     */
    private $copiamultafax;

    /**
     * @var string
     *
     * @ORM\Column(name="telematico", type="string", length=0, nullable=false)
     */
    private $telematico;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numComunic", type="integer", nullable=true, options={"default"="2"})
     */
    private $numcomunic = '2';

    /**
     * @var int|null
     *
     * @ORM\Column(name="horasC", type="integer", nullable=true, options={"default"="48"})
     */
    private $horasc = '48';

    /**
     * @var int|null
     *
     * @ORM\Column(name="horasSC", type="integer", nullable=true, options={"default"="24"})
     */
    private $horassc = '24';

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipoflota", type="string", length=7, nullable=true)
     */
    private $tipoflota;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emailNotificaciones", type="string", length=100, nullable=true)
     */
    private $emailnotificaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="noMailIdentificaciones", type="string", length=0, nullable=false)
     */
    private $nomailidentificaciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoArval", type="string", length=20, nullable=true)
     */
    private $codigoarval;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigoOtros", type="integer", nullable=true)
     */
    private $codigootros;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correocopiacomunicaciones", type="string", length=100, nullable=true)
     */
    private $correocopiacomunicaciones;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idcontratos.
     *
     * @return int
     */
    public function getIdcontratos()
    {
        return $this->idcontratos;
    }

    /**
     * Set origen.
     *
     * @param string $origen
     *
     * @return Contratos
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set cif.
     *
     * @param string|null $cif
     *
     * @return Contratos
     */
    public function setCif($cif = null)
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * Get cif.
     *
     * @return string|null
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set nombre.
     *
     * @param string|null $nombre
     *
     * @return Contratos
     */
    public function setNombre($nombre = null)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string|null
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set alias.
     *
     * @param string|null $alias
     *
     * @return Contratos
     */
    public function setAlias($alias = null)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias.
     *
     * @return string|null
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set domicilio.
     *
     * @param string|null $domicilio
     *
     * @return Contratos
     */
    public function setDomicilio($domicilio = null)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio.
     *
     * @return string|null
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return Contratos
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set poblacion.
     *
     * @param string|null $poblacion
     *
     * @return Contratos
     */
    public function setPoblacion($poblacion = null)
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * Get poblacion.
     *
     * @return string|null
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * Set pais.
     *
     * @param string|null $pais
     *
     * @return Contratos
     */
    public function setPais($pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return string|null
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set nombrecontacto.
     *
     * @param string|null $nombrecontacto
     *
     * @return Contratos
     */
    public function setNombrecontacto($nombrecontacto = null)
    {
        $this->nombrecontacto = $nombrecontacto;

        return $this;
    }

    /**
     * Get nombrecontacto.
     *
     * @return string|null
     */
    public function getNombrecontacto()
    {
        return $this->nombrecontacto;
    }

    /**
     * Set telcontacto.
     *
     * @param string|null $telcontacto
     *
     * @return Contratos
     */
    public function setTelcontacto($telcontacto = null)
    {
        $this->telcontacto = $telcontacto;

        return $this;
    }

    /**
     * Get telcontacto.
     *
     * @return string|null
     */
    public function getTelcontacto()
    {
        return $this->telcontacto;
    }

    /**
     * Set faxcontacto.
     *
     * @param string|null $faxcontacto
     *
     * @return Contratos
     */
    public function setFaxcontacto($faxcontacto = null)
    {
        $this->faxcontacto = $faxcontacto;

        return $this;
    }

    /**
     * Get faxcontacto.
     *
     * @return string|null
     */
    public function getFaxcontacto()
    {
        return $this->faxcontacto;
    }

    /**
     * Set emailcontacto.
     *
     * @param string|null $emailcontacto
     *
     * @return Contratos
     */
    public function setEmailcontacto($emailcontacto = null)
    {
        $this->emailcontacto = $emailcontacto;

        return $this;
    }

    /**
     * Get emailcontacto.
     *
     * @return string|null
     */
    public function getEmailcontacto()
    {
        return $this->emailcontacto;
    }

    /**
     * Set noenvio.
     *
     * @param string $noenvio
     *
     * @return Contratos
     */
    public function setNoenvio($noenvio)
    {
        $this->noenvio = $noenvio;

        return $this;
    }

    /**
     * Get noenvio.
     *
     * @return string
     */
    public function getNoenvio()
    {
        return $this->noenvio;
    }

    /**
     * Set autoidnt.
     *
     * @param string $autoidnt
     *
     * @return Contratos
     */
    public function setAutoidnt($autoidnt)
    {
        $this->autoidnt = $autoidnt;

        return $this;
    }

    /**
     * Get autoidnt.
     *
     * @return string
     */
    public function getAutoidnt()
    {
        return $this->autoidnt;
    }

    /**
     * Set condbase.
     *
     * @param string $condbase
     *
     * @return Contratos
     */
    public function setCondbase($condbase)
    {
        $this->condbase = $condbase;

        return $this;
    }

    /**
     * Get condbase.
     *
     * @return string
     */
    public function getCondbase()
    {
        return $this->condbase;
    }

    /**
     * Set sendmail.
     *
     * @param string $sendmail
     *
     * @return Contratos
     */
    public function setSendmail($sendmail)
    {
        $this->sendmail = $sendmail;

        return $this;
    }

    /**
     * Get sendmail.
     *
     * @return string
     */
    public function getSendmail()
    {
        return $this->sendmail;
    }

    /**
     * Set sendfax.
     *
     * @param string $sendfax
     *
     * @return Contratos
     */
    public function setSendfax($sendfax)
    {
        $this->sendfax = $sendfax;

        return $this;
    }

    /**
     * Get sendfax.
     *
     * @return string
     */
    public function getSendfax()
    {
        return $this->sendfax;
    }

    /**
     * Set copiamultamail.
     *
     * @param string $copiamultamail
     *
     * @return Contratos
     */
    public function setCopiamultamail($copiamultamail)
    {
        $this->copiamultamail = $copiamultamail;

        return $this;
    }

    /**
     * Get copiamultamail.
     *
     * @return string
     */
    public function getCopiamultamail()
    {
        return $this->copiamultamail;
    }

    /**
     * Set copiamultafax.
     *
     * @param string $copiamultafax
     *
     * @return Contratos
     */
    public function setCopiamultafax($copiamultafax)
    {
        $this->copiamultafax = $copiamultafax;

        return $this;
    }

    /**
     * Get copiamultafax.
     *
     * @return string
     */
    public function getCopiamultafax()
    {
        return $this->copiamultafax;
    }

    /**
     * Set telematico.
     *
     * @param string $telematico
     *
     * @return Contratos
     */
    public function setTelematico($telematico)
    {
        $this->telematico = $telematico;

        return $this;
    }

    /**
     * Get telematico.
     *
     * @return string
     */
    public function getTelematico()
    {
        return $this->telematico;
    }

    /**
     * Set numcomunic.
     *
     * @param int|null $numcomunic
     *
     * @return Contratos
     */
    public function setNumcomunic($numcomunic = null)
    {
        $this->numcomunic = $numcomunic;

        return $this;
    }

    /**
     * Get numcomunic.
     *
     * @return int|null
     */
    public function getNumcomunic()
    {
        return $this->numcomunic;
    }

    /**
     * Set horasc.
     *
     * @param int|null $horasc
     *
     * @return Contratos
     */
    public function setHorasc($horasc = null)
    {
        $this->horasc = $horasc;

        return $this;
    }

    /**
     * Get horasc.
     *
     * @return int|null
     */
    public function getHorasc()
    {
        return $this->horasc;
    }

    /**
     * Set horassc.
     *
     * @param int|null $horassc
     *
     * @return Contratos
     */
    public function setHorassc($horassc = null)
    {
        $this->horassc = $horassc;

        return $this;
    }

    /**
     * Get horassc.
     *
     * @return int|null
     */
    public function getHorassc()
    {
        return $this->horassc;
    }

    /**
     * Set tipoflota.
     *
     * @param string|null $tipoflota
     *
     * @return Contratos
     */
    public function setTipoflota($tipoflota = null)
    {
        $this->tipoflota = $tipoflota;

        return $this;
    }

    /**
     * Get tipoflota.
     *
     * @return string|null
     */
    public function getTipoflota()
    {
        return $this->tipoflota;
    }

    /**
     * Set emailnotificaciones.
     *
     * @param string|null $emailnotificaciones
     *
     * @return Contratos
     */
    public function setEmailnotificaciones($emailnotificaciones = null)
    {
        $this->emailnotificaciones = $emailnotificaciones;

        return $this;
    }

    /**
     * Get emailnotificaciones.
     *
     * @return string|null
     */
    public function getEmailnotificaciones()
    {
        return $this->emailnotificaciones;
    }

    /**
     * Set nomailidentificaciones.
     *
     * @param string $nomailidentificaciones
     *
     * @return Contratos
     */
    public function setNomailidentificaciones($nomailidentificaciones)
    {
        $this->nomailidentificaciones = $nomailidentificaciones;

        return $this;
    }

    /**
     * Get nomailidentificaciones.
     *
     * @return string
     */
    public function getNomailidentificaciones()
    {
        return $this->nomailidentificaciones;
    }

    /**
     * Set codigoarval.
     *
     * @param string|null $codigoarval
     *
     * @return Contratos
     */
    public function setCodigoarval($codigoarval = null)
    {
        $this->codigoarval = $codigoarval;

        return $this;
    }

    /**
     * Get codigoarval.
     *
     * @return string|null
     */
    public function getCodigoarval()
    {
        return $this->codigoarval;
    }

    /**
     * Set codigootros.
     *
     * @param int|null $codigootros
     *
     * @return Contratos
     */
    public function setCodigootros($codigootros = null)
    {
        $this->codigootros = $codigootros;

        return $this;
    }

    /**
     * Get codigootros.
     *
     * @return int|null
     */
    public function getCodigootros()
    {
        return $this->codigootros;
    }

    /**
     * Set correocopiacomunicaciones.
     *
     * @param string|null $correocopiacomunicaciones
     *
     * @return Contratos
     */
    public function setCorreocopiacomunicaciones($correocopiacomunicaciones = null)
    {
        $this->correocopiacomunicaciones = $correocopiacomunicaciones;

        return $this;
    }

    /**
     * Get correocopiacomunicaciones.
     *
     * @return string|null
     */
    public function getCorreocopiacomunicaciones()
    {
        return $this->correocopiacomunicaciones;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Contratos
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
