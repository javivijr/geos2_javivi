<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrenthertzportugalexpediente
 *
 * @ORM\Table(name="carrenthertzportugalexpediente", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_88B3B85C59A462DE", columns={"idexpedientes"})}, indexes={@ORM\Index(name="IDX_88B3B85CCF3154", columns={"idcarrenthertzportugal"})})
 * @ORM\Entity
 */
class Carrenthertzportugalexpediente
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrenthertzportugalexpediente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrenthertzportugalexpediente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=1000, nullable=true)
     */
    private $observaciones;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Carrenthertzportugal
     *
     * @ORM\ManyToOne(targetEntity="Carrenthertzportugal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcarrenthertzportugal", referencedColumnName="idcarrenthertzportugal")
     * })
     */
    private $idcarrenthertzportugal;



    /**
     * Get idcarrenthertzportugalexpediente.
     *
     * @return int
     */
    public function getIdcarrenthertzportugalexpediente()
    {
        return $this->idcarrenthertzportugalexpediente;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Carrenthertzportugalexpediente
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Carrenthertzportugalexpediente
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Carrenthertzportugalexpediente
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idcarrenthertzportugal.
     *
     * @param \AppBundle\Entity\Globalcar\Carrenthertzportugal|null $idcarrenthertzportugal
     *
     * @return Carrenthertzportugalexpediente
     */
    public function setIdcarrenthertzportugal(\AppBundle\Entity\Globalcar\Carrenthertzportugal $idcarrenthertzportugal = null)
    {
        $this->idcarrenthertzportugal = $idcarrenthertzportugal;

        return $this;
    }

    /**
     * Get idcarrenthertzportugal.
     *
     * @return \AppBundle\Entity\Globalcar\Carrenthertzportugal|null
     */
    public function getIdcarrenthertzportugal()
    {
        return $this->idcarrenthertzportugal;
    }
}
