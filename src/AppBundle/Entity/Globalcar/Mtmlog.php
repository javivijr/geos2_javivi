<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmlog
 *
 * @ORM\Table(name="mtmlog", indexes={@ORM\Index(name="idusuariosmtm_ML_FK_idx", columns={"idusuariomtm"})})
 * @ORM\Entity
 */
class Mtmlog
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmlog", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmlog;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=false)
     */
    private $ip;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariomtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariomtm;



    /**
     * Get idmtmlog.
     *
     * @return int
     */
    public function getIdmtmlog()
    {
        return $this->idmtmlog;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Mtmlog
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return Mtmlog
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set idusuariomtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariomtm
     *
     * @return Mtmlog
     */
    public function setIdusuariomtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariomtm = null)
    {
        $this->idusuariomtm = $idusuariomtm;

        return $this;
    }

    /**
     * Get idusuariomtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariomtm()
    {
        return $this->idusuariomtm;
    }
}
