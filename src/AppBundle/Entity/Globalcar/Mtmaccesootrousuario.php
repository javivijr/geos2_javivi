<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmaccesootrousuario
 *
 * @ORM\Table(name="mtmaccesootrousuario", indexes={@ORM\Index(name="idusuariosmtm_MAOU_FK_idx", columns={"idusuariosmtm"}), @ORM\Index(name="idotrousuariosmtm_MAOU_FK_idx", columns={"idotrousuariosmtm"})})
 * @ORM\Entity
 */
class Mtmaccesootrousuario
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmaccesootrousuario", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmaccesootrousuario;

    /**
     * @var int
     *
     * @ORM\Column(name="perfil", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $perfil;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idotrousuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idotrousuariosmtm;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmaccesootrousuario.
     *
     * @return int
     */
    public function getIdmtmaccesootrousuario()
    {
        return $this->idmtmaccesootrousuario;
    }

    /**
     * Set perfil.
     *
     * @param int $perfil
     *
     * @return Mtmaccesootrousuario
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil.
     *
     * @return int
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set idotrousuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idotrousuariosmtm
     *
     * @return Mtmaccesootrousuario
     */
    public function setIdotrousuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idotrousuariosmtm = null)
    {
        $this->idotrousuariosmtm = $idotrousuariosmtm;

        return $this;
    }

    /**
     * Get idotrousuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdotrousuariosmtm()
    {
        return $this->idotrousuariosmtm;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmaccesootrousuario
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
