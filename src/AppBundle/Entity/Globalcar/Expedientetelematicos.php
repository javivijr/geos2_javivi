<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedientetelematicos
 *
 * @ORM\Table(name="expedientetelematicos", indexes={@ORM\Index(name="idexptelemat_ET_FK_idx", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Expedientetelematicos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idexpedientetelematicos", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idexpedientetelematicos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificador", type="string", length=20, nullable=true)
     */
    private $identificador;

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $orden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="miscelanea", type="string", length=300, nullable=true)
     */
    private $miscelanea;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idexpedientetelematicos.
     *
     * @return int
     */
    public function getIdexpedientetelematicos()
    {
        return $this->idexpedientetelematicos;
    }

    /**
     * Set identificador.
     *
     * @param string|null $identificador
     *
     * @return Expedientetelematicos
     */
    public function setIdentificador($identificador = null)
    {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador.
     *
     * @return string|null
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }

    /**
     * Set orden.
     *
     * @param int|null $orden
     *
     * @return Expedientetelematicos
     */
    public function setOrden($orden = null)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden.
     *
     * @return int|null
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set miscelanea.
     *
     * @param string|null $miscelanea
     *
     * @return Expedientetelematicos
     */
    public function setMiscelanea($miscelanea = null)
    {
        $this->miscelanea = $miscelanea;

        return $this;
    }

    /**
     * Get miscelanea.
     *
     * @return string|null
     */
    public function getMiscelanea()
    {
        return $this->miscelanea;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Expedientetelematicos
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
