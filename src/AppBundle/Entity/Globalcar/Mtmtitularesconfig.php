<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmtitularesconfig
 *
 * @ORM\Table(name="mtmtitularesconfig", indexes={@ORM\Index(name="idtitulares_MTC_FK_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Mtmtitularesconfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmtitularesconfig", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmtitularesconfig;

    /**
     * @var string
     *
     * @ORM\Column(name="legalconductor", type="string", length=0, nullable=false)
     */
    private $legalconductor;

    /**
     * @var string
     *
     * @ORM\Column(name="areaconductor", type="string", length=0, nullable=false)
     */
    private $areaconductor;

    /**
     * @var string
     *
     * @ORM\Column(name="tipolimitacion", type="string", length=0, nullable=false)
     */
    private $tipolimitacion;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer", nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="pagoconductor", type="string", length=0, nullable=false)
     */
    private $pagoconductor;

    /**
     * @var string
     *
     * @ORM\Column(name="pago", type="string", length=0, nullable=false)
     */
    private $pago;

    /**
     * @var string
     *
     * @ORM\Column(name="comisionpago", type="decimal", precision=4, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $comisionpago = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="contratoobligatorio", type="string", length=0, nullable=false)
     */
    private $contratoobligatorio;

    /**
     * @var string
     *
     * @ORM\Column(name="compartirautocbase", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $compartirautocbase = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="gestionpuntos", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $gestionpuntos = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="descuentocomisionpago", type="decimal", precision=4, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $descuentocomisionpago = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="emailpeticiondatos", type="string", length=0, nullable=false, options={"default"="s"})
     */
    private $emailpeticiondatos = 's';

    /**
     * @var string
     *
     * @ORM\Column(name="segundoaviso", type="string", length=0, nullable=false, options={"default"="ec"})
     */
    private $segundoaviso = 'ec';

    /**
     * @var string
     *
     * @ORM\Column(name="esdemo", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $esdemo = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="informes", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $informes = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="emailresumen", type="string", length=0, nullable=false, options={"default"="n"})
     */
    private $emailresumen = 'n';

    /**
     * @var string
     *
     * @ORM\Column(name="pagoarrendatario", type="string", length=0, nullable=false, options={"default"="s"})
     */
    private $pagoarrendatario = 's';

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;

    /**
     * @var string
     *
     * @ORM\Column(name="apiext", type="string", nullable=true)
     */
    private $apiext = null;



    /**
     * Get idmtmtitularesconfig.
     *
     * @return int
     */
    public function getIdmtmtitularesconfig()
    {
        return $this->idmtmtitularesconfig;
    }

    /**
     * Set legalconductor.
     *
     * @param string $legalconductor
     *
     * @return Mtmtitularesconfig
     */
    public function setLegalconductor($legalconductor)
    {
        $this->legalconductor = $legalconductor;

        return $this;
    }

    /**
     * Get legalconductor.
     *
     * @return string
     */
    public function getLegalconductor()
    {
        return $this->legalconductor;
    }

    /**
     * Set areaconductor.
     *
     * @param string $areaconductor
     *
     * @return Mtmtitularesconfig
     */
    public function setAreaconductor($areaconductor)
    {
        $this->areaconductor = $areaconductor;

        return $this;
    }

    /**
     * Get areaconductor.
     *
     * @return string
     */
    public function getAreaconductor()
    {
        return $this->areaconductor;
    }

    /**
     * Set tipolimitacion.
     *
     * @param string $tipolimitacion
     *
     * @return Mtmtitularesconfig
     */
    public function setTipolimitacion($tipolimitacion)
    {
        $this->tipolimitacion = $tipolimitacion;

        return $this;
    }

    /**
     * Get tipolimitacion.
     *
     * @return string
     */
    public function getTipolimitacion()
    {
        return $this->tipolimitacion;
    }

    /**
     * Set numero.
     *
     * @param int $numero
     *
     * @return Mtmtitularesconfig
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero.
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set pagoconductor.
     *
     * @param string $pagoconductor
     *
     * @return Mtmtitularesconfig
     */
    public function setPagoconductor($pagoconductor)
    {
        $this->pagoconductor = $pagoconductor;

        return $this;
    }

    /**
     * Get pagoconductor.
     *
     * @return string
     */
    public function getPagoconductor()
    {
        return $this->pagoconductor;
    }

    /**
     * Set pago.
     *
     * @param string $pago
     *
     * @return Mtmtitularesconfig
     */
    public function setPago($pago)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago.
     *
     * @return string
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set comisionpago.
     *
     * @param string $comisionpago
     *
     * @return Mtmtitularesconfig
     */
    public function setComisionpago($comisionpago)
    {
        $this->comisionpago = $comisionpago;

        return $this;
    }

    /**
     * Get comisionpago.
     *
     * @return string
     */
    public function getComisionpago()
    {
        return $this->comisionpago;
    }

    /**
     * Set contratoobligatorio.
     *
     * @param string $contratoobligatorio
     *
     * @return Mtmtitularesconfig
     */
    public function setContratoobligatorio($contratoobligatorio)
    {
        $this->contratoobligatorio = $contratoobligatorio;

        return $this;
    }

    /**
     * Get contratoobligatorio.
     *
     * @return string
     */
    public function getContratoobligatorio()
    {
        return $this->contratoobligatorio;
    }

    /**
     * Set compartirautocbase.
     *
     * @param string $compartirautocbase
     *
     * @return Mtmtitularesconfig
     */
    public function setCompartirautocbase($compartirautocbase)
    {
        $this->compartirautocbase = $compartirautocbase;

        return $this;
    }

    /**
     * Get compartirautocbase.
     *
     * @return string
     */
    public function getCompartirautocbase()
    {
        return $this->compartirautocbase;
    }

    /**
     * Set gestionpuntos.
     *
     * @param string $gestionpuntos
     *
     * @return Mtmtitularesconfig
     */
    public function setGestionpuntos($gestionpuntos)
    {
        $this->gestionpuntos = $gestionpuntos;

        return $this;
    }

    /**
     * Get gestionpuntos.
     *
     * @return string
     */
    public function getGestionpuntos()
    {
        return $this->gestionpuntos;
    }

    /**
     * Set descuentocomisionpago.
     *
     * @param string $descuentocomisionpago
     *
     * @return Mtmtitularesconfig
     */
    public function setDescuentocomisionpago($descuentocomisionpago)
    {
        $this->descuentocomisionpago = $descuentocomisionpago;

        return $this;
    }

    /**
     * Get descuentocomisionpago.
     *
     * @return string
     */
    public function getDescuentocomisionpago()
    {
        return $this->descuentocomisionpago;
    }

    /**
     * Set emailpeticiondatos.
     *
     * @param string $emailpeticiondatos
     *
     * @return Mtmtitularesconfig
     */
    public function setEmailpeticiondatos($emailpeticiondatos)
    {
        $this->emailpeticiondatos = $emailpeticiondatos;

        return $this;
    }

    /**
     * Get emailpeticiondatos.
     *
     * @return string
     */
    public function getEmailpeticiondatos()
    {
        return $this->emailpeticiondatos;
    }

    /**
     * Set segundoaviso.
     *
     * @param string $segundoaviso
     *
     * @return Mtmtitularesconfig
     */
    public function setSegundoaviso($segundoaviso)
    {
        $this->segundoaviso = $segundoaviso;

        return $this;
    }

    /**
     * Get segundoaviso.
     *
     * @return string
     */
    public function getSegundoaviso()
    {
        return $this->segundoaviso;
    }

    /**
     * Set esdemo.
     *
     * @param string $esdemo
     *
     * @return Mtmtitularesconfig
     */
    public function setEsdemo($esdemo)
    {
        $this->esdemo = $esdemo;

        return $this;
    }

    /**
     * Get esdemo.
     *
     * @return string
     */
    public function getEsdemo()
    {
        return $this->esdemo;
    }

    /**
     * Set informes.
     *
     * @param string $informes
     *
     * @return Mtmtitularesconfig
     */
    public function setInformes($informes)
    {
        $this->informes = $informes;

        return $this;
    }

    /**
     * Get informes.
     *
     * @return string
     */
    public function getInformes()
    {
        return $this->informes;
    }

    /**
     * Set emailresumen.
     *
     * @param string $emailresumen
     *
     * @return Mtmtitularesconfig
     */
    public function setEmailresumen($emailresumen)
    {
        $this->emailresumen = $emailresumen;

        return $this;
    }

    /**
     * Get emailresumen.
     *
     * @return string
     */
    public function getEmailresumen()
    {
        return $this->emailresumen;
    }

    /**
     * Set pagoarrendatario.
     *
     * @param string $pagoarrendatario
     *
     * @return Mtmtitularesconfig
     */
    public function setPagoarrendatario($pagoarrendatario)
    {
        $this->pagoarrendatario = $pagoarrendatario;

        return $this;
    }

    /**
     * Get pagoarrendatario.
     *
     * @return string
     */
    public function getPagoarrendatario()
    {
        return $this->pagoarrendatario;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmtitularesconfig
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * @return string
     */
    public function getApiext()
    {
        return $this->apiext;
    }

    /**
     * @param string $apiext
     */
    public function setApiext($apiext)
    {
        $this->apiext = $apiext;
    }
}
