<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Listacallesbarcelona
 *
 * @ORM\Table(name="listacallesbarcelona")
 * @ORM\Entity
 */
class Listacallesbarcelona
{
    /**
     * @var int
     *
     * @ORM\Column(name="idlistacallesbarcelona", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlistacallesbarcelona;

    /**
     * @var string
     *
     * @ORM\Column(name="tipovia", type="string", length=10, nullable=false)
     */
    private $tipovia;

    /**
     * @var string
     *
     * @ORM\Column(name="calle", type="string", length=300, nullable=false)
     */
    private $calle;



    /**
     * Get idlistacallesbarcelona.
     *
     * @return int
     */
    public function getIdlistacallesbarcelona()
    {
        return $this->idlistacallesbarcelona;
    }

    /**
     * Set tipovia.
     *
     * @param string $tipovia
     *
     * @return Listacallesbarcelona
     */
    public function setTipovia($tipovia)
    {
        $this->tipovia = $tipovia;

        return $this;
    }

    /**
     * Get tipovia.
     *
     * @return string
     */
    public function getTipovia()
    {
        return $this->tipovia;
    }

    /**
     * Set calle.
     *
     * @param string $calle
     *
     * @return Listacallesbarcelona
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle.
     *
     * @return string
     */
    public function getCalle()
    {
        return $this->calle;
    }
}
