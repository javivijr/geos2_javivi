<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hechos
 *
 * @ORM\Table(name="hechos", indexes={@ORM\Index(name="hecho_HEC_IDX", columns={"hecho"}), @ORM\Index(name="IDX_40894132678A09C1", columns={"idhechoscategorias"})})
 * @ORM\Entity
 */
class Hechos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhechos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhechos;

    /**
     * @var string
     *
     * @ORM\Column(name="hecho", type="string", length=1000, nullable=false)
     */
    private $hecho;

    /**
     * @var string
     *
     * @ORM\Column(name="revisarLegal", type="string", length=0, nullable=false)
     */
    private $revisarlegal;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=0, nullable=false, options={"default"="es"})
     */
    private $idioma = 'es';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecharcategorizacion", type="datetime", nullable=true)
     */
    private $fecharcategorizacion;

    /**
     * @var Hechoscategorias
     *
     * @ORM\ManyToOne(targetEntity="Hechoscategorias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idhechoscategorias", referencedColumnName="idhechoscategorias")
     * })
     */
    private $idhechoscategorias;



    /**
     * Get idhechos.
     *
     * @return int
     */
    public function getIdhechos()
    {
        return $this->idhechos;
    }

    /**
     * Set hecho.
     *
     * @param string $hecho
     *
     * @return Hechos
     */
    public function setHecho($hecho)
    {
        $this->hecho = $hecho;

        return $this;
    }

    /**
     * Get hecho.
     *
     * @return string
     */
    public function getHecho()
    {
        return $this->hecho;
    }

    /**
     * Set revisarlegal.
     *
     * @param string $revisarlegal
     *
     * @return Hechos
     */
    public function setRevisarlegal($revisarlegal)
    {
        $this->revisarlegal = $revisarlegal;

        return $this;
    }

    /**
     * Get revisarlegal.
     *
     * @return string
     */
    public function getRevisarlegal()
    {
        return $this->revisarlegal;
    }

    /**
     * Set idioma.
     *
     * @param string $idioma
     *
     * @return Hechos
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma.
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set fecharcategorizacion.
     *
     * @param \DateTime|null $fecharcategorizacion
     *
     * @return Hechos
     */
    public function setFecharcategorizacion($fecharcategorizacion = null)
    {
        $this->fecharcategorizacion = $fecharcategorizacion;

        return $this;
    }

    /**
     * Get fecharcategorizacion.
     *
     * @return \DateTime|null
     */
    public function getFecharcategorizacion()
    {
        return $this->fecharcategorizacion;
    }

    /**
     * Set idhechoscategorias.
     *
     * @param \AppBundle\Entity\Globalcar\Hechoscategorias|null $idhechoscategorias
     *
     * @return Hechos
     */
    public function setIdhechoscategorias(\AppBundle\Entity\Globalcar\Hechoscategorias $idhechoscategorias = null)
    {
        $this->idhechoscategorias = $idhechoscategorias;

        return $this;
    }

    /**
     * Get idhechoscategorias.
     *
     * @return \AppBundle\Entity\Globalcar\Hechoscategorias|null
     */
    public function getIdhechoscategorias()
    {
        return $this->idhechoscategorias;
    }
}
