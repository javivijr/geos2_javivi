<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hechoscategorias
 *
 * @ORM\Table(name="hechoscategorias")
 * @ORM\Entity
 */
class Hechoscategorias
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhechoscategorias", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhechoscategorias;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=255, nullable=false)
     */
    private $categoria;

    /**
     * @var string|null
     *
     * @ORM\Column(name="aliascategoria", type="string", length=255, nullable=true)
     */
    private $aliascategoria;

    /**
     * @var int
     *
     * @ORM\Column(name="fiabilidad", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $fiabilidad = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais", type="string", length=2, nullable=true, options={"default"="ES","fixed"=true})
     */
    private $pais = 'ES';



    /**
     * Get idhechoscategorias.
     *
     * @return int
     */
    public function getIdhechoscategorias()
    {
        return $this->idhechoscategorias;
    }

    /**
     * Set categoria.
     *
     * @param string $categoria
     *
     * @return Hechoscategorias
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria.
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set aliascategoria.
     *
     * @param string|null $aliascategoria
     *
     * @return Hechoscategorias
     */
    public function setAliascategoria($aliascategoria = null)
    {
        $this->aliascategoria = $aliascategoria;

        return $this;
    }

    /**
     * Get aliascategoria.
     *
     * @return string|null
     */
    public function getAliascategoria()
    {
        return $this->aliascategoria;
    }

    /**
     * Set fiabilidad.
     *
     * @param int $fiabilidad
     *
     * @return Hechoscategorias
     */
    public function setFiabilidad($fiabilidad)
    {
        $this->fiabilidad = $fiabilidad;

        return $this;
    }

    /**
     * Get fiabilidad.
     *
     * @return int
     */
    public function getFiabilidad()
    {
        return $this->fiabilidad;
    }

    /**
     * Set pais.
     *
     * @param string|null $pais
     *
     * @return Hechoscategorias
     */
    public function setPais($pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return string|null
     */
    public function getPais()
    {
        return $this->pais;
    }
}
