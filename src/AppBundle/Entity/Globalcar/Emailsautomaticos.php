<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Emailsautomaticos
 *
 * @ORM\Table(name="emailsautomaticos", indexes={@ORM\Index(name="idemailIndex", columns={"idemail"}), @ORM\Index(name="usuemaFK_idx", columns={"idusuarios"})})
 * @ORM\Entity
 */
class Emailsautomaticos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idemailsautomaticos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idemailsautomaticos;

    /**
     * @var string
     *
     * @ORM\Column(name="idemail", type="string", length=45, nullable=false)
     */
    private $idemail;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarios", referencedColumnName="idusuarios")
     * })
     */
    private $idusuarios;



    /**
     * Get idemailsautomaticos.
     *
     * @return int
     */
    public function getIdemailsautomaticos()
    {
        return $this->idemailsautomaticos;
    }

    /**
     * Set idemail.
     *
     * @param string $idemail
     *
     * @return Emailsautomaticos
     */
    public function setIdemail($idemail)
    {
        $this->idemail = $idemail;

        return $this;
    }

    /**
     * Get idemail.
     *
     * @return string
     */
    public function getIdemail()
    {
        return $this->idemail;
    }

    /**
     * Set idusuarios.
     *
     * @param \AppBundle\Entity\Globalcar\Usuarios|null $idusuarios
     *
     * @return Emailsautomaticos
     */
    public function setIdusuarios(\AppBundle\Entity\Globalcar\Usuarios $idusuarios = null)
    {
        $this->idusuarios = $idusuarios;

        return $this;
    }

    /**
     * Get idusuarios.
     *
     * @return \AppBundle\Entity\Globalcar\Usuarios|null
     */
    public function getIdusuarios()
    {
        return $this->idusuarios;
    }
}
