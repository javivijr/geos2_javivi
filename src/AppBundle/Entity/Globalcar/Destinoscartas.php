<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Destinoscartas
 *
 * @ORM\Table(name="destinoscartas")
 * @ORM\Entity
 */
class Destinoscartas
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddestinosCartas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddestinoscartas;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=45, nullable=false)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=4, scale=2, nullable=false)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreinforme", type="string", length=30, nullable=false)
     */
    private $nombreinforme;



    /**
     * Get iddestinoscartas.
     *
     * @return int
     */
    public function getIddestinoscartas()
    {
        return $this->iddestinoscartas;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Destinoscartas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set precio.
     *
     * @param float $precio
     *
     * @return Destinoscartas
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio.
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set nombreinforme.
     *
     * @param string $nombreinforme
     *
     * @return Destinoscartas
     */
    public function setNombreinforme($nombreinforme)
    {
        $this->nombreinforme = $nombreinforme;

        return $this;
    }

    /**
     * Get nombreinforme.
     *
     * @return string
     */
    public function getNombreinforme()
    {
        return $this->nombreinforme;
    }
}
