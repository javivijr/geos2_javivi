<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carrentavisexpediente
 *
 * @ORM\Table(name="carrentavisexpediente", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_2ED0A07F59A462DE", columns={"idexpedientes"})}, indexes={@ORM\Index(name="IDX_2ED0A07F23821C01", columns={"idcarrentavis"})})
 * @ORM\Entity
 */
class Carrentavisexpediente
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcarrentavisexpediente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcarrentavisexpediente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=1000, nullable=true)
     */
    private $observaciones;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;

    /**
     * @var \Carrentavis
     *
     * @ORM\ManyToOne(targetEntity="Carrentavis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcarrentavis", referencedColumnName="idcarrentavis")
     * })
     */
    private $idcarrentavis;



    /**
     * Get idcarrentavisexpediente.
     *
     * @return int
     */
    public function getIdcarrentavisexpediente()
    {
        return $this->idcarrentavisexpediente;
    }

    /**
     * Set observaciones.
     *
     * @param string|null $observaciones
     *
     * @return Carrentavisexpediente
     */
    public function setObservaciones($observaciones = null)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones.
     *
     * @return string|null
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Carrentavisexpediente
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Carrentavisexpediente
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }

    /**
     * Set idcarrentavis.
     *
     * @param \AppBundle\Entity\Globalcar\Carrentavis|null $idcarrentavis
     *
     * @return Carrentavisexpediente
     */
    public function setIdcarrentavis(\AppBundle\Entity\Globalcar\Carrentavis $idcarrentavis = null)
    {
        $this->idcarrentavis = $idcarrentavis;

        return $this;
    }

    /**
     * Get idcarrentavis.
     *
     * @return \AppBundle\Entity\Globalcar\Carrentavis|null
     */
    public function getIdcarrentavis()
    {
        return $this->idcarrentavis;
    }
}
