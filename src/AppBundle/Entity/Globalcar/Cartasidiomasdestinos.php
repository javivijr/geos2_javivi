<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cartasidiomasdestinos
 *
 * @ORM\Table(name="cartasidiomasdestinos")
 * @ORM\Entity
 */
class Cartasidiomasdestinos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcartasidiomasdestinos", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcartasidiomasdestinos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poblacion", type="string", length=100, nullable=true)
     */
    private $poblacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=100, nullable=true)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=100, nullable=false)
     */
    private $pais;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=3, nullable=false)
     */
    private $idioma;

    /**
     * @var int
     *
     * @ORM\Column(name="destino", type="integer", nullable=false)
     */
    private $destino;



    /**
     * Get idcartasidiomasdestinos.
     *
     * @return int
     */
    public function getIdcartasidiomasdestinos()
    {
        return $this->idcartasidiomasdestinos;
    }

    /**
     * Set poblacion.
     *
     * @param string|null $poblacion
     *
     * @return Cartasidiomasdestinos
     */
    public function setPoblacion($poblacion = null)
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    /**
     * Get poblacion.
     *
     * @return string|null
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * Set provincia.
     *
     * @param string|null $provincia
     *
     * @return Cartasidiomasdestinos
     */
    public function setProvincia($provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia.
     *
     * @return string|null
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set pais.
     *
     * @param string $pais
     *
     * @return Cartasidiomasdestinos
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais.
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set idioma.
     *
     * @param string $idioma
     *
     * @return Cartasidiomasdestinos
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma.
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set destino.
     *
     * @param int $destino
     *
     * @return Cartasidiomasdestinos
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino.
     *
     * @return int
     */
    public function getDestino()
    {
        return $this->destino;
    }
}
