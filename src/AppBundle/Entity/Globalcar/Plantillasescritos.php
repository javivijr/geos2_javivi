<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Plantillasescritos
 *
 * @ORM\Table(name="plantillasescritos", uniqueConstraints={@ORM\UniqueConstraint(name="unica_PE", columns={"idtitulares", "idlegalescritosalida", "idioma"})}, indexes={@ORM\Index(name="idtitulares_PE_FK_idx", columns={"idtitulares"}), @ORM\Index(name="idlegalescritosalida_PE_FK_idx", columns={"idlegalescritosalida"}), @ORM\Index(name="idplantillas_PE_FK_idx", columns={"idplantillas"})})
 * @ORM\Entity
 */
class Plantillasescritos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idplantillasescritos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idplantillasescritos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tiposalida", type="string", length=0, nullable=true)
     */
    private $tiposalida;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=0, nullable=false, options={"default"="ES"})
     */
    private $idioma = 'ES';

    /**
     * @var \Legalescritosalida
     *
     * @ORM\ManyToOne(targetEntity="Legalescritosalida")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idlegalescritosalida", referencedColumnName="idlegalEscritoSalida")
     * })
     */
    private $idlegalescritosalida;

    /**
     * @var \Plantillas
     *
     * @ORM\ManyToOne(targetEntity="Plantillas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idplantillas", referencedColumnName="idplantillas")
     * })
     */
    private $idplantillas;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idplantillasescritos.
     *
     * @return int
     */
    public function getIdplantillasescritos()
    {
        return $this->idplantillasescritos;
    }

    /**
     * Set tiposalida.
     *
     * @param string|null $tiposalida
     *
     * @return Plantillasescritos
     */
    public function setTiposalida($tiposalida = null)
    {
        $this->tiposalida = $tiposalida;

        return $this;
    }

    /**
     * Get tiposalida.
     *
     * @return string|null
     */
    public function getTiposalida()
    {
        return $this->tiposalida;
    }

    /**
     * Set idioma.
     *
     * @param string $idioma
     *
     * @return Plantillasescritos
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma.
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set idlegalescritosalida.
     *
     * @param \AppBundle\Entity\Globalcar\Legalescritosalida|null $idlegalescritosalida
     *
     * @return Plantillasescritos
     */
    public function setIdlegalescritosalida(\AppBundle\Entity\Globalcar\Legalescritosalida $idlegalescritosalida = null)
    {
        $this->idlegalescritosalida = $idlegalescritosalida;

        return $this;
    }

    /**
     * Get idlegalescritosalida.
     *
     * @return \AppBundle\Entity\Globalcar\Legalescritosalida|null
     */
    public function getIdlegalescritosalida()
    {
        return $this->idlegalescritosalida;
    }

    /**
     * Set idplantillas.
     *
     * @param \AppBundle\Entity\Globalcar\Plantillas|null $idplantillas
     *
     * @return Plantillasescritos
     */
    public function setIdplantillas(\AppBundle\Entity\Globalcar\Plantillas $idplantillas = null)
    {
        $this->idplantillas = $idplantillas;

        return $this;
    }

    /**
     * Get idplantillas.
     *
     * @return \AppBundle\Entity\Globalcar\Plantillas|null
     */
    public function getIdplantillas()
    {
        return $this->idplantillas;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Plantillasescritos
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
