<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vigilanciavehiculos
 *
 * @ORM\Table(name="vigilanciavehiculos", uniqueConstraints={@ORM\UniqueConstraint(name="matricula_UNIQUE", columns={"matricula"})})
 * @ORM\Entity
 */
class Vigilanciavehiculos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idvigilanciavehiculos", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idvigilanciavehiculos;

    /**
     * @var string
     *
     * @ORM\Column(name="matricula", type="string", length=10, nullable=false)
     */
    private $matricula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motivo", type="string", length=255, nullable=true)
     */
    private $motivo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechainicio", type="datetime", nullable=true)
     */
    private $fechainicio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechafin", type="datetime", nullable=true)
     */
    private $fechafin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="crearcomunicado", type="string", length=1, nullable=true, options={"default"="n","fixed"=true})
     */
    private $crearcomunicado = 'n';



    /**
     * Get idvigilanciavehiculos.
     *
     * @return int
     */
    public function getIdvigilanciavehiculos()
    {
        return $this->idvigilanciavehiculos;
    }

    /**
     * Set matricula.
     *
     * @param string $matricula
     *
     * @return Vigilanciavehiculos
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula.
     *
     * @return string
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set motivo.
     *
     * @param string|null $motivo
     *
     * @return Vigilanciavehiculos
     */
    public function setMotivo($motivo = null)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo.
     *
     * @return string|null
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Set fechainicio.
     *
     * @param \DateTime|null $fechainicio
     *
     * @return Vigilanciavehiculos
     */
    public function setFechainicio($fechainicio = null)
    {
        $this->fechainicio = $fechainicio;

        return $this;
    }

    /**
     * Get fechainicio.
     *
     * @return \DateTime|null
     */
    public function getFechainicio()
    {
        return $this->fechainicio;
    }

    /**
     * Set fechafin.
     *
     * @param \DateTime|null $fechafin
     *
     * @return Vigilanciavehiculos
     */
    public function setFechafin($fechafin = null)
    {
        $this->fechafin = $fechafin;

        return $this;
    }

    /**
     * Get fechafin.
     *
     * @return \DateTime|null
     */
    public function getFechafin()
    {
        return $this->fechafin;
    }

    /**
     * Set crearcomunicado.
     *
     * @param string|null $crearcomunicado
     *
     * @return Vigilanciavehiculos
     */
    public function setCrearcomunicado($crearcomunicado = null)
    {
        $this->crearcomunicado = $crearcomunicado;

        return $this;
    }

    /**
     * Get crearcomunicado.
     *
     * @return string|null
     */
    public function getCrearcomunicado()
    {
        return $this->crearcomunicado;
    }
}
