<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmnovedadesproveedores
 *
 * @ORM\Table(name="mtmnovedadesproveedores", indexes={@ORM\Index(name="idmtmnovedades_mnp_FK_idx", columns={"idmtmnovedades"}), @ORM\Index(name="idtitulares_mnp_FK_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Mtmnovedadesproveedores
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmnovedadesproveedores", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmnovedadesproveedores;

    /**
     * @var \Mtmnovedades
     *
     * @ORM\ManyToOne(targetEntity="Mtmnovedades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idmtmnovedades", referencedColumnName="idmtmNovedades")
     * })
     */
    private $idmtmnovedades;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;



    /**
     * Get idmtmnovedadesproveedores.
     *
     * @return int
     */
    public function getIdmtmnovedadesproveedores()
    {
        return $this->idmtmnovedadesproveedores;
    }

    /**
     * Set idmtmnovedades.
     *
     * @param \AppBundle\Entity\Globalcar\Mtmnovedades|null $idmtmnovedades
     *
     * @return Mtmnovedadesproveedores
     */
    public function setIdmtmnovedades(\AppBundle\Entity\Globalcar\Mtmnovedades $idmtmnovedades = null)
    {
        $this->idmtmnovedades = $idmtmnovedades;

        return $this;
    }

    /**
     * Get idmtmnovedades.
     *
     * @return \AppBundle\Entity\Globalcar\Mtmnovedades|null
     */
    public function getIdmtmnovedades()
    {
        return $this->idmtmnovedades;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmnovedadesproveedores
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }
}
