<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cartasfirefly
 *
 * @ORM\Table(name="cartasfirefly", indexes={@ORM\Index(name="IDX_DB2DD44359A462DE", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class Cartasfirefly
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcartasfirefly", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcartasfirefly;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaenvio", type="datetime", nullable=false)
     */
    private $fechaenvio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecharegreso", type="datetime", nullable=true)
     */
    private $fecharegreso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contrato", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $contrato;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idcartasfirefly.
     *
     * @return int
     */
    public function getIdcartasfirefly()
    {
        return $this->idcartasfirefly;
    }

    /**
     * Set fechaenvio.
     *
     * @param \DateTime $fechaenvio
     *
     * @return Cartasfirefly
     */
    public function setFechaenvio($fechaenvio)
    {
        $this->fechaenvio = $fechaenvio;

        return $this;
    }

    /**
     * Get fechaenvio.
     *
     * @return \DateTime
     */
    public function getFechaenvio()
    {
        return $this->fechaenvio;
    }

    /**
     * Set fecharegreso.
     *
     * @param \DateTime|null $fecharegreso
     *
     * @return Cartasfirefly
     */
    public function setFecharegreso($fecharegreso = null)
    {
        $this->fecharegreso = $fecharegreso;

        return $this;
    }

    /**
     * Get fecharegreso.
     *
     * @return \DateTime|null
     */
    public function getFecharegreso()
    {
        return $this->fecharegreso;
    }

    /**
     * Set contrato.
     *
     * @param string|null $contrato
     *
     * @return Cartasfirefly
     */
    public function setContrato($contrato = null)
    {
        $this->contrato = $contrato;

        return $this;
    }

    /**
     * Get contrato.
     *
     * @return string|null
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return Cartasfirefly
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
