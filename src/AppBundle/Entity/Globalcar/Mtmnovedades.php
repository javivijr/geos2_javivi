<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmnovedades
 *
 * @ORM\Table(name="mtmnovedades")
 * @ORM\Entity
 */
class Mtmnovedades
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmNovedades", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmnovedades;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo", type="string", length=200, nullable=true)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="texto", type="text", length=65535, nullable=true)
     */
    private $texto;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;



    /**
     * Get idmtmnovedades.
     *
     * @return int
     */
    public function getIdmtmnovedades()
    {
        return $this->idmtmnovedades;
    }

    /**
     * Set titulo.
     *
     * @param string|null $titulo
     *
     * @return Mtmnovedades
     */
    public function setTitulo($titulo = null)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo.
     *
     * @return string|null
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto.
     *
     * @param string|null $texto
     *
     * @return Mtmnovedades
     */
    public function setTexto($texto = null)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto.
     *
     * @return string|null
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Mtmnovedades
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
