<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmaccesoindicados
 *
 * @ORM\Table(name="mtmaccesoindicados", indexes={@ORM\Index(name="idusuariosmtm_MAI_FK_idx", columns={"idusuariosmtm"}), @ORM\Index(name="idtitulares_MAI_FK_idx", columns={"idtitulares"})})
 * @ORM\Entity
 */
class Mtmaccesoindicados
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmaccesoindicados", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmaccesoindicados;

    /**
     * @var int
     *
     * @ORM\Column(name="perfil", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $perfil;

    /**
     * @var \Titulares
     *
     * @ORM\ManyToOne(targetEntity="Titulares")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtitulares", referencedColumnName="idtitulares")
     * })
     */
    private $idtitulares;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosmtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmaccesoindicados.
     *
     * @return int
     */
    public function getIdmtmaccesoindicados()
    {
        return $this->idmtmaccesoindicados;
    }

    /**
     * Set perfil.
     *
     * @param int $perfil
     *
     * @return Mtmaccesoindicados
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil.
     *
     * @return int
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set idtitulares.
     *
     * @param \AppBundle\Entity\Globalcar\Titulares|null $idtitulares
     *
     * @return Mtmaccesoindicados
     */
    public function setIdtitulares(\AppBundle\Entity\Globalcar\Titulares $idtitulares = null)
    {
        $this->idtitulares = $idtitulares;

        return $this;
    }

    /**
     * Get idtitulares.
     *
     * @return \AppBundle\Entity\Globalcar\Titulares|null
     */
    public function getIdtitulares()
    {
        return $this->idtitulares;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmaccesoindicados
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
