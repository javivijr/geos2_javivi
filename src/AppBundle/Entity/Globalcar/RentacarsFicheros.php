<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * RentacarsFicheros
 *
 * @ORM\Table(name="rentacars_ficheros", indexes={@ORM\Index(name="rtcfich_FK_idx", columns={"idexpedientes"})})
 * @ORM\Entity
 */
class RentacarsFicheros
{
    /**
     * @var int
     *
     * @ORM\Column(name="idrentacars_ficheros", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrentacarsFicheros;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="importacion", type="datetime", nullable=true)
     */
    private $importacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="exportacion", type="datetime", nullable=true)
     */
    private $exportacion;

    /**
     * @var \Expedientes
     *
     * @ORM\ManyToOne(targetEntity="Expedientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idexpedientes", referencedColumnName="idexpedientes")
     * })
     */
    private $idexpedientes;



    /**
     * Get idrentacarsFicheros.
     *
     * @return int
     */
    public function getIdrentacarsFicheros()
    {
        return $this->idrentacarsFicheros;
    }

    /**
     * Set importacion.
     *
     * @param \DateTime|null $importacion
     *
     * @return RentacarsFicheros
     */
    public function setImportacion($importacion = null)
    {
        $this->importacion = $importacion;

        return $this;
    }

    /**
     * Get importacion.
     *
     * @return \DateTime|null
     */
    public function getImportacion()
    {
        return $this->importacion;
    }

    /**
     * Set exportacion.
     *
     * @param \DateTime|null $exportacion
     *
     * @return RentacarsFicheros
     */
    public function setExportacion($exportacion = null)
    {
        $this->exportacion = $exportacion;

        return $this;
    }

    /**
     * Get exportacion.
     *
     * @return \DateTime|null
     */
    public function getExportacion()
    {
        return $this->exportacion;
    }

    /**
     * Set idexpedientes.
     *
     * @param \AppBundle\Entity\Globalcar\Expedientes|null $idexpedientes
     *
     * @return RentacarsFicheros
     */
    public function setIdexpedientes(\AppBundle\Entity\Globalcar\Expedientes $idexpedientes = null)
    {
        $this->idexpedientes = $idexpedientes;

        return $this;
    }

    /**
     * Get idexpedientes.
     *
     * @return \AppBundle\Entity\Globalcar\Expedientes|null
     */
    public function getIdexpedientes()
    {
        return $this->idexpedientes;
    }
}
