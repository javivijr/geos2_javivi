<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mtmnovedadescerradas
 *
 * @ORM\Table(name="mtmnovedadescerradas", indexes={@ORM\Index(name="idusuariosMtm_NC_FK_idx", columns={"idusuariosMtm"}), @ORM\Index(name="idnovedades_NC_FK_idx", columns={"idnovedades"})})
 * @ORM\Entity
 */
class Mtmnovedadescerradas
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmtmNovedadesCerradas", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmtmnovedadescerradas;

    /**
     * @var \Mtmnovedades
     *
     * @ORM\ManyToOne(targetEntity="Mtmnovedades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idnovedades", referencedColumnName="idmtmNovedades")
     * })
     */
    private $idnovedades;

    /**
     * @var \Usuariosmtm
     *
     * @ORM\ManyToOne(targetEntity="Usuariosmtm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuariosMtm", referencedColumnName="idusuariosMtm")
     * })
     */
    private $idusuariosmtm;



    /**
     * Get idmtmnovedadescerradas.
     *
     * @return int
     */
    public function getIdmtmnovedadescerradas()
    {
        return $this->idmtmnovedadescerradas;
    }

    /**
     * Set idnovedades.
     *
     * @param \AppBundle\Entity\Globalcar\Mtmnovedades|null $idnovedades
     *
     * @return Mtmnovedadescerradas
     */
    public function setIdnovedades(\AppBundle\Entity\Globalcar\Mtmnovedades $idnovedades = null)
    {
        $this->idnovedades = $idnovedades;

        return $this;
    }

    /**
     * Get idnovedades.
     *
     * @return \AppBundle\Entity\Globalcar\Mtmnovedades|null
     */
    public function getIdnovedades()
    {
        return $this->idnovedades;
    }

    /**
     * Set idusuariosmtm.
     *
     * @param \AppBundle\Entity\Globalcar\Usuariosmtm|null $idusuariosmtm
     *
     * @return Mtmnovedadescerradas
     */
    public function setIdusuariosmtm(\AppBundle\Entity\Globalcar\Usuariosmtm $idusuariosmtm = null)
    {
        $this->idusuariosmtm = $idusuariosmtm;

        return $this;
    }

    /**
     * Get idusuariosmtm.
     *
     * @return \AppBundle\Entity\Globalcar\Usuariosmtm|null
     */
    public function getIdusuariosmtm()
    {
        return $this->idusuariosmtm;
    }
}
