<?php

namespace AppBundle\Entity\Globalcar;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personaspdfnie
 *
 * @ORM\Table(name="personaspdfnie", indexes={@ORM\Index(name="idpersonas_PPN_FK_idx", columns={"idpersonas"}), @ORM\Index(name="idcontratos_PPN_FK_idx", columns={"idcontrato"})})
 * @ORM\Entity
 */
class Personaspdfnie
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpersonasPdfNie", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonaspdfnie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rutaNie", type="string", length=200, nullable=true)
     */
    private $rutanie;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var \Contratos
     *
     * @ORM\ManyToOne(targetEntity="Contratos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcontrato", referencedColumnName="idcontratos")
     * })
     */
    private $idcontrato;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersonas", referencedColumnName="idpersonas")
     * })
     */
    private $idpersonas;



    /**
     * Get idpersonaspdfnie.
     *
     * @return int
     */
    public function getIdpersonaspdfnie()
    {
        return $this->idpersonaspdfnie;
    }

    /**
     * Set rutanie.
     *
     * @param string|null $rutanie
     *
     * @return Personaspdfnie
     */
    public function setRutanie($rutanie = null)
    {
        $this->rutanie = $rutanie;

        return $this;
    }

    /**
     * Get rutanie.
     *
     * @return string|null
     */
    public function getRutanie()
    {
        return $this->rutanie;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime|null $fecha
     *
     * @return Personaspdfnie
     */
    public function setFecha($fecha = null)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime|null
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idcontrato.
     *
     * @param \AppBundle\Entity\Globalcar\Contratos|null $idcontrato
     *
     * @return Personaspdfnie
     */
    public function setIdcontrato(\AppBundle\Entity\Globalcar\Contratos $idcontrato = null)
    {
        $this->idcontrato = $idcontrato;

        return $this;
    }

    /**
     * Get idcontrato.
     *
     * @return \AppBundle\Entity\Globalcar\Contratos|null
     */
    public function getIdcontrato()
    {
        return $this->idcontrato;
    }

    /**
     * Set idpersonas.
     *
     * @param \AppBundle\Entity\Globalcar\Personas|null $idpersonas
     *
     * @return Personaspdfnie
     */
    public function setIdpersonas(\AppBundle\Entity\Globalcar\Personas $idpersonas = null)
    {
        $this->idpersonas = $idpersonas;

        return $this;
    }

    /**
     * Get idpersonas.
     *
     * @return \AppBundle\Entity\Globalcar\Personas|null
     */
    public function getIdpersonas()
    {
        return $this->idpersonas;
    }
}
