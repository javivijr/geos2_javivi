<?php

namespace AppBundle\Entity\Geos2;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatosExpediente
 *
 * @ORM\Table("datos_expediente",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="datexp_idx_idexp", columns={"id_expediente"})}
 *     )
 * @ORM\Entity
 */
class DatosExpediente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_expediente", type="integer")
     */
    protected $idexpediente;

    /**
     * @var array
     *
     * @ORM\Column(name="datos", type="array")
     */
    protected $datos = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getIdexpediente(): array
    {
        return $this->idexpediente;
    }

    /**
     * @param integer $idUsuario
     */
    public function setIdexpediente($idexpediente): void
    {
        $this->idexpediente = $idexpediente;
    }

    /**
     * @return array
     */
    public function getDatos(): array
    {
        return $this->datos;
    }

    /**
     * @return string|null
     */
    public function getDato($key)
    {
        if ($this->hasDato($key))
            return $this->datos[$key];
        else
            return null;
    }

    public function hasDato($key)
    {
        if (is_array($this->datos))
            return array_key_exists($key, $this->datos);
        else
            return false;
    }

    public function removeDato($key)
    {
        if (array_key_exists(strtoupper($key), $this->datos)) {
            unset($this->datos[$key]);
            $this->datos = array_values($this->datos);
        }

        return $this;
    }

    /**
     * @param array $datos
     */
    public function addDato($key,$valor): void
    {
        $this->datos[$key] = $this->normalizaDato($valor);
    }

    public function setDatos(array $datos)
    {
        $this->datos = array();

        foreach ($datos as $key=>$value) {
            $this->addDato($key,$value);
        }

        return $this;
    }

    /**
     * @param $dato
     * @return string
     *
     * Funcion que normaliza los datos antes de meterlos en el array.
     */
    private function normalizaDato($dato) {
        if (is_bool($dato))
            return $dato;
        else if ($dato === '')
            return null;
        else
            return trim($dato);
    }
}