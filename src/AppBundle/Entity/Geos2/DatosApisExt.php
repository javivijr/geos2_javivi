<?php

namespace AppBundle\Entity\Geos2;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatosApisExt
 *
 * @ORM\Table("datos_apisext", indexes={
 *     @ORM\Index(name="datapiext_idx_apiid", columns={"api_id"}),
 *     @ORM\Index(name="datapiext_idx_fecha", columns={"fecha_llamada"}),
 *     @ORM\Index(name="datapiext_idx_idllamada", columns={"id_llamada"}),
 *     @ORM\Index(name="datapiext_idx_idclave", columns={"id_clave"}),
 *     @ORM\Index(name="datapiext_idx_clave", columns={"clave"})
 *     })
 * @ORM\Entity
 */
class DatosApisExt
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="api_id", type="string", length=50, nullable=false)
     */
    protected $apiId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_llamada", type="datetime", nullable=false)
     */
    protected $fechaLlamada;

    /**
     * @var string
     *
     * @ORM\Column(name="id_llamada", type="string", length=50, nullable=false)
     */
    protected $idLlamada;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clave", type="integer", nullable=false)
     */
    protected $idClave;

    /**
     * @var string
     *
     * @ORM\Column(name="clave", type="string", length=100, nullable=false)
     */
    protected $clave;

    /**
     * @var array
     *
     * @ORM\Column(name="datos_entrada", type="array", nullable=false)
     */
    protected $datosEntrada;

    /**
     * @var array
     *
     * @ORM\Column(name="datos_salida", type="array", nullable=false)
     */
    protected $datosSalida;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getApiId(): string
    {
        return $this->apiId;
    }

    /**
     * @param string $apiId
     * @return DatosApisExt
     */
    public function setApiId(string $apiId): DatosApisExt
    {
        $this->apiId = $apiId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFechaLlamada(): \DateTime
    {
        return $this->fechaLlamada;
    }

    /**
     * @param \DateTime $fechaLlamada
     * @return DatosApisExt
     */
    public function setFechaLlamada(\DateTime $fechaLlamada): DatosApisExt
    {
        $this->fechaLlamada = $fechaLlamada;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdLlamada(): string
    {
        return $this->idLlamada;
    }

    /**
     * @param string $idLlamada
     * @return DatosApisExt
     */
    public function setIdLlamada(string $idLlamada): DatosApisExt
    {
        $this->idLlamada = $idLlamada;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdClave(): int
    {
        return $this->idClave;
    }

    /**
     * @param int $idClave
     */
    public function setIdClave(int $idClave): void
    {
        $this->idClave = $idClave;
    }

    /**
     * @return string
     */
    public function getClave(): string
    {
        return $this->clave;
    }

    /**
     * @param string $clave
     */
    public function setClave(string $clave): void
    {
        $this->clave = $clave;
    }

    /**
     * @return array
     */
    public function getDatosEntrada(): array
    {
        return $this->datosEntrada;
    }

    /**
     * @param array $datosEntrada
     * @return DatosApisExt
     */
    public function setDatosEntrada(array $datosEntrada): DatosApisExt
    {
        $this->datosEntrada = $datosEntrada;
        return $this;
    }

    /**
     * @return array
     */
    public function getDatosSalida(): array
    {
        return $this->datosSalida;
    }

    /**
     * @param array $datosSalida
     * @return DatosApisExt
     */
    public function setDatosSalida(array $datosSalida): DatosApisExt
    {
        $this->datosSalida = $datosSalida;
        return $this;
    }


}