<?php

namespace AppBundle\Entity\Geos2;

use Doctrine\ORM\Mapping as ORM;


/**
 * User
 *
 * @ORM\Table("datos_usuario")
 * @ORM\Entity
 */
class DatosUsuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Geos2\Usuario")
     * @ORM\JoinColumn(name="id_usuario", referencedColumnName="id", nullable=false)
     */
    protected $usuario;

    /**
     * @var array
     *
     * @ORM\Column(name="datos", type="array")
     */
    protected $datos = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getUsuario(): array
    {
        return $this->usuario;
    }

    /**
     * @param integer $idUsuario
     */
    public function setUsuario(Usuario $usuario): void
    {
        $this->usuario = $usuario;
    }

    /**
     * @return array
     */
    public function getDatos(): array
    {
        return $this->datos;
    }

    /**
     * @return string|null
     */
    public function getDato($key)
    {
        if ($this->hasDato($key))
            return $this->datos[$key];
        else
            return null;
    }

    public function hasDato($key)
    {
        return array_key_exists(strtoupper($key), $this->getDatos());
    }

    public function removeDato($key)
    {
        if (array_key_exists(strtoupper($key), $this->datos)) {
            unset($this->datos[$key]);
            $this->datos = array_values($this->datos);
        }

        return $this;
    }

    /**
     * @param array $datos
     */
    public function addDato($key,$valor): void
    {
        $this->datos[strtoupper($key)] = $valor;
    }

    public function setDatos(array $datos)
    {
        $this->datos = array();

        foreach ($datos as $key=>$value) {
            $this->addDato($key,$value);
        }

        return $this;
    }


}