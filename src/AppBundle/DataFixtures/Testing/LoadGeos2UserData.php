<?php

namespace AppBundle\DataFixtures\Testing;

use AppBundle\Entity\Geos2\APIUser;
use AppBundle\Entity\Globalcar\Titulares;
use AppBundle\Entity\Globalcar\Titularesproveedores;
use AppBundle\Entity\Globalcar\Usuariosmtm;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadGeos2UserData implements FixtureInterface, ContainerAwareInterface
{
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        // Usuario en geos2 con titular en globalcar asociado

        /**
         * @var $emGb4 ObjectManager
         */
        $emGb4 = $this->container->get('doctrine')->getManager('gb4');
        //Titular JaviviCarting
        $tit = new Titulares();
        $tit->setNombre('JaviviCarting');
        $tit->setNif('12345678A');
        $tit->setTipoparticular('n');
        $tit->setProveedor('n');
        $emGb4->persist($tit);

        $prov = new Titulares();
        $prov->setNombre('ProveedorJJ');
        $prov->setNif('87654321A');
        $prov->setTipoparticular('n');
        $prov->setProveedor('s');
        $emGb4->persist($prov);

        $emGb4->flush();

        $titProv = new Titularesproveedores();
        $titProv->setIdtitulares($tit);
        $titProv->setIdproveedor($prov);
        $emGb4->persist($titProv);

        $userMtm = new Usuariosmtm();
        $userMtm->setUsuario('api.javivi');
        $userMtm->setBloqueo('n');
        $userMtm->setActivo('s');
        $userMtm->setRecibirinfo('n');
        $userMtm->setSololectura('n');
        $userMtm->setNoenvio('n');
        $userMtm->setNomailident('n');
        $userMtm->setBloqueomtm('n');
        $userMtm->setPerfil(666);
        $emGb4->persist($userMtm);

        $emGb4->flush();

        /**
         * @var $tit Titulares
         */
//        $tit = $emGb4->getRepository('AppBundle:Titulares')->findOneBy(array(
//            'nombre' => 'JaviviCarting'
//        ));

        $usuario = new APIUser();
        $usuario->setUsername('javivi');
        $usuario->setEmail('javier.jimenez@gesthispania.es');
        $usuario->setPassword('tuputo');
        $usuario->setIsActive(1);
        $usuario->setIdGeos($tit->getIdtitulares());

        $manager->persist($usuario);
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
