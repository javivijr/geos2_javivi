<?php

namespace AppBundle\Service\Wrap;

/*
 * En este servicio se va a encapsular el funcionamiento del bundle knp_gaufrette
 *
 * Si por algun vaiven del destino esta gente decide dejar de mantener el bundle,
 * con modificar este servicio se podra implementar un sustituto modificando unicamente el servicio.
 */

use AppBundle\Model\FilesystemModel;
use Knp\Bundle\GaufretteBundle\FilesystemMap;

class SistemaArchivos
{
    const AWSS3_TYPE = 'aws_s3';

    public $rutaRoot;
    public $rutaTemp;
    public $adaptadores;

    private $servicioKNP;

    private $fileSystemMap = array();
    private $configFS = array();

    public function __construct(FilesystemMap $KNP, $rutaRoot, $carpetaTemporal, $adaptadores)
    {
        $this->servicioKNP = $KNP;
        $this->rutaRoot = $rutaRoot;
        $this->rutaTemp = $rutaRoot.'/../'.$carpetaTemporal;
        $this->adaptadores = $adaptadores;
    }

    public function getAdaptadores() {
        return $this->adaptadores;
    }

    public function getAdaptador($adaptador) {
        if (!array_key_exists($adaptador,$this->adaptadores))
            throw new \InvalidArgumentException('Adaptador '.$adaptador.' no existente');
        return $this->adaptadores[$adaptador];
    }

    /**
     * @param $fs
     * De entrada tenemos el array de parametros.
     * Los parametros son:
     * id: el id del sistema de archivos a acceder - con KNPGaufrette definido en config.yml
     * type: tipo de adaptador - actualmente los soportados por KNPGaufrette
     */
    public function setFileSystem(FilesystemModel $fs)
    {
        $idFS = $fs->getId();
        if (!array_key_exists($idFS, $this->fileSystemMap)) {
            $this->fileSystemMap[$idFS] = $this->servicioKNP->get($idFS);
            $this->configFS[$idFS] = $fs;
        }
    }

    /**
     * @param $fs
     * @param string $key
     * @return mixed
     *
     * El metodo listkeys del Gaufrette, por lo general, devuelve un array asociativo.
     * clave 'keys' para los archivos y clave 'dir' para los directorios.
     * Casi todo esta programado esperando ese array, pero el adaptador del aws_s3
     * cambia y devuelve un array escalar con todo lo que encuentre por key.
     * Solucion, devolverlo siempre con el array asociativo (clave 'keys' al menos).
     *
     */
    public function listaKey(FilesystemModel $fs, $key='')
    {
        $salida = $this->fileSystemMap[$fs->getId()]->listKeys($key);
        if (!array_key_exists('keys', $salida)) {
            $aConKeys['keys'] = $salida;
            return $aConKeys;
        } else {
            return $salida;
        }
    }

    public function listaCarpeta(FilesystemModel $fs, $ruta='')
    {
        $l = $this->fileSystemMap[$fs->getId()]->listKeys($ruta);
        $listaFicheros = array();
        if (isset($l['keys'][0])) {
            $listaFicheros= $this->filtraPorRuta($l['keys'], $ruta);
        }
        return $listaFicheros;
    }

    public function leeFichero(FilesystemModel $fs, $rutaFichero)
    {
        // en algunas rutas puede venir la / duplicada
        $rutaFichero = str_replace('//','/',$rutaFichero);
        return $this->fileSystemMap[$fs->getId()]->read($rutaFichero);
    }

    public function escribeFichero(FilesystemModel $fs, $rutaFichero, $contenido, $sobreescribir = true)
    {
        // Para mantener el sistema de rutas de guardado de ficheros que hay actualmente en el S3
        // TODO - Darle una vuelta a esto
        if ($fs->getHasRutaPrefix() === true) {
            $rutaFichero = $fs->getRutaPrefix().$rutaFichero;
        }

        if ($this->fileSystemMap[$fs->getId()]->write($rutaFichero, $contenido, $sobreescribir)) {
            return $rutaFichero;
        } else {
            return false;
        }
    }

    public function eliminaFichero(FilesystemModel $fs, $rutaFichero)
    {
        if ($fs->getHasRutaPrefix() === true) {
            $rutaFichero = $fs->getRutaPrefix().$rutaFichero;
        }

        return $this->fileSystemMap[$fs->getId()]->delete($rutaFichero);
    }

    public function existeFichero(FilesystemModel $fs, $rutaFichero)
    {
        return $this->fileSystemMap[$fs->getId()]->has($rutaFichero);
    }

    public function getModifiedTime(FilesystemModel $fs, $rutafichero)
    {
        return $this->fileSystemMap[$fs->getId()]->mtime($rutafichero);
    }

    /**
     * Esta funcion filtra, de la lista de ficheros obtenida
     * aquellos que esten dentro de la ruta pasada como parametro
     *
     * @param $listaFicheros
     * @param $ruta
     * @return array
     */
    private function filtraPorRuta($listaFicheros, $ruta)
    {
        if ($ruta != '') {
            $listaFicherosSalida = array();
            $arrayRuta = explode('/', $ruta);

            foreach ($listaFicheros as $f) {

                // Convertimos la ruta string en un array
                // y de ese array eliminamos el ultimo elemento
                // que es el nombre del fichero.
                $arrayRutaFichero = explode('/', $f);
                array_splice($arrayRutaFichero, count($arrayRutaFichero)-1);

                // comparamos el array de la ruta pasada como parametro
                // con el array resultante del fichero.
                // esto mete en $result los elementos que esten en el primer array que no esten en el segundo.
                $result = array_diff($arrayRutaFichero, $arrayRuta);

                if (count($result)==0) {
                    $listaFicherosSalida[] = $f;
                }
            }
        } else {
            $listaFicherosSalida = $listaFicheros;
        }
        return $listaFicherosSalida;
    }

//    public function getRuta($otro='') {
//        $resul = '';
//
//        if($otro==='raiz')
//            return '/';
//
//        if ($this->aplicacion == self::GB4 || $this->aplicacion=='copiasficherosintercambio' || $this->aplicacion=='logemails') {
//            if($otro==='miniaturas')
//                $resul = $otro.'/';
//            else
//                $resul = Date($this->getFormatoRuta());
//        }else if ($this->aplicacion == self::TKL) {
//            $resul = Date($this->getFormatoRuta()).$otro.'/';
//        }else if ($this->aplicacion == self::MVR) {
//            $resul = Date($this->getFormatoRuta());
//        }else if ($this->aplicacion == self::GES2) {
//            $resul = '/';
//        }
//        return $resul;
//    }

//    private function getFormatoRuta(FilesystemModel $fs){
//        $resul = '';
//
//        if ($fs->getId() === 'amazonS3FS')
//            $resul = date('Y/m/d/');
//
//        /*if ($this->aplicacion == self::GB4 || $this->aplicacion == self::TKL || $this->aplicacion == self::MVR || $this->aplicacion=='copiasficherosintercambio' || $this->aplicacion=='logemails') {
//
//        }*/
//
//        return $resul;
//    }
}
