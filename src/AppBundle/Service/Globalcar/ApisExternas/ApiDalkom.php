<?php

namespace AppBundle\Service\Globalcar\ApisExternas;

use AppBundle\Entity\Geos2\DatosExpediente;
use AppBundle\Entity\Globalcar\Documentos;
use AppBundle\Entity\Globalcar\Expedientes;
use AppBundle\Entity\Globalcar\Historicopersonas;
use AppBundle\Entity\Globalcar\Organismos;
use AppBundle\Entity\Globalcar\Personas;
use AppBundle\Entity\Globalcar\Procedimientos;
use AppBundle\Entity\Globalcar\Repository\DocumentosRepository;
use AppBundle\Model\ContenedorRespuesta;
use AppBundle\Model\FilesystemModel;
use AppBundle\Service\Manager\Globalcar\DatosExpedienteManager;
use AppBundle\Service\LoggingApiExt;
use AppBundle\Service\Wrap\CurlRequest;
use AppBundle\Service\Wrap\SistemaArchivos;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ApiDalkom
{

    const idApi = 'Dalkom';
    const idioma = 'es-ES';

    // Datos expediente Dalkom
    const Dalkom_idUsuario = 'Dalkom_idUsuario';
//    const Dalkom_idGestor = 'Dalkom_idGestor';
    const Dalkom_Gestor_nombre = 'Dalkom_Gestor_nombre';
    const Dalkom_Gestor_email = 'Dalkom_Gestor_email';
//    const Dalkom_CentroCoste1 = 'Dalkom_CentroCoste1';
    const Dalkom_Conductor_nombre = 'Dalkom_Conductor_nombre';
    const Dalkom_Conductor_direccion = 'Dalkom_Conductor_direccion';
    const Dalkom_Conductor_cp = 'Dalkom_Conductor_cp';
    const Dalkom_Conductor_localidad = 'Dalkom_Conductor_localidad';
    const Dalkom_Conductor_idProvincia = 'Dalkom_Conductor_idProvincia';
    const Dalkom_Conductor_provincia = 'Dalkom_Conductor_provincia';
    const Dalkom_Conductor_dni = 'Dalkom_Conductor_dni';
    const Dalkom_Conductor_email = 'Dalkom_Conductor_email';
    const Dalkom_Conductor_telefono = 'Dalkom_Conductor_telefono';
    const Dalkom_EsPool = 'Dalkom_EsPool';
//    const Dalkom_Fecha_Datos_Conductor = 'Dalkom_Fecha_Datos_Conductor';
    const Dalkom_idExpediente_Dalkom = 'Dalkom_idExpediente_Dalkom';
    const Dalkom_idlog_consulta = 'Dalkom_idlog_consulta';
    const Dalkom_idLog_altaSancion = 'Dalkom_idLog_altaSancion';


    //Booleans
    const Dalkom_multaSubida = 'Dalkom_multaSubida';

    private $respuesta;
    private $urlBase;
    private $usuario;
    private $password;
    private $datExpMan;

    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;
    /**
     * @var $emGb4 EntityManager
     */
    private $emGb4;

    /**
     * @var $sa SistemaArchivos
     */
    private $sa;

    /**
     * @var $logging LoggingApiExt
     */
    private $logging;

    /**
     * @var $curlRequest CurlRequest
     */
    private $curlRequest;

    public function __construct(
        ContenedorRespuesta $respuesta,
        $urlBase,
        $usuario,
        $password,
        DatosExpedienteManager $datExpMan,
        RegistryInterface $doctrine,
        SistemaArchivos $sa,
        LoggingApiExt $log,
        CurlRequest $curlRequest)
    {
        $this->respuesta = $respuesta;
        $this->usuario = $usuario;
        $this->password = $password;
        $this->urlBase = $urlBase;
        $this->datExpMan = $datExpMan;
        $this->emGeos2 = $doctrine->getManager('geos2');
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->sa = $sa;
        $this->logging = $log;
        $this->curlRequest = $curlRequest;
    }

    public function solicitudDatosConductor($datos) {

        $idExpediente = $datos['idexpediente'];

        $datosPeticion = array(
            'Operador_usuario' => $this->usuario,
            'Operador_clave' => $this->password,
            'Localizacion_Idioma' => self::idioma,
            'Matricula_20' => $datos['matricula'],
            'Fecha' => $datos['fechadenuncia']
            //'Fecha' => \DateTime::createFromFormat('Y/m/d H:i',$fechadenuncia)
        );

        if (isset($datos['referencia']) && $datos['referencia'] != 'null' && $datos['referencia'] != "") {
            $datosExpediente = $this->datExpMan->getDatosExpediente($idExpediente);
            if ($datosExpediente->hasDato(self::Dalkom_EsPool) && $datosExpediente->getDato(self::Dalkom_EsPool) == true) {
                $datosPeticion['Conductor_login_20'] = $datos['referencia'];
                unset($datosPeticion['Matricula_20']);
                unset($datosPeticion['Fecha']);
            }
        }

        $url = $this->urlBase.'FLOTA_MULTA/CONSULTAR/Usuario_Identificar/';
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        $result = $this->llamada($url, http_build_query($datosPeticion), $headers);

        $datosRespuesta = json_decode($result, true);

        $idLog = $this->logging->guardaLog(
            ApiExtManager::SOLICITUD_DATOS_CONDUCTOR,
            LoggingApiExt::DALKOM,
            LoggingApiExt::MATRICULA,
            $datos['matricula'],
            $datosPeticion,
            $datosRespuesta);

        if ( $datosRespuesta['Errores'] !== null && count($datosRespuesta['Errores'])>0 ) {
            $stringErrores='';
            foreach ($datosRespuesta['Errores'] as $error) {
                $stringErrores = $stringErrores.$error;
            }
            $this->respuesta->addAviso($stringErrores,ContenedorRespuesta::MENSAJE_ALERTA);
            $this->respuesta->setEstado(false);
        }
        else {                                                                                        // SI, CON UN ESPACIO, CON DOS COJONES
            if ($datosRespuesta['Valor_Devuelto'][0]['EsPool'] && $datosRespuesta['Valor_Devuelto'][0]['idUsuario '] == 0) {
//                $this->respuesta->addAviso('Vodafone no tiene información del conductor en su sistema. Consulte a '.$datosRespuesta['Valor_Devuelto'][0]['Gestor_nombre'].'. Email '.$datosRespuesta['Valor_Devuelto'][0]['Gestor_email'],ContenedorRespuesta::MENSAJE_ALERTA);
                $this->respuesta->addMensaje('Vodafone no tiene actualizada la información del conductor en su sistema. Consulte a '.$datosRespuesta['Valor_Devuelto'][0]['Gestor_nombre'].' (Email: '.$datosRespuesta['Valor_Devuelto'][0]['Gestor_email'].") para obtener el ID del conductor.\nUna vez obtenido el ID, introduzcalo en el campo referencia y realice de nuevo la consulta.",ContenedorRespuesta::MENSAJE_ALERTA);
            }
            $datosRespuesta['Valor_Devuelto'][0]['idlog_consulta'] = $idLog;
            $this->guardaDatosAsociadosExpediente($idExpediente,$datosRespuesta['Valor_Devuelto'][0]);
            $datosRespuesta = $this->datosParaFront($datosRespuesta);
            $this->respuesta->setDatos($datosRespuesta);
            $this->respuesta->setEstado(true);
        }

        return $this->respuesta;
    }

    private function datosParaFront($datos) {

        // Conductor_nombre
        if (isset($datos['Valor_Devuelto'][0]['Conductor_nombre'])) {
            $aNombre = explode(',',$datos['Valor_Devuelto'][0]['Conductor_nombre']);
            if (count($aNombre)>1) {
                $datos['Valor_Devuelto'][0]['Conductor_nombre'] = trim($aNombre[count($aNombre) - 1]);
                $datos['Valor_Devuelto'][0]['Conductor_apellido1'] = trim($aNombre[0]);
            }
        }
        $datos['Valor_Devuelto'][0]['Conductor_tipodoc'] = 'DNI';

        return $datos;
    }

    public function altaSancion($datosEntrada) {

        /**
         * @var $datosExpediente DatosExpediente
         */
        $datosExpediente = $this->datExpMan->getDatosExpediente($datosEntrada['idexpediente']);

        /**
         * @var $expediente Expedientes
         */
        $expediente = $this->emGb4->getRepository('AppBundle:Expedientes')->findOneBy(array(
            'idexpedientes' => $datosEntrada['idexpediente']
        ));

        /**
         * @var $procedimiento Procedimientos
         */
        $procedimiento = $expediente->getIdprocedimientos();

        $url = $this->urlBase.'FLOTA_MULTA/SOLICITAR/Alta_Multa/';
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );


        // Se comprueba que se tengan todos los datos obtenidos en la llamada consulta de datos de conductor
        if (!(
            $datosExpediente->hasDato(self::Dalkom_idUsuario) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_nombre) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_direccion) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_cp) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_localidad) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_idProvincia) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_provincia) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_dni) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_email) ||
            $datosExpediente->hasDato(self::Dalkom_Conductor_telefono)
            )
        )
            throw new \Exception('Este expediente debe idenfificarse consultando la API externa.');

        $expedienteIdentificado = $this->emGb4->getRepository('AppBundle:Expedientesidentificados')->findOneBy(array(
            'idexpedientes' => $expediente
        ));

        if (!$expedienteIdentificado)
            return $this->respuesta;

        /**
         * @var $historicoPersona Historicopersonas
         */
        $historicoPersona = $expedienteIdentificado->getIdhistoricopersonas();

        /**
         * @var $persona Personas
         */
        $persona = $historicoPersona->getIdpersonas();

        if (($datosExpediente->getDato(self::Dalkom_EsPool) === false) && $persona->getNif() != $datosExpediente->getDato(self::Dalkom_Conductor_dni)) {
            $this->respuesta->addAviso('El documento identificativo de nuestro expediente y el de Dalkom no coincide.', ContenedorRespuesta::MENSAJE_ALERTA);
//            $this->respuesta->setEstado(false);
//            return $this->respuesta;
        }

        /**
         * @var $organismo Organismos
         */
        $organismo = $expediente->getIdorganismos();

        $datosPost = array(
            'Operador_usuario' => $this->usuario,
            'Operador_clave' => $this->password,
            'Localizacion_Idioma' => self::idioma,
            'idUsuario' => $datosExpediente->getDato(self::Dalkom_idUsuario),
            'Conductor_nombre_200' => $persona->getApellido1().' '.$persona->getApellido2().', '.$persona->getNombre(),
            'Conductor_direccion_150' => $historicoPersona->getCalle(),
            'Conductor_cp' => $historicoPersona->getCp(),
            'Conductor_localidad_100' => $historicoPersona->getPoblacion(),
            'Conductor_idProvincia' => $datosExpediente->getDato(self::Dalkom_Conductor_idProvincia),
            'Conductor_provincia' => $historicoPersona->getProvincia(),
            'Conductor_dni_15' => $persona->getNif(),
            'Conductor_email_150' => $historicoPersona->getEmail(),
            'Conductor_telefono_15' => ($historicoPersona->getTelefono()) ? $historicoPersona->getTelefono() : 'notelf',
            'Fecha' => $procedimiento->getFechadenuncia()->format('Y/m/d H:i:s'),
//            'nExpediente_25' => $expediente->getIdexpedientes(),
            'nExpediente_25' => $expediente->getCodigosancion(),
            'Matricula_20' => $procedimiento->getIdhistoricovehiculos()->getIdvehiculo()->getMatricula(),
            'Organismo_tiutlo1_100' => $organismo->getNombre(),
            'Organismo_tiutlo2_100' => '',
            'Organismo_direccion_150' => $organismo->getDireccion(),
            'Organismo_cp' => '0',
            'Organismo_localidad_100' => '',
            'Organismo_provincia_200' => '',
            'Sancion_tipo_100' => substr($expediente->getIdhechos()->getHecho(),0,100),
            'Sancion_importe' => $expediente->getImporte()
        );

        $result = $this->llamada($url, http_build_query($datosPost), $headers);

        $arrayResultado = json_decode($result,true);

        $idLog = $this->logging->guardaLog(
            ApiExtManager::ALTA_SANCION,
            LoggingApiExt::DALKOM,
            LoggingApiExt::EXPEDIENTE,
            $expediente->getIdexpedientes(),
            $datosPost,
            $arrayResultado);

        if (is_array($arrayResultado['Errores']) && count($arrayResultado['Errores'])>0) {
            foreach ($arrayResultado['Errores'] as $error) {
             $this->respuesta->addAviso('Dalkom - '.$error,ContenedorRespuesta::MENSAJE_ALERTA);
             $this->respuesta->setEstado(false);
            }
            return $this->respuesta;
        }

        if (is_numeric($arrayResultado['Valor_Devuelto'])) {
            $dato = array(
                'idExpediente_Dalkom' => $arrayResultado['Valor_Devuelto'],
                'idLog_altaSancion' => $idLog
            );
            $this->guardaDatosAsociadosExpediente($datosEntrada['idexpediente'], $dato);
            $this->emGeos2->refresh($datosExpediente);
            $this->respuesta->addAviso('Sanción dada de alta correctamente.',ContenedorRespuesta::MENSAJE_CONFIRMACION);
        } else {
            $this->respuesta->addMensaje(json_encode($arrayResultado),ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->addAviso(json_encode($arrayResultado),ContenedorRespuesta::MENSAJE_ERROR);
        }

        return $this->respuesta;
    }

    public function altaDocuSancion($datosEntrada) {

        /**
         * @var $datosExpediente DatosExpediente
         */
        $datosExpediente = $this->datExpMan->getDatosExpediente($datosEntrada['idexpediente']);

        // Comprobar si existe idDalkom de la sancion, si no es asi no se puede subir la documentacion
        if (!$datosExpediente->getDato(self::Dalkom_idExpediente_Dalkom)) {
            $this->respuesta->addAviso('Antes de subir la multa debe dar de alta la sanción.',ContenedorRespuesta::MENSAJE_ALERTA);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }

        // Si la multa ya esta subida no la subimos de nuevo
        if ($datosExpediente->getDato(self::Dalkom_multaSubida)) {
            $this->respuesta->addAviso('Documento ya subido a la API externa.',ContenedorRespuesta::MENSAJE_ALERTA);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }


        /**
         * @var $expediente Expedientes
         */
        $expediente = $this->emGb4->getRepository('AppBundle:Expedientes')->findOneBy(array(
            'idexpedientes' => $datosEntrada['idexpediente']
        ));

        $documentos = $this->consultaDocumentoMultaExpediente($expediente);

        if ($documentos) {
            $fsm = new FilesystemModel($this->sa->getAdaptador('amazonS3FS'));
            $this->sa->setFileSystem($fsm);
            /**
             * @var $docu Documentos
             */
            foreach ($documentos as $docu) {
                $fichero = $this->sa->leeFichero($fsm,$docu->getRuta());
                try {
                    $this->subeDocumento($fichero, $docu->getRuta(), $expediente->getIdexpedientes(), $datosExpediente->getDato(self::Dalkom_idExpediente_Dalkom));
                }
                catch (\Exception $e) {
                    $this->respuesta->addAviso("Error subiendo ".$docu->getRuta().' a Dalkom.',ContenedorRespuesta::MENSAJE_ALERTA);
                    $this->respuesta->setEstado(false);
                }
            }
        }
        else {
            $this->respuesta->addAviso('El expediente no tiene multa asociada.',ContenedorRespuesta::MENSAJE_ALERTA);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }

        if ($this->respuesta->getEstado()) {
            $datosExpediente->addDato(self::Dalkom_multaSubida, true);
            $this->respuesta->addAviso('Documento subido correctamente.',ContenedorRespuesta::MENSAJE_CONFIRMACION);
            $this->datExpMan->persistDatosExpediente($datosExpediente);
        }

        return $this->respuesta;

    }

    public function checkAccionesSobreExpediente($idexpediente) {
        /**
         * @var $datosExpediente DatosExpediente
         */
        $datosExpediente = $this->datExpMan->getDatosExpediente($idexpediente);

        if (!$datosExpediente->hasDato(self::Dalkom_idExpediente_Dalkom)) {
            $this->respuesta->addAviso('Multa no enviada a API externa.',ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
        }

        if (!$datosExpediente->hasDato(self::Dalkom_multaSubida) ||
           ($datosExpediente->hasDato(self::Dalkom_multaSubida) && $datosExpediente->getDato(self::Dalkom_multaSubida) == false)
        ) {
            $this->respuesta->addAviso('Documento no enviado a API externa.',ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
        }

        return $this->respuesta;
    }

    private function subeDocumento($contenido, $ruta, $idExpedienteGB, $idExpedienteDalkom) {

        $url = $this->urlBase.'FLOTA_MULTA/SOLICITAR/Alta_Multa_Documento/';
        $aRuta = explode('/', $ruta);
        $nombreFichero = $aRuta[count($aRuta)-1];

        $contentB64 = base64_encode($contenido);

        $headers = array(
            'Content-Type: application/json'
        );

        $datosPeticion = array(
            'Operador_usuario' => $this->usuario,
            'Operador_clave' => $this->password,
            'Localizacion_Idioma' => self::idioma,
            'idRegistro' => $idExpedienteDalkom,
            'NombreFichero_250' => $nombreFichero,
            'Descripcion_250' => '',
            'Imagen' => $contentB64
        );

        $result = $this->llamada($url,json_encode($datosPeticion),$headers);

        $datosRespuesta = json_decode($result,true);

        $idLog = $this->logging->guardaLog(
            ApiExtManager::ALTA_DOCU_SANCION,
            LoggingApiExt::DALKOM,
            LoggingApiExt::EXPEDIENTE,
            $idExpedienteGB,
            $datosPeticion,
            $datosRespuesta);

        return $datosRespuesta;

    }

    private function llamada($url, $datos, $headers) {

        $this->curlRequest->setUrl($url);
        $this->curlRequest->setOption(CURLOPT_POST,1);
        $this->curlRequest->setOption(CURLOPT_HTTPHEADER, $headers);
        $this->curlRequest->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->curlRequest->setOption(CURLOPT_POSTFIELDS, $datos);
        $result = $this->curlRequest->execute();
        $this->curlRequest->close();

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL,$url);
//        curl_setopt($ch,CURLOPT_POST,1);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $datos);
//        $result = curl_exec($ch);
//        curl_close($ch);

        if (!$result)
            throw new \Exception('Error accediendo a servicio Dalkom. Consulte con tecnología');

        return $result;
    }

    private function guardaDatosAsociadosExpediente($idexpediente, $datos)
    {
        /**
         * @var $datosExpediente DatosExpediente
         */
        $datosExpediente = $this->datExpMan->getDatosExpediente($idexpediente);
        foreach ($datos as $key => $value)
            $datosExpediente->addDato(self::idApi.'_'.trim($key), $value);
        $this->datExpMan->persistDatosExpediente($datosExpediente);
    }

    private function consultaDocumentoMultaExpediente(Expedientes $expediente) {
        /**
         * @var $docuRepo DocumentosRepository
         */
        $docuRepo = $this->emGb4->getRepository('AppBundle:Documentos');
        $documentosMulta = $docuRepo->findDocumentoMultaExpediente($expediente);

        if (count($documentosMulta)>0)
            return $documentosMulta;
        else
            return null;
    }



}