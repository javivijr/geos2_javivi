<?php

namespace AppBundle\Service\Globalcar\ApisExternas;


use AppBundle\Entity\Geos2\DatosExpediente;
use AppBundle\Entity\Globalcar\Documentos;
use AppBundle\Entity\Globalcar\Documentosdescriptores;
use AppBundle\Entity\Globalcar\Expedientes;
use AppBundle\Entity\Globalcar\Organismos;
use AppBundle\Entity\Globalcar\Organismosconfig;
use AppBundle\Model\ContenedorRespuesta;
use AppBundle\Model\FilesystemModel;
use AppBundle\Service\LoggingApiExt;
use AppBundle\Service\Manager\DescriptorManager;
use AppBundle\Service\Manager\DocumentoDescriptorManager;
use AppBundle\Service\Manager\DocumentoManager;
use AppBundle\Service\Manager\Globalcar\DatosExpedienteManager;
use AppBundle\Service\Manager\Globalcar\ExpedienteManager;
use AppBundle\Service\Manager\Globalcar\OrganismosconfigManager;
use AppBundle\Service\Wrap\CurlRequest;
use AppBundle\Service\Wrap\SistemaArchivos;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ApiFurgoline
{
    const idApi = 'Furgoline';

    const incidenciaNoContratoAlquilerFirmado = "No se dispone del contrato de alquiler firmado";

    /**
     * @var $respuesta ContenedorRespuesta
     */
    private $respuesta;

    private $urlBase;
    private $usuario;
    private $password;

    /**
     * @var $datExpMan DatosExpedienteManager
     */
    private $datExpMan;

    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;
    /**
     * @var $emGb4 EntityManager
     */
    private $emGb4;

    /**
     * @var $sa SistemaArchivos
     */
    private $sa;

    /**
     * @var $expedienteManager ExpedienteManager
     */
    private $expedienteManager;

    /**
     * @var $organismosconfigManager OrganismosconfigManager
     */
    private $organismosconfigManager;

    /**
     * @var $documentoManager DocumentoManager
     */
    private $documentoManager;

    /**
     * @var $documentoDescriptorManager DocumentoDescriptorManager
     */
    private $documentoDescriptorManager;

    /**
     * @var $descriptorManager DescriptorManager
     */
    private $descriptorManager;

    /**
     * @var $logging LoggingApiExt
     */
    private $logging;

    /**
     * @var $curlRequest CurlRequest
     */
    private $curlRequest;


    /**
     * @var $expediente Expedientes
     */
    private $expediente;
    private $organismoCopiaContrato;
    private $organismoCopiaContratoFirmado;
    private $incidenciaFaltaContratoFirmado;



    public function __construct(
        ContenedorRespuesta $respuesta,
        $urlBase,
        $usuario,
        $password,
        DatosExpedienteManager $datExpMan,
        ExpedienteManager $expedienteManager,
        OrganismosconfigManager $organismosconfigManager,
        DocumentoManager $documentoManager,
        DocumentoDescriptorManager $documentoDescriptorManager,
        DescriptorManager $descriptorManager,
        RegistryInterface $doctrine,
        SistemaArchivos $sa,
        LoggingApiExt $log,
        CurlRequest $curlRequest
    ) {
        $this->respuesta = $respuesta;
        $this->usuario = $usuario;
        $this->password = $password;
        $this->urlBase = $urlBase;
        $this->expedienteManager = $expedienteManager;
        $this->organismosconfigManager = $organismosconfigManager;
        $this->documentoManager = $documentoManager;
        $this->documentoDescriptorManager = $documentoDescriptorManager;
        $this->descriptorManager = $descriptorManager;
        $this->datExpMan = $datExpMan;
        $this->emGeos2 = $doctrine->getManager('geos2');
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->sa = $sa;
        $this->logging = $log;
        $this->curlRequest = $curlRequest;


        $this->organismoCopiaContrato = 'N';
        $this->organismoCopiaContratoFirmado = 'N';
    }

    public function solicitudDatosConductor($datos) {
        $idExpediente = $datos['idexpediente'];
//        $copiaContrato = 'N';
//        $copiaContratoFirmado = 'N';

        $this->expediente = $this->expedienteManager->findOneBy(array(
            'idexpedientes' => $idExpediente
        ));

        if (!$this->expediente)
            throw new \RuntimeException('ApiFurgoline - Expediente inexistente');

        /**
         * @var $organismo Organismos
         */
        $organismo = $this->expediente->getIdorganismos();

        /**
         * @var $organismoConfig Organismosconfig
         */
        $organismoConfig = $this->organismosconfigManager->findOneBy(array(
            'idorganismos' => $organismo->getIdorganismos()
        ));

        if ($organismoConfig->getCopiacontratorentacar() == 's') {
            $this->organismoCopiaContrato = 'S';
            if ($organismoConfig->getContratofirmadorentacar() == 's')
                $this->organismoCopiaContratoFirmado = 'S';
        }

        $fecha = \DateTime::createFromFormat('Y/m/d H:i',$datos['fechadenuncia']);

        $datosPeticion = array(
            'Usuario' => $this->usuario,
            'Clave' => $this->password,
            'Matricula' => $datos['matricula'],
            'Fecha' => $fecha->format('Y-m-d H:i:s'),
            'Contrato_alquiler' => $this->organismoCopiaContrato,
            'Contrato_alquiler_firmado' => $this->organismoCopiaContratoFirmado,
            'NIE_Necesario' => isset($datos['nienecesario']) ? $datos['contratoalquiler'] : 'N',
        );

        $url = $this->urlBase;
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        $result = $this->llamada($url, http_build_query($datosPeticion), $headers);

        $datosRespuesta = json_decode($result, true);

        $idLog = $this->logging->guardaLog(
            ApiExtManager::SOLICITUD_DATOS_CONDUCTOR,
            LoggingApiExt::FURGOLINE,
            LoggingApiExt::MATRICULA,
            $datos['matricula'],
            $datosPeticion,
            $datosRespuesta);

        if ( $datosRespuesta['Errores'] !== null && count($datosRespuesta['Errores'])>0 ) {
            $stringErrores='';
            foreach ($datosRespuesta['Errores'] as $error) {
                $stringErrores = $stringErrores.$error;
            }
            $this->respuesta->addAviso($stringErrores,ContenedorRespuesta::MENSAJE_ALERTA);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }

        if ( $datosRespuesta['Incidencias'] !== null && count($datosRespuesta['Incidencias'])>0 )
            $this->procesaIncidencias($datosRespuesta);

        $this->evaluaContrato($datosRespuesta);

        $datosRespuesta['Valor'][0]['idlog_consulta'] = $idLog;
        $this->guardaDatosAsociadosExpediente($idExpediente,$datosRespuesta['Valor'][0]);
        $datosRespuesta = $this->datosParaFront($datosRespuesta);
        $this->respuesta->setDatos($datosRespuesta);
        $this->respuesta->setEstado(true);


        return $this->respuesta;
    }

    private function evaluaContrato($datosRespuesta) {

        $firmado = false;

        if ($this->organismoCopiaContrato == 'N')
            return;

        if (!isset($datosRespuesta['Valor'][0]['contrato_documento_copia']) ||
            (isset($datosRespuesta['Valor'][0]['contrato_documento_copia']) && $datosRespuesta['Valor'][0]['contrato_documento_copia'] === null) )
        {
            $this->respuesta->addAviso("Este organismo necesita copia de contrato y Furgoline no lo tiene en su sistema.",ContenedorRespuesta::MENSAJE_ALERTA);
            return;
        }

        if ($this->organismoCopiaContratoFirmado == 'S' && $this->incidenciaFaltaContratoFirmado == true) {
            $this->respuesta->addAviso("Este organismo necesita copia de contrato firmado y Furgoline no lo tiene en su sistema.",ContenedorRespuesta::MENSAJE_ALERTA);
            $firmado = false;
        }
        elseif ($this->organismoCopiaContratoFirmado == 'S')
            $firmado = true;
        else
            $firmado = false;

        $this->subeContrato($datosRespuesta['Valor'][0]['contrato_documento_copia'], $firmado);

    }

    private function subeContrato($contrato, $firmado) {

//        $nombreFichero = 'APIEXT-FURGOLINE-'.rand().'-SolicitudDatosConductor-'.'Api Furgoline'.'-'.date('Y-m-d\TH.i.s').'.pdf';
        $nombreFichero = 'APIEXT-FURGOLINE-'.rand().'-SolicitudDatosConductor-'.'Contrato Exp '.$this->expediente->getIdexpedientes().'-'.date('Y-m-d\TH.i.s').'.pdf';

        $fsm = new FilesystemModel($this->sa->getAdaptador('amazonS3FS'));
        $this->sa->setFileSystem($fsm);
        $escritura = $this->sa->escribeFichero(
            $fsm,
            $nombreFichero,
            base64_decode($contrato)
        );

        if (!$escritura)
            throw new \RuntimeException('Error escribiendo fichero.');

        $documento = new Documentos();
        $documento->setAlta(new \DateTime('today'));
        $documento->setAltareal(new \DateTime());
        $documento->setIdexpedientes($this->expediente);
        $documento->setRuta($escritura);
        $documento->setUsuario('apiext.furgoline');
        $documento->setSha1(sha1(base64_decode($contrato)));
        $documento->setExiste('s');
        $documento->setVisiblemtm('auto');
        $this->documentoManager->guardar($documento,true);

        if ($firmado)
            $descriptor = $this->descriptorManager->findOneBy(array(
                'clave' => 'CAF'
            ));
        else
            $descriptor = $this->descriptorManager->findOneBy(array(
                'clave' => 'ALQ'
            ));

        $documentoDescriptor = new Documentosdescriptores();
        $documentoDescriptor->setIddocumentos($documento);
        $documentoDescriptor->setIddescriptores($descriptor);

        $this->documentoDescriptorManager->guardar($documentoDescriptor,true);

        return;
    }

    private function procesaIncidencias($datosRespuesta) {
        $stringIncidencias = '';
        foreach ($datosRespuesta['Incidencias'] as $incidencia) {

            if ($incidencia == self::incidenciaNoContratoAlquilerFirmado) {
                $this->incidenciaFaltaContratoFirmado = true;
//                continue;
            }

            $stringIncidencias = $stringIncidencias .$incidencia;

        }
        $this->respuesta->addAviso($stringIncidencias,ContenedorRespuesta::MENSAJE_ALERTA);
    }

    private function llamada($url, $datos, $headers) {

        $this->curlRequest->setUrl($url);
        $this->curlRequest->setOption(CURLOPT_POST,1);
        $this->curlRequest->setOption(CURLOPT_HTTPHEADER, $headers);
        $this->curlRequest->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->curlRequest->setOption(CURLOPT_POSTFIELDS, $datos);
        $result = $this->curlRequest->execute();
        $this->curlRequest->close();

        if (!$result)
            throw new \Exception('Error accediendo a servicio de Furgoline. Consulte con tecnología');

        return $result;
    }

    private function guardaDatosAsociadosExpediente($idexpediente, $datos)
    {
        /**
         * @var $datosExpediente DatosExpediente
         */
        $datosExpediente = $this->datExpMan->getDatosExpediente($idexpediente);
        foreach ($datos as $key => $value)
            $datosExpediente->addDato(self::idApi.'_'.trim($key), $value);
        $this->datExpMan->persistDatosExpediente($datosExpediente);
    }

    private function datosParaFront($datos) {
        $res = array();

        $res['Errores'] = $datos['Errores'];
        $res['Incidencias'] = $datos['Incidencias'];
        $res['Valor_Devuelto'][0]['Conductor_dni'] = $datos['Valor'][0]['documento_conductor_20'];

        // TODO - Darle una vuelta al tipodoc aqui y en el front
        switch ($datos['Valor'][0]['tipo_documento_conductor_3']) {
            case 'NIF':
                $res['Valor_Devuelto'][0]['Conductor_tipodoc'] = 'DNI';
                break;
            case 'CIF':
                $res['Valor_Devuelto'][0]['Conductor_tipodoc'] = 'CIF';
                break;
            case 'NIE':
                $res['Valor_Devuelto'][0]['Conductor_tipodoc'] = 'NIE';
                break;
            default:
                // Si no coincide con ninguno de los case de arriba, por defectgo un DNI (hay que poner algo en el front)
                $res['Valor_Devuelto'][0]['Conductor_tipodoc'] = 'DNI';
        }
//        $res['Valor_Devuelto'][0]['Conductor_tipodoc'] = $datos['Valor'][0]['tipo_documento_conductor_3'];
        $res['Valor_Devuelto'][0]['Conductor_nombre'] = $datos['Valor'][0]['conductor_apellido1_150'];
        $res['Valor_Devuelto'][0]['Conductor_direccion'] = $datos['Valor'][0]['conductor_nombre_via_80'];
        $res['Valor_Devuelto'][0]['Conductor_cp'] = $datos['Valor'][0]['conductor_codigo_postal_9'];
        $res['Valor_Devuelto'][0]['Conductor_localidad'] = $datos['Valor'][0]['conductor_poblacion_50'];
        $res['Valor_Devuelto'][0]['Conductor_provincia'] = $datos['Valor'][0]['conductor_provincia_50'];
        $res['Valor_Devuelto'][0]['Conductor_pais'] = $datos['Valor'][0]['conductor_pais_50'];
        $res['Valor_Devuelto'][0]['Conductor_fecha_nacimiento'] = $datos['Valor'][0]['conductor_fecha_nacimiento'];
        $res['Valor_Devuelto'][0]['Conductor_email'] = $datos['Valor'][0]['conductor_email_50'];

        return $res;
    }

}