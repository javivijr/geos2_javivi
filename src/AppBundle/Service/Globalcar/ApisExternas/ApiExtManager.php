<?php

namespace AppBundle\Service\Globalcar\ApisExternas;


use AppBundle\Entity\Globalcar\Escritoentrada;
use AppBundle\Entity\Globalcar\Expedientes;
use AppBundle\Entity\Globalcar\Expedientesdenunciado;
use AppBundle\Entity\Globalcar\Historicovehiculos;
use AppBundle\Entity\Globalcar\Mtmtitularesconfig;
use AppBundle\Entity\Globalcar\Procedimientos;
use AppBundle\Entity\Globalcar\Titularconfig;
use AppBundle\Entity\Globalcar\Titulares;
use AppBundle\Entity\Globalcar\Vehiculos;
use AppBundle\Model\ContenedorRespuesta;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ApiExtManager
{

    const SOLICITUD_DATOS_CONDUCTOR = 'solicitudDatosConductor';
    const ALTA_SANCION = 'altaSancion';
    const ALTA_DOCU_SANCION = 'altaDocuSancion';
    const CHECK_ACCIONES_SOBRE_EXPEDIENTE = 'checkAccionesSobreExpediente';

    const ACCIONES = array(
        'solicitudDatosConductor',
        'altaSancion',
        'altaDocuSancion',
        'checkAccionesSobreExpediente'
    );

    private $respuesta;
    private $emGb4;
    private $apis;


    public function __construct(ContenedorRespuesta $respuesta, RegistryInterface $doctrine, ServicioApisExt $apis) {
        $this->respuesta = $respuesta;
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->apis = $apis->getApis();
    }

    public function solicitudDatosConductor($datosEntrada) {
        try {

            // Obtenemos, si la hay, la key de la API asociada al titular de ese vehiculo en esa fecha
            $keyApi = $this->obtieneIdApiConMatriculaFecha($datosEntrada['matricula'], $datosEntrada['fechadenuncia']);

            if (!$keyApi ||
                !$this->compruebaAccionEnAPI(self::SOLICITUD_DATOS_CONDUCTOR,$keyApi))
            {
                $this->respuesta->addMensaje('Servicio no disponible para ese vehiculo en esa fecha', ContenedorRespuesta::MENSAJE_ALERTA);
                $this->respuesta->setEstado(false);
                return $this->respuesta;
            }

            $metodo = self::SOLICITUD_DATOS_CONDUCTOR;
            $this->respuesta = $this->apis[$keyApi]->$metodo($datosEntrada);
            return $this->respuesta;
        }
        catch (\Exception $e) {
            $this->respuesta->addMensaje($e->getMessage(),ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }
    }

    public function altaSancion($datosEntrada) {

        try {
            $keyApi = $this->obtieneIdApiConIdExpediente($datosEntrada['idexpediente']);

            if (!$keyApi ||
                !$this->compruebaAccionEnAPI(self::ALTA_SANCION,$keyApi))
            {
                $this->respuesta->addMensaje('Servicio no de sanciones externo no disponible para este expediente', ContenedorRespuesta::MENSAJE_ALERTA);
                $this->respuesta->setEstado(false);
                return $this->respuesta;
            }
            $metodo = self::ALTA_SANCION;
            $this->respuesta = $this->apis[$keyApi]->$metodo($datosEntrada);
            return $this->respuesta;
        }
        catch (\Exception $e) {
            $this->respuesta->addMensaje($e->getMessage(),ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }

    }

    public function altaDocuSancion($datosEntrada) {

        try {
            $keyApi = $this->obtieneIdApiConIdExpediente($datosEntrada['idexpediente']);

            if (!$keyApi ||
                !$this->compruebaAccionEnAPI(self::ALTA_DOCU_SANCION,$keyApi))
            {
                $this->respuesta->addMensaje('Servicio no de sanciones externo no disponible para este expediente', ContenedorRespuesta::MENSAJE_ALERTA);
                $this->respuesta->setEstado(false);
                return $this->respuesta;
            }
            $metodo = self::ALTA_DOCU_SANCION;
            $this->respuesta = $this->apis[$keyApi]->$metodo($datosEntrada);
            return $this->respuesta;
        }
        catch (\Exception $e) {
            $this->respuesta->addMensaje($e->getMessage(),ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }

    }

    public function obtieneAccionesDisponiblesExpediente($idexpediente) {

        try {
            $keyApi = $this->obtieneIdApiConIdExpediente($idexpediente);

            if (!$keyApi)
                return $this->respuesta;

            $metodosPublicosApi = get_class_methods(get_class($this->apis[$keyApi]));
            $metodosDisponibles = $this->cruzaListaMetodos($metodosPublicosApi);
            //return $metodosDisponibles;
            $this->respuesta->setDatos($metodosDisponibles);
            return $this->respuesta;

        }
        catch (\Exception $e) {
            $this->respuesta->addMensaje($e->getMessage(),ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }
    }

    public function compruebaAccionesRealizadasExpediente($idexpediente) {
        try {
            $keyApi = $this->obtieneIdApiConIdExpediente($idexpediente);

            if (!$keyApi ||
                !$this->compruebaAccionEnAPI(self::CHECK_ACCIONES_SOBRE_EXPEDIENTE,$keyApi))
                return $this->respuesta;

            $metodo = self::CHECK_ACCIONES_SOBRE_EXPEDIENTE;
            $this->respuesta = $this->apis[$keyApi]->$metodo($idexpediente);
            return $this->respuesta;
        }
        catch (\Exception $e) {
            $this->respuesta->addMensaje($e->getMessage(),ContenedorRespuesta::MENSAJE_ERROR);
            $this->respuesta->setEstado(false);
            return $this->respuesta;
        }
    }

    private function cruzaListaMetodos(Array $listaMetodos) {

        $metodosDisponibles = array();

        foreach ($listaMetodos as $metodo)
            if (in_array($metodo,self::ACCIONES))
                $metodosDisponibles[] = $metodo;

        return $metodosDisponibles;

    }

    private function obtieneIdApiConMatriculaFecha($matricula, $fecha) {

        /**
         * @var $vehiculo Vehiculos
         */
        $vehiculo = $this->emGb4->getRepository('AppBundle:Vehiculos')->findOneBy(array(
            'matricula' => $matricula
        ));

        if (!$vehiculo)
            return false;

        /**
         * @var $fecha \DateTime
         */
        $fecha = \DateTime::createFromFormat('Y/m/d H:i',$fecha);

        $historicos = $this->emGb4->getRepository('AppBundle:Historicovehiculos')
            ->findHistoricosPorVehiculoFecha($vehiculo, $fecha);

        if (!$historicos)
            return false;

        $hvTit = array();

        /**
         * @var $historico Historicovehiculos
         */
        foreach ($historicos as $historico) {
            // Sacamos los titulares de los historicos encontrados
            if (!array_key_exists($historico->getIdtitular()->getIdtitulares(),$hvTit))
                $hvTit[$historico->getIdtitular()->getIdtitulares()][]=$historico;
        }
        // En teoria, para una fecha concreta solo debe salir un titular.

        if (count($hvTit)>1)
            throw new \Exception('Vehiculo con mas de un titular en esa fecha');

        /**
         * @var $hv Historicovehiculos
         */
        $hv = array_shift($hvTit);
        $hv = array_shift($hv);
        // Para sacar el tipo de titular, es mas fiable sacarlo desde el historico
        switch ($hv->getTipotitular()) {
            case 'r':
                $idAPI = $this->obtieneIdApiTitularRenting($hv->getIdtitular());
                break;

            case 'rc':
//                $idAPI = null;
                $idAPI = $this->obtieneIdApititularRentacar($hv->getIdtitular());
                break;
        }

        return $idAPI;

    }

    private function obtieneIdApiConIdExpediente($idexpediente) {

        $expediente = $this->emGb4->getRepository('AppBundle:Expedientes')->findOneBy(array(
            'idexpedientes' => $idexpediente
        ));

        if (!$expediente)
            throw new \RuntimeException('ApiExtManager - Expediente inexistente');

        // Solamente si el expediente esta en identificado o inferior se intenta dar de alta la sancion en servicios externos
        // Esto en teoria viene validado ya desde el front.
        // TODO - Orden en lugar de idestadosexpediente
//        if ($expediente->getIdestado()->getIdestadosexpediente() > 4) {
//            return false;
//        }

        // Se comprueba el tipo de proveedor
        /**
         * @var $proveedor Titulares
         */
        $proveedor = $expediente->getIdtitulares();


        /**
         * @var $titularConfigProveedor Titularconfig
         */
        $titularConfigProveedor = $proveedor->getTitularconfig();

        if ($titularConfigProveedor->getTipo() == 'r') {

            // Si el proveedor es tipo renting, hacemos comprobaciones extra del expediente.
            if (!$this->evaluaExpedienteRenting($expediente))
                return false;

            return $this->obtieneIdApiTitularRenting($expediente->getIdprocedimientos()->getIdhistoricovehiculos()->getIdtitular());
        }
        else if ($titularConfigProveedor->getTipo() == 'rc')
            return $this->obtieneIdApititularRentacar($proveedor);

    }

    private function obtieneIdApiTitularRenting(Titulares $titular) {

        $mtmtitularconfig = $this->emGb4->getRepository('AppBundle:Mtmtitularesconfig')->findOneBy(array(
            'idtitulares' => $titular
        ));

        if (!$mtmtitularconfig)
            return null;

        if ($mtmtitularconfig->getApiext()!=null &&
            array_key_exists($mtmtitularconfig->getApiext(),$this->apis))
            return $mtmtitularconfig->getApiext();
        else
            return null;

    }

    private function obtieneIdApititularRentacar(Titulares $titular) {
        /**
         * @var $titularconfig Titularconfig
         */
        $titularconfig = $this->emGb4->getRepository('AppBundle:Titularconfig')->findOneBy(array(
            'idtitulares' => $titular
        ));

        if (!$titularconfig)
            return null;


        if ($titularconfig->getApiext() != null &&
            array_key_exists($titularconfig->getApiext(),$this->apis))
            return $titularconfig->getApiext();
        else
            return null;

    }

    /**
     * @param Expedientes $expediente
     *
     * Los expedientes de renting tienen condiciones especificas que hay que evaluar
     * para ver si es cosa nuestra o no.
     */
    public function evaluaExpedienteRenting(Expedientes $expediente) {

        // TODO - Y EMPEZAMOS PRONTO CON LAS FUCKING ÑAPAS; ME CAGO EN MI VIDA.
        // Si el expediente es legal y el denunciado es el mismo que el proveedor (no api disponible)
        // Si el organismo es francia y el denunciado es el mismo que el proveedor. (no api disponible)

        /**
         * @var $denunciado[] Expedientesdenunciado
         */
        $denunciado = $this->emGb4->getRepository('AppBundle:Expedientesdenunciado')
            ->findBy(array(
                'idexpedientes' => $expediente->getIdexpedientes()
            ));

        if (!$denunciado)
            return true;

        /**
         * @var $escritoEntrada Escritoentrada
         */
        $escritoEntrada = $expediente->getIdescritoentrada()->getCodigo();
        if ($escritoEntrada != 'RI' && $escritoEntrada != 'CDP')
            if (is_array($denunciado))
                foreach ($denunciado as $d)
                    if ($d->getIdhistoricopersonas()->getIdpersonas()->getNif() == $expediente->getIdtitulares()->getNif())
                        return false;

        if ($expediente->getIdorganismos()->getIdorganismos() == 8338791)
            return false;

        return true;

    }

    private function compruebaAccionEnAPI($accion,$keyApi) {
        return method_exists($this->apis[$keyApi],$accion);
    }



}