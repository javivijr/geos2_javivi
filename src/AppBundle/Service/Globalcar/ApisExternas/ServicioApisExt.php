<?php

namespace AppBundle\Service\Globalcar\ApisExternas;


class ServicioApisExt
{
    public $apis = array();

    public function __construct(iterable $apis) {
        foreach ($apis as $api)
            $this->apis[$api::idApi] = $api;
    }

    public function getApis() {
        return $this->apis;
    }
}