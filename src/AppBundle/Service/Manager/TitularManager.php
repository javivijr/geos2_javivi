<?php


namespace AppBundle\Service\Manager;


use Symfony\Bridge\Doctrine\RegistryInterface;

class TitularManager
{
    private $emGb4;
    private $titularRepository;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->titularRepository = $this->emGb4->getRepository('AppBundle:Titulares');
    }

    public function titularFindOneBy(array $criteria)
    {
        if (count($criteria)==0)
            return null;

        $resultado = $this->titularRepository->findOneBy($criteria);
        if ($resultado)
            return $resultado;
        return null;
    }


}