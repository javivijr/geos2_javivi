<?php

namespace AppBundle\Service\Manager;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractManagerDoctrine
{

    /**
     * @var $emGb4 \Doctrine\Common\Persistence\ObjectManager
     */
    protected $em;

    /**
     * @var $repo EntityRepository
     */
    protected $repo;

    abstract public function __construct(RegistryInterface $doctrine);

    public function findBy(array $criteria, $orderBy = null, $limit = null, $offset = null) {
        if (count($criteria)==0)
            return null;

        $resultado = $this->repo->findBy($criteria, $orderBy, $limit, $offset);

        if ($resultado)
            return $resultado;
        return null;
    }
    public function findOneBy(array $criteria) {
        if (count($criteria)==0)
            return null;

        $resultado = $this->repo->findOneBy($criteria);

        if ($resultado)
            return $resultado;
        return null;
    }

    public function guardar($entidad, bool $flush) {
        $this->em->persist($entidad);
        if ($flush)
            $this->em->flush();
    }
}