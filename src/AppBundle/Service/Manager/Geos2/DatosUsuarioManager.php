<?php

namespace AppBundle\Service\Manager\Geos2;


use AppBundle\Clase\Interfaces\ManagerRepositoryInterface;
use AppBundle\Entity\Geos2\DatosUsuario;
use AppBundle\Entity\Geos2\Usuario;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DatosUsuarioManager
{
    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;

    /**
     * @var $usuarioRepository EntityRepository
     */
    private $usuarioRepository;

    /**
     * @var $datosUsuarioRepository EntityRepository
     */
    private $datosUsuarioRepository;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->emGeos2 = $doctrine->getManager('geos2');
        $this->usuarioRepository = $this->emGeos2->getRepository('AppBundle:Usuario');
        $this->datosUsuarioRepository = $this->emGeos2->getRepository('AppBundle:DatosUsuario');
    }

    public function getDatosUsuario($usuario) {
        /**
         * @var $usuario Usuario
         */
        $usuario = $this->usuarioRepository->findOneBy(array(
            'username' => $usuario->getUsername()
        ));
        if (!$usuario)
            throw new \RuntimeException('DatosUsuarioManager - Usuario inexistente');

        /**
         * @var $datosUsuario DatosUsuario
         */
        $datosUsuario = $this->datosUsuarioRepository->findOneBy(array(
            'usuario' => $usuario
        ));
        if (!$datosUsuario) {
            $datosUsuario = new DatosUsuario();
            $datosUsuario->setUsuario($usuario);
            $this->persistDatosUsuario($datosUsuario);
        }

        return $datosUsuario;
    }

    public function persistDatosUsuario(DatosUsuario $datos) {
        $this->emGeos2->persist($datos);
        $this->emGeos2->flush();
    }

}