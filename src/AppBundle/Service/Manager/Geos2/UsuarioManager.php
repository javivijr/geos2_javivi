<?php

namespace AppBundle\Service\Manager\Geos2;


use AppBundle\Clase\Interfaces\ManagerRepositoryInterface;
use AppBundle\Entity\Geos2\Usuario;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UsuarioManager implements ManagerRepositoryInterface
{

    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;
    /**
     * @var $usuarioRepository EntityRepository
     */
    private $usuarioRepository;

    public function __construct(RegistryInterface $doctrine) {
        $this->emGeos2 = $doctrine->getManager('geos2');
        $this->usuarioRepository = $this->emGeos2->getRepository('AppBundle:Usuario');
    }

    /**
     * @param array $criteria ($campo => $valor)
     * @return null|Usuario
     */
    public function findOneBy(array $criteria) {

        if (count($criteria)==0)
            return null;

        /**
         * @var $usuario Usuario
         */
        $usuario = $this->usuarioRepository->findOneBy($criteria);
        if ($usuario)
            return $usuario;
        return null;
    }

    public function findBy(array $criteria)
    {
        // TODO: Implement findBy() method.
    }

    public function findAll()
    {
        // TODO: Implement findAll() method.
    }
}