<?php

namespace AppBundle\Service\Manager;

use AppBundle\Entity\Globalcar\Preexpedientes;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PreexpedienteManager
{
    const TIPO_IDENTIFICACION = 'ident';

    private $emGb4;
    private $titularRepository;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->titularRepository = $this->emGb4->getRepository('AppBundle:Preexpedientes');
    }

    public function preexpedienteFindOneBy(array $criteria)
    {
        if (count($criteria)==0)
            return null;

        $resultado = $this->titularRepository->findOneBy($criteria);
        if ($resultado)
            return $resultado;
        return null;
    }

    public function persist(Preexpedientes $preexpediente, $flush) {
        $this->emGb4->persist($preexpediente);
        if ($flush)
            $this->emGb4->flush();
    }

}
