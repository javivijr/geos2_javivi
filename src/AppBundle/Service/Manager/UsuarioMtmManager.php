<?php

namespace AppBundle\Service\Manager;


use Symfony\Bridge\Doctrine\RegistryInterface;

class UsuarioMtmManager
{
    private $emGb4;
    private $usuarioMtmRepository;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->usuarioMtmRepository = $this->emGb4->getRepository('AppBundle:Usuariosmtm');
    }

    public function usuarioMtmFindOneBy(array $criteria)
    {
        if (count($criteria)==0)
            return null;

        $resultado = $this->usuarioMtmRepository->findOneBy($criteria);
        if ($resultado)
            return $resultado;
        return null;
    }
}