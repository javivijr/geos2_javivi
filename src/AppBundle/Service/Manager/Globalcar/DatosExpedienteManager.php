<?php

namespace AppBundle\Service\Manager\Globalcar;

use AppBundle\Entity\Geos2\DatosExpediente;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DatosExpedienteManager
{

    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;

    /**
     * @var $repoGb4 EntityRepository
     */
    private $repoGb4;

    /**
     * @var $repoGeos2 EntityRepository
     */
    private $repoGeos2;

    public function __construct(RegistryInterface $doctrine) {
        $this->emGeos2 = $doctrine->getManager('geos2');
        $this->repoGb4 = $doctrine->getManager('gb4')->getRepository('AppBundle:Expedientes');
        $this->repoGeos2 = $doctrine->getManager('geos2')->getRepository('AppBundle:DatosExpediente');
    }

    /**
     * @param $idexpediente
     * @return DatosExpediente|null|DatosExpediente
     *
     * Este metodo devuelve el registro asociado a un expediente de la tabla
     * datos_expediente, si no existira, instancia uno nuevo y lo devuelve.
     *
     */
    public function getDatosExpediente($idexpediente) {
        $expedienteGb4 = $this->repoGb4->findOneBy(array(
            'idexpedientes' => $idexpediente
        ));
        if (!$expedienteGb4)
            throw new \RuntimeException('DatosExpedienteManager - Expediente inexistente');

        /**
         * @var $datosExpediente DatosExpediente
         */
        $datosExpediente = $this->repoGeos2->findOneBy(array(
            'idexpediente' => $idexpediente
        ));
        if (!$datosExpediente) {
            $datosExpediente = new DatosExpediente();
            $datosExpediente->setIdexpediente($idexpediente);
        }

        return $datosExpediente;
    }

    public function persistDatosExpediente($ent) {
        $this->emGeos2->persist($ent);
        $this->emGeos2->flush();
    }
}