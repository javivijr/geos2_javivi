<?php

namespace AppBundle\Service\Manager\Globalcar;


use AppBundle\Service\Manager\AbstractManagerDoctrine;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ExpedienteManager extends AbstractManagerDoctrine
{

    public function __construct(RegistryInterface $doctrine) {
        $this->em = $doctrine->getManager('gb4');
        $this->repo = $doctrine->getManager('gb4')->getRepository('AppBundle:Expedientes');
    }

}