<?php

namespace AppBundle\Service\Manager;


use AppBundle\Clase\Interfaces\ManagerRepositoryInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VehiculoManager
{

    private $emGb4;
    private $vehiculoRepository;
    private $historicoVehiculoRepository;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->emGb4 = $doctrine->getManager('gb4');
        $this->vehiculoRepository = $this->emGb4->getRepository('AppBundle:Vehiculos');
        $this->historicoVehiculoRepository = $this->emGb4->getRepository('AppBundle:Historicovehiculos');
    }

    public function vehiculoFindOneBy(array $criteria)
    {
        if (count($criteria)==0)
            return null;

        $resultado = $this->vehiculoRepository->findOneBy($criteria);
        if ($resultado)
            return $resultado;
        return null;
    }

    public function vehiculoFindBy(array $criteria)
    {
        // TODO: Implement findBy() method.
    }

    public function vehiculoFindAll()
    {
        // TODO: Implement findAll() method.
    }

    public function historicoVehiculoFindBy(array $criteria) {
        if (count($criteria)==0)
            return null;

        $resultado = $this->historicoVehiculoRepository->findOneBy($criteria);
        if ($resultado)
            return $resultado;
        return null;
    }
}