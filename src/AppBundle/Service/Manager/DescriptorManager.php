<?php

namespace AppBundle\Service\Manager;


use Symfony\Bridge\Doctrine\RegistryInterface;

class DescriptorManager extends AbstractManagerDoctrine
{

    public function __construct(RegistryInterface $doctrine)
    {
        $this->em = $doctrine->getManager('gb4');
        $this->repo = $doctrine->getManager('gb4')->getRepository('AppBundle:Descriptores');
    }
}