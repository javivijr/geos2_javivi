<?php

namespace AppBundle\Service;

use AppBundle\Entity\Geos2\DatosApisExt;
use Symfony\Bridge\Doctrine\RegistryInterface;

class LoggingApiExt
{

    // Ids Apis
    const DALKOM = 1;
    const FURGOLINE = 2;

    // ids Claves
    const MATRICULA = 1;
    const EXPEDIENTE = 2;

    private $doctrine;
    private $emGeos2;

    public function __construct(RegistryInterface $doctrine) {
        $this->doctrine = $doctrine;
        $this->emGeos2 = $doctrine->getManager('geos2');
    }

    public function guardaLog($llamada, $apiId, $idClave, $clave, $datosEntrada, $datosSalida) {
        // TODO - Este insert, en un futuro, lo suyo es mandarlo a una cola de rabbitmq
        $entidad = new DatosApisExt();
        $entidad->setApiId($apiId);
        $entidad->setIdLlamada($llamada);
        $entidad->setIdClave($idClave);
        $entidad->setClave($clave);
        $entidad->setFechaLlamada(new \DateTime('now'));
        $entidad->setDatosEntrada($datosEntrada);
        $entidad->setDatosSalida($datosSalida);
        $this->emGeos2->persist($entidad);
        $this->emGeos2->flush();

        return $entidad->getId();
    }

}