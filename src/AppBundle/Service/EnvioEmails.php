<?php

namespace AppBundle\Service;


use AppBundle\Model\ContenedorEmail;
use Swift_RfcComplianceException;

class EnvioEmails
{

    const AMAZON_SES = 1;
    const GMAIL_OAUTH2 = 2;
    const MANDRILL = 3;

    private $entorno;
    private $mailerSes;
    /**
     * @var $mailers \Swift_Mailer[]
     */
    private $mailers = array();

    public function __construct($entorno, \Swift_Mailer $mailerSes, $mailerMandrill) {
        $this->entorno = $entorno;
        $this->mailerSes = $mailerSes;
        $this->mailers[self::AMAZON_SES] = $mailerSes;
        $this->mailers[self::MANDRILL] = $mailerMandrill;
    }

    /**
     * enviarEmail.
     *
     * Metodo que envia un email.
     *
     * @author  Alejandro
     *
     * @since 1.0
     *
     * @param  $email	contenedorEmail			Datos del email a enviar
     * @return bool		True si se envio correctamente, false en caso contrario, produce una excepcion si hay algun error
     */
    public function enviarEmail(contenedorEmail $email, &$hashmensaje=null) {
        \Swift_Preferences::getInstance()->setCacheType('array');
        \Swift_Preferences::getInstance()->setCharset('UTF-8');
        $transport = null;
        $mailer = null;
        try{
//            if ($this->entorno == 'prod' || $this->entorno == 'test') {

//                if ($email->getMetodoenvio() == self::AMAZON_SES) {
//                    $transport = 'AMAZONSES'; // ÑAPA PARA FORZAR QUE USE EL DE SYMFONY2
//                    $para = array_keys($email->getTo());
//                    if(preg_match('|@gesthispania|i',$para[0])) {
//                        // ES UN CORREO DE USO INTERNOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO, NO USAMOS AMAZON SES, USAMOS SENDMAIL
//                        $mailer = $this->container->get('swiftmailer.mailer.sendmail');
//                    }
//                    //}
//                }else if($email->getMetodoenvio() == self::GMAIL_OAUTH2){
//                    $gapi = new GoogleApiController();
//                    $gapi->setContainer($this->container);
//                    $transport = $gapi->getTransportSwiftOauth($email->getFrom());
//                }else if($email->getMetodoenvio() == self::MANDRILL){
//                    $mailer = $this->container->get('swiftmailer.mailer.mandrill');
//                }
//                else ($transport === null && $mailer === null)
//                    throw new \Exception('El metodo de envio no es correcto');

//            }
            //validamos los datos
            if ($email->getAsunto() == '')
                throw new \Exception('Debe indicar un asunto para el correo');
            if ($email->getBodyhtml() == '' && $email->getBodytxt() == '')
                throw new \Exception('Debe indicar el cuerpo del mensaje');
            if ($email->getBodyhtml() != '' && $email->getBodytxt() == '')
                $email->setBodytxt('Email no disponible en formato texto, consulte con el remitente o actualice su gestor de correo para que permita leer emails en formato HTML');
            if (count($email->getTo()) == 0)
                throw new \Exception('Debe indicar un destinatario');
            if ($email->getFrom() == '')
                throw new \Exception('Debe indicar el campo from del email');

            //enviamos el email
            $mensaje = \Swift_Message::newInstance();
            $mensaje->setCharset('UTF-8');
            $mensaje->setSubject(preg_replace('[\n|\r|\n\r|<br />|<br>]','',$email->getAsunto()));
            $mensaje->setFrom(preg_replace('[\n|\r|\n\r|<br />|<br>]', '', $email->getFrom()));
            $mensaje->setTo($email->getTo());
            if (count($email->getCc()) > 0)
                $mensaje->setCc($email->getCc());
            if (count($email->getBcc()) > 0)
                $mensaje->setBcc($email->getBcc());

            /**********************************************************************
             * Recogemos los archivos incrustados para llamar al método embed de
             * Swiftmailer y vamos sustituyendo cada ancla que nos encontremos en
             * el bodyhtml con el formato "~incrustado[index]~" por el tag:
             * <img src='[ruta devuelta por el método embed]'>
             * @fecha:23-03-16  @autor:Raul
             *
             */

            if (count($email->getIncrustados()) > 0 && $email->getBodyhtml() != '') {
                $incrustados = $email->getIncrustados();

                $cid_index=0;   //indice de archivo incrustado

                foreach ($incrustados as $k => $incrustado) {
                    //$cid = $mensaje->embed(Swift_Image::fromPath($incrustado));
                    var_dump($k);
                    var_dump($incrustado);

                    $embswift = \Swift_Image::fromPath($incrustado);

                    $cid = $mensaje->embed($embswift);

                    //var_dump($cid);

                    if($cid){

                        $imgEmbed="<img src='".$cid."'>";

                        $cuerpoIncrustados=$email->getBodyhtml();

                        //$cuerpoIncrustadosImg=str_replace("~incrustado".$cid_index."~",$imgEmbed,$cuerpoIncrustados);
                        $cuerpoIncrustadosImg=str_replace("~incrustado".$cid_index."~",$imgEmbed,$cuerpoIncrustados);

                        //var_dump($cuerpoIncrustadosImg);

                        $email->setBodyhtml($cuerpoIncrustadosImg);

                        $cid_index++;
                    }
                }
            }
            /**********************************************************************
             * Fin recogida de archivos incrustados
             */

            //hay datos incrustados en el email?
            if(count($email->getIncrustados())>0){
                //el body se añade con el método setBody()
                if ($email->getBodyhtml() != '')
                    $mensaje->setBody($email->getBodyhtml(), 'text/html');//$mensaje->addPart($email->getBodyhtml(), 'text/html');
            }else{
                //si no, se usa el código existente
                $mensaje->setBody($email->getBodytxt(), 'text/plain');
                if ($email->getBodyhtml() != '')
                    $mensaje->addPart($email->getBodyhtml(), 'text/html');
            }

            // Adjuntos por ruta de archivo
            if (count($email->getAdjuntos()) > 0) {
                $adjuntos = $email->getAdjuntos();
                foreach ($adjuntos as $k => $adj) {
                    $adjswift = \Swift_Attachment::fromPath($adj);
                    if (!is_numeric($k)) {
                        $adjswift->setFilename($k);
                    }
                    $mensaje->attach($adjswift);
                }
            }
            // Adjuntos con el contenido del fichero
            if (count($email->getAdjuntosDatos()) > 0) {
                $adjuntos = $email->getAdjuntosDatos();
                foreach ($adjuntos as $nombre => $contenido) {
                    $adjswift = new \Swift_Attachment($contenido,$nombre);
                    $mensaje->attach($adjswift);
                }
            }

            if($email->getReplyTo()!=''){
                $headers = $mensaje->getHeaders();
                $headers->addTextHeader('Reply-To', '<'.$email->getReplyTo().'>');
            }
            if($email->getRespuestaTo()!=''){
                $headers = $mensaje->getHeaders();
                $headers->addTextHeader('Disposition-Notification-To', '<'.$email->getRespuestaTo().'>');
            }
            $customheaders = $email->getAllCustomHeaders();
            if(count($customheaders)>0){
                foreach($customheaders as $k => $ch){
                    $headers = $mensaje->getHeaders();
                    $headers->addTextHeader($k, $ch);
                }
            }

//            if($mailer !== null) {
//                $cuantos = $mailer->send($mensaje);
//            } elseif ($transport !== null && $transport !== 'AMAZONSES') {
//                $cuantos = $this->container->get('mailer')->newInstance($transport)->send($mensaje);
//            } else {
//                try {
//                    $cuantos = $this->container->get('mailer')->send($mensaje);
//                } catch (\Swift_TransportException $e) {
//                    $cuantos = $this->container->get('swiftmailer.mailer.gmailreserva')->send($mensaje);
//                } catch (\Exception $e) {
//                    $cuantos = $this->container->get('swiftmailer.mailer.gmailreserva')->send($mensaje);
//                }
//            }

            $cuantos = $this->mailers[$email->getMetodoenvio()]->send($mensaje);


            $hashmensaje = spl_object_hash($mensaje);
            return ($cuantos > 0);
        } catch (Swift_RfcComplianceException $e) {
            throw new \Exception('Error de validación del email');
        }
    }
}