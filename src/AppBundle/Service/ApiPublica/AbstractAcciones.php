<?php

namespace AppBundle\Service\ApiPublica;


use AppBundle\Model\RespuestaAPIPublica;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

abstract class AbstractAcciones
{
    /**
     * @var \Symfony\Component\Validator\Validator\ValidatorInterface
     */
    protected $validator;

    protected $asserts;

    /**
     * @var $doctrine Registry
     */
    protected $doctrine;
    /**
     * @var $respuesta RespuestaAPIPublica
     */
    protected $respuesta;

    protected $version;
    protected $accion;

    public function __construct(RegistryInterface $doctrine, RespuestaAPIPublica $respuesta) {
        $this->validator = Validation::createValidator();
        $this->doctrine = $doctrine;
        $this->respuesta = $respuesta;
    }

    abstract protected function setAsserts();
    abstract public function validaDatosEntrada($datos): ConstraintViolationListInterface;
    abstract public function procesaLlamada($datos): RespuestaAPIPublica;
    abstract public function getAccion();
    abstract public function getVersion();
    abstract public function getPermiso();



}