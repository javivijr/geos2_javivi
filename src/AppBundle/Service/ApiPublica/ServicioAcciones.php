<?php

namespace AppBundle\Service\ApiPublica;


class ServicioAcciones
{

    public $acciones = array();

    public function __construct(iterable $acciones) {
        foreach ($acciones as $accion)
            /**
             * @var $accion AbstractAcciones
             */
            $this->acciones[$accion->getVersion()][$accion->getAccion()] = $accion;
    }

    public function getAcciones() {
        return $this->acciones;
    }

}