<?php

namespace AppBundle\Service\ApiPublica;


use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

abstract class AccionApiPublica
{
    /**
     * @var \Symfony\Component\Validator\Validator\ValidatorInterface
     */
    protected $validator;

    protected $asserts;
    protected $version;
    protected $accion;
    protected $permiso;

    public function __construct() {
        $this->validator = Validation::createValidator();
    }

    public function validaDatosEntrada($datos): ConstraintViolationListInterface {
        return $this->validator->validate($datos, $this->asserts);
    }

    abstract protected function setAsserts();
    abstract public function procesaLlamada($datos) : array;
    public function getAccion() {
        return $this->accion;
    }
    public function getVersion() {
        return $this->version;
    }
    public function getPermiso() {
        return $this->permiso;
    }

}