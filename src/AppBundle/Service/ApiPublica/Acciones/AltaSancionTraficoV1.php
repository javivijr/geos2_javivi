<?php

namespace AppBundle\Service\ApiPublica\Acciones;

use AppBundle\Entity\Geos2\DatosUsuario;
use AppBundle\Entity\Geos2\Usuario;
use AppBundle\Entity\Globalcar\Preexpedientes;
use AppBundle\Model\ContenedorEmail;
use AppBundle\Model\FilesystemModel;
use AppBundle\Model\RespuestaAPIPublica;
use AppBundle\Service\ApiPublica\AbstractAcciones;
use AppBundle\Service\EnvioEmails;
use AppBundle\Service\Wrap\SistemaArchivos;
use AppBundle\Validation\FileBase64String;
use AppBundle\Validation\Emails;
use AppBundle\Model\Globalcar\ConstantesDatosGB;
use Swift_Mailer;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Model\MTM\ConstantesDatosMTM;


class AltaSancionTraficoV1 extends AbstractAcciones
{

    /**
     * @var $fm SistemaArchivos
     */
    protected $fm;
    protected $permiso = "ALTA_SANCION";

    /**
     * @var $mailer Swift_Mailer
     */
    protected $mailer;
    /**
     * @var $twig \Twig_Environment
     */
    protected $twig;

    protected $version = 'v1';
    protected $accion = 'altasanciontrafico';

    public function __construct(
        RegistryInterface $doctrine,
        RespuestaAPIPublica $respuesta,
        SistemaArchivos $fm,
        EnvioEmails $mailer,
        \Twig_Environment $twig) {

        $this->setAsserts();
        $this->fm = $fm;
        $this->twig = $twig;
        $this->mailer = $mailer;

        parent::__construct($doctrine, $respuesta);
    }

    public function getPermiso() {
        return $this->permiso;
    }

    protected function setAsserts() {
        /**
         * @var $asserts Assert\Collection
         */
        $this->asserts = new Assert\Collection(
            array(
                'fields' => array(
                    'Fecha_Notificacion' => new Assert\Required(array (
                        new Assert\DateTime( array(
                            'message' => 'Formato de fecha incorrecto',
                            'format' => 'Y-m-d\TH:i:s'
                        )),
                        new Assert\NotBlank(array(
                            'message' => 'Campo obligatorio'
                        ))
                    )),
                    'Matricula' => new Assert\Required(array(
                        new Assert\NotBlank(array(
                            'message' => 'Campo obligatorio'
                        ))
                    )),
                    'Contenido_Documento' => new Assert\Required(array(
                        new FileBase64String(array(
                            'maxsize' => 8*1024*1024  // 8MB de tamaño maximo
                        )),
                        new Assert\NotBlank(array(
                            'message' => 'Campo obligatorio'
                        ))
                    )),
                    'Email' => new Assert\Optional(array(
                        new Emails(array(
                            'message' => 'Email invalido.'
                        )),
                        new Assert\NotBlank(array(
                            'message' => 'Si el campo email va incluido no puede ir vacío.'
                        ))
                    )),
                    //'File' => new Assert\Optional(new Assert\File())
                ),
                'allowExtraFields' => true,
                'missingFieldsMessage' => 'Campo obligatorio'
            )
        );
    }

    public function validaDatosEntrada($datos): ConstraintViolationListInterface {
        return $this->validator->validate($datos, $this->asserts);
    }

    public function getAccion() {
        return $this->accion;
    }
    public function getVersion()
    {
        return $this->version;
    }

    public function procesaLlamada($datos): RespuestaAPIPublica
    {
        /**
         * @var $this->doctrine Registry
         */
        $em_gb4mtm41 = $this->doctrine->getManager('gb4');
        $em_geos2 = $this->doctrine->getManager('geos2');

        // TODO - Por ahora se va a enlazar el usuario de la API con el titular de GEOS por su Idtitular.
        /**
         * @var $usuario Usuario
         */
        $usuario = $em_geos2->getRepository('AppBundle:Usuario')->findOneBy(array(
            'username' => $datos['Usuario']
        ));

        /**
         * @var $datosUsuario DatosUsuario
         */
        $datosUsuario = $em_geos2->getRepository('AppBundle:DatosUsuario')->findOneBy(array(
            'usuario' => $usuario->getId()
        ));

        /* TODO - Para esta llamada necesitamos el titular asociado de Globalcar y el Usuario asociado de MTM
         * Como todavia no tenemos hecho el diseño de la BB.DD., por ahora he creado una tabla, datos_usuario,
         * en la que cada user tendra un array de datos.
        */

        if (!$datosUsuario->hasDato(ConstantesDatosGB::$GB_IDTITULAR))
            throw new HttpException(401,"Titular no asociado.");
        if (!$datosUsuario->hasDato(ConstantesDatosMTM::$MTM_IDUSUARIO))
            throw new HttpException(401,"Usuario MTM no asociado.");

        $vehiculo = $em_gb4mtm41->getRepository('AppBundle:Vehiculos')->findOneBy(array(
            'matricula' => $datos['Matricula']
        ));

        if ($vehiculo)
            $historicos = $em_gb4mtm41->getRepository('AppBundle:Historicovehiculos')->findOneBy(array(
                'idvehiculo' => $vehiculo->getIdvehiculos(),
                'idtitular' => $datosUsuario->getDatos()[ConstantesDatosGB::$GB_IDTITULAR]
            ));

        if (!$vehiculo || !$historicos) {
            $this->respuesta->addError(array('[Matricula]:' => 'Vehículo no encontrado.'));
            $this->respuesta->setCodigoRespuesta(404);
            return $this->respuesta;
        }

        $arrendatario = $em_gb4mtm41->getRepository('AppBundle:Titulares')->findOneBy(array(
            'idtitulares' => $datosUsuario->getDatos()[ConstantesDatosGB::$GB_IDTITULAR]
        ));

        $titular = $em_gb4mtm41->getRepository('AppBundle:Titularesproveedores')->findOneBy(array(
            'idtitulares' => $arrendatario->getIdtitulares()
        ));

        $usuarioMtm = $em_gb4mtm41->getRepository('AppBundle:Usuariosmtm')->findOneBy(array(
            'idusuariosmtm' => $datosUsuario->getDatos()[ConstantesDatosMTM::$MTM_IDUSUARIO]
        ));

        /**
         * @var $preexp Preexpedientes
         */
        $preexp = new Preexpedientes();
        $fechaNotificacion = \DateTime::createFromFormat('Y-m-d\TH:i:s',$datos['Fecha_Notificacion']);

        $preexp->setFechanotificacion($fechaNotificacion);
        $preexp->setIdarrendatario($arrendatario);
        $preexp->setIdtitulares($titular->getIdproveedor());
        $preexp->setIdvehiculos($vehiculo);
        $fechaAlta = new \DateTime('now');
        $preexp->setFechaalta($fechaAlta);
        $preexp->setTipo('ident');
        $preexp->setObservaciones('API - Usuario:'.$usuario->getUsername().' - Email:'.$usuario->getEmail());
        $preexp->setIdusuariosmtm($usuarioMtm);

        $nombreFichero = 'API-'.rand().'-AltaSancionTraficoV1-'.$usuario->getUsername().'-'.date('Y-m-d\TH:i:s').'.pdf';

        $fsm = new FilesystemModel($this->fm->getAdaptador('amazonS3FS'));
        $this->fm->setFileSystem($fsm);
        $escritura = $this->fm->escribeFichero(
            $fsm,
            $nombreFichero,
            base64_decode($datos['Contenido_Documento'])
        );

        if (!$escritura)
            throw new \RuntimeException('Error escribiendo fichero.');

        $preexp->setRuta($escritura);

        try {
            $em_gb4mtm41->persist($preexp);
            $em_gb4mtm41->flush();
        }
        catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }

        // Se comprueba solamente si existe, porque en caso de existir ya se ha validado.
        if (isset($datos['Email'])) {

            $mensaje = new ContenedorEmail();
            $mensaje->setMetodoenvio(EnvioEmails::MANDRILL);
            $mensaje->setAsunto('Gesthispania - API - Alta de sanción');
//            $mensaje->setBodytxt("Probando alta de marron - Paga la multa, primer aviso.");
            $mensaje->setBodyhtml($this->twig->render('@App/ApiPublica/AltaSancionTraficoV1.html.twig',
                array(
                    'idpreexp' => $preexp->getIdpreexpedientes(),
                    'fechanotif' => $preexp->getFechanotificacion()->format('d-m-Y'),
                    'fechaalta' => $preexp->getFechaalta()->format('d-m-Y')
                )));
            $mensaje->setFrom('noreply@gesthispania.com');

            $arrayEmails = explode(',',$datos['Email']);

            // TODO - temporalmente que nos ponga en copia.
            $arrayEmails[] = 'tecnologia@gesthispania.com';
            
            foreach ($arrayEmails as $email)
                $mensaje->addTo($email);
            
            // TODO - Temporalmente tecnologia en copia.
            $mensaje->addTo('tecnologia@gesthispania.com');
            
            $mensaje->addAdjuntosDatos(base64_decode($datos['Contenido_Documento']),rand().'.pdf');

            $this->mailer->enviarEmail($mensaje);

        }

        $this->respuesta->addValor(array(
            'idPreexpediente' => $preexp->getIdpreexpedientes(),
            'FechaNotificacion' => $fechaNotificacion->format('Y-m-d\TH:i:s'),
            'FechaRecepcion' => $fechaAlta->format('Y-m-d\TH:i:s')
        ));

        $this->respuesta->setCodigoRespuesta(201);
        return $this->respuesta;
    }

}