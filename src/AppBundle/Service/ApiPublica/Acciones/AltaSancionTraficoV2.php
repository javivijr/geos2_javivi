<?php
namespace AppBundle\Service\ApiPublica\Acciones;

use AppBundle\Entity\Globalcar\Preexpedientes;
use AppBundle\Entity\Globalcar\Titulares;
use AppBundle\Entity\Globalcar\Vehiculos;
use AppBundle\Model\ContenedorEmail;
use AppBundle\Model\FilesystemModel;
use AppBundle\Model\Globalcar\ConstantesDatosGB;
use AppBundle\Model\MTM\ConstantesDatosMTM;
use AppBundle\Service\ApiPublica\AccionApiPublica;
use AppBundle\Service\EnvioEmails;
use AppBundle\Service\Manager\Geos2\DatosUsuarioManager;
use AppBundle\Service\Manager\Geos2\UsuarioManager;
use AppBundle\Service\Manager\Globalcar\DatosExpedienteManager;
use AppBundle\Service\Manager\PreexpedienteManager;
use AppBundle\Service\Manager\TitularManager;
use AppBundle\Service\Manager\UsuarioMtmManager;
use AppBundle\Service\Manager\VehiculoManager;
use AppBundle\Service\Wrap\SistemaArchivos;
use AppBundle\Validation\Emails;
use AppBundle\Validation\FileBase64String;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints as Assert;

class AltaSancionTraficoV2 extends AccionApiPublica
{

    /**
     * @var DatosExpedienteManager DatosExpedienteManager
     */
    private $datosExpedienteManager;

    /**
     * @var $datosUsuarioManager DatosUsuarioManager
     */
    private $datosUsuarioManager;

    /**
     * @var $usuarioManager UsuarioManager
     */
    private $usuarioManager;

    /**
     * @var $vehiculoManager VehiculoManager
     */
    private $vehiculoManager;

    /**
     * @var $titularManager TitularManager
     */
    private $titularManager;

    /**
     * @var $usuarioMtmManager UsuarioMtmManager
     */
    private $usuarioMtmManager;
    /**
     * @var $preexpedienteManager PreexpedienteManager
     */
    private $preexpedienteManager;
    /**
     * @var $fm SistemaArchivos
     */
    private $fm;
    /**
     * @var $mailer EnvioEmails
     */
    private $mailer;
    /**
     * @var $twig \Twig_Environment
     */
    private $twig;

    public function __construct(
        DatosExpedienteManager $datosExpedienteManager,
        UsuarioManager $usuarioManager,
        DatosUsuarioManager $datosUsuarioManager,
        VehiculoManager $vehiculoManager,
        TitularManager $titularManager,
        UsuarioMtmManager $usuarioMtmManager,
        PreexpedienteManager $preexpedienteManager,
        SistemaArchivos $fm,
        EnvioEmails $mailer,
        \Twig_Environment $twig
    ) {
        $this->accion = 'altasanciontrafico';
        $this->version = 'v2';
        $this->permiso = 'ALTA_SANCION';
        $this->setAsserts();

        $this->datosExpedienteManager = $datosExpedienteManager;
        $this->datosUsuarioManager = $datosUsuarioManager;
        $this->vehiculoManager = $vehiculoManager;
        $this->titularManager = $titularManager;
        $this->usuarioMtmManager = $usuarioMtmManager;
        $this->usuarioManager = $usuarioManager;
        $this->preexpedienteManager = $preexpedienteManager;
        $this->fm = $fm;
        $this->mailer = $mailer;
        $this->twig = $twig;

        parent::__construct();
    }

    public function procesaLlamada($datosEntrada): array {

        $usuarioEntrada = $datosEntrada['Usuario'];

        $usuario = $this->usuarioManager->findOneBy(array(
            'username' => $usuarioEntrada
        ));

        $fechaNotificacion = $datosEntrada['Fecha_Notificacion'];

        $datosUsuario = $this->datosUsuarioManager->getDatosUsuario($usuario);

        if (!$datosUsuario->hasDato(ConstantesDatosGB::$GB_IDTITULAR))
            throw new HttpException(401,'Usuario sin titular enlazado.');
        if (!$datosUsuario->hasDato(ConstantesDatosMTM::$MTM_IDUSUARIO))
            throw new HttpException(401,'Usuario sin MTM enlazado.');

        /**
         * @var $vehiculo Vehiculos
         */
        $vehiculo = $this->vehiculoManager->vehiculoFindOneBy(array(
            'matricula' => $datosEntrada['Matricula']
        ));

        if (!$vehiculo)
            throw new HttpException(401,'[Matricula]: => Vehículo no encontrado.');

        /**
         * @var $arrendatarioAsociado Titulares
         */
        $arrendatarioAsociado = $this->titularManager->titularFindOneBy(array(
            'idtitulares' => $datosUsuario->getDato(ConstantesDatosGB::$GB_IDTITULAR)
        ));

        if (!$arrendatarioAsociado)
            throw new HttpException(401,'[Usuario]: => Titular asociado erroneo.');

        $historicoVehiculo = $this->vehiculoManager->historicoVehiculoFindBy(array(
            'idvehiculo' => $vehiculo->getIdvehiculos(),
            'idtitular' => $datosUsuario->getDato(ConstantesDatosGB::$GB_IDTITULAR)
        ));

        if (!$historicoVehiculo)
            throw new HttpException(401,'[Matricula]: => Vehículo no encontrado.');

        $usuarioMtm = $this->usuarioMtmManager->usuarioMtmFindOneBy(array(
            'idusuariosmtm' => $datosUsuario->getDato(ConstantesDatosMTM::$MTM_IDUSUARIO)
        ));

        if (!$usuarioMtm)
            throw new HttpException(401,'[Usuario]: => UsuarioMTM asociado erroneo.');

        /**
         * @var $preexp Preexpedientes
         */
        $preexp = new Preexpedientes();
        $fechaNotificacion = \DateTime::createFromFormat('Y-m-d\TH:i:s',$fechaNotificacion);

        $preexp->setFechanotificacion($fechaNotificacion);
        $preexp->setIdarrendatario($arrendatarioAsociado);
        $preexp->setIdtitulares($arrendatarioAsociado);
        $preexp->setIdvehiculos($vehiculo);
        $fechaAlta = new \DateTime('now');
        $preexp->setFechaalta($fechaAlta);
        $preexp->setTipo('ident');
        $preexp->setObservaciones('API - Usuario:'.$usuario->getUsername().' - Email:'.$usuario->getEmail());
        $preexp->setIdusuariosmtm($usuarioMtm);

        $nombreFichero = 'API-'.rand().'-AltaSancionTraficoV2-'.$usuario->getUsername().'-'.date('Y-m-d\TH:i:s').'.pdf';

        $fsm = new FilesystemModel($this->fm->getAdaptador('amazonS3FS'));
        $this->fm->setFileSystem($fsm);
        $escritura = $this->fm->escribeFichero(
            $fsm,
            $nombreFichero,
            base64_decode($datosEntrada['Contenido_Documento'])
        );

        if (!$escritura)
            throw new \RuntimeException('Error escribiendo fichero.');

        $preexp->setRuta($escritura);

        try {
            $this->preexpedienteManager->persist($preexp, true);
        }
        catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }

        // Se comprueba solamente si existe, porque en caso de existir ya se ha validado.
        if (isset($datos['Email'])) {

            $mensaje = new ContenedorEmail();
            $mensaje->setMetodoenvio(EnvioEmails::MANDRILL);
            $mensaje->setAsunto('Gesthispania - API - Alta de sanción');
//            $mensaje->setBodytxt("Probando alta de marron - Paga la multa, primer aviso.");
            $mensaje->setBodyhtml($this->twig->render('@Globalcar/ApiPublica/AltaSancionTraficoV1.html.twig',
                array(
                    'idpreexp' => $preexp->getIdpreexpedientes(),
                    'fechanotif' => $preexp->getFechanotificacion()->format('d-m-Y'),
                    'fechaalta' => $preexp->getFechaalta()->format('d-m-Y')
                )));
            $mensaje->setFrom('noreply@gesthispania.com');

            // TODO - Temporalmente tecnologia en copia.
            $arrayEmails[] = 'tecnologia@gesthispania.com';

            $arrayEmails = explode(',',$datosEntrada['Email']);

            // TODO - temporalmente que nos ponga en copia.
            $arrayEmails[] = 'tecnologia@gesthispania.com';

            foreach ($arrayEmails as $email)
                $mensaje->addTo($email);
            $mensaje->addAdjuntosDatos(base64_decode($datosEntrada['Contenido_Documento']),rand().'.pdf');

            $this->mailer->enviarEmail($mensaje);

        }

//        $this->respuesta->addValor(array(
//            'idPreexpediente' => $preexp->getIdpreexpedientes(),
//            'FechaNotificacion' => $fechaNotificacion->format('Y-m-d\TH:i:s'),
//            'FechaRecepcion' => $fechaAlta->format('Y-m-d\TH:i:s')
//        ));
//
//        $this->respuesta->setCodigoRespuesta(201);
//        return $this->respuesta;

        $respuesta = array(
            'idPreexpediente' => $preexp->getIdpreexpedientes(),
            'FechaNotificacion' => $fechaNotificacion->format('Y-m-d\TH:i:s'),
            'FechaRecepcion' => $fechaAlta->format('Y-m-d\TH:i:s')
        );

        return $respuesta;

    }

    protected function setAsserts()
    {
        // TODO - Desacoplar las validaciones del framework.
        /**
         * @var $asserts Assert\Collection
         */
        $this->asserts = new Assert\Collection(
            array(
                'fields' => array(
                    'Fecha_Notificacion' => new Assert\Required(array (
                        new Assert\DateTime( array(
                            'message' => 'Formato de fecha incorrecto',
                            'format' => 'Y-m-d\TH:i:s'
                        )),
                        new Assert\NotBlank(array(
                            'message' => 'Campo obligatorio'
                        ))
                    )),
                    'Matricula' => new Assert\Required(array(
                        new Assert\NotBlank(array(
                            'message' => 'Campo obligatorio'
                        ))
                    )),
                    'Contenido_Documento' => new Assert\Required(array(
                        new FileBase64String(array(
                            'maxsize' => 8*1024*1024  // 8MB de tamaño maximo
                        )),
                        new Assert\NotBlank(array(
                            'message' => 'Campo obligatorio'
                        ))
                    )),
                    'Email' => new Assert\Optional(array(
                        new Emails(array(
                            'message' => 'Email invalido.'
                        )),
                        new Assert\NotBlank(array(
                            'message' => 'Si el campo email va incluido no puede ir vacío.'
                        ))
                    )),
                    //'File' => new Assert\Optional(new Assert\File())
                ),
                'allowExtraFields' => true,
                'missingFieldsMessage' => 'Campo obligatorio'
            )
        );
    }

    public function getPermiso()
    {
        return $this->permiso;
    }
}