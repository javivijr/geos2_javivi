<?php

namespace AppBundle\Service\ApiPublica;


use AppBundle\Exception\ValidationException;
use AppBundle\Model\RespuestaAPIPublica;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ApiPublicaManager
{

//    private $serializer;
    private $respuesta;
    private $acciones;

    /**
     * @var $authorizationChecker AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(
        RespuestaAPIPublica $respuesta,
//        ClassMetadataFactoryInterface $cmdf,
        ServicioAcciones $servicioAcciones,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        //$classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
//        $this->serializer = new Serializer(
//            array(new ObjectNormalizer($cmdf)),
//            array(new JsonEncoder())
//        );
        $this->respuesta = $respuesta;
        $this->acciones = $servicioAcciones->getAcciones();
        $this->authorizationChecker = $authorizationChecker;
    }

    public function getAcciones() {
        return $this->acciones;
    }

    public function compruebaVersionAccion($version, $accion ) {
        if (!array_key_exists($version,$this->acciones) ) {
            throw new HttpException(400,'Version incorrecta');
        }
        if (!array_key_exists($accion,$this->acciones[$version]) ) {
            throw new HttpException(400,'Accion incorrecta');
        }
        return true;
    }

//    public function serializaRespuesta(RespuestaAPIPublica $objeto) {
//        return $this->serializer->serialize($objeto, 'json', array('groups' => array('camposRespuesta') ));
//    }

    /**
     * @param $version
     * @param $accion
     * @param $datos
     * @throws \Exception
     * @throws ValidationException
     */
    public function procesaLlamada($version, $accion, $datos) {
        /**
         * @var $servicioAccion AbstractAcciones
         */
        $servicioAccion = $this->acciones[$version][$accion];

        if (!$this->authorizationChecker->isGranted($servicioAccion->getPermiso(),$servicioAccion))
            throw new HttpException(401,"Permiso denegado.");

        $validaciones = $servicioAccion->validaDatosEntrada($datos);
        if (count($validaciones)>0)
            throw new ValidationException($validaciones);

        $servicioAccion->procesaLlamada($datos);

    }

}