<?php

namespace AppBundle\Controller;


use AppBundle\Exception\ValidationException;
use AppBundle\Model\RespuestaAPIPublica;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ExceptionController extends FOSRestController
{
    use ControllerTrait;

    /**
     * @param Request $request
     * @param $exception
     * @param DebugLoggerInterface|null $logger
     * @return View
     * @Rest\View(serializerGroups={"camposRespuesta"})
     */
    public function showAction(Request $request, $exception, DebugLoggerInterface $logger = null, RespuestaAPIPublica $respuesta)
    {
        if ( $exception instanceof ValidationException)
        {
            $respuesta->setErrores($exception->getErrores());
            return $this->view($respuesta,$exception->getStatusCode());
//            return $this->getView($exception->getStatusCode(), json_decode($exception->getMessage(), true));
        }

        if ($exception instanceof HttpException) {
            $respuesta->addError($exception->getMessage());
            return $this->view($respuesta,$exception->getStatusCode());
//            return $this->getView($exception->getStatusCode(), $exception->getMessage());
        }

        return $this->view("Error inesperado",500);
    }

    /**
     * @param int|null $statusCode
     * @param $message
     * @return View
     */
    private function getView(?int $statusCode, $message) : View
    {
        $data = [
            'code' => $statusCode ?? 500,
            'message' => $message
        ];

        return $this->view($data, $statusCode ?? 500);
    }
}