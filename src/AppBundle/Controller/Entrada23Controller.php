<?php

namespace AppBundle\Controller;

use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Entrada23Controller extends Controller
{

    /**
     * @Route("/redirecciones23/solicituddatosconductorapiext", name="SolicitudDatosConductorAPI")
     */
    public function solicitudDatosAPIExtAction(Request $peticion) {

        // Datos obligatorios:
        // matricula, fecha de denuncia e id de expediente

        $datosEntrada = $peticion->request->all();
        $respuestaJson = new JsonResponse();

        if (!isset($datosEntrada['matricula']) ||
            !isset($datosEntrada['fechadenuncia']) ||
            !isset($datosEntrada['idexpediente']))
            return $respuestaJson->setData('ApiExtManager - Datos de entrada incorrectos')->setStatusCode(400);

        $apiExtManager = $this->get('AppBundle\Service\Globalcar\ApisExternas\ApiExtManager');
        try {
            $respuesta = $apiExtManager->solicitudDatosConductor($datosEntrada);
        }
        catch (\Exception $e) {
            return $respuestaJson->setData($e->getMessage())->setStatusCode(400);
        }

        return $respuestaJson->setData($respuesta)->setStatusCode(200);
    }

    /**
     * @Route("/redirecciones23/altasancionapiext", name="AltaSancionApiExt")
     */
    public function altaSancionApiExt(Request $peticion) {

        // Datos obligatorios:
        // idexpediente (globalcar)

        $datosEntrada = $peticion->request->all();
        $respuestaJson = new JsonResponse();

        if (!isset($datosEntrada['idexpediente']))
            return $respuestaJson->setData('ApiExtManager - Datos de entrada incorrectos')->setStatusCode(400);

        $apiExtManager = $this->get('AppBundle\Service\Globalcar\ApisExternas\ApiExtManager');
        try {
            $respuesta = $apiExtManager->altaSancion($datosEntrada);
        }
        catch (\Exception $e) {
            return $respuestaJson->setData($e->getMessage())->setStatusCode(400);
        }

        return $respuestaJson->setData($respuesta)->setStatusCode(200);
    }

    /**
     * @Route("/redirecciones23/altadocusancionapiext", name="AltaDocuSancionApiExt")
     */
    public function altaDocuSancionApiExt(Request $peticion) {

        // Datos obligatorios:
        // idexpediente (globalcar)

        $datosEntrada = $peticion->request->all();
        $respuestaJson = new JsonResponse();

        if (!isset($datosEntrada['idexpediente']))
            return $respuestaJson->setData('ApiExtManager - Datos de entrada incorrectos')->setStatusCode(400);

        $apiExtManager = $this->get('AppBundle\Service\Globalcar\ApisExternas\ApiExtManager');
        try {
            $respuesta = $apiExtManager->altaDocuSancion($datosEntrada);
        }
        catch (\Exception $e) {
            return $respuestaJson->setData($e->getMessage())->setStatusCode(400);
        }

        return $respuestaJson->setData($respuesta)->setStatusCode(200);
    }

    /**
     * @Route("/redirecciones23/checkdisponibilidadaccionesexpediente", name="CheckDisponibilidadAccionesExpediente")
     */
    public function checkDisponibilidadAccionesExpediente(Request $peticion) {

        // Datos obligatorios:
        // idexpediente (globalcar)

        $datosEntrada = $peticion->request->all();
        $respuestaJson = new JsonResponse();

        if (!isset($datosEntrada['idexpediente']))
            return $respuestaJson->setData('ApiExtManager - Datos de entrada incorrectos')->setStatusCode(400);

        $apiExtManager = $this->get('AppBundle\Service\Globalcar\ApisExternas\ApiExtManager');
        try {
            $respuesta = $apiExtManager->obtieneAccionesDisponiblesExpediente($datosEntrada);
        }
        catch (\Exception $e) {
            return $respuestaJson->setData($e->getMessage())->setStatusCode(400);
        }


        return $respuestaJson->setData($respuesta)->setStatusCode(200);

    }

    /**
     * @Route("/redirecciones23/checkaccionesrealizadasexpediente", name="CheckAccionesRealizadasExpediente")
     */
    public function checkAccionesRealizadasExpediente(Request $peticion) {

        // Datos obligatorios:
        // idexpediente (globalcar)

        $datosEntrada = $peticion->request->all();
        $respuestaJson = new JsonResponse();

        if (!isset($datosEntrada['idexpediente']))
            return $respuestaJson->setData('ApiExtManager - Datos de entrada incorrectos')->setStatusCode(400);

        $apiExtManager = $this->get('AppBundle\Service\Globalcar\ApisExternas\ApiExtManager');
        try {
            $respuesta = $apiExtManager->compruebaAccionesRealizadasExpediente($datosEntrada);
        }
        catch (\Exception $e) {
            return $respuestaJson->setData($e->getMessage())->setStatusCode(400);
        }

        return $respuestaJson->setData($respuesta)->setStatusCode(200);

    }

}