<?php

namespace AppBundle\Controller;


use AppBundle\Exception\ValidationException;
use AppBundle\Model\RespuestaAPIPublica;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class ApiPublicaController extends FOSRestController
{

    /**
     * @Rest\View(serializerGroups={"camposRespuesta"})
     * @Route("/api/{version}/{accion}", name="EntryPointRoute")     *
     */
    public function entryPointAction(Request $peticion, $version, $accion) {

        $datosEntrada = $peticion->request->all();
        $datosRespuesta = $this->get('AppBundle\Model\RespuestaAPIPublica');
        $apiManager = $this->get('AppBundle\Service\ApiPublica\ApiPublicaManager');

        //Primero comprobamos la version y la llamada que se esta realizando
        $apiManager->compruebaVersionAccion($version,$accion);
        $apiManager->procesaLlamada($version,$accion, $datosEntrada);

        $view = $this->view($datosRespuesta,$datosRespuesta->getCodigoRespuesta());
        return $view;
    }

    /**
     * @Rest\View(serializerGroups={"camposRespuesta"})
     * @Route("/apiapi/{version}/{accion}", name="EntryPointRoute2")     *
     */
    public function entryPointAction2(Request $peticion, $version, $accion) {

        $datosEntrada = $peticion->request->all();
        $datosRespuesta = $this->get('AppBundle\Model\RespuestaAPIPublica');
        $apiManager = $this->get('AppBundle\Service\ApiPublica\ApiPublicaManager');

        //Primero comprobamos la version y la llamada que se esta realizando
        $apiManager->compruebaVersionAccion($version,$accion);
        $apiManager->procesaLlamada($version,$accion, $datosEntrada);

        $view = $this->view($datosRespuesta,$datosRespuesta->getCodigoRespuesta());
        return $view;
    }

    /*
     * @Route("/apiapi/{version}/{accion}/{operador}", name="EntryPointRoute")
     *
     */
//    public function entryPointAction2(Request $request, $version, $accion, $operador) {
//
//        /*
//         * Datos a la entrada:
//         * Matricula del vehiculo sancionado
//         * Fecha de notificacion - formato "2017-12-20T00:00:00" - (Y-m-d\TH:i:s)
//         * Email para envio de correo de confirmacion (opcional)
//         * Documento de la sancion - en Base64-String
//         */
//
//        $datosEntrada = $request->request->all();
//        $respuestaJSON = new JsonResponse();
//        $datosRespuesta = $this->get('AppBundle\Model\RespuestaAPIPublica');
//        $apiManager = $this->get('AppBundle\Service\ApiPublica\ApiPublicaManager');
//
//        //Primero comprobamos la version y la llamada que se esta realizando
//        try{
//            $apiManager->compruebaVersionAccion($version,$accion);
//        }
//        catch (HttpException $e) {
//            // TODO - Empollar el serializado de objetos
//            // https://symfony.com/doc/3.4/components/serializer.html
//            // https://symfony.com/doc/3.4/serializer.html
//            $datosRespuesta->addError($e->getMessage());
//            $respuestaJSON->setStatusCode($e->getStatusCode());
//            $respuestaJSON->setJson($apiManager->serializaRespuesta($datosRespuesta));
//            return $respuestaJSON;
//        }
//
//        try {
//            $apiManager->procesaLlamada($version,$accion, $datosEntrada);
//        }
//        catch (\Exception $e) {
//            if ($e instanceof ValidationException)
//                $datosRespuesta->addError($e->getErrores());
//            elseif ($e instanceof HttpException) {
//                $datosRespuesta->addError($e->getMessage());
//                $respuestaJSON->setStatusCode($e->getStatusCode());
//            }
//            else
//                $respuestaJSON->setStatusCode(400);
//            $respuestaJSON->setJson($apiManager->serializaRespuesta($datosRespuesta));
//            return $respuestaJSON;
//        }
//
//        $respuestaJSON->setStatusCode($datosRespuesta->getCodigoRespuesta());
//        $respuestaJSON->setJson($apiManager->serializaRespuesta($datosRespuesta));
//
//        return $respuestaJSON;
//
//    }

    private function validarFecha($stringFecha) {
        $d = \DateTime::createFromFormat('Y-m-d\TH:i:s',$stringFecha);
        return $d && $d->format('Y-m-d\TH:i:s') == $stringFecha;
    }

}