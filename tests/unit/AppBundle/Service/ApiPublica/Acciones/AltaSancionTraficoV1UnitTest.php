<?php

namespace Tests\AppBundle\Service\ApiPublica;

use AppBundle\DataFixtures\Testing\Geos2DatosUsuarioFixture;
use AppBundle\Entity\Geos2\DatosUsuario;
use AppBundle\Entity\Geos2\Usuario;
use AppBundle\Entity\Globalcar\Historicovehiculos;
use AppBundle\Entity\Globalcar\Titulares;
use AppBundle\Entity\Globalcar\Titularesproveedores;
use AppBundle\Entity\Globalcar\Usuariosmtm;
use AppBundle\Entity\Globalcar\Vehiculos;
use AppBundle\Model\RespuestaAPIPublica;
use AppBundle\Service\ApiPublica\Acciones\AltaSancionTraficoV2;
use AppBundle\Service\EnvioEmails;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use AppBundle\Model\Globalcar\ConstantesDatosGB;
use AppBundle\Service\ApiPublica\Acciones\AltaSancionTraficoV1;
use GlobalcarBundle\DataFixtures\Testing\LoadGb4TestAltaSancionV1;
use AppBundle\Service\Wrap\SistemaArchivos;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ObjectManager;
//use Liip\FunctionalTestBundle\Test\WebTestCase;
use AppBundle\Model\MTM\ConstantesDatosMTM;
use PHPUnit\Framework\TestCase;

class AltaSancionTraficoV1UnitTest extends TestCase
{

    /**
     * @var $respuesta RespuestaAPIPublica
     */
    private $respuesta;
    /**
     * @var $clase AltaSancionTraficoV1
     */
    private static $clase;

    public static function setUpBeforeClass()
    {
//        self::bootKernel();
//        self::$doctrine = self::$kernel->getContainer()->get('doctrine');
//        self::$fm = self::$kernel->getContainer()->get('AppBundle\Service\Wrap\SistemaArchivos');
//        self::$swiftmailer = self::$kernel->getContainer()->get('swiftmailer.mailer');
//        self::$clase = self::$kernel->getContainer()->get('AltaSancion');
//        parent::setUpBeforeClass();
    }

    public function setUp()
    {

    }

    /**
     * @dataProvider datosValidacionProvider
     */
    public function testValidacionAltaSancionV1($fechaNotificacion = null, $matricula = null, $contenidoDocumento = null) {
        $mockDoctrine = $this->createMock(Registry::class);
        $mockFm = $this->createMock(SistemaArchivos::class);
        $mockSM = $this->createMock(EnvioEmails::class);
        $mockTwig = $this->createMock(\Twig_Environment::class);
        $this->respuesta = new RespuestaAPIPublica('test');
        self::$clase = new AltaSancionTraficoV1($mockDoctrine,$this->respuesta, $mockFm, $mockSM, $mockTwig);

        $datos['Fecha_Notificacion'] = $fechaNotificacion;
        $datos['Matricula'] = $matricula;
        $datos['Contenido_Documento'] = $contenidoDocumento;

        $resultadoValidacion = self::$clase->validaDatosEntrada($datos);
        if ($this->dataName() == 'Datos correctos')
            $this->assertEquals(0, count($resultadoValidacion));
        else
            $this->assertEquals(1, count($resultadoValidacion));

    }

    public function testProcesaLlamadaAltaSancionV1() {

        //$this->markTestIncomplete('En construccion');
        $datos['Fecha_Notificacion'] = '2018-02-12T12:30:00';
        $datos['Matricula'] = '2233PRU';
        $datos['Contenido_Documento'] = 'gftbyqw0yfewfdasfńflojiaesdrft4w5325f24fwdaffdpoaf';
        $datos['Usuario'] = 'javivi';
        $datos['Pass'] = 'topotamadre';
        $datos['Email'] = 'javier.jimenez@gesthispania.es';

        $mockDoctrine = $this->createMock(Registry::class);

        $mockEmGeos2 = $this->createMock(EntityManager::class);
        $mockEmGB4 = $this->createMock(EntityManager::class);

        $mockUsuarioRepository = $this->createMock(ObjectRepository::class);
        $mockDatosUsuarioRepository = $this->createMock(ObjectRepository::class);
        $mockVehiculoRepository = $this->createMock(ObjectRepository::class);
        $mockHistoricoVehiculoRepository = $this->createMock(ObjectRepository::class);
        $mockTitularesRepository = $this->createMock(ObjectRepository::class);
        $mockTitularesProveedoresRepository = $this->createMock(ObjectRepository::class);
        $mockUsuariosmtmRepository = $this->createMock(ObjectRepository::class);

        $mockUsuarioEntity = $this->createMock(Usuario::class);
        $mockDatosUsuarioEntity = $this->createMock(DatosUsuario::class);

        $mockVehiculoEntity = $this->createMock(Vehiculos::class);
        $mockHistoricoVehiculoEntity = $this->createMock(Historicovehiculos::class);
        $mockArrendatarioEntity = $this->createMock(Titulares::class);
        $mockTitularesProveedoresEntity = $this->createMock(Titularesproveedores::class);
        $mockTitularEntity = $this->createMock(Titulares::class);
        $mockUsuariosMtmEntity = $this->createMock(Usuariosmtm::class);

        // Managers en Registry
        $map = [
            ['gb4',$mockEmGB4],
            ['geos2', $mockEmGeos2]
        ];
        $mockDoctrine->method('getManager')
            ->willReturnMap($map);

        // Repositorios en Managers

        $map = [
            ['AppBundle:Usuario',$mockUsuarioRepository],
            ['AppBundle:DatosUsuario',$mockDatosUsuarioRepository]
        ];
        $mockEmGeos2->method('getRepository')
            ->willReturnMap($map);

        $map = [
            ['AppBundle:Vehiculos', $mockVehiculoRepository],
            ['AppBundle:Historicovehiculos', $mockHistoricoVehiculoRepository],
            ['AppBundle:Titulares', $mockTitularesRepository],
            ['AppBundle:Titularesproveedores', $mockTitularesProveedoresRepository],
            ['AppBundle:Usuariosmtm', $mockUsuariosmtmRepository]
        ];
        $mockEmGB4->method('getRepository')
            ->willReturnMap($map);

        // Mocks de entidades Geos2

        $mockUsuarioEntity
            ->method('getId')
            ->willReturn(1);
        $mockUsuarioEntity
            ->method('getUsername')
            ->willReturn('javivi')
        ;

        $map = [
            [ConstantesDatosGB::$GB_IDTITULAR,11],
            [ConstantesDatosMTM::$MTM_IDUSUARIO,111]
        ];
        $mockDatosUsuarioEntity->method('hasDato')
            ->willReturnMap($map);
        $mockDatosUsuarioEntity->method('getDatos')
            ->willReturn(array(
                ConstantesDatosGB::$GB_IDTITULAR => 11,
                ConstantesDatosMTM::$MTM_IDUSUARIO => 111
            ));

        // Entidades a repositorios en Geos2
        $mockUsuarioRepository->method('findOneBy')
            ->willReturn($mockUsuarioEntity);
        $mockDatosUsuarioRepository->method('findOneBy')
            ->willReturn($mockDatosUsuarioEntity);


        // Mocks entidades GB4
        $mockVehiculoEntity
            ->method('getIdvehiculos')
            ->willReturn(666);
        $mockVehiculoRepository
            ->method('findOneBy')
            ->willReturn($mockVehiculoEntity);
        $mockHistoricoVehiculoRepository
            ->method('findOneBy')
            ->willReturn($mockHistoricoVehiculoEntity);


        $mockArrendatarioEntity
            ->method('getIdtitulares')
            ->willReturn(1);
        $mockTitularesRepository
            ->method('findOneBy')
            ->willReturn($mockArrendatarioEntity);

        $mockTitularesProveedoresEntity
            ->method('getIdtitulares')
            ->willReturn($mockTitularEntity);
        $mockTitularesProveedoresRepository
            ->method('findOneBy')
            ->willReturn($mockTitularesProveedoresEntity);

        $mockUsuariosmtmRepository
            ->method('findOneBy')
            ->willReturn($mockUsuariosMtmEntity);

        $mockFm = $this->createMock(SistemaArchivos::class);

        $mockFm->method('getAdaptador')
            ->willReturn(array(
                'id' => 'idid',
                'type' => 'tipotipo'
            ));
        $mockFm->method('escribeFichero')
            ->willReturn(true);


        $mockSM = $this->createMock(EnvioEmails::class);
        $mockTwig = $this->createMock(\Twig_Environment::class);
        $respuesta = new RespuestaAPIPublica('test');

        $objeto = new AltaSancionTraficoV1($mockDoctrine,$respuesta, $mockFm, $mockSM, $mockTwig);

        $respuesta = $objeto->procesaLlamada($datos);

        $this->assertTrue($respuesta instanceof RespuestaAPIPublica);
        // TODO - Comprobar datos del objeto.
        $this->assertArrayHasKey('idPreexpediente',$respuesta->getValor()[0]);
        $this->assertArrayHasKey('FechaNotificacion',$respuesta->getValor()[0]);
        $this->assertEquals($datos['Fecha_Notificacion'],$respuesta->getValor()[0]['FechaNotificacion']);
        $this->assertEquals(201,$respuesta->getCodigoRespuesta());

    }

//    private function cargaDatosDePrueba(ObjectManager $emGeos2, ObjectManager $emGb4) {
//
//        $purgerGeos2 = new ORMPurger($emGeos2);
//        $purgerGeos2->purge();
//        $purgerGb4 = new ORMPurger($emGb4);
//        $purgerGb4->purge();
//
//        $this->loadFixtures([
//            LoadGeos2UserData::class
//        ],'geos2');
//
//        // No consigo que pille las referencias, la entidad titular guardada en el ReferenceRepository no es accesible desde LoadGeos2UserData
//        // Asi que por ahora, dentro de los fixtures se instancia el contenedor y se obtienen los managers que se necesiten
//        // TODO - Seguir investigando como guardar las referencias entre las dos llamadas
//
//        $this->setExcludedDoctrineTables(array('titulares','titularesproveedores', 'usuariosmtm'));
//        $this->loadFixtures([
//            LoadGb4TestAltaSancionV1::class
//        ], 'gb4');
//
//    }

    public function datosValidacionProvider() {
        $r = array();
        $r['Datos correctos'] = array(
            'Fecha_Notificacion' => '2018-02-12T12:30:00',
            'Matricula' => '2233PRU',
            'Contenido_Documento' => 'dlshkbgfshkfgshafgdhasgfdhkasgfh'
        );
        $r['Falta matricula'] = array(
            'Fecha_Notificacion' => '2018-02-12T12:30:00',
            'Matricula' => '',
            'Contenido_Documento' => 'dlshkbgfshkfgshafgdhasgfdhkasgfh'
        );
        $r['Falta fecha'] = array(
            'Fecha_Notificacion' => '',
            'Matricula' => '2233PRU',
            'Contenido_Documento' => 'dlshkbgfshkfgshafgdhasgfdhkasgfh'
        );
        $r['Falta contenido_documento'] = array(
            'Fecha_Notificacion' => '2018-02-12T12:30:00',
            'Matricula' => '2233PRU'
        );

        return $r;
    }

}