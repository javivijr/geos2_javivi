<?php

namespace Tests\AppBundle\Controller;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ApiPublicaControllerTest extends TestCase
{
    public static function setUpBeforeClass()
    {
    }

    public function setUp()
    {
    }

    public function testAltaSancion()
    {
        $this->markTestIncomplete();

        $client = new Client([
            'base_uri' => 'http://172.17.0.1:91',
            'exceptions' => false
        ]);

        $data = array(
            'Usuario' => 'javivi',
            'Operador_clave' => 'topotamadre'
        );

        $response = $client->post('/api/v1/altasanciontrafico/tester', [
            'json' => $data
        ]);

        $dataResponse = json_decode($response->getBody(true), true);

        /*
         * A la salida debe venir
         * idPreexpediente creado
         * fecha de notificacion del preexpediente - formato "2017-12-20T00:00:00"
         * fecha de alta del preexpediente - formato "2017-12-20T00:00:00"
         */

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('idPreexpediente', $dataResponse);
        $this->assertArrayHasKey('fechaNotificacion', $dataResponse);
        $this->assertArrayHasKey('fechaAlta', $dataResponse);
    }
}
