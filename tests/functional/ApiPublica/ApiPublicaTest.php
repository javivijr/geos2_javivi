<?php

namespace Tests\functional\ApiPublica;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiPublicaTest extends WebTestCase
{

    public function setUp() {

    }

    public function testAltaSancionV1() {

        $client = static::createClient();

        $post = array(
            'Usuario' => 'usuario23',
            'Clave' => 'usuario23',

        );

        $crawler = $client->request('POST', '/apiapi/v2/topota');

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::class, get_class($response));
        //$this->assertContains('Hello World', $client->getResponse()->getContent());

    }

//    public function testAltaSancion()
//    {
//        $this->markTestIncomplete();
//
//        $client = new Client([
//            'base_uri' => 'http://172.17.0.1:91',
//            'exceptions' => false
//        ]);
//
//        $data = array(
//            'Usuario' => 'javivi',
//            'Operador_clave' => 'topotamadre'
//        );
//
//        $response = $client->post('/api/v1/altasanciontrafico/tester', [
//            'json' => $data
//        ]);
//
//        $dataResponse = json_decode($response->getBody(true), true);
//
//        /*
//         * A la salida debe venir
//         * idPreexpediente creado
//         * fecha de notificacion del preexpediente - formato "2017-12-20T00:00:00"
//         * fecha de alta del preexpediente - formato "2017-12-20T00:00:00"
//         */
//
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->assertArrayHasKey('idPreexpediente', $dataResponse);
//        $this->assertArrayHasKey('fechaNotificacion', $dataResponse);
//        $this->assertArrayHasKey('fechaAlta', $dataResponse);
//    }

    public function testAutenticacionFallida() {
        $client = static::createClient();

        $crawler = $client->request('POST', '/api/vx/nuse');

        $response = $client->getResponse();
        $this->assertEquals(JsonResponse::class, get_class($response));
        $arrayRespuesta = json_decode($response->getContent(),true);
        $this->assertEquals($arrayRespuesta['message'], 'Error de autenticación');
        $this->assertEquals($response->getStatusCode(), 401);
    }

}