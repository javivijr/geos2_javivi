<?php

namespace Tests\TestFixture\integration\Manager;



use AppBundle\Entity\Geos2\DatosUsuario;
use AppBundle\Entity\Geos2\Usuario;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DatosUsuarioManagerTestFixture implements FixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @var $usuario Usuario
         */
        $usuario = new Usuario();
        $usuario->setUsername('usuarioTestConDatos');
        $usuario->setEmail('correo@test.es');
        $usuario->setPassword('sucontraseña');
        $manager->persist($usuario);
        $manager->flush();

        $datosUsuario = new DatosUsuario();
        $datosUsuario->setUsuario($usuario);
        $datosUsuario->addDato('KeyDePrueba','ValorDePrueba');
        $manager->persist($datosUsuario);
        $manager->flush();

        $usuario = new Usuario();
        $usuario->setUsername('usuarioTestSinDatos');
        $usuario->setEmail('correo2@test.es');
        $usuario->setPassword('sucontraseña');
        $manager->persist($usuario);
        $manager->flush();

    }
}