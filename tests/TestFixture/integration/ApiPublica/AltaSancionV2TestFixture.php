<?php

namespace Tests\TestFixture\integration\ApiPublica;


use AppBundle\Entity\Geos2\DatosUsuario;
use AppBundle\Entity\Geos2\Usuario;
use AppBundle\Entity\Globalcar\Historicovehiculos;
use AppBundle\Entity\Globalcar\Titulares;
use AppBundle\Entity\Globalcar\Usuariosmtm;
use AppBundle\Entity\Globalcar\Vehiculos;
use AppBundle\Model\Globalcar\ConstantesDatosGB;
use AppBundle\Model\MTM\ConstantesDatosMTM;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AltaSancionV2TestFixture implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var $container ContainerInterface
     */
    private $container;

    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;
    /**
     * @var $emGb4 EntityManager
     */
    private $emGb4;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $this->emGb4 = $manager;
        $this->emGeos2 = $this->container->get('doctrine')->getManager('geos2');

        // Usuario GEOS2 - Test OK
        /**
         * @var $usuario Usuario
         */
        $usuario = new Usuario();
        $usuario->setUsername('usuarioTest');
        $usuario->setEmail('correo@test.es');
        $usuario->setPassword('sucontraseña');

        $this->emGeos2->persist($usuario);

        // Titular Arval
        $titularProveedor = new Titulares();
        $titularProveedor->setNombre('TitularArval');
        $titularProveedor->setTipoparticular('n');
        $titularProveedor->setProveedor('s');
        $this->emGb4->persist($titularProveedor);

        // Titular Arrendatario (Renting)
        $titularArrendatario = new Titulares();
        $titularArrendatario->setNombre('TitularArrendatario');
        $titularArrendatario->setTipoparticular('n');
        $titularArrendatario->setProveedor('n');
        $this->emGb4->persist($titularArrendatario);

        // UsuarioMTM
        $usuarioMtm = new Usuariosmtm();
        $usuarioMtm->setUsuario('usuarioMtmTest');
        $usuarioMtm->setBloqueo('n');
        $usuarioMtm->setNoenvio('n');
        $usuarioMtm->setNomailident('n');
        $usuarioMtm->setBloqueomtm('n');
        $usuarioMtm->setActivo('s');
        $this->emGb4->persist($usuarioMtm);

        $this->emGb4->flush();

        $datosUsuario = new DatosUsuario();
        $datosUsuario->setUsuario($usuario);
        $datos = array(
            ConstantesDatosGB::$GB_IDTITULAR => $titularArrendatario->getIdtitulares(),
            ConstantesDatosMTM::$MTM_IDUSUARIO => $usuarioMtm->getIdusuariosmtm()
        );

        $datosUsuario->setDatos($datos);
        $this->emGeos2->persist($datosUsuario);

        // Vehiculo
        $vehiculo = new Vehiculos();
        $vehiculo->setMatricula('2233PRU');
        $this->emGb4->persist($vehiculo);
        $this->emGb4->flush();
        $historicoVehiculo = new Historicovehiculos();
        $historicoVehiculo->setIdvehiculo($vehiculo);
        $historicoVehiculo->setIdtitular($titularArrendatario);
        $historicoVehiculo->setOrigen('g');
        $historicoVehiculo->setTipotitular('r'); // Tipo titular renting
        $historicoVehiculo->setTemporal('n');
        $historicoVehiculo->setActivo('s');
        $this->emGb4->persist($historicoVehiculo);
        $this->emGb4->flush();

        $this->usuarioSinGb4MtmEnlazado();
        $this->usuarioSinMtmEnlazado($titularArrendatario);

        $this->vehiculoSinHistorico();
        $this->titularGBAsociadoUsuarioInexistente();
        $this->usuarioMtmAsociadoUsuarioInexistente($titularArrendatario);

    }

    private function usuarioSinGb4MtmEnlazado() {
        // Usuario sin Titular ni UsuarioMTM enlazado
        $usuario = new Usuario();
        $usuario->setUsername('usuarioTestSinTitular');
        $usuario->setEmail('correo2@test.es');
        $usuario->setPassword('sucontraseña');
        $this->emGeos2->persist($usuario);
        $this->emGeos2->flush();
    }

    private function usuarioSinMtmEnlazado(Titulares $titulararrendatario) {
        // Usuario sin usuario mtm enlazado
        $usuario = new Usuario();
        $usuario->setUsername('usuarioTestSinUsuarioMtm');
        $usuario->setEmail('correo3@test.es');
        $usuario->setPassword('sucontraseña');
        $this->emGeos2->persist($usuario);
        $this->emGeos2->flush();
        $datosUsuario = new DatosUsuario();
        $datosUsuario->setUsuario($usuario);
        $datos = array(
            ConstantesDatosGB::$GB_IDTITULAR => $titulararrendatario->getIdtitulares()
        );
        $datosUsuario->setDatos($datos);
        $this->emGeos2->persist($datosUsuario);
        $this->emGeos2->flush();
    }

    private function vehiculoSinHistorico() {
        // Vehiculo
        $vehiculo = new Vehiculos();
        $vehiculo->setMatricula('2222STI');
        $this->emGb4->persist($vehiculo);
        $this->emGb4->flush();
    }

    private function titularGBAsociadoUsuarioInexistente() {
        // Usuario sin usuario mtm enlazado
        $usuario = new Usuario();
        $usuario->setUsername('usuarioTestUsuarioGB4Erroneo');
        $usuario->setEmail('usuarioTestUsuarioGB4Erroneo@test.es');
        $usuario->setPassword('sucontraseña');
        $this->emGeos2->persist($usuario);
        $this->emGeos2->flush();
        $datosUsuario = new DatosUsuario();
        $datosUsuario->setUsuario($usuario);
        $datos = array(
            ConstantesDatosGB::$GB_IDTITULAR => 454545,
            ConstantesDatosMTM::$MTM_IDUSUARIO => 121212
        );
        $datosUsuario->setDatos($datos);
        $this->emGeos2->persist($datosUsuario);
        $this->emGeos2->flush();
    }

    private function usuarioMtmAsociadoUsuarioInexistente(Titulares $titularArrendatario) {
        // Usuario sin usuario mtm enlazado
        $usuario = new Usuario();
        $usuario->setUsername('usuarioTestUsuarioMtmErroneo');
        $usuario->setEmail('usuarioTestUsuarioMtmErroneo@test.es');
        $usuario->setPassword('sucontraseña');
        $this->emGeos2->persist($usuario);
        $this->emGeos2->flush();

        $datosUsuario = new DatosUsuario();
        $datosUsuario->setUsuario($usuario);
        $datos = array(
            ConstantesDatosGB::$GB_IDTITULAR => $titularArrendatario->getIdtitulares(),
            ConstantesDatosMTM::$MTM_IDUSUARIO => 121212
        );

        $datosUsuario->setDatos($datos);
        $this->emGeos2->persist($datosUsuario);
        $this->emGeos2->flush();
    }
}