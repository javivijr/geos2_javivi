<?php


namespace Tests\TestFixture\integration\Globalcar\ApisExternas;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiFurgolineTestFixture implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var $container ContainerInterface
     */
    private $container;
    /**
     * @var $emGeos2 EntityManager
     */
    private $emGeos2;
    /**
     * @var $emGb4 EntityManager
     */
    private $emGb4;


    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->emGb4 = $manager;
        $this->emGeos2 = $this->container->get('doctrine')->getManager('geos2');


    }
}