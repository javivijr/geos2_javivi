<?php

namespace Tests\integration\Globalcar\ApisExternas;


use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\TestFixture\integration\Globalcar\ApisExternas\ApiFurgolineTestFixture;

class ApiFurgolineTest extends KernelTestCase
{

    /**
     * @var $sut AltaSancionTraficoV2
     */
    static private $sut;

    /**
     * @var $emGeos2 EntityManager
     */
    static private $emGeos2;

    /**
     * @var $emGeos2 EntityManager
     */
    static private $emGb4;

    /**
     * @var $loader Loader
     */
    static private $loaderGeos2;

    /**
     * @var $loader Loader
     */
    static private $loaderGb4;

    /**
     * @var $purger ORMPurger
     */
    static private $purger;

    /**
     * @var $purger2 ORMPurger
     */
    static private $purger2;

    /**
     * @var $executor ORMExecutor
     */
    static private $executorGeos2;

    /**
     * @var $executor ORMExecutor
     */
    static private $executorGb4;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$sut = self::$kernel->getContainer()->get('apipublica.altasanciontraficor2');
        self::$emGeos2 = self::$kernel->getContainer()->get('doctrine')->getManager('geos2');
        self::$emGb4 = self::$kernel->getContainer()->get('doctrine')->getManager('gb4');
        self::$purger = new ORMPurger();
        self::$purger2 = new ORMPurger();
        self::$executorGeos2 = new ORMExecutor(self::$emGeos2,self::$purger);
        self::$executorGb4 = new ORMExecutor(self::$emGb4,self::$purger2);
        self::$executorGeos2->purge();
        self::$executorGb4->purge();
        self::$loaderGeos2 = new Loader();
        self::$loaderGb4 = new Loader();

        $fixture = new ApiFurgolineTestFixture();
        $fixture->setContainer(self::$kernel->getContainer());

        self::$loaderGb4->addFixture($fixture);
        self::$executorGb4->execute(self::$loaderGb4->getFixtures());

        parent::setUpBeforeClass();
    }

}