<?php

namespace Tests\integration\Manager;


use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

//use Liip\FunctionalTestBundle\Test\WebTestCase;

class DatosExpedienteManagerTest extends WebTestCase
{

    private function cargaDatosDePrueba(ObjectManager $emGeos2, ObjectManager $emGb4) {

        $purgerGeos2 = new ORMPurger($emGeos2);
        $purgerGeos2->purge();
        $purgerGb4 = new ORMPurger($emGb4);
        $purgerGb4->purge();

//        $this->loadFixtures([
//            LoadGeos2UserData::class
//        ],'geos2');

        // No consigo que pille las referencias, la entidad titular guardada en el ReferenceRepository no es accesible desde LoadGeos2UserData
        // Asi que por ahora, dentro de los fixtures se instancia el contenedor y se obtienen los managers que se necesiten
        // TODO - Seguir investigando como guardar las referencias entre las dos llamadas

//        $this->setExcludedDoctrineTables(array('titulares','titularesproveedores', 'usuariosmtm'));
//        $this->loadFixtures([
//            LoadGb4TestAltaSancionV1::class
//        ], 'gb4');

    }

}