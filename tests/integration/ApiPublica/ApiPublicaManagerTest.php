<?php

namespace Tests\AppBundle\Service\ApiPublica;


use AppBundle\Model\RespuestaAPIPublica;
use AppBundle\Service\ApiPublica\ApiPublicaManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiPublicaManagerTest extends KernelTestCase
{

    /**
     * @var $manager ApiPublicaManager
     */
    private static $manager;
    
//    public $manager;

    /**
     * @var $respuestta RespuestaAPIPublica
     */
    public $respuesta;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$manager = self::$kernel->getContainer()->get('AppBundle\Service\ApiPublica\ApiPublicaManager');
        parent::setUpBeforeClass();
    }

    public function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub
    }

    public function testCompruebaLlamadas(/*$version, $llamadas*/) {

        $a = self::$manager->getAcciones();

        foreach ($a as $version => $acciones) {
            foreach ($acciones as $nombre => $clase) {
                $this->assertTrue(self::$manager->compruebaVersionAccion($version,$nombre));
                $this->assertTrue(is_object($clase));
            }
        }

        $this->expectException(HttpException::class);
        self::$manager->compruebaVersionAccion('versionfalsa','nombrefalso');

    }

}